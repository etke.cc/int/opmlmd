package main

import (
	"flag"
	"log"
	"net/url"
	"os"
	"sort"
	"strings"
	"time"

	md "github.com/JohannesKaufmann/html-to-markdown"
	"github.com/gilliek/go-opml/opml"
	"github.com/mmcdole/gofeed"
)

var (
	opmlAddress string
	output      string
)

type Item struct {
	URL         string
	Feed        string
	Title       string
	Content     string
	PublishedAt time.Time
}

func main() {
	flag.StringVar(&opmlAddress, "opml", "", "OPML url or file")
	flag.StringVar(&output, "o", "opml.md", "Output file")
	flag.Parse()

	opmlfeeds, err := ParseOPML(opmlAddress)
	if err != nil {
		panic(err)
	}
	feeds, count, err := ParseFeeds(opmlfeeds)
	if err != nil {
		panic(err)
	}
	items := ParseItems(feeds, count)
	err = Export(items)
	if err != nil {
		panic(err)
	}
}

// ParseOPML into map of feed name => rss url
func ParseOPML(address string) (map[string]string, error) {
	var err error
	var parsed *opml.OPML
	log.Println("parsing OPML")
	_, urlErr := url.ParseRequestURI(address)
	if urlErr == nil {
		parsed, err = opml.NewOPMLFromURL(address)
	} else {
		parsed, err = opml.NewOPMLFromFile(address)
	}

	if err != nil {
		return nil, err
	}

	outlines := parsed.Outlines()
	feeds := make(map[string]string, len(outlines))
	for _, outline := range outlines { //nolint:gocritic // TODO
		feeds[outline.Title] = outline.XMLURL
	}

	return feeds, nil
}

// ParseFeeds from opml into feed name => feed object with items. Returns map, total count of items and error (if any)
func ParseFeeds(opmlfeeds map[string]string) (feeds map[string]*gofeed.Feed, count int, err error) {
	fp := gofeed.NewParser()
	feeds = make(map[string]*gofeed.Feed, len(opmlfeeds))

	for name, address := range opmlfeeds {
		log.Println("parsing feed ", name)
		feed, err := fp.ParseURL(address)
		if err != nil {
			return nil, 0, err
		}
		count += len(feed.Items)
		feeds[name] = feed
	}

	return feeds, count, nil
}

func ParseItems(feeds map[string]*gofeed.Feed, count int) []Item {
	log.Println("parsing items")
	items := make([]Item, 0, count)
	for name, feed := range feeds {
		for _, item := range feed.Items {
			var publishedAt time.Time
			if item.UpdatedParsed != nil {
				publishedAt = *item.UpdatedParsed
			}
			if item.PublishedParsed != nil {
				publishedAt = *item.PublishedParsed
			}

			items = append(items, Item{
				URL:         item.Link,
				Feed:        name,
				Title:       item.Title,
				Content:     item.Content,
				PublishedAt: publishedAt,
			})
		}
	}
	sort.SliceStable(items, func(i, j int) bool {
		return items[i].PublishedAt.After(items[j].PublishedAt)
	})
	return items
}

func Export(items []Item) error {
	log.Println("exporting to", output)
	file, cerr := os.Create(output)
	if cerr != nil {
		return cerr
	}
	defer file.Close()
	var text strings.Builder
	render := md.NewConverter("", true, nil)
	for _, item := range items {
		text.WriteString("# ")
		text.WriteString(item.Feed)
		text.WriteString(" ")
		text.WriteString(item.Title)
		text.WriteString("\n\n")

		text.WriteString("> published at ")
		text.WriteString(item.PublishedAt.Format(time.DateOnly))
		text.WriteString(" [source](")
		text.WriteString(item.URL)
		text.WriteString(")")
		text.WriteString("\n\n")

		if item.Content == "" {
			text.WriteString("> no description provided")
			text.WriteString("\n\n---\n\n")
		}

		content, err := render.ConvertString(item.Content)
		if err != nil {
			text.WriteString("> ERROR: ")
			text.WriteString(err.Error())
			text.WriteString("\n\n")
		}
		text.WriteString(content)
		text.WriteString("\n\n---\n\n")
	}
	_, werr := file.WriteString(text.String())

	log.Println("done ", output)
	return werr
}
