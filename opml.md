# mautrix-telegram v0.13.0

> published at 2023-02-26 [source](https://github.com/mautrix/telegram/releases/tag/v0.13.0)

### Added

- Added `allow_contact_info` config option to specify whether personal names and avatars for other users should be bridged.

  - The option is only safe to enable on single-user instances, using it anywhere else will cause ghost user profiles to flip back and forth between personal and default ones.
- Added config option to notify Matrix room if bridging an incoming message fails.

### Improved

- Updated Docker image to Alpine 3.17.
- Updated to Telegram API layer 152.
- Improved handling users getting logged out.
- Removed support for creating accounts, as Telegram only allows requesting SMS login codes on the official mobile clients now.
- Replaced moviepy with calling ffmpeg directly for generating video thumbnails.

### Fixed

- Fixed handling Telegram chat upgrades when backfilling is enabled.
- Fixed file transfers failing if transfering the thumbnail fails.
- Fixed bridging unnamed files with unrecognized mime types.
- Fixed enqueueing more backfill.
- Fixed timestamps not being saved in `telegram_file` table.
- Fixed issues with old events being replayed if the bridge was shut down uncleanly.

---

# kuma 1.20.2

> published at 2023-02-26 [source](https://github.com/louislam/uptime-kuma/releases/tag/1.20.2)

⚠️ Please make sure your server have enough disk space before upgrading from older versions.

Also, always perform a backup of your services before upgrading.

### 🐛 Bug Fixes

- [#2820](https://github.com/louislam/uptime-kuma/issues/2820) Fix memory leak issue from MongoDB driver (Thanks [@Lanhild](https://github.com/Lanhild) [@chakflying](https://github.com/chakflying))
- [#2083](https://github.com/louislam/uptime-kuma/pull/2083) Recompile `healthcheck.go`. Forgot to compile it in 1.20.0, hence the issue is still presented in 1.20.0.

---

# ntfy v2.1.0

> published at 2023-02-26 [source](https://github.com/binwiederhier/ntfy/releases/tag/v2.1.0)

## Changelog

- [`f1bcc26`](https://github.com/binwiederhier/ntfy/commit/f1bcc26cfe61384fba85e72d912cb396be0a7d95) Bump deps

---

# grafana v0.0.0-test

> published at 2023-02-24 [source](https://github.com/grafana/grafana/releases/tag/v0.0.0-test)

Merge commit '9d0377d385baa9c4682fcbe66fd16636e9154859' into main

---

# kuma 1.20.0

> published at 2023-02-23 [source](https://github.com/louislam/uptime-kuma/releases/tag/1.20.0)

### 🆕 New Features

- [#1690](https://github.com/louislam/uptime-kuma/pull/1690) Tags Manager (Thanks [@chakflying](https://github.com/chakflying))
- [#2566](https://github.com/louislam/uptime-kuma/pull/2566) GameDig monitor, now you can monitor a lot of game servers via this monitor, [check this game list here](https://github.com/gamedig/node-gamedig#games-list). (Thanks [@WhyKickAmooCow](https://github.com/WhyKickAmooCow))
- [#2541](https://github.com/louislam/uptime-kuma/pull/2541) Redis monitor (Thanks [@long2ice](https://github.com/long2ice))
- [#2328](https://github.com/louislam/uptime-kuma/pull/2328) MongoDB monitor (Thanks [@rmarops](https://github.com/rmarops))
- [#2211](https://github.com/louislam/uptime-kuma/pull/2211) More badges (See [here](https://github.com/louislam/uptime-kuma/pull/2211)) (Thanks [@JRedOW](https://github.com/JRedOW))
- [#1892](https://github.com/louislam/uptime-kuma/pull/1892) \[Ping Monitor\] Configurable packet size (Thanks [@Computroniks](https://github.com/Computroniks))
- [#2570](https://github.com/louislam/uptime-kuma/pull/2570) \[Status Page\] Markdown support (Thanks [@Computroniks](https://github.com/Computroniks))
- [#2567](https://github.com/louislam/uptime-kuma/pull/2567) \[Status Page\] Ability to add Google Analytics code
- \[Non-docker\] Configure environment variables via the `.env` file

### 🐛 Bug Fixes

- [#2632](https://github.com/louislam/uptime-kuma/pull/2632) Docker monitor do not respect with the default timeout and the dns cache setting (Thanks [@chakflying](https://github.com/chakflying))
- [#2525](https://github.com/louislam/uptime-kuma/pull/2525) Pending & maintenance status do not display correctly in badges (Thanks [@chakflying](https://github.com/chakflying))
- [#2083](https://github.com/louislam/uptime-kuma/pull/2083)Healthcheck issue on K8s with specific container name `uptime-kuma` (Thanks [@DevKyleS](https://github.com/DevKyleS)) (Edit: Forget to compile)
- [#2729](https://github.com/louislam/uptime-kuma/pull/2729) \[MySQL monitor\]: Fix error when connection failed (Thanks [@chakflying](https://github.com/chakflying))

### 🚨 Security Fixes

- [`c12b063`](https://github.com/louislam/uptime-kuma/commit/c12b06348b500834d3f6ae9b242d180f062d59d3) xxs issues of status pages (Thanks [@manuel-sommer](https://github.com/manuel-sommer)) Details:

  - [GHSA-wh8j-xr66-f296](https://github.com/louislam/uptime-kuma/security/advisories/GHSA-wh8j-xr66-f296 "GHSA-wh8j-xr66-f296")
  - [GHSA-553g-fcpf-m3wp](https://github.com/louislam/uptime-kuma/security/advisories/GHSA-553g-fcpf-m3wp "GHSA-553g-fcpf-m3wp")

### 💇‍♀️ Improvements

- [#2635](https://github.com/louislam/uptime-kuma/pull/2635) Uptime on status pages will now be capped at 100%, in case you system time is out of sync. (A common issue on


  Raspberry Pi) (Thanks [@Computroniks](https://github.com/Computroniks))
- [#2684](https://github.com/louislam/uptime-kuma/pull/2684) Save button now is easier to find on mobile (Thanks [@gitstart](https://github.com/gitstart))
- [#2647](https://github.com/louislam/uptime-kuma/pull/2647) Trim white space for `hostname` (Thanks [@chakflying](https://github.com/chakflying))
- Changing language is not working in the setup page

### 🦎 Translation Contributors

- [#2611](https://github.com/louislam/uptime-kuma/pull/2611) You can translate Uptime Kuma into your language on Weblate now (Thanks [@401U](https://github.com/401U))

  [https://weblate.kuma.pet](https://weblate.kuma.pet)
- [#2638](https://github.com/louislam/uptime-kuma/pull/2638) New language: Arabic (Thanks [@amworx](https://github.com/amworx))
- [`271cca0`](https://github.com/louislam/uptime-kuma/commit/271cca0d234914fc37a92088433b385ab05ac78e) New language: Romanian (Thanks [@EdyDeveloper](https://github.com/EdyDeveloper))
- [#2663](https://github.com/louislam/uptime-kuma/pull/2663) [#2670](https://github.com/louislam/uptime-kuma/pull/2670) [#2676](https://github.com/louislam/uptime-kuma/pull/2676) [#2713](https://github.com/louislam/uptime-kuma/pull/2713)

  [@amworx](https://github.com/amworx) [@401U](https://github.com/401U) [@Buchtic](https://github.com/Buchtic) [@black23](https://github.com/black23) [@deluxghost](https://github.com/deluxghost) [@cyril59310](https://github.com/cyril59310) [@sovushik](https://github.com/sovushik) [@Genc](https://github.com/Genc) [@Saibamen](https://github.com/Saibamen) [@MrEddX](https://github.com/MrEddX) [@DimitriDR](https://github.com/DimitriDR) [@chakflying](https://github.com/chakflying) [@rubesaca](https://github.com/rubesaca) [@mrkbaji](https://github.com/mrkbaji) [@dhfhfk](https://github.com/dhfhfk) [@lazki](https://github.com/lazki) [@GiuseppeMonaco](https://github.com/GiuseppeMonaco) [@BlackScreen](https://github.com/BlackScreen) [@edo2313](https://github.com/edo2313) [@ciccio0476](https://github.com/ciccio0476) [@MagicFun1241](https://github.com/MagicFun1241) [@ufukart](https://github.com/ufukart) [@401U](https://github.com/401U) [@SavvasMohito](https://github.com/SavvasMohito) [@Scan4u](https://github.com/Scan4u) [@thefourcraft](https://github.com/thefourcraft) [@rbunpat](https://github.com/rbunpat) [@dubsector](https://github.com/dubsector) [@EdyDeveloper](https://github.com/EdyDeveloper) [@djtms](https://github.com/djtms) [@Genc](https://github.com/Genc) [@AnnAngela](https://github.com/AnnAngela) [@moucho](https://github.com/moucho) [@devnied](https://github.com/devnied) [@ReneDyhr](https://github.com/ReneDyhr) [@ldev-coder](https://github.com/ldev-coder) [@magyarlatin](https://github.com/magyarlatin) [@marchelzi](https://github.com/marchelzi) [@funkill](https://github.com/funkill) [@hamx01](https://github.com/hamx01) [@JonneSaloranta](https://github.com/JonneSaloranta) [@aditbaco](https://github.com/aditbaco)


  (Let me know if your name is missing here)

### Others

- Other small changes and code refactoring:

  [@black23](https://github.com/black23) [@Metatropics](https://github.com/Metatropics) [@Buchtic](https://github.com/Buchtic) [@cyril59310](https://github.com/cyril59310) [@chakflying](https://github.com/chakflying) [@Saibamen](https://github.com/Saibamen)


  (Let me know if your name is missing here. If your pull request have been merged in this version)

---

# radicale 3.1.8.1

> published at 2023-02-22 [source](https://github.com/tomsquest/docker-radicale/releases/tag/3.1.8.1)

## What's Changed

- chore: update alpine to 3.17.2 by [@jauderho](https://github.com/jauderho) in [#127](https://github.com/tomsquest/docker-radicale/pull/127)
- fix: drop support for arm v7 by [@tomsquest](https://github.com/tomsquest) in [#128](https://github.com/tomsquest/docker-radicale/pull/128)

## New Contributors

- [@jauderho](https://github.com/jauderho) made their first contribution in [#127](https://github.com/tomsquest/docker-radicale/pull/127)

**Full Changelog**: [`3.1.8.0...3.1.8.1`](https://github.com/tomsquest/docker-radicale/compare/3.1.8.0...3.1.8.1)

---

# radicale latest

> published at 2023-02-22 [source](https://github.com/tomsquest/docker-radicale/releases/tag/latest)

doc: update CHANGELOG.md

---

# element v1.11.24-rc.2

> published at 2023-02-22 [source](https://github.com/vector-im/element-web/releases/tag/v1.11.24-rc.2)

## ✨ Features

- Polls: show warning about undecryptable relations ( [#10179](https://github.com/matrix-org/matrix-react-sdk/pull/10179)). Contributed by [@kerryarchibald](https://github.com/kerryarchibald).
- Display "The sender has blocked you from receiving this message" error message instead of "Unable to decrypt message" ( [#10202](https://github.com/matrix-org/matrix-react-sdk/pull/10202)). Contributed by [@florianduros](https://github.com/florianduros).

## 🐛 Bug Fixes

- Add link to next file in the export ( [#10190](https://github.com/matrix-org/matrix-react-sdk/pull/10190)). Fixes [#20272](https://github.com/vector-im/element-web/issues/20272). Contributed by [@grimhilt](https://github.com/grimhilt).
- Stop access token overflowing the box ( [#10069](https://github.com/matrix-org/matrix-react-sdk/pull/10069)). Fixes [#24023](https://github.com/vector-im/element-web/issues/24023). Contributed by [@sbjaj33](https://github.com/sbjaj33).
- Ended poll tiles: add ended the poll message ( [#10193](https://github.com/matrix-org/matrix-react-sdk/pull/10193)). Fixes [#24579](https://github.com/vector-im/element-web/issues/24579). Contributed by [@kerryarchibald](https://github.com/kerryarchibald).

---

# radicale 3.1.8.0

> published at 2023-02-22 [source](https://github.com/tomsquest/docker-radicale/releases/tag/3.1.8.0)

- Upgrade to [Radicale 3.1.8](https://github.com/Kozea/Radicale/blob/master/CHANGELOG.md#318)

**Full Changelog**: [`3.1.7.0...3.1.8.0`](https://github.com/tomsquest/docker-radicale/compare/3.1.7.0...3.1.8.0)

---

# soft-serve v0.4.6

> published at 2023-02-21 [source](https://github.com/charmbracelet/soft-serve/releases/tag/v0.4.6)

## Changelog

### New Features

- [`eafb45f`](https://github.com/charmbracelet/soft-serve/commit/eafb45f60709eb9b492d0a3e47a008df1197a142): feat(cfg): debug logging environment variable ( [@aymanbagabas](https://github.com/aymanbagabas))

### Bug fixes

- [`be76262`](https://github.com/charmbracelet/soft-serve/commit/be762627e37ff27d0065f8e00a3f796b19e4064d): fix(server): session race test ( [@aymanbagabas](https://github.com/aymanbagabas))

* * *

[![The Charm logo](https://camo.githubusercontent.com/65459c24b86d0476085210fd0387503a161a1359b3cf5034324346f55907cbb8/68747470733a2f2f73747566662e636861726d2e73682f636861726d2d62616467652e6a7067)](https://charm.sh/)

Thoughts? Questions? We love hearing from you. Feel free to reach out on [Twitter](https://twitter.com/charmcli), [The Fediverse](https://mastodon.technology/@charm), or on [Discord](https://charm.sh/chat).

---

# synapse v1.78.0rc1

> published at 2023-02-21 [source](https://github.com/matrix-org/synapse/releases/tag/v1.78.0rc1)

# Synapse 1.78.0rc1 (2023-02-21)

## Features

- Implement the experimental `exact_event_match` push rule condition from [MSC3758](https://github.com/matrix-org/matrix-spec-proposals/pull/3758). ( [#14964](https://github.com/matrix-org/synapse/issues/14964))
- Add account data to the command line [user data export tool](https://matrix-org.github.io/synapse/v1.78/usage/administration/admin_faq.html#how-can-i-export-user-data). ( [#14969](https://github.com/matrix-org/synapse/issues/14969))
- Implement [MSC3873](https://github.com/matrix-org/matrix-spec-proposals/pull/3873) to disambiguate push rule keys with dots in them. ( [#15004](https://github.com/matrix-org/synapse/issues/15004))
- Allow Synapse to use a specific Redis [logical database](https://redis.io/commands/select/) in worker-mode deployments. ( [#15034](https://github.com/matrix-org/synapse/issues/15034))
- Tag opentracing spans for federation requests with the name of the worker serving the request. ( [#15042](https://github.com/matrix-org/synapse/issues/15042))
- Implement the experimental `exact_event_property_contains` push rule condition from [MSC3966](https://github.com/matrix-org/matrix-spec-proposals/pull/3966). ( [#15045](https://github.com/matrix-org/synapse/issues/15045))
- Remove spurious `dont_notify` action from the defaults for the `.m.rule.reaction` pushrule. ( [#15073](https://github.com/matrix-org/synapse/issues/15073))
- Update the error code returned when user sends a duplicate annotation. ( [#15075](https://github.com/matrix-org/synapse/issues/15075))

## Bugfixes

- Prevent clients from reporting nonexistent events. ( [#13779](https://github.com/matrix-org/synapse/issues/13779))
- Return spec-compliant JSON errors when unknown endpoints are requested. ( [#14605](https://github.com/matrix-org/synapse/issues/14605))
- Fix a long-standing bug where the room aliases returned could be corrupted. ( [#15038](https://github.com/matrix-org/synapse/issues/15038))
- Fix a bug introduced in Synapse 1.76.0 where partially-joined rooms could not be deleted using the [purge room API](https://matrix-org.github.io/synapse/latest/admin_api/rooms.html#delete-room-api). ( [#15068](https://github.com/matrix-org/synapse/issues/15068))
- Fix a long-standing bug where federated joins would fail if the first server in the list of servers to try is not in the room. ( [#15074](https://github.com/matrix-org/synapse/issues/15074))
- Fix a bug introduced in Synapse v1.74.0 where searching with colons when using ICU for search term tokenisation would fail with an error. ( [#15079](https://github.com/matrix-org/synapse/issues/15079))
- Reduce the likelihood of a rare race condition where rejoining a restricted room over federation would fail. ( [#15080](https://github.com/matrix-org/synapse/issues/15080))
- Fix a bug introduced in Synapse 1.76 where workers would fail to start if the `health` listener was configured. ( [#15096](https://github.com/matrix-org/synapse/issues/15096))
- Fix a bug introduced in Synapse 1.75 where the [portdb script](https://matrix-org.github.io/synapse/release-v1.78/postgres.html#porting-from-sqlite) would fail to run after a room had been faster-joined. ( [#15108](https://github.com/matrix-org/synapse/issues/15108))

## Improved Documentation

- Document how to start Synapse with Poetry. Contributed by [@thezaidbintariq](https://github.com/thezaidbintariq). ( [#14892](https://github.com/matrix-org/synapse/issues/14892), [#15022](https://github.com/matrix-org/synapse/issues/15022))
- Update delegation documentation to clarify that SRV DNS delegation does not eliminate all needs to serve files from .well-known locations. Contributed by [@williamkray](https://github.com/williamkray). ( [#14959](https://github.com/matrix-org/synapse/issues/14959))
- Fix a mistake in registration\_shared\_secret\_path docs. ( [#15078](https://github.com/matrix-org/synapse/issues/15078))
- Refer to a more recent blog post on the [Database Maintenance Tools](https://matrix-org.github.io/synapse/latest/usage/administration/database_maintenance_tools.html) page. Contributed by [@jahway603](https://github.com/jahway603). ( [#15083](https://github.com/matrix-org/synapse/issues/15083))

## Internal Changes

- Re-type hint some collections as read-only. ( [#13755](https://github.com/matrix-org/synapse/issues/13755))
- Faster joins: don't stall when another user joins during a partial-state room resync. ( [#14606](https://github.com/matrix-org/synapse/issues/14606))
- Add a class `UnpersistedEventContext` to allow for the batching up of storing state groups. ( [#14675](https://github.com/matrix-org/synapse/issues/14675))
- Add a check to ensure that locked dependencies have source distributions available. ( [#14742](https://github.com/matrix-org/synapse/issues/14742))
- Tweak comment on `_is_local_room_accessible` as part of room visibility in `/hierarchy` to clarify the condition for a room being visible. ( [#14834](https://github.com/matrix-org/synapse/issues/14834))
- Prevent `WARNING: there is already a transaction in progress` lines appearing in PostgreSQL's logs on some occasions. ( [#14840](https://github.com/matrix-org/synapse/issues/14840))
- Use `StrCollection` to avoid potential bugs with `Collection[str]`. ( [#14929](https://github.com/matrix-org/synapse/issues/14929))
- Improve performance of `/sync` in a few situations. ( [#14973](https://github.com/matrix-org/synapse/issues/14973))
- Limit concurrent event creation for a room to avoid state resolution when sending bursts of events to a local room. ( [#14977](https://github.com/matrix-org/synapse/issues/14977))
- Skip calculating unread push actions in /sync when enable\_push is false. ( [#14980](https://github.com/matrix-org/synapse/issues/14980))
- Add a schema dump symlinks inside `contrib`, to make it easier for IDEs to interrogate Synapse's database schema. ( [#14982](https://github.com/matrix-org/synapse/issues/14982))
- Improve type hints. ( [#15008](https://github.com/matrix-org/synapse/issues/15008), [#15026](https://github.com/matrix-org/synapse/issues/15026), [#15027](https://github.com/matrix-org/synapse/issues/15027), [#15028](https://github.com/matrix-org/synapse/issues/15028), [#15031](https://github.com/matrix-org/synapse/issues/15031), [#15035](https://github.com/matrix-org/synapse/issues/15035), [#15052](https://github.com/matrix-org/synapse/issues/15052), [#15072](https://github.com/matrix-org/synapse/issues/15072), [#15084](https://github.com/matrix-org/synapse/issues/15084))
- Update [MSC3952](https://github.com/matrix-org/matrix-spec-proposals/pull/3952) support based on changes to the MSC. ( [#15037](https://github.com/matrix-org/synapse/issues/15037))
- Avoid mutating a cached value in `get_user_devices_from_cache`. ( [#15040](https://github.com/matrix-org/synapse/issues/15040))
- Fix a rare exception in logs on start up. ( [#15041](https://github.com/matrix-org/synapse/issues/15041))
- Update pyo3-log to v0.8.1. ( [#15043](https://github.com/matrix-org/synapse/issues/15043))
- Avoid mutating cached values in `_generate_sync_entry_for_account_data`. ( [#15047](https://github.com/matrix-org/synapse/issues/15047))
- Refactor arguments of `try_unbind_threepid` and `_try_unbind_threepid_with_id_server` to not use dictionaries. ( [#15053](https://github.com/matrix-org/synapse/issues/15053))
- Merge debug logging from the hotfixes branch. ( [#15054](https://github.com/matrix-org/synapse/issues/15054))
- Faster joins: omit device list updates originating from partial state rooms in /sync responses without lazy loading of members enabled. ( [#15069](https://github.com/matrix-org/synapse/issues/15069))
- Fix clashing database transaction name. ( [#15070](https://github.com/matrix-org/synapse/issues/15070))
- Upper-bound frozendict dependency. This works around us being unable to test installing our wheels against Python 3.11 in CI. ( [#15114](https://github.com/matrix-org/synapse/issues/15114))
- Tweak logging for when a worker waits for its view of a replication stream to catch up. ( [#15120](https://github.com/matrix-org/synapse/issues/15120))

Locked dependency updates

- Bump bleach from 5.0.1 to 6.0.0. ( [#15059](https://github.com/matrix-org/synapse/issues/15059))
- Bump cryptography from 38.0.4 to 39.0.1. ( [#15020](https://github.com/matrix-org/synapse/issues/15020))
- Bump ruff version from 0.0.230 to 0.0.237. ( [#15033](https://github.com/matrix-org/synapse/issues/15033))
- Bump dtolnay/rust-toolchain from 9cd00a88a73addc8617065438eff914dd08d0955 to 25dc93b901a87e864900a8aec6c12e9aa794c0c3. ( [#15060](https://github.com/matrix-org/synapse/issues/15060))
- Bump systemd-python from 234 to 235. ( [#15061](https://github.com/matrix-org/synapse/issues/15061))
- Bump serde\_json from 1.0.92 to 1.0.93. ( [#15062](https://github.com/matrix-org/synapse/issues/15062))
- Bump types-requests from 2.28.11.8 to 2.28.11.12. ( [#15063](https://github.com/matrix-org/synapse/issues/15063))
- Bump types-pillow from 9.4.0.5 to 9.4.0.10. ( [#15064](https://github.com/matrix-org/synapse/issues/15064))
- Bump sentry-sdk from 1.13.0 to 1.15.0. ( [#15065](https://github.com/matrix-org/synapse/issues/15065))
- Bump types-jsonschema from 4.17.0.3 to 4.17.0.5. ( [#15099](https://github.com/matrix-org/synapse/issues/15099))
- Bump types-bleach from 5.0.3.1 to 6.0.0.0. ( [#15100](https://github.com/matrix-org/synapse/issues/15100))
- Bump dtolnay/rust-toolchain from 25dc93b901a87e864900a8aec6c12e9aa794c0c3 to e12eda571dc9a5ee5d58eecf4738ec291c66f295. ( [#15101](https://github.com/matrix-org/synapse/issues/15101))
- Bump dawidd6/action-download-artifact from 2.24.3 to 2.25.0. ( [#15102](https://github.com/matrix-org/synapse/issues/15102))
- Bump types-pillow from 9.4.0.10 to 9.4.0.13. ( [#15104](https://github.com/matrix-org/synapse/issues/15104))
- Bump types-setuptools from 67.1.0.0 to 67.3.0.1. ( [#15105](https://github.com/matrix-org/synapse/issues/15105))

---

# element v1.11.24-rc.1

> published at 2023-02-21 [source](https://github.com/vector-im/element-web/releases/tag/v1.11.24-rc.1)

## ✨ Features

- Poll history: fetch last 30 days of polls ( [#10157](https://github.com/matrix-org/matrix-react-sdk/pull/10157)). Contributed by [@kerryarchibald](https://github.com/kerryarchibald).
- Poll history - ended polls list items ( [#10119](https://github.com/matrix-org/matrix-react-sdk/pull/10119)). Contributed by [@kerryarchibald](https://github.com/kerryarchibald).
- Remove threads labs flag and the ability to disable threads ( [#9878](https://github.com/matrix-org/matrix-react-sdk/pull/9878)). Fixes [#24365](https://github.com/vector-im/element-web/issues/24365).
- Show a success dialog after setting up the key backup ( [#10177](https://github.com/matrix-org/matrix-react-sdk/pull/10177)). Fixes [#24487](https://github.com/vector-im/element-web/issues/24487).
- Release Sign in with QR out of labs ( [#10182](https://github.com/matrix-org/matrix-react-sdk/pull/10182)). Contributed by [@hughns](https://github.com/hughns).
- Release Sign in with QR out of labs ( [#10066](https://github.com/matrix-org/matrix-react-sdk/pull/10066)). Contributed by [@hughns](https://github.com/hughns).
- Hide indent button in rte ( [#10149](https://github.com/matrix-org/matrix-react-sdk/pull/10149)). Contributed by [@alunturner](https://github.com/alunturner).
- Add option to find own location in map views ( [#10083](https://github.com/matrix-org/matrix-react-sdk/pull/10083)).
- Render poll end events in timeline ( [#10027](https://github.com/matrix-org/matrix-react-sdk/pull/10027)). Contributed by [@kerryarchibald](https://github.com/kerryarchibald).

## 🐛 Bug Fixes

- Fix accidentally inverted condition for room ordering ( [#10178](https://github.com/matrix-org/matrix-react-sdk/pull/10178)). Fixes [#24527](https://github.com/vector-im/element-web/issues/24527). Contributed by [@justjanne](https://github.com/justjanne).
- Re-focus the composer on dialogue quit ( [#10007](https://github.com/matrix-org/matrix-react-sdk/pull/10007)). Fixes [#22832](https://github.com/vector-im/element-web/issues/22832). Contributed by [@Ashu999](https://github.com/Ashu999).
- Try to resolve emails before creating a DM ( [#10164](https://github.com/matrix-org/matrix-react-sdk/pull/10164)).
- Disable poll response loading test ( [#10168](https://github.com/matrix-org/matrix-react-sdk/pull/10168)). Contributed by [@justjanne](https://github.com/justjanne).
- Fix email lookup in invite dialog ( [#10150](https://github.com/matrix-org/matrix-react-sdk/pull/10150)). Fixes [#23353](https://github.com/vector-im/element-web/issues/23353).
- Remove duplicate white space characters from translation keys ( [#10152](https://github.com/matrix-org/matrix-react-sdk/pull/10152)). Contributed by [@luixxiul](https://github.com/luixxiul).
- Fix the caption of new sessions manager on Labs settings page for localization ( [#10143](https://github.com/matrix-org/matrix-react-sdk/pull/10143)). Contributed by [@luixxiul](https://github.com/luixxiul).
- Prevent start another DM with a user if one already exists ( [#10127](https://github.com/matrix-org/matrix-react-sdk/pull/10127)). Fixes [#23138](https://github.com/vector-im/element-web/issues/23138).
- Remove white space characters before the horizontal ellipsis ( [#10130](https://github.com/matrix-org/matrix-react-sdk/pull/10130)). Contributed by [@luixxiul](https://github.com/luixxiul).
- Fix Selectable Text on 'Delete All' and 'Retry All' Buttons ( [#10128](https://github.com/matrix-org/matrix-react-sdk/pull/10128)). Fixes [#23232](https://github.com/vector-im/element-web/issues/23232). Contributed by [@akshattchhabra](https://github.com/akshattchhabra).
- Correctly Identify emoticons ( [#10108](https://github.com/matrix-org/matrix-react-sdk/pull/10108)). Fixes [#19472](https://github.com/vector-im/element-web/issues/19472). Contributed by [@adarsh-sgh](https://github.com/adarsh-sgh).
- Remove a redundant white space ( [#10129](https://github.com/matrix-org/matrix-react-sdk/pull/10129)). Contributed by [@luixxiul](https://github.com/luixxiul).

---

# dynamic-dns v3.10.0-ls110

> published at 2023-02-21 [source](https://github.com/linuxserver/docker-ddclient/releases/tag/v3.10.0-ls110)

**LinuxServer Changes:**

Rebase to Alpine 3.17, migrate to s6v3.

**ddclient Changes:**

**Note**

Please also read the changelog for v3.10.0\_2 and v3.10.0\_1 which contains dependency changes.

### New features

- Added support for domaindiscount24.com
- Added support for njal.la

---

# draupnir v1.80.1

> published at 2023-02-20 [source](https://github.com/Gnuxie/Draupnir/releases/tag/v1.80.1)

This is just a patch release with the following bug fixes:

- Fixed a bug where the ban command wouldn't be able to resolve room aliases when they were explicitly given as an argument.
- Fixed a bug where the ban command would crash when prompting for a policy list.

#### Development

- add .gitattributes & .editorconfig by [@Mikaela](https://github.com/Mikaela) in [#25](https://github.com/Gnuxie/Draupnir/pull/25)
- `noImplicitThis` is now `true` in `tsconfig.json`

## New Contributors

- [@Mikaela](https://github.com/Mikaela) made their first contribution in [#25](https://github.com/Gnuxie/Draupnir/pull/25)

**Full Changelog**: [`v1.80.0...v1.80.1`](https://github.com/Gnuxie/Draupnir/compare/v1.80.0...v1.80.1)

---

# ansible-core 2.14.3rc1

> published at 2023-02-20 [source](https://pypi.org/project/ansible-core/2.14.3rc1/)

> no description provided

---



---

# ansible-core 2.13.8rc1

> published at 2023-02-20 [source](https://pypi.org/project/ansible-core/2.13.8rc1/)

> no description provided

---



---

# prometheus 2.37.6 / 2023-02-20

> published at 2023-02-20 [source](https://github.com/prometheus/prometheus/releases/tag/v2.37.6)

This release contains a toolchain update. It is built on top of Go 1.19, as the Go

1.18 release is no longer supported upstream.

---

# com.devture.ansible.role.traefik v2.9.8

> published at 2023-02-20 [source](https://github.com/traefik/traefik/releases/tag/v2.9.8)

**Bug fixes:**

- **\[server\]** Update golang.org/x/net to v0.7.0 ( [#9716](https://github.com/traefik/traefik/pull/9716) by [ldez](https://github.com/ldez))

fix [CVE-2022-41724](https://www.cve.org/CVERecord?id=CVE-2022-41724)

---

# prometheus v0.37.6

> published at 2023-02-20 [source](https://github.com/prometheus/prometheus/releases/tag/v0.37.6)

v0.37.6

---

# ntfy v2.0.1

> published at 2023-02-18 [source](https://github.com/binwiederhier/ntfy/releases/tag/v2.0.1)

This is a quick bugfix release to address a panic that happens when `attachment-cache-dir` is not set.

**Bug fixes + maintenance:**

- Avoid panic in manager when `attachment-cache-dir` is not set ( [#617](https://github.com/binwiederhier/ntfy/issues/617), thanks to [@ksurl](https://github.com/ksurl))
- Ensure that calls to standard logger `log.Println` also output JSON (no ticket)

---

# ntfy v2.0.0

> published at 2023-02-18 [source](https://github.com/binwiederhier/ntfy/releases/tag/v2.0.0)

This is the biggest ntfy server release I've ever done 🥳 . Lots of new and exciting features.

**Brand-new features:**

- **User signup/login & account sync**: If enabled, users can now register to create a user account, and then login to the web app. Once logged in, topic subscriptions and user settings are stored server-side in the user account (as opposed to only in the browser storage). So far, this is implemented only in the web app only. Once it's in the Android/iOS app, you can easily keep your account in sync. Relevant [config options](https://docs.ntfy.sh/config/#config-options) are `enable-signup` and `enable-login`.
- **Topic reservations** 🎉: If enabled, users can now **reserve topics and restrict access to other users**. Once this is fully rolled out, you may reserve `ntfy.sh/philbackups` and define access so that only you can publish/subscribe to the topic. Reservations let you claim ownership of a topic, and you can define access permissions for others as `deny-all` (only you have full access), `read-only` (you can publish/subscribe, others can subscribe), `write-only` (you can publish/subscribe, others can publish), `read-write` (everyone can publish/subscribe, but you remain the owner). Topic reservations can be [configured](https://docs.ntfy.sh/config/#config-options) in the web app if `enable-reservations` is enabled, and only if the user has a [tier](https://docs.ntfy.sh/config/#tiers) that supports reservations.
- **Access tokens:** It is now possible to create user access tokens for a user account. Access tokens are useful to avoid having to paste your password to various applications or scripts. For instance, you may want to use a dedicated token to publish from your backup host, and one from your home automation system. Tokens can be configured in the web app, or via the `ntfy token` command. See [creating tokens](https://docs.ntfy.sh/config/#access-tokens), and [publishing using tokens](https://docs.ntfy.sh/publish/#access-tokens).
- **Structured logging:** I've redone a lot of the logging to make it more structured, and to make it easier to debug and troubleshoot. Logs can now be written to a file, and as JSON (if configured). Each log event carries context fields that you can filter and search on using tools like `jq`. On top of that, you can override the log level if certain fields match. For instance, you can say `user_name=phil -> debug` to log everything related to a certain user with debug level. See [logging & debugging](https://docs.ntfy.sh/config/#logging-debugging).
- **Tiers:** You can now define and associate usage tiers to users. Tiers can be used to grant users higher limits, such as daily message limits, attachment size, or make it possible for users to reserve topics. You could, for instance, have a tier `Standard` that allows 500 messages/day, 15 MB attachments and 5 allowed topic reservations, and another tier `Friends & Family` with much higher limits. For ntfy.sh, I'll mostly use these tiers to facilitate paid plans (see below). Tiers can be configured via the `ntfy tier ...` command. See [tiers](https://docs.ntfy.sh/config/#tiers).
- **Paid tiers:** Starting very soon, I will be offering paid tiers for ntfy.sh on top of the free service. You'll be able to subscribe to tiers with higher rate limits (more daily messages, bigger attachments) and topic reservations. Paid tiers are facilitated by integrating [Stripe](https://stripe.com) as a payment provider. See [payments](https://docs.ntfy.sh/config/#payments) for details.

**ntfy is forever open source!**

Yes, I will be offering some paid plans. But you don't need to panic! I won't be taking any features away, and everything will remain forever open source, so you can self-host if you like. Similar to the donations via [GitHub Sponsors](https://github.com/sponsors/binwiederhier) and [Liberapay](https://en.liberapay.com/ntfy/), paid plans will help pay for the service and keep me motivated to keep going. It'll only make ntfy better.

**Special thanks:**

A big Thank-you goes to everyone who tested the user account and payments work. I very much appreciate all the feedback,

suggestions, and bug reports. Thank you, [@nwithan8](https://github.com/nwithan8), @deadcade, [@xenrox](https://github.com/xenrox), [@cmeis](https://github.com/cmeis), [@wunter8](https://github.com/wunter8), and the others who I forgot.

---

# mautrix-whatsapp v0.8.2

> published at 2023-02-16 [source](https://github.com/mautrix/whatsapp/releases/tag/v0.8.2)

- Updated portal room power levels to always allow poll votes.
- Fixed disappearing message timing being implemented incorrectly.
- Fixed server rejecting messages not being handled as an error.
- Fixed sent files not being downloadable on latest WhatsApp beta versions.
- Fixed `sync space` command not syncing DMs into the space properly.
- Added workaround for broken clients like Element iOS that can't render normal image messages correctly.

---

# mautrix-discord v0.1.1

> published at 2023-02-16 [source](https://github.com/mautrix/discord/releases/tag/v0.1.1)

- Started automatically subscribing to bridged guilds. This fixes two problems:
  - Typing notifications should now work automatically in guilds.
  - Huge guilds now actually get messages bridged.
- Added support for converting animated lottie stickers to raster formats using [lottieconverter](https://github.com/sot-tech/LottieConverter).
- Added basic bridging for call start and guild join messages.
- Improved markdown parsing to disable more features that don't exist on Discord.
- Removed width from inline images (e.g. in the `guilds status` output) to handle non-square images properly.
- Fixed ghost user info not being synced when receiving reactions.

---

# kuma 1.20.1

> published at 2023-02-15 [source](https://github.com/louislam/uptime-kuma/releases/tag/1.20.1)

Please make sure your server have enough disk space before upgrading from older versions.

Also, always perform a backup of your services before upgrading.

### 🐛 Bug Fixes

- [#2779](https://github.com/louislam/uptime-kuma/pull/2779) Database deleted unexpectedly if disk space is not enough when updating (Thanks [@Computroniks](https://github.com/Computroniks))
- [`d1175ff`](https://github.com/louislam/uptime-kuma/commit/d1175ff471ff814e652f48cb97132dae51939037) Postgres monitor crashed if the sql query statement was not provided

### 🦎 Translation Contributors

- [#2770](https://github.com/louislam/uptime-kuma/pull/2770) New language: Finnish (Thanks [@JonneSaloranta](https://github.com/JonneSaloranta))
- [#2767](https://github.com/louislam/uptime-kuma/pull/2767) [@victorpahuus](https://github.com/victorpahuus) [@denyskon](https://github.com/denyskon) [@lfac76](https://github.com/lfac76) [@Genc](https://github.com/Genc) [@eltionb](https://github.com/eltionb) [@JonneSaloranta](https://github.com/JonneSaloranta) [@AmadeusGraves](https://github.com/AmadeusGraves) [@MrEddX](https://github.com/MrEddX) [@black23](https://github.com/black23) darkslash#2558


  (Let me know if your name is missing here)

### Others

- Other small changes and code refactoring:

  [@chakflying](https://github.com/chakflying)


  (Let me know if your name is missing here. If your pull request have been merged in this version)

---

# ntfy v1.31.0

> published at 2023-02-15 [source](https://github.com/binwiederhier/ntfy/releases/tag/v1.31.0)

This is a tiny release before the really big release, and also the last before the big v2.0.0. The most interesting things in this release are the new preliminary health endpoint to allow monitoring in K8s (and others), and the removal of `upx` binary packing (which was causing erroneous virus flagging). Aside from that, the `go-smtp` library did a breaking-change upgrade, which required some work to get working again.

**Features:**

- Preliminary `/v1/health` API endpoint for service monitoring (no ticket)
- Add basic health check to `Dockerfile` ( [#555](https://github.com/binwiederhier/ntfy/pull/555), thanks to [@bt90](https://github.com/bt90))

**Bug fixes + maintenance:**

- Fix `chown` issues with RHEL-like based systems ( [#566](https://github.com/binwiederhier/ntfy/issues/566)/ [#565](https://github.com/binwiederhier/ntfy/pull/565), thanks to [@danieldemus](https://github.com/danieldemus))
- Removed `upx` (binary packing) for all builds due to false virus warnings ( [#576](https://github.com/binwiederhier/ntfy/issues/576), thanks to [@shawnhwei](https://github.com/shawnhwei) for reporting)
- Upgraded `go-smtp` library and tests to v0.16.0 ( [#569](https://github.com/binwiederhier/ntfy/issues/569))

**Documentation:**

- Add HTTP/2 and TLSv1.3 support to nginx docs ( [#553](https://github.com/binwiederhier/ntfy/issues/553), thanks to [@bt90](https://github.com/bt90))
- Small wording change for `client.yml` ( [#562](https://github.com/binwiederhier/ntfy/pull/562), thanks to [@fleopaulD](https://github.com/fleopaulD))
- Fix K8s install docs ( [#582](https://github.com/binwiederhier/ntfy/pull/582), thanks to [@Remedan](https://github.com/Remedan))
- Updated Jellyseer docs ( [#604](https://github.com/binwiederhier/ntfy/pull/604), thanks to [@Y0ngg4n](https://github.com/Y0ngg4n))
- Updated iOS developer docs ( [#605](https://github.com/binwiederhier/ntfy/pull/605), thanks to [@SticksDev](https://github.com/SticksDev))

**Additional languages:**

- Portuguese (thanks to [@ssantos](https://hosted.weblate.org/user/ssantos/))

---

# hydrogen v0.3.8

> published at 2023-02-14 [source](https://github.com/vector-im/hydrogen-web/releases/tag/v0.3.8)

- add initial support for cross-signing behind a feature flag, for now you can just sign your own hydrogen device ( [#953](https://github.com/vector-im/hydrogen-web/issues/953), [#954](https://github.com/vector-im/hydrogen-web/issues/954), [#955](https://github.com/vector-im/hydrogen-web/issues/955))
- fix /me (emotes) sending invalid messages, that are furthermore not rendered by hydrogen giving the impression that nothing is sent.

---

# com.devture.ansible.role.traefik v2.9.7

> published at 2023-02-14 [source](https://github.com/traefik/traefik/releases/tag/v2.9.7)

**Bug fixes:**

- **\[acme\]** Update go-acme/lego to v4.10.0 ( [#9705](https://github.com/traefik/traefik/pull/9705) by [ldez](https://github.com/ldez))
- **\[ecs\]** Prevent panicking when a container has no network interfaces ( [#9661](https://github.com/traefik/traefik/pull/9661) by [rtribotte](https://github.com/rtribotte))
- **\[file\]** Make file provider more resilient wrt first configuration ( [#9595](https://github.com/traefik/traefik/pull/9595) by [mpl](https://github.com/mpl))
- **\[logs\]** Differentiate UDP stream and TCP connection in logs ( [#9687](https://github.com/traefik/traefik/pull/9687) by [rtribotte](https://github.com/rtribotte))
- **\[middleware\]** Prevent from no rate limiting when average is zero ( [#9621](https://github.com/traefik/traefik/pull/9621) by [witalisoft](https://github.com/witalisoft))
- **\[middleware\]** Prevents superfluous WriteHeader call in the error middleware ( [#9620](https://github.com/traefik/traefik/pull/9620) by [tomMoulard](https://github.com/tomMoulard))
- **\[middleware\]** Sanitize X-Forwarded-Proto header in RedirectScheme middleware ( [#9598](https://github.com/traefik/traefik/pull/9598) by [ldez](https://github.com/ldez))
- **\[plugins\]** Update paerser to v0.2.0 ( [#9671](https://github.com/traefik/traefik/pull/9671) by [ldez](https://github.com/ldez))
- **\[plugins\]** Update Yaegi to v0.15.0 ( [#9700](https://github.com/traefik/traefik/pull/9700) by [ldez](https://github.com/ldez))
- **\[tls,http3\]** Bump quic-go to 89769f409f ( [#9685](https://github.com/traefik/traefik/pull/9685) by [mpl](https://github.com/mpl))
- **\[tls,tcp\]** Adds the support for IPv6 in the TCP HostSNI matcher ( [#9692](https://github.com/traefik/traefik/pull/9692) by [rtribotte](https://github.com/rtribotte))

**Documentation:**

- **\[acme\]** Add CNAME support and gotchas ( [#9698](https://github.com/traefik/traefik/pull/9698) by [mpl](https://github.com/mpl))
- **\[acme\]** Further Let's Encrypt ratelimit warnings ( [#9627](https://github.com/traefik/traefik/pull/9627) by [hcooper](https://github.com/hcooper))
- **\[k8s\]** Add info admonition about routing to k8 services ( [#9645](https://github.com/traefik/traefik/pull/9645) by [svx](https://github.com/svx))
- **\[k8s\]** Improve TLSStore CRD documentation ( [#9579](https://github.com/traefik/traefik/pull/9579) by [mloiseleur](https://github.com/mloiseleur))
- **\[middleware\]** doc: add note about remoteaddr strategy ( [#9701](https://github.com/traefik/traefik/pull/9701) by [mpl](https://github.com/mpl))
- Update copyright to match new standard ( [#9651](https://github.com/traefik/traefik/pull/9651) by [paulocfjunior](https://github.com/paulocfjunior))
- Update copyright for 2023 ( [#9631](https://github.com/traefik/traefik/pull/9631) by [kevinpollet](https://github.com/kevinpollet))
- Update submitting pull requests to include language about drafts ( [#9609](https://github.com/traefik/traefik/pull/9609) by [tfny](https://github.com/tfny))

---

# synapse v1.77.0

> published at 2023-02-14 [source](https://github.com/matrix-org/synapse/releases/tag/v1.77.0)

# Synapse 1.77.0 (2023-02-14)

No significant changes since 1.77.0rc2.

# Synapse 1.77.0rc2 (2023-02-10)

## Bugfixes

- Fix bug where retried replication requests would return a failure. Introduced in v1.76.0. ( [#15024](https://github.com/matrix-org/synapse/issues/15024))

## Internal Changes

- Prepare for future database schema changes. ( [#15036](https://github.com/matrix-org/synapse/issues/15036))

# Synapse 1.77.0rc1 (2023-02-07)

## Features

- Experimental support for [MSC3952](https://github.com/matrix-org/matrix-spec-proposals/pull/3952): intentional mentions. ( [#14823](https://github.com/matrix-org/synapse/issues/14823), [#14943](https://github.com/matrix-org/synapse/issues/14943), [#14957](https://github.com/matrix-org/synapse/issues/14957), [#14958](https://github.com/matrix-org/synapse/issues/14958))
- Experimental support to suppress notifications from message edits ( [MSC3958](https://github.com/matrix-org/matrix-spec-proposals/pull/3958)). ( [#14960](https://github.com/matrix-org/synapse/issues/14960), [#15016](https://github.com/matrix-org/synapse/issues/15016))
- Add profile information, devices and connections to the command line [user data export tool](https://matrix-org.github.io/synapse/v1.77/usage/administration/admin_faq.html#how-can-i-export-user-data). ( [#14894](https://github.com/matrix-org/synapse/issues/14894))
- Improve performance when joining or sending an event in large rooms. ( [#14962](https://github.com/matrix-org/synapse/issues/14962))
- Improve performance of joining and leaving large rooms with many local users. ( [#14971](https://github.com/matrix-org/synapse/issues/14971))

## Bugfixes

- Fix a bug introduced in Synapse 1.53.0 where `next_batch` tokens from `/sync` could not be used with the `/relations` endpoint. ( [#14866](https://github.com/matrix-org/synapse/issues/14866))
- Fix a bug introduced in Synapse 1.35.0 where the module API's `send_local_online_presence_to` would fail to send presence updates over federation. ( [#14880](https://github.com/matrix-org/synapse/issues/14880))
- Fix a bug introduced in Synapse 1.70.0 where the background updates to add non-thread unique indexes on receipts could fail when upgrading from 1.67.0 or earlier. ( [#14915](https://github.com/matrix-org/synapse/issues/14915))
- Fix a regression introduced in Synapse 1.69.0 which can result in database corruption when database migrations are interrupted on sqlite. ( [#14926](https://github.com/matrix-org/synapse/issues/14926))
- Fix a bug introduced in Synapse 1.68.0 where we were unable to service remote joins in rooms with `@room` notification levels set to `null` in their (malformed) power levels. ( [#14942](https://github.com/matrix-org/synapse/issues/14942))
- Fix a bug introduced in Synapse 1.64.0 where boolean power levels were erroneously permitted in [v10 rooms](https://spec.matrix.org/v1.5/rooms/v10/). ( [#14944](https://github.com/matrix-org/synapse/issues/14944))
- Fix a long-standing bug where sending messages on servers with presence enabled would spam "Re-starting finished log context" log lines. ( [#14947](https://github.com/matrix-org/synapse/issues/14947))
- Fix a bug introduced in Synapse 1.68.0 where logging from the Rust module was not properly logged. ( [#14976](https://github.com/matrix-org/synapse/issues/14976))
- Fix various long-standing bugs in Synapse's config, event and request handling where booleans were unintentionally accepted where an integer was expected. ( [#14945](https://github.com/matrix-org/synapse/issues/14945))

## Internal Changes

- Add missing type hints. ( [#14879](https://github.com/matrix-org/synapse/issues/14879), [#14886](https://github.com/matrix-org/synapse/issues/14886), [#14887](https://github.com/matrix-org/synapse/issues/14887), [#14904](https://github.com/matrix-org/synapse/issues/14904), [#14927](https://github.com/matrix-org/synapse/issues/14927), [#14956](https://github.com/matrix-org/synapse/issues/14956), [#14983](https://github.com/matrix-org/synapse/issues/14983), [#14984](https://github.com/matrix-org/synapse/issues/14984), [#14985](https://github.com/matrix-org/synapse/issues/14985), [#14987](https://github.com/matrix-org/synapse/issues/14987), [#14988](https://github.com/matrix-org/synapse/issues/14988), [#14990](https://github.com/matrix-org/synapse/issues/14990), [#14991](https://github.com/matrix-org/synapse/issues/14991), [#14992](https://github.com/matrix-org/synapse/issues/14992), [#15007](https://github.com/matrix-org/synapse/issues/15007))
- Use `StrCollection` to avoid potential bugs with `Collection[str]`. ( [#14922](https://github.com/matrix-org/synapse/issues/14922))
- Allow running the complement tests suites with the asyncio reactor enabled. ( [#14858](https://github.com/matrix-org/synapse/issues/14858))
- Improve performance of `/sync` in a few situations. ( [#14908](https://github.com/matrix-org/synapse/issues/14908), [#14970](https://github.com/matrix-org/synapse/issues/14970))
- Document how to handle Dependabot pull requests. ( [#14916](https://github.com/matrix-org/synapse/issues/14916))
- Fix typo in release script. ( [#14920](https://github.com/matrix-org/synapse/issues/14920))
- Update build system requirements to allow building with poetry-core 1.5.0. ( [#14949](https://github.com/matrix-org/synapse/issues/14949), [#15019](https://github.com/matrix-org/synapse/issues/15019))
- Add an [lnav](https://lnav.org) config file for Synapse logs to `/contrib/lnav`. ( [#14953](https://github.com/matrix-org/synapse/issues/14953))
- Faster joins: Refactor internal handling of servers in room to never store an empty list. ( [#14954](https://github.com/matrix-org/synapse/issues/14954))
- Faster joins: tag `v2/send_join/` requests to indicate if they served a partial join response. ( [#14950](https://github.com/matrix-org/synapse/issues/14950))
- Allow running `cargo` without the `extension-module` option. ( [#14965](https://github.com/matrix-org/synapse/issues/14965))
- Preparatory work for adding a denormalised event stream ordering column in the future. Contributed by Nick @ Beeper ( [@Fizzadar](https://github.com/Fizzadar)). ( [#14979](https://github.com/matrix-org/synapse/issues/14979), [9cd7610](https://github.com/matrix-org/synapse/commit/9cd7610f86ab5051c9365dd38d1eec405a5f8ca6), [f10caa7](https://github.com/matrix-org/synapse/commit/f10caa73eee0caa91cf373966104d1ededae2aee); see [#15014](https://github.com/matrix-org/synapse/issues/15014))
- Add tests for `_flatten_dict`. ( [#14981](https://github.com/matrix-org/synapse/issues/14981), [#15002](https://github.com/matrix-org/synapse/issues/15002))

Dependabot updates

- Bump dtolnay/rust-toolchain from e645b0cf01249a964ec099494d38d2da0f0b349f to 9cd00a88a73addc8617065438eff914dd08d0955. ( [#14968](https://github.com/matrix-org/synapse/issues/14968))
- Bump docker/build-push-action from 3 to 4. ( [#14952](https://github.com/matrix-org/synapse/issues/14952))
- Bump ijson from 3.1.4 to 3.2.0.post0. ( [#14935](https://github.com/matrix-org/synapse/issues/14935))
- Bump types-pyyaml from 6.0.12.2 to 6.0.12.3. ( [#14936](https://github.com/matrix-org/synapse/issues/14936))
- Bump types-jsonschema from 4.17.0.2 to 4.17.0.3. ( [#14937](https://github.com/matrix-org/synapse/issues/14937))
- Bump types-pillow from 9.4.0.3 to 9.4.0.5. ( [#14938](https://github.com/matrix-org/synapse/issues/14938))
- Bump hiredis from 2.0.0 to 2.1.1. ( [#14939](https://github.com/matrix-org/synapse/issues/14939))
- Bump hiredis from 2.1.1 to 2.2.1. ( [#14993](https://github.com/matrix-org/synapse/issues/14993))
- Bump types-setuptools from 65.6.0.3 to 67.1.0.0. ( [#14994](https://github.com/matrix-org/synapse/issues/14994))
- Bump prometheus-client from 0.15.0 to 0.16.0. ( [#14995](https://github.com/matrix-org/synapse/issues/14995))
- Bump anyhow from 1.0.68 to 1.0.69. ( [#14996](https://github.com/matrix-org/synapse/issues/14996))
- Bump serde\_json from 1.0.91 to 1.0.92. ( [#14997](https://github.com/matrix-org/synapse/issues/14997))
- Bump isort from 5.11.4 to 5.11.5. ( [#14998](https://github.com/matrix-org/synapse/issues/14998))
- Bump phonenumbers from 8.13.4 to 8.13.5. ( [#14999](https://github.com/matrix-org/synapse/issues/14999))

---

# element v1.11.23

> published at 2023-02-14 [source](https://github.com/vector-im/element-web/releases/tag/v1.11.23)

## ✨ Features

- Description of QR code sign in labs feature ( [#23513](https://github.com/vector-im/element-web/pull/23513)). Contributed by [@hughns](https://github.com/hughns).
- Add option to find own location in map views ( [#10083](https://github.com/matrix-org/matrix-react-sdk/pull/10083)).
- Render poll end events in timeline ( [#10027](https://github.com/matrix-org/matrix-react-sdk/pull/10027)). Contributed by [@kerryarchibald](https://github.com/kerryarchibald).
- Indicate unread messages in tab title ( [#10096](https://github.com/matrix-org/matrix-react-sdk/pull/10096)). Contributed by [@tnt7864](https://github.com/tnt7864).
- Open message in editing mode when keyboard up is pressed (RTE) ( [#10079](https://github.com/matrix-org/matrix-react-sdk/pull/10079)). Contributed by [@florianduros](https://github.com/florianduros).
- Hide superseded rooms from the room list using dynamic room predecessors ( [#10068](https://github.com/matrix-org/matrix-react-sdk/pull/10068)). Contributed by [@andybalaam](https://github.com/andybalaam).
- Support MSC3946 in RoomListStore ( [#10054](https://github.com/matrix-org/matrix-react-sdk/pull/10054)). Fixes [#24325](https://github.com/vector-im/element-web/issues/24325). Contributed by [@andybalaam](https://github.com/andybalaam).
- Auto focus security key field ( [#10048](https://github.com/matrix-org/matrix-react-sdk/pull/10048)).
- use Poll model with relations API in poll rendering ( [#9877](https://github.com/matrix-org/matrix-react-sdk/pull/9877)). Contributed by [@kerryarchibald](https://github.com/kerryarchibald).
- Support MSC3946 in the RoomCreate tile ( [#10041](https://github.com/matrix-org/matrix-react-sdk/pull/10041)). Fixes [#24323](https://github.com/vector-im/element-web/issues/24323). Contributed by [@andybalaam](https://github.com/andybalaam).
- Update labs flag description for RTE ( [#10058](https://github.com/matrix-org/matrix-react-sdk/pull/10058)). Contributed by [@florianduros](https://github.com/florianduros).
- Change ul list style to disc when editing message ( [#10043](https://github.com/matrix-org/matrix-react-sdk/pull/10043)). Contributed by [@alunturner](https://github.com/alunturner).
- Improved click detection within PiP windows ( [#10040](https://github.com/matrix-org/matrix-react-sdk/pull/10040)). Fixes [#24371](https://github.com/vector-im/element-web/issues/24371).
- Add RTE keyboard navigation in editing ( [#9980](https://github.com/matrix-org/matrix-react-sdk/pull/9980)). Fixes [#23621](https://github.com/vector-im/element-web/issues/23621). Contributed by [@florianduros](https://github.com/florianduros).
- Paragraph integration for rich text editor ( [#10008](https://github.com/matrix-org/matrix-react-sdk/pull/10008)). Contributed by [@alunturner](https://github.com/alunturner).
- Add indentation increasing/decreasing to RTE ( [#10034](https://github.com/matrix-org/matrix-react-sdk/pull/10034)). Contributed by [@florianduros](https://github.com/florianduros).
- Add ignore user confirmation dialog ( [#6116](https://github.com/matrix-org/matrix-react-sdk/pull/6116)). Fixes [#14746](https://github.com/vector-im/element-web/issues/14746).
- Use monospace font for room, message IDs in View Source modal ( [#9956](https://github.com/matrix-org/matrix-react-sdk/pull/9956)). Fixes [#21937](https://github.com/vector-im/element-web/issues/21937). Contributed by [@paragpoddar](https://github.com/paragpoddar).
- Implement MSC3946 for AdvancedRoomSettingsTab ( [#9995](https://github.com/matrix-org/matrix-react-sdk/pull/9995)). Fixes [#24322](https://github.com/vector-im/element-web/issues/24322). Contributed by [@andybalaam](https://github.com/andybalaam).
- Implementation of MSC3824 to make the client OIDC-aware ( [#8681](https://github.com/matrix-org/matrix-react-sdk/pull/8681)). Contributed by [@hughns](https://github.com/hughns).
- Improves a11y for avatar uploads ( [#9985](https://github.com/matrix-org/matrix-react-sdk/pull/9985)). Contributed by [@GoodGuyMarco](https://github.com/GoodGuyMarco).
- Add support for \[token authenticated registration\](https ( [#7275](https://github.com/matrix-org/matrix-react-sdk/pull/7275)). Fixes [#18931](https://github.com/vector-im/element-web/issues/18931). Contributed by [@govynnus](https://github.com/govynnus).

## 🐛 Bug Fixes

- Jitsi requests 'requires\_client' capability if auth token is provided ( [#24294](https://github.com/vector-im/element-web/pull/24294)). Contributed by [@maheichyk](https://github.com/maheichyk).
- Remove duplicate white space characters from translation keys ( [#10152](https://github.com/matrix-org/matrix-react-sdk/pull/10152)). Contributed by [@luixxiul](https://github.com/luixxiul).
- Fix the caption of new sessions manager on Labs settings page for localization ( [#10143](https://github.com/matrix-org/matrix-react-sdk/pull/10143)). Contributed by [@luixxiul](https://github.com/luixxiul).
- Prevent start another DM with a user if one already exists ( [#10127](https://github.com/matrix-org/matrix-react-sdk/pull/10127)). Fixes [#23138](https://github.com/vector-im/element-web/issues/23138).
- Remove white space characters before the horizontal ellipsis ( [#10130](https://github.com/matrix-org/matrix-react-sdk/pull/10130)). Contributed by [@luixxiul](https://github.com/luixxiul).
- Fix Selectable Text on 'Delete All' and 'Retry All' Buttons ( [#10128](https://github.com/matrix-org/matrix-react-sdk/pull/10128)). Fixes [#23232](https://github.com/vector-im/element-web/issues/23232). Contributed by [@akshattchhabra](https://github.com/akshattchhabra).
- Correctly Identify emoticons ( [#10108](https://github.com/matrix-org/matrix-react-sdk/pull/10108)). Fixes [#19472](https://github.com/vector-im/element-web/issues/19472). Contributed by [@adarsh-sgh](https://github.com/adarsh-sgh).
- Should open new 1:1 chat room after leaving the old one ( [#9880](https://github.com/matrix-org/matrix-react-sdk/pull/9880)). Contributed by [@ahmadkadri](https://github.com/ahmadkadri).
- Remove a redundant white space ( [#10129](https://github.com/matrix-org/matrix-react-sdk/pull/10129)). Contributed by [@luixxiul](https://github.com/luixxiul).
- Fix a crash when removing persistent widgets (updated) ( [#10099](https://github.com/matrix-org/matrix-react-sdk/pull/10099)). Fixes [#24412](https://github.com/vector-im/element-web/issues/24412). Contributed by [@andybalaam](https://github.com/andybalaam).
- Fix wrongly grouping 3pid invites into a single repeated transition ( [#10087](https://github.com/matrix-org/matrix-react-sdk/pull/10087)). Fixes [#24432](https://github.com/vector-im/element-web/issues/24432).
- Fix scrollbar colliding with checkbox in add to space section ( [#10093](https://github.com/matrix-org/matrix-react-sdk/pull/10093)). Fixes [#23189](https://github.com/vector-im/element-web/issues/23189). Contributed by [@Arnabdaz](https://github.com/Arnabdaz).
- Add a whitespace character after 'broadcast?' ( [#10097](https://github.com/matrix-org/matrix-react-sdk/pull/10097)). Contributed by [@luixxiul](https://github.com/luixxiul).
- Seekbar in broadcast PiP view is now updated when switching between different broadcasts ( [#10072](https://github.com/matrix-org/matrix-react-sdk/pull/10072)). Fixes [#24415](https://github.com/vector-im/element-web/issues/24415).
- Add border to "reject" button on room preview card for clickable area indication. It fixes [#22623](https://github.com/vector-im/element-web/issues/22623) ( [#9205](https://github.com/matrix-org/matrix-react-sdk/pull/9205)). Contributed by [@gefgu](https://github.com/gefgu).
- Element-R: fix rageshages ( [#10081](https://github.com/matrix-org/matrix-react-sdk/pull/10081)). Fixes [#24430](https://github.com/vector-im/element-web/issues/24430).
- Fix markdown paragraph display in timeline ( [#10071](https://github.com/matrix-org/matrix-react-sdk/pull/10071)). Fixes [#24419](https://github.com/vector-im/element-web/issues/24419). Contributed by [@alunturner](https://github.com/alunturner).
- Prevent the remaining broadcast time from being exceeded ( [#10070](https://github.com/matrix-org/matrix-react-sdk/pull/10070)).
- Fix cursor position when new line is created by pressing enter (RTE) ( [#10064](https://github.com/matrix-org/matrix-react-sdk/pull/10064)). Contributed by [@florianduros](https://github.com/florianduros).
- Ensure room is actually in space hierarchy when resolving its latest version ( [#10010](https://github.com/matrix-org/matrix-react-sdk/pull/10010)).
- Fix new line for inline code ( [#10062](https://github.com/matrix-org/matrix-react-sdk/pull/10062)). Contributed by [@florianduros](https://github.com/florianduros).
- Member avatars without canvas ( [#9990](https://github.com/matrix-org/matrix-react-sdk/pull/9990)). Contributed by [@clarkf](https://github.com/clarkf).
- Apply more general fix for base avatar regressions ( [#10045](https://github.com/matrix-org/matrix-react-sdk/pull/10045)). Fixes [#24382](https://github.com/vector-im/element-web/issues/24382) and [#24370](https://github.com/vector-im/element-web/issues/24370).
- Replace list, code block and quote icons by new icons ( [#10035](https://github.com/matrix-org/matrix-react-sdk/pull/10035)). Contributed by [@florianduros](https://github.com/florianduros).
- fix regional emojis converted to flags ( [#9294](https://github.com/matrix-org/matrix-react-sdk/pull/9294)). Fixes [#19000](https://github.com/vector-im/element-web/issues/19000). Contributed by [@grimhilt](https://github.com/grimhilt).
- resolved emoji description text overflowing issue ( [#10028](https://github.com/matrix-org/matrix-react-sdk/pull/10028)). Contributed by [@fahadNoufal](https://github.com/fahadNoufal).
- Fix MessageEditHistoryDialog crashing on complex input ( [#10018](https://github.com/matrix-org/matrix-react-sdk/pull/10018)). Fixes [#23665](https://github.com/vector-im/element-web/issues/23665). Contributed by [@clarkf](https://github.com/clarkf).
- Unify unread notification state determination ( [#9941](https://github.com/matrix-org/matrix-react-sdk/pull/9941)). Contributed by [@clarkf](https://github.com/clarkf).
- Fix layout and visual regressions around default avatars ( [#10031](https://github.com/matrix-org/matrix-react-sdk/pull/10031)). Fixes [#24375](https://github.com/vector-im/element-web/issues/24375) and [#24369](https://github.com/vector-im/element-web/issues/24369).
- Fix useUnreadNotifications exploding with falsey room, like in notif panel ( [#10030](https://github.com/matrix-org/matrix-react-sdk/pull/10030)). Fixes [matrix-org/element-web-rageshakes#19334](https://github.com/matrix-org/element-web-rageshakes/issues/19334).
- Fix "\[object Promise\]" appearing in HTML exports ( [#9975](https://github.com/matrix-org/matrix-react-sdk/pull/9975)). Fixes [#24272](https://github.com/vector-im/element-web/issues/24272). Contributed by [@clarkf](https://github.com/clarkf).
- changing the color of message time stamp ( [#10016](https://github.com/matrix-org/matrix-react-sdk/pull/10016)). Contributed by [@nawarajshah](https://github.com/nawarajshah).
- Fix link creation with backward selection ( [#9986](https://github.com/matrix-org/matrix-react-sdk/pull/9986)). Fixes [#24315](https://github.com/vector-im/element-web/issues/24315). Contributed by [@florianduros](https://github.com/florianduros).
- Misaligned reply preview in thread composer [#23396](https://github.com/vector-im/element-web/issues/23396) ( [#9977](https://github.com/matrix-org/matrix-react-sdk/pull/9977)). Fixes [#23396](https://github.com/vector-im/element-web/issues/23396). Contributed by [@mustafa-kapadia1483](https://github.com/mustafa-kapadia1483).

---

# dynamic-dns v3.10.0-ls109

> published at 2023-02-13 [source](https://github.com/linuxserver/docker-ddclient/releases/tag/v3.10.0-ls109)

**LinuxServer Changes:**

Rebase to Alpine 3.17, migrate to s6v3.

**ddclient Changes:**

**Note**

Please also read the changelog for v3.10.0\_2 and v3.10.0\_1 which contains dependency changes.

### New features

- Added support for domaindiscount24.com
- Added support for njal.la

---

# coturn docker/4.6.1-r2

> published at 2023-02-13 [source](https://github.com/coturn/coturn/releases/tag/docker%2F4.6.1-r2)

`4.6.1-r2` Docker image version of 4.6.1 Coturn release.

[Docker Hub](https://hub.docker.com/r/coturn/coturn) \| [GitHub Container Registry](https://github.com/orgs/coturn/packages/container/package/coturn) \| [Quay.io](https://quay.io/repository/coturn/coturn)

[Changelog](https://github.com/coturn/coturn/blob/docker/4.6.1-r2/docker/coturn/CHANGELOG.md#461-r2--2023-02-13)

---

# languagetool v6.0-dockerupdate-3: Bump alpine from 3.17.1 to 3.17.2

> published at 2023-02-13 [source](https://github.com/Erikvl87/docker-languagetool/releases/tag/v6.0-dockerupdate-3)

Bumps alpine from 3.17.1 to 3.17.2.

* * *

updated-dependencies:

- dependency-name: alpine


  dependency-type: direct:production


  update-type: version-update:semver-patch


  ...

Signed-off-by: dependabot\[bot\] [support@github.com](mailto:support@github.com)

---

# draupnir v1.80.0

> published at 2023-02-11 [source](https://github.com/Gnuxie/Draupnir/releases/tag/v1.80.0)

## Pre-release strategy:

For the foreseeable future I'm planning a series of pre-releases as I head towards v2.0.0. The reason being there are a number of breaking changes since the last pre-fork version of Mjolnir and there will be even more to come. Please join #draupnir:matrix.org if something breaks. You can find docker images [here](https://hub.docker.com/r/gnuxie/draupnir/tags). If you are using Mjolnir, there are no migration steps, just change the image being used. You will also want to add `draupnir` to your `additionalPrefixes` in your config (or remove the `additionalPrefixes` entry entirely).

## Changes of behaviour:

- The bot is now called draupnir, the code base still contains references to Mjolnir, but most of the documentation has been made consistent.

- An entirely new framework has been developed for parsing commands. Currently the commands that have changed are:


  \- The ban command has changed to accept `!draupnir ban <entity> [list] [reason]`. You will now be prompted for missing arguments such as the list, which is useful when using a phone. Default reasons to be prompted with can be configured with `commands.ban.defaultReasons`.


  \- The unban command has been changed to accept `!draupnir unban [--true] <entity> [list]`. Again, you will be prompted for the list to use. `--true` is only required if the `entity` that was unbanned is a glob.


  \- The `!mjolnir make admin` command has been renamed `!draupnir hijack room` to stop people confusing it with the `powerlevel` command.


  \- Errors experienced in commands using the new framework will be displayed to the management room with a uuid so that they can be easily discovered in the logs.

- watched policy lists are stored as matrix.to urls to room ids, any aliases will be resolved and stored as a room id. This is a different approach to what Mjolnir has taken where failing to resolve an alias to a watched list will silently fail. See [Mjolnir#404](https://github.com/matrix-org/mjolnir/issues/404)

- The matrix-bot-sdk `MatrixHttpClient` & `MatrixClientLite` modules are now silenced. This was causing a lot of alarm fatigue and red herrings when debugging logs sent to us for Mjolnir, as any http error in the bot-sdk is logged at the error level. It can be re-enabled by changing the setting `logMutedModules`.

- JSX Templates can now be used to render the responses of commands. These can be transparently split across multiple messages if they render a message that is too large (currently this is clunky but the idea is later they will go into the same thread).

- Messages that are over the size limit will be split into a thread.

- `!mjolnir status joins` has been moved to `!draupnir joins`


## Other things:

- `!draupnir status` now includes version information.
- New and old commands are shown in help, the information is still under development and is bare, but it still tells you about the arguments.
- Incorperate Mjolnir's new Policy List Manager by [@Yoric](https://github.com/Yoric) in [#5](https://github.com/Gnuxie/Draupnir/pull/5)
- Incorperate: fix 2 issues in `!config get` by [@jesopo](https://github.com/jesopo) in [#15](https://github.com/Gnuxie/Draupnir/pull/15)
- Rename the bot from Mjolnir to Draupnir in the documentation by [@Gnuxie](https://github.com/Gnuxie) in [#16](https://github.com/Gnuxie/Draupnir/pull/16)

**Full Changelog**: [`v1.6.1...v1.80.0`](https://github.com/Gnuxie/Draupnir/compare/v1.6.1...v1.80.0)

---

# dendrite Dendrite 0.11.1

> published at 2023-02-10 [source](https://github.com/matrix-org/dendrite/releases/tag/v0.11.1)

**⚠️ DEPRECATION WARNING: This is the last release to have polylith and HTTP API mode. Future releases are monolith only.**

### Features

- Dendrite can now be compiled against Go 1.20
- Initial store and forward support has been added
- A landing page showing that Dendrite is running has been added (contributed by [LukasLJL](https://github.com/LukasLJL))

### Fixes

- `/sync` is now using significantly less database round trips when using Postgres, resulting in faster initial syncs, allowing larger accounts to login again
- Many under the hood pinecone improvements
- Publishing rooms is now possible again

---

# dendrite helm-dendrite-0.11.1

> published at 2023-02-10 [source](https://github.com/matrix-org/dendrite/releases/tag/helm-dendrite-0.11.1)

Dendrite Matrix Homeserver

---

# synapse v1.77.0rc2

> published at 2023-02-10 [source](https://github.com/matrix-org/synapse/releases/tag/v1.77.0rc2)

# Synapse 1.77.0rc2 (2023-02-10)

## Bugfixes

- Fix bug where retried replication requests would return a failure. Introduced in v1.76.0. ( [#15024](https://github.com/matrix-org/synapse/issues/15024))

## Internal Changes

- Prepare for future database schema changes. ( [#15036](https://github.com/matrix-org/synapse/issues/15036))

---

# hydrogen v0.3.7

> published at 2023-02-10 [source](https://github.com/vector-im/hydrogen-web/releases/tag/v0.3.7)

- add experimental support for audio/video calls (see settings to enable)
- fix loading more messages failing in some rooms ( [#1009](https://github.com/vector-im/hydrogen-web/issues/1009) )

---

# postmoogle v0.9.12

> published at 2023-02-09 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.12)

- fix uploads from incoming emails into matrix threads
- fix emails dequeue (account data cleanup)
- rewrite recipients handling (Cc, To, etc.)

---

# appservice-irc 0.37.0 (2023-02-08)

> published at 2023-02-08 [source](https://github.com/matrix-org/matrix-appservice-irc/releases/tag/0.37.0)

## Features

- Add support for Node.JS 19. ( [#1646](https://github.com/matrix-org/matrix-appservice-irc/issues/1646))
- Refactor provisioning API to use `matrix-appservice-bridge`, adding support for OpenID token auth among other improvements. ( [#1655](https://github.com/matrix-org/matrix-appservice-irc/issues/1655))
- Add a UI in the form of a Matrix widget for linking and unlinking channels from a Matrix room. ( [#1656](https://github.com/matrix-org/matrix-appservice-irc/issues/1656))

## Bugfixes

- Document 8090 to be the default port, to match the code's behaviour. ( [#1427](https://github.com/matrix-org/matrix-appservice-irc/issues/1427))
- Fix the configured bind port being overridden in Docker. ( [#1654](https://github.com/matrix-org/matrix-appservice-irc/issues/1654))

## Internal Changes

- Update builder image for Freebind from Stretch (EOL) to Buster. ( [#1653](https://github.com/matrix-org/matrix-appservice-irc/issues/1653))

---

# draupnir v1.80.0-beta.0

> published at 2023-02-08 [source](https://github.com/Gnuxie/Draupnir/releases/tag/v1.80.0-beta.0)

## Pre-release strategy:

For the foreseeable future I'm planning a series of pre-releases as I head towards v2.0.0. The reason being there are a number of breaking changes since the last pre-fork version of Mjolnir and there will be even more to come. So don't expect smooth sailing. That being said please join #draupnir:matrix.org if something breaks. You can find docker images [here](https://hub.docker.com/r/gnuxie/draupnir/tags). If you are using Mjolnir, there are no migration steps, just change the image being used. You will also want to add `draupnir` to your `additionalPrefixes` in your config (or remove the `additionalPrefixes` entry entirely).

## Changes of behaviour:

- The bot is now called draupnir, the code base still contains references to Mjolnir, but most of the documentation has been made consistent.

- An entirely new framework has been developed for parsing commands. Currently the commands that have changed are:


  \- The ban command has changed to accept `!draupnir ban <entity> [list] [reason]`. You will now be prompted for missing arguments such as the list, which is useful when using a phone. Default reasons to be prompted with can be configured with `commands.ban.defaultReasons`.


  \- The unban command has been changed to accept `!draupnir unban [--true] <entity> [list]`. Again, you will be prompted for the list to use. `--true` is only required if the `entity` that was unbanned is a glob.


  \- The `!mjolnir make admin` command has been renamed `!draupnir hijack room` to stop people confusing it with the `powerlevel` command.


  \- Errors experienced in commands using the new framework will be displayed to the management room with a uuid so that they can be easily discovered in the logs.

- watched policy lists are stored as matrix.to urls to room ids, any aliases will be resolved and stored as a room id. This is a different approach to what Mjolnir has taken where failing to resolve an alias to a watched list will silently fail. See [Mjolnir#404](https://github.com/matrix-org/mjolnir/issues/404)

- The matrix-bot-sdk `MatrixHttpClient` & `MatrixClientLite` modules are now silenced. This was causing a lot of alarm fatigue and red herrings when debugging logs sent to us for Mjolnir, as any http error in the bot-sdk is logged at the error level. It can be re-enabled by changing the setting `logMutedModules`.

- JSX Templates can now be used to render the responses of commands. These can be transparently split across multiple messages if they render a message that is too large (currently this is clunky but the idea is later they will go into the same thread).


## Other things:

- Incorperate Mjolnir's new Policy List Manager by [@Yoric](https://github.com/Yoric) in [#5](https://github.com/Gnuxie/Draupnir/pull/5)
- Incorperate: fix 2 issues in `!config get` by [@jesopo](https://github.com/jesopo) in [#15](https://github.com/Gnuxie/Draupnir/pull/15)
- Rename the bot from Mjolnir to Draupnir in the documentation by [@Gnuxie](https://github.com/Gnuxie) in [#16](https://github.com/Gnuxie/Draupnir/pull/16)
- update CONTRIBUTING. by [@Gnuxie](https://github.com/Gnuxie) in [#17](https://github.com/Gnuxie/Draupnir/pull/17)

**Full Changelog**: [https://github.com/Gnuxie/Draupnir/commits/v1.80.0-beta.0](https://github.com/Gnuxie/Draupnir/commits/v1.80.0-beta.0)

---

# chatgpt v2.2.1

> published at 2023-02-08 [source](https://github.com/matrixgpt/matrix-chatgpt-bot/releases/tag/v2.2.1)

## What's Changed

- Hotfix disabled encryption.
- Bump @waylaidwanderer/chatgpt-api from 1.10.1 to 1.10.5 by [@dependabot](https://github.com/dependabot) in [#94](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/94)
- Bump zod from 3.20.2 to 3.20.3 by [@dependabot](https://github.com/dependabot) in [#93](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/93)

**Full Changelog**: [`v2.2.0...v2.2.1`](https://github.com/matrixgpt/matrix-chatgpt-bot/compare/v2.2.0...v2.2.1)

---

# chatgpt v2.2.0

> published at 2023-02-08 [source](https://github.com/matrixgpt/matrix-chatgpt-bot/releases/tag/v2.2.0)

## What's Changed

- Bump typescript from 4.9.4 to 4.9.5 by [@dependabot](https://github.com/dependabot) in [#74](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/74)
- Remove KEYV\_BOT\_ENCRYPTION by [@bertybuttface](https://github.com/bertybuttface) in [#85](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/85)
- Switch to waylaidwanderer by [@bertybuttface](https://github.com/bertybuttface) in [#86](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/86)
- Cleanup of npm / yarn by [@max298](https://github.com/max298) in [#89](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/89)
- ignore non-text messages by [@max298](https://github.com/max298) in [#90](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/90)
- Show errors from OpenAI by [@max298](https://github.com/max298) in [#91](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/91)

**Full Changelog**: [`v2.1.0...v2.2.0`](https://github.com/matrixgpt/matrix-chatgpt-bot/compare/v2.1.0...v2.2.0)

---

# synapse v1.77.0rc1

> published at 2023-02-08 [source](https://github.com/matrix-org/synapse/releases/tag/v1.77.0rc1)

# Synapse 1.77.0rc1 (2023-02-07)

## Features

- Experimental support for [MSC3952](https://github.com/matrix-org/matrix-spec-proposals/pull/3952): intentional mentions. ( [#14823](https://github.com/matrix-org/synapse/issues/14823), [#14943](https://github.com/matrix-org/synapse/issues/14943), [#14957](https://github.com/matrix-org/synapse/issues/14957), [#14958](https://github.com/matrix-org/synapse/issues/14958))
- Experimental support to suppress notifications from message edits ( [MSC3958](https://github.com/matrix-org/matrix-spec-proposals/pull/3958)). ( [#14960](https://github.com/matrix-org/synapse/issues/14960), [#15016](https://github.com/matrix-org/synapse/issues/15016))
- Add profile information, devices and connections to the command line [user data export tool](https://matrix-org.github.io/synapse/v1.77/usage/administration/admin_faq.html#how-can-i-export-user-data). ( [#14894](https://github.com/matrix-org/synapse/issues/14894))
- Improve performance when joining or sending an event in large rooms. ( [#14962](https://github.com/matrix-org/synapse/issues/14962))
- Improve performance of joining and leaving large rooms with many local users. ( [#14971](https://github.com/matrix-org/synapse/issues/14971))

## Bugfixes

- Fix a bug introduced in Synapse 1.53.0 where `next_batch` tokens from `/sync` could not be used with the `/relations` endpoint. ( [#14866](https://github.com/matrix-org/synapse/issues/14866))
- Fix a bug introduced in Synapse 1.35.0 where the module API's `send_local_online_presence_to` would fail to send presence updates over federation. ( [#14880](https://github.com/matrix-org/synapse/issues/14880))
- Fix a bug introduced in Synapse 1.70.0 where the background updates to add non-thread unique indexes on receipts could fail when upgrading from 1.67.0 or earlier. ( [#14915](https://github.com/matrix-org/synapse/issues/14915))
- Fix a regression introduced in Synapse 1.69.0 which can result in database corruption when database migrations are interrupted on sqlite. ( [#14926](https://github.com/matrix-org/synapse/issues/14926))
- Fix a bug introduced in Synapse 1.68.0 where we were unable to service remote joins in rooms with `@room` notification levels set to `null` in their (malformed) power levels. ( [#14942](https://github.com/matrix-org/synapse/issues/14942))
- Fix a bug introduced in Synapse 1.64.0 where boolean power levels were erroneously permitted in [v10 rooms](https://spec.matrix.org/v1.5/rooms/v10/). ( [#14944](https://github.com/matrix-org/synapse/issues/14944))
- Fix a long-standing bug where sending messages on servers with presence enabled would spam "Re-starting finished log context" log lines. ( [#14947](https://github.com/matrix-org/synapse/issues/14947))
- Fix a bug introduced in Synapse 1.68.0 where logging from the Rust module was not properly logged. ( [#14976](https://github.com/matrix-org/synapse/issues/14976))
- Fix various long-standing bugs in Synapse's config, event and request handling where booleans were unintentionally accepted where an integer was expected. ( [#14945](https://github.com/matrix-org/synapse/issues/14945))

## Internal Changes

- Add missing type hints. ( [#14879](https://github.com/matrix-org/synapse/issues/14879), [#14886](https://github.com/matrix-org/synapse/issues/14886), [#14887](https://github.com/matrix-org/synapse/issues/14887), [#14904](https://github.com/matrix-org/synapse/issues/14904), [#14927](https://github.com/matrix-org/synapse/issues/14927), [#14956](https://github.com/matrix-org/synapse/issues/14956), [#14983](https://github.com/matrix-org/synapse/issues/14983), [#14984](https://github.com/matrix-org/synapse/issues/14984), [#14985](https://github.com/matrix-org/synapse/issues/14985), [#14987](https://github.com/matrix-org/synapse/issues/14987), [#14988](https://github.com/matrix-org/synapse/issues/14988), [#14990](https://github.com/matrix-org/synapse/issues/14990), [#14991](https://github.com/matrix-org/synapse/issues/14991), [#14992](https://github.com/matrix-org/synapse/issues/14992), [#15007](https://github.com/matrix-org/synapse/issues/15007))
- Use `StrCollection` to avoid potential bugs with `Collection[str]`. ( [#14922](https://github.com/matrix-org/synapse/issues/14922))
- Allow running the complement tests suites with the asyncio reactor enabled. ( [#14858](https://github.com/matrix-org/synapse/issues/14858))
- Improve performance of `/sync` in a few situations. ( [#14908](https://github.com/matrix-org/synapse/issues/14908), [#14970](https://github.com/matrix-org/synapse/issues/14970))
- Document how to handle Dependabot pull requests. ( [#14916](https://github.com/matrix-org/synapse/issues/14916))
- Fix typo in release script. ( [#14920](https://github.com/matrix-org/synapse/issues/14920))
- Update build system requirements to allow building with poetry-core 1.5.0. ( [#14949](https://github.com/matrix-org/synapse/issues/14949), [#15019](https://github.com/matrix-org/synapse/issues/15019))
- Add an [lnav](https://lnav.org) config file for Synapse logs to `/contrib/lnav`. ( [#14953](https://github.com/matrix-org/synapse/issues/14953))
- Faster joins: Refactor internal handling of servers in room to never store an empty list. ( [#14954](https://github.com/matrix-org/synapse/issues/14954))
- Faster joins: tag `v2/send_join/` requests to indicate if they served a partial join response. ( [#14950](https://github.com/matrix-org/synapse/issues/14950))
- Allow running `cargo` without the `extension-module` option. ( [#14965](https://github.com/matrix-org/synapse/issues/14965))
- Preparatory work for adding a denormalised event stream ordering column in the future. Contributed by Nick @ Beeper ( [@Fizzadar](https://github.com/Fizzadar)). ( [#14979](https://github.com/matrix-org/synapse/issues/14979), [9cd7610](https://github.com/matrix-org/synapse/commit/9cd7610f86ab5051c9365dd38d1eec405a5f8ca6), [f10caa7](https://github.com/matrix-org/synapse/commit/f10caa73eee0caa91cf373966104d1ededae2aee); see [#15014](https://github.com/matrix-org/synapse/issues/15014))
- Add tests for `_flatten_dict`. ( [#14981](https://github.com/matrix-org/synapse/issues/14981), [#15002](https://github.com/matrix-org/synapse/issues/15002))

Dependabot updates

- Bump dtolnay/rust-toolchain from e645b0cf01249a964ec099494d38d2da0f0b349f to 9cd00a88a73addc8617065438eff914dd08d0955. ( [#14968](https://github.com/matrix-org/synapse/issues/14968))
- Bump docker/build-push-action from 3 to 4. ( [#14952](https://github.com/matrix-org/synapse/issues/14952))
- Bump ijson from 3.1.4 to 3.2.0.post0. ( [#14935](https://github.com/matrix-org/synapse/issues/14935))
- Bump types-pyyaml from 6.0.12.2 to 6.0.12.3. ( [#14936](https://github.com/matrix-org/synapse/issues/14936))
- Bump types-jsonschema from 4.17.0.2 to 4.17.0.3. ( [#14937](https://github.com/matrix-org/synapse/issues/14937))
- Bump types-pillow from 9.4.0.3 to 9.4.0.5. ( [#14938](https://github.com/matrix-org/synapse/issues/14938))
- Bump hiredis from 2.0.0 to 2.1.1. ( [#14939](https://github.com/matrix-org/synapse/issues/14939))
- Bump hiredis from 2.1.1 to 2.2.1. ( [#14993](https://github.com/matrix-org/synapse/issues/14993))
- Bump types-setuptools from 65.6.0.3 to 67.1.0.0. ( [#14994](https://github.com/matrix-org/synapse/issues/14994))
- Bump prometheus-client from 0.15.0 to 0.16.0. ( [#14995](https://github.com/matrix-org/synapse/issues/14995))
- Bump anyhow from 1.0.68 to 1.0.69. ( [#14996](https://github.com/matrix-org/synapse/issues/14996))
- Bump serde\_json from 1.0.91 to 1.0.92. ( [#14997](https://github.com/matrix-org/synapse/issues/14997))
- Bump isort from 5.11.4 to 5.11.5. ( [#14998](https://github.com/matrix-org/synapse/issues/14998))
- Bump phonenumbers from 8.13.4 to 8.13.5. ( [#14999](https://github.com/matrix-org/synapse/issues/14999))

---

# element v1.11.23-rc.1

> published at 2023-02-07 [source](https://github.com/vector-im/element-web/releases/tag/v1.11.23-rc.1)

## ✨ Features

- Description of QR code sign in labs feature ( [#23513](https://github.com/vector-im/element-web/pull/23513)). Contributed by [@hughns](https://github.com/hughns).
- Indicate unread messages in tab title ( [#10096](https://github.com/matrix-org/matrix-react-sdk/pull/10096)). Contributed by [@tnt7864](https://github.com/tnt7864).
- Open message in editing mode when keyboard up is pressed (RTE) ( [#10079](https://github.com/matrix-org/matrix-react-sdk/pull/10079)). Contributed by [@florianduros](https://github.com/florianduros).
- Hide superseded rooms from the room list using dynamic room predecessors ( [#10068](https://github.com/matrix-org/matrix-react-sdk/pull/10068)). Contributed by [@andybalaam](https://github.com/andybalaam).
- Support MSC3946 in RoomListStore ( [#10054](https://github.com/matrix-org/matrix-react-sdk/pull/10054)). Fixes [#24325](https://github.com/vector-im/element-web/issues/24325). Contributed by [@andybalaam](https://github.com/andybalaam).
- Auto focus security key field ( [#10048](https://github.com/matrix-org/matrix-react-sdk/pull/10048)).
- use Poll model with relations API in poll rendering ( [#9877](https://github.com/matrix-org/matrix-react-sdk/pull/9877)). Contributed by [@kerryarchibald](https://github.com/kerryarchibald).
- Support MSC3946 in the RoomCreate tile ( [#10041](https://github.com/matrix-org/matrix-react-sdk/pull/10041)). Fixes [#24323](https://github.com/vector-im/element-web/issues/24323). Contributed by [@andybalaam](https://github.com/andybalaam).
- Update labs flag description for RTE ( [#10058](https://github.com/matrix-org/matrix-react-sdk/pull/10058)). Contributed by [@florianduros](https://github.com/florianduros).
- Change ul list style to disc when editing message ( [#10043](https://github.com/matrix-org/matrix-react-sdk/pull/10043)). Contributed by [@alunturner](https://github.com/alunturner).
- Improved click detection within PiP windows ( [#10040](https://github.com/matrix-org/matrix-react-sdk/pull/10040)). Fixes [#24371](https://github.com/vector-im/element-web/issues/24371).
- Add RTE keyboard navigation in editing ( [#9980](https://github.com/matrix-org/matrix-react-sdk/pull/9980)). Fixes [#23621](https://github.com/vector-im/element-web/issues/23621). Contributed by [@florianduros](https://github.com/florianduros).
- Paragraph integration for rich text editor ( [#10008](https://github.com/matrix-org/matrix-react-sdk/pull/10008)). Contributed by [@alunturner](https://github.com/alunturner).
- Add indentation increasing/decreasing to RTE ( [#10034](https://github.com/matrix-org/matrix-react-sdk/pull/10034)). Contributed by [@florianduros](https://github.com/florianduros).
- Add ignore user confirmation dialog ( [#6116](https://github.com/matrix-org/matrix-react-sdk/pull/6116)). Fixes [#14746](https://github.com/vector-im/element-web/issues/14746).
- Use monospace font for room, message IDs in View Source modal ( [#9956](https://github.com/matrix-org/matrix-react-sdk/pull/9956)). Fixes [#21937](https://github.com/vector-im/element-web/issues/21937). Contributed by [@paragpoddar](https://github.com/paragpoddar).
- Implement MSC3946 for AdvancedRoomSettingsTab ( [#9995](https://github.com/matrix-org/matrix-react-sdk/pull/9995)). Fixes [#24322](https://github.com/vector-im/element-web/issues/24322). Contributed by [@andybalaam](https://github.com/andybalaam).
- Implementation of MSC3824 to make the client OIDC-aware ( [#8681](https://github.com/matrix-org/matrix-react-sdk/pull/8681)). Contributed by [@hughns](https://github.com/hughns).
- Improves a11y for avatar uploads ( [#9985](https://github.com/matrix-org/matrix-react-sdk/pull/9985)). Contributed by [@GoodGuyMarco](https://github.com/GoodGuyMarco).
- Add support for \[token authenticated registration\](https ( [#7275](https://github.com/matrix-org/matrix-react-sdk/pull/7275)). Fixes [#18931](https://github.com/vector-im/element-web/issues/18931). Contributed by [@govynnus](https://github.com/govynnus).

## 🐛 Bug Fixes

- Jitsi requests 'requires\_client' capability if auth token is provided ( [#24294](https://github.com/vector-im/element-web/pull/24294)). Contributed by [@maheichyk](https://github.com/maheichyk).
- Fix a crash when removing persistent widgets (updated) ( [#10099](https://github.com/matrix-org/matrix-react-sdk/pull/10099)). Fixes [#24412](https://github.com/vector-im/element-web/issues/24412). Contributed by [@andybalaam](https://github.com/andybalaam).
- Fix wrongly grouping 3pid invites into a single repeated transition ( [#10087](https://github.com/matrix-org/matrix-react-sdk/pull/10087)). Fixes [#24432](https://github.com/vector-im/element-web/issues/24432).
- Fix scrollbar colliding with checkbox in add to space section ( [#10093](https://github.com/matrix-org/matrix-react-sdk/pull/10093)). Fixes [#23189](https://github.com/vector-im/element-web/issues/23189). Contributed by [@Arnabdaz](https://github.com/Arnabdaz).
- Add a whitespace character after 'broadcast?' ( [#10097](https://github.com/matrix-org/matrix-react-sdk/pull/10097)). Contributed by [@luixxiul](https://github.com/luixxiul).
- Seekbar in broadcast PiP view is now updated when switching between different broadcasts ( [#10072](https://github.com/matrix-org/matrix-react-sdk/pull/10072)). Fixes [#24415](https://github.com/vector-im/element-web/issues/24415).
- Add border to "reject" button on room preview card for clickable area indication. It fixes [#22623](https://github.com/vector-im/element-web/issues/22623) ( [#9205](https://github.com/matrix-org/matrix-react-sdk/pull/9205)). Contributed by [@gefgu](https://github.com/gefgu).
- Element-R: fix rageshages ( [#10081](https://github.com/matrix-org/matrix-react-sdk/pull/10081)). Fixes [#24430](https://github.com/vector-im/element-web/issues/24430).
- Fix markdown paragraph display in timeline ( [#10071](https://github.com/matrix-org/matrix-react-sdk/pull/10071)). Fixes [#24419](https://github.com/vector-im/element-web/issues/24419). Contributed by [@alunturner](https://github.com/alunturner).
- Prevent the remaining broadcast time from being exceeded ( [#10070](https://github.com/matrix-org/matrix-react-sdk/pull/10070)).
- Fix cursor position when new line is created by pressing enter (RTE) ( [#10064](https://github.com/matrix-org/matrix-react-sdk/pull/10064)). Contributed by [@florianduros](https://github.com/florianduros).
- Ensure room is actually in space hierarchy when resolving its latest version ( [#10010](https://github.com/matrix-org/matrix-react-sdk/pull/10010)).
- Fix new line for inline code ( [#10062](https://github.com/matrix-org/matrix-react-sdk/pull/10062)). Contributed by [@florianduros](https://github.com/florianduros).
- Replace list, code block and quote icons by new icons ( [#10035](https://github.com/matrix-org/matrix-react-sdk/pull/10035)). Contributed by [@florianduros](https://github.com/florianduros).
- fix regional emojis converted to flags ( [#9294](https://github.com/matrix-org/matrix-react-sdk/pull/9294)). Fixes [#19000](https://github.com/vector-im/element-web/issues/19000). Contributed by [@grimhilt](https://github.com/grimhilt).
- resolved emoji description text overflowing issue ( [#10028](https://github.com/matrix-org/matrix-react-sdk/pull/10028)). Contributed by [@fahadNoufal](https://github.com/fahadNoufal).
- Fix MessageEditHistoryDialog crashing on complex input ( [#10018](https://github.com/matrix-org/matrix-react-sdk/pull/10018)). Fixes [#23665](https://github.com/vector-im/element-web/issues/23665). Contributed by [@clarkf](https://github.com/clarkf).
- Unify unread notification state determination ( [#9941](https://github.com/matrix-org/matrix-react-sdk/pull/9941)). Contributed by [@clarkf](https://github.com/clarkf).
- Fix useUnreadNotifications exploding with falsey room, like in notif panel ( [#10030](https://github.com/matrix-org/matrix-react-sdk/pull/10030)). Fixes [matrix-org/element-web-rageshakes#19334](https://github.com/matrix-org/element-web-rageshakes/issues/19334).
- Fix "\[object Promise\]" appearing in HTML exports ( [#9975](https://github.com/matrix-org/matrix-react-sdk/pull/9975)). Fixes [#24272](https://github.com/vector-im/element-web/issues/24272). Contributed by [@clarkf](https://github.com/clarkf).
- Should open new 1:1 chat room after leaving the old one ( [#9880](https://github.com/matrix-org/matrix-react-sdk/pull/9880)). Contributed by [@ahmadkadri](https://github.com/ahmadkadri).
- changing the color of message time stamp ( [#10016](https://github.com/matrix-org/matrix-react-sdk/pull/10016)). Contributed by [@nawarajshah](https://github.com/nawarajshah).
- Fix link creation with backward selection ( [#9986](https://github.com/matrix-org/matrix-react-sdk/pull/9986)). Fixes [#24315](https://github.com/vector-im/element-web/issues/24315). Contributed by [@florianduros](https://github.com/florianduros).
- Misaligned reply preview in thread composer [#23396](https://github.com/vector-im/element-web/issues/23396) ( [#9977](https://github.com/matrix-org/matrix-react-sdk/pull/9977)). Fixes [#23396](https://github.com/vector-im/element-web/issues/23396). Contributed by [@mustafa-kapadia1483](https://github.com/mustafa-kapadia1483).

---

# heisenbridge v1.14.2

> published at 2023-02-07 [source](https://github.com/hifi/heisenbridge/releases/tag/v1.14.2)

No content.

---

# com.devture.ansible.role.postgres REL_11_19

> published at 2023-02-06 [source](https://github.com/postgres/postgres/releases/tag/REL_11_19)

Stamp 11.19.

---

# com.devture.ansible.role.postgres REL_12_14

> published at 2023-02-06 [source](https://github.com/postgres/postgres/releases/tag/REL_12_14)

Stamp 12.14.

---

# com.devture.ansible.role.postgres REL_13_10

> published at 2023-02-06 [source](https://github.com/postgres/postgres/releases/tag/REL_13_10)

Stamp 13.10.

---

# com.devture.ansible.role.postgres REL_14_7

> published at 2023-02-06 [source](https://github.com/postgres/postgres/releases/tag/REL_14_7)

Stamp 14.7.

---

# com.devture.ansible.role.postgres REL_15_2

> published at 2023-02-06 [source](https://github.com/postgres/postgres/releases/tag/REL_15_2)

Stamp 15.2.

---

# synapse-admin 0.8.7

> published at 2023-02-06 [source](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/0.8.7)

## Bugfixes

Removes fixed PUBLIC\_URL in Dockerfile to allow serving on different prefixes [#322](https://github.com/Awesome-Technologies/synapse-admin/issues/322)

---

# chatgpt v2.1.0

> published at 2023-02-04 [source](https://github.com/matrixgpt/matrix-chatgpt-bot/releases/tag/v2.1.0)

## What's Changed

- add some information about used model by [@max298](https://github.com/max298) in [#81](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/81)
- Add persistence by [@bertybuttface](https://github.com/bertybuttface) in [#83](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/83)

**Full Changelog**: [`v2.0.0...v2.1.0`](https://github.com/matrixgpt/matrix-chatgpt-bot/compare/v2.0.0...v2.1.0)

---

# chatgpt v2.0.0

> published at 2023-02-03 [source](https://github.com/matrixgpt/matrix-chatgpt-bot/releases/tag/v2.0.0)

## What's Changed

The main change here is the switch from using ChatGPT via Puppeteer to using it via the API. This is a breaking change which is why we bumped the major version number. Please read the updated README file before upgrading.

A huge thanks to the following 2 contributors:

- Update README.md by [@Helikoptermann2](https://github.com/Helikoptermann2) in [#71](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/71)
- Update chatgpt-api to use proper api by [@max298](https://github.com/max298) in [#78](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/78)

## New Contributors

- [@Helikoptermann2](https://github.com/Helikoptermann2) made their first contribution in [#71](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/71)
- [@max298](https://github.com/max298) made their first contribution in [#78](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/78)

**Full Changelog**: [`v1.4.1...v2.0.0`](https://github.com/matrixgpt/matrix-chatgpt-bot/compare/v1.4.1...v2.0.0)

---

# kuma 1.20.0-beta.0

> published at 2023-02-03 [source](https://github.com/louislam/uptime-kuma/releases/tag/1.20.0-beta.0)

### 🆕 New Features

- [#1690](https://github.com/louislam/uptime-kuma/pull/1690) Tags Manager (Thanks [@chakflying](https://github.com/chakflying))
- [#2566](https://github.com/louislam/uptime-kuma/pull/2566) GameDig monitor, now you can monitor a lot of game servers via this monitor, [check this game list here](https://github.com/gamedig/node-gamedig#games-list). (Thanks [@WhyKickAmooCow](https://github.com/WhyKickAmooCow))
- [#2541](https://github.com/louislam/uptime-kuma/pull/2541) Redis monitor (Thanks [@long2ice](https://github.com/long2ice))
- [#2328](https://github.com/louislam/uptime-kuma/pull/2328) MongoDB monitor (Thanks [@rmarops](https://github.com/rmarops))
- [#2211](https://github.com/louislam/uptime-kuma/pull/2211) More badges (See [here](https://github.com/louislam/uptime-kuma/pull/2211)) (Thanks [@JRedOW](https://github.com/JRedOW))
- [#2638](https://github.com/louislam/uptime-kuma/pull/2638) Arabic language (Thanks [@amworx](https://github.com/amworx))
- [#1892](https://github.com/louislam/uptime-kuma/pull/1892) \[Ping Monitor\] Configurable packet size (Thanks [@Computroniks](https://github.com/Computroniks))
- [#2570](https://github.com/louislam/uptime-kuma/pull/2570) \[Status Page\] Markdown support (Thanks [@Computroniks](https://github.com/Computroniks))
- \[Non-docker\] Configure environment variables via the `.env` file

### 🐛 Bug Fixes

- [#2632](https://github.com/louislam/uptime-kuma/pull/2632) Docker monitor do not respect with the default timeout and the dns cache setting (Thanks [@chakflying](https://github.com/chakflying))
- [#2525](https://github.com/louislam/uptime-kuma/pull/2525) Pending & maintenance status do not display correctly in badges (Thanks [@chakflying](https://github.com/chakflying))
- [#2083](https://github.com/louislam/uptime-kuma/pull/2083) Healthcheck issue on K8s with specific container name `uptime-kuma` (Thanks [@DevKyleS](https://github.com/DevKyleS))

### 💇‍♀️ Improvements

- [#2635](https://github.com/louislam/uptime-kuma/pull/2635) Uptime on status pages will now be capped at 100%, in case you system time is out of sync. (A common issue on


  Raspberry Pi) (Thanks [@Computroniks](https://github.com/Computroniks))
- [#2684](https://github.com/louislam/uptime-kuma/pull/2684) Save button now is easier to find on mobile (Thanks [@gitstart](https://github.com/gitstart))
- [#2647](https://github.com/louislam/uptime-kuma/pull/2647) Trim white space for `hostname` (Thanks [@chakflying](https://github.com/chakflying))

### 🦎 Translation Contributors

- [#2663](https://github.com/louislam/uptime-kuma/pull/2663) [#2670](https://github.com/louislam/uptime-kuma/pull/2670) [#2676](https://github.com/louislam/uptime-kuma/pull/2676)

  [@amworx](https://github.com/amworx) @YehowahLiu [@Buchtic](https://github.com/Buchtic) [@black23](https://github.com/black23) [@deluxghost](https://github.com/deluxghost) [@cyril59310](https://github.com/cyril59310) [@sovushik](https://github.com/sovushik) [@Genc](https://github.com/Genc) [@Saibamen](https://github.com/Saibamen) [@MrEddX](https://github.com/MrEddX) [@DimitriDR](https://github.com/DimitriDR) [@chakflying](https://github.com/chakflying) [@rubesaca](https://github.com/rubesaca) [@mrkbaji](https://github.com/mrkbaji) [@dhfhfk](https://github.com/dhfhfk)


  (Let me know if your name is missing here)

### Others

- [#2611](https://github.com/louislam/uptime-kuma/pull/2611) You can translate Uptime Kuma into your language on Weblate now (Thanks @YehowahLiu)

  [https://weblate.kuma.pet](https://weblate.kuma.pet)
- Other small changes and code refactoring:

  [@black23](https://github.com/black23) [@Metatropics](https://github.com/Metatropics) [@Buchtic](https://github.com/Buchtic) [@cyril59310](https://github.com/cyril59310) [@chakflying](https://github.com/chakflying) [@Saibamen](https://github.com/Saibamen)


  (Let me know if your name is missing here. If your pull request have been merged in this version)

---

# prometheus v0.42.0

> published at 2023-02-01 [source](https://github.com/prometheus/prometheus/releases/tag/v0.42.0)

v0.42.0

---

# prometheus 2.42.0 / 2023-01-31

> published at 2023-02-01 [source](https://github.com/prometheus/prometheus/releases/tag/v2.42.0)

This release comes with a bunch of feature coverage for native histograms and breaking changes.

If you are trying native histograms already, we recommend you remove the `wal` directory when upgrading.

Because the old WAL record for native histograms is not backward compatible in v2.42.0, this will lead to some data loss for the latest data.

Additionally, if you scrape "float histograms" or use recording rules on native histograms in v2.42.0 (which writes float histograms),

it is a one-way street since older versions do not support float histograms.

- \[CHANGE\] **breaking** TSDB: Changed WAL record format for the experimental native histograms. [#11783](https://github.com/prometheus/prometheus/pull/11783)
- \[FEATURE\] Add 'keep\_firing\_for' field to alerting rules. [#11827](https://github.com/prometheus/prometheus/pull/11827)
- \[FEATURE\] Promtool: Add support of selecting timeseries for TSDB dump. [#11872](https://github.com/prometheus/prometheus/pull/11872)
- \[ENHANCEMENT\] Agent: Native histogram support. [#11842](https://github.com/prometheus/prometheus/pull/11842)
- \[ENHANCEMENT\] Rules: Support native histograms in recording rules. [#11838](https://github.com/prometheus/prometheus/pull/11838)
- \[ENHANCEMENT\] SD: Add container ID as a meta label for pod targets for Kubernetes. [#11844](https://github.com/prometheus/prometheus/pull/11844)
- \[ENHANCEMENT\] SD: Add VM size label to azure service discovery. [#11650](https://github.com/prometheus/prometheus/pull/11650)
- \[ENHANCEMENT\] Support native histograms in federation. [#11830](https://github.com/prometheus/prometheus/pull/11830)
- \[ENHANCEMENT\] TSDB: Add gauge histogram support. [#11783](https://github.com/prometheus/prometheus/pull/11783) [#11840](https://github.com/prometheus/prometheus/pull/11840) [#11814](https://github.com/prometheus/prometheus/pull/11814)
- \[ENHANCEMENT\] TSDB/Scrape: Support FloatHistogram that represents buckets as float64 values. [#11522](https://github.com/prometheus/prometheus/pull/11522) [#11817](https://github.com/prometheus/prometheus/pull/11817) [#11716](https://github.com/prometheus/prometheus/pull/11716)
- \[ENHANCEMENT\] UI: Show individual scrape pools on /targets page. [#11142](https://github.com/prometheus/prometheus/pull/11142)

---

# ansible 7.2.0

> published at 2023-01-31 [source](https://pypi.org/project/ansible/7.2.0/)

> no description provided

---



---

# synapse v1.76.0

> published at 2023-01-31 [source](https://github.com/matrix-org/synapse/releases/tag/v1.76.0)

# Synapse 1.76.0 (2023-01-31)

The 1.76 release is the first to enable faster joins ( [MSC3706](https://github.com/matrix-org/matrix-spec-proposals/pull/3706) and [MSC3902](https://github.com/matrix-org/matrix-spec-proposals/pull/3902)) by default. Admins can opt-out: see [the upgrade notes](https://github.com/matrix-org/synapse/blob/release-v1.76/docs/upgrade.md#faster-joins-are-enabled-by-default) for more details.

The upgrade from 1.75 to 1.76 changes the account data replication streams in a backwards-incompatible manner. Server operators running a multi-worker deployment should consult [the upgrade notes](https://github.com/matrix-org/synapse/blob/release-v1.76/docs/upgrade.md#changes-to-the-account-data-replication-streams).

Those who are `poetry install` ing from source using our lockfile should ensure their poetry version is 1.3.2 or higher; [see upgrade notes](https://github.com/matrix-org/synapse/blob/release-v1.76/docs/upgrade.md#minimum-version-of-poetry-is-now-132).

## Notes on faster joins

The faster joins project sees the most benefit when joining a room with a large number of members (joined or historical). We expect it to be particularly useful for joining large public rooms like the [Matrix HQ](https://matrix.to/#/#matrix:matrix.org) or [Synapse Admins](https://matrix.to/#/#synapse:matrix.org) rooms.

After a faster join, Synapse considers that room "partially joined". In this state, you should be able to

- read incoming messages;
- see incoming state changes, e.g. room topic changes; and
- send messages, if the room is unencrypted.

Synapse has to spend more effort to complete the join in the background. Once this finishes, you will be able to

- send messages, if the room is in encrypted;
- retrieve room history from before your join, if permitted by the room settings; and
- access the full list of room members.

## Improved Documentation

- Describe the ideas and the internal machinery behind faster joins. ( [#14677](https://github.com/matrix-org/synapse/issues/14677))

# Synapse 1.76.0rc2 (2023-01-27)

## Bugfixes

- Faster joins: Fix a bug introduced in Synapse 1.69 where device list EDUs could fail to be handled after a restart when a faster join sync is in progress. ( [#14914](https://github.com/matrix-org/synapse/issues/14914))

## Internal Changes

- Faster joins: Improve performance of looking up partial-state status of rooms. ( [#14917](https://github.com/matrix-org/synapse/issues/14917))

# Synapse 1.76.0rc1 (2023-01-25)

## Features

- Update the default room version to [v10](https://spec.matrix.org/v1.5/rooms/v10/) ( [MSC 3904](https://github.com/matrix-org/matrix-spec-proposals/pull/3904)). Contributed by [@FSG-Cat](https://github.com/FSG-Cat). ( [#14111](https://github.com/matrix-org/synapse/issues/14111))
- Add a `set_displayname()` method to the module API for setting a user's display name. ( [#14629](https://github.com/matrix-org/synapse/issues/14629))
- Add a dedicated listener configuration for `health` endpoint. ( [#14747](https://github.com/matrix-org/synapse/issues/14747))
- Implement support for [MSC3890](https://github.com/matrix-org/matrix-spec-proposals/pull/3890): Remotely silence local notifications. ( [#14775](https://github.com/matrix-org/synapse/issues/14775))
- Implement experimental support for [MSC3930](https://github.com/matrix-org/matrix-spec-proposals/pull/3930): Push rules for ( [MSC3381](https://github.com/matrix-org/matrix-spec-proposals/pull/3381)) Polls. ( [#14787](https://github.com/matrix-org/synapse/issues/14787))
- Per [MSC3925](https://github.com/matrix-org/matrix-spec-proposals/pull/3925), bundle the whole of the replacement with any edited events, and optionally inhibit server-side replacement. ( [#14811](https://github.com/matrix-org/synapse/issues/14811))
- Faster joins: always serve a partial join response to servers that request it with the stable query param. ( [#14839](https://github.com/matrix-org/synapse/issues/14839))
- Faster joins: allow non-lazy-loading ("eager") syncs to complete after a partial join by omitting partial state rooms until they become fully stated. ( [#14870](https://github.com/matrix-org/synapse/issues/14870))
- Faster joins: request partial joins by default. Admins can opt-out of this for the time being---see the upgrade notes. ( [#14905](https://github.com/matrix-org/synapse/issues/14905))

## Bugfixes

- Add index to improve performance of the `/timestamp_to_event` endpoint used for jumping to a specific date in the timeline of a room. ( [#14799](https://github.com/matrix-org/synapse/issues/14799))
- Fix a long-standing bug where Synapse would exhaust the stack when processing many federation requests where the remote homeserver has disconencted early. ( [#14812](https://github.com/matrix-org/synapse/issues/14812), [#14842](https://github.com/matrix-org/synapse/issues/14842))
- Fix rare races when using workers. ( [#14820](https://github.com/matrix-org/synapse/issues/14820))
- Fix a bug introduced in Synapse 1.64.0 when using room version 10 with frozen events enabled. ( [#14864](https://github.com/matrix-org/synapse/issues/14864))
- Fix a long-standing bug where the `populate_room_stats` background job could fail on broken rooms. ( [#14873](https://github.com/matrix-org/synapse/issues/14873))
- Faster joins: Fix a bug in worker deployments where the room stats and user directory would not get updated when finishing a fast join until another event is sent or received. ( [#14874](https://github.com/matrix-org/synapse/issues/14874))
- Faster joins: Fix incompatibility with joins into restricted rooms where no local users have the ability to invite. ( [#14882](https://github.com/matrix-org/synapse/issues/14882))
- Fix a regression introduced in Synapse 1.69.0 which can result in database corruption when database migrations are interrupted on sqlite. ( [#14910](https://github.com/matrix-org/synapse/issues/14910))

## Updates to the Docker image

- Bump default Python version in the Dockerfile from 3.9 to 3.11. ( [#14875](https://github.com/matrix-org/synapse/issues/14875))

## Improved Documentation

- Include `x_forwarded` entry in the HTTP listener example configs and remove the remaining `worker_main_http_uri` entries. ( [#14667](https://github.com/matrix-org/synapse/issues/14667))
- Remove duplicate commands from the Code Style documentation page; point to the Contributing Guide instead. ( [#14773](https://github.com/matrix-org/synapse/issues/14773))
- Add missing documentation for `tag` to `listeners` section. ( [#14803](https://github.com/matrix-org/synapse/issues/14803))
- Updated documentation in configuration manual for `user_directory.search_all_users`. ( [#14818](https://github.com/matrix-org/synapse/issues/14818))
- Add `worker_manhole` to configuration manual. ( [#14824](https://github.com/matrix-org/synapse/issues/14824))
- Fix the example config missing the `id` field in [application service documentation](https://matrix-org.github.io/synapse/latest/application_services.html). ( [#14845](https://github.com/matrix-org/synapse/issues/14845))
- Minor corrections to the logging configuration documentation. ( [#14868](https://github.com/matrix-org/synapse/issues/14868))
- Document the export user data command. Contributed by [@thezaidbintariq](https://github.com/thezaidbintariq). ( [#14883](https://github.com/matrix-org/synapse/issues/14883))

## Deprecations and Removals

- Poetry 1.3.2 or higher is now required when `poetry install` ing from source. ( [#14860](https://github.com/matrix-org/synapse/issues/14860))

## Internal Changes

- Faster remote room joins (worker mode): do not populate external hosts-in-room cache when sending events as this requires blocking for full state. ( [#14749](https://github.com/matrix-org/synapse/issues/14749))
- Enable Complement tests for Faster Remote Room Joins against worker-mode Synapse. ( [#14752](https://github.com/matrix-org/synapse/issues/14752))
- Add some clarifying comments and refactor a portion of the `Keyring` class for readability. ( [#14804](https://github.com/matrix-org/synapse/issues/14804))
- Add local poetry config files ( `poetry.toml`) to `.gitignore`. ( [#14807](https://github.com/matrix-org/synapse/issues/14807))
- Add missing type hints. ( [#14816](https://github.com/matrix-org/synapse/issues/14816), [#14885](https://github.com/matrix-org/synapse/issues/14885), [#14889](https://github.com/matrix-org/synapse/issues/14889))
- Refactor push tests. ( [#14819](https://github.com/matrix-org/synapse/issues/14819))
- Re-enable some linting that was disabled when we switched to ruff. ( [#14821](https://github.com/matrix-org/synapse/issues/14821))
- Add `cargo fmt` and `cargo clippy` to the lint script. ( [#14822](https://github.com/matrix-org/synapse/issues/14822))
- Drop unused table `presence`. ( [#14825](https://github.com/matrix-org/synapse/issues/14825))
- Merge the two account data and the two device list replication streams. ( [#14826](https://github.com/matrix-org/synapse/issues/14826), [#14833](https://github.com/matrix-org/synapse/issues/14833))
- Faster joins: use stable identifiers from [MSC3706](https://github.com/matrix-org/matrix-spec-proposals/pull/3706). ( [#14832](https://github.com/matrix-org/synapse/issues/14832), [#14841](https://github.com/matrix-org/synapse/issues/14841))
- Add a parameter to control whether the federation client performs a partial state join. ( [#14843](https://github.com/matrix-org/synapse/issues/14843))
- Add check to avoid starting duplicate partial state syncs. ( [#14844](https://github.com/matrix-org/synapse/issues/14844))
- Add an early return when handling no-op presence updates. ( [#14855](https://github.com/matrix-org/synapse/issues/14855))
- Fix `wait_for_stream_position` to correctly wait for the right instance to advance its token. ( [#14856](https://github.com/matrix-org/synapse/issues/14856), [#14872](https://github.com/matrix-org/synapse/issues/14872))
- Always notify replication when a stream advances automatically. ( [#14877](https://github.com/matrix-org/synapse/issues/14877))
- Reduce max time we wait for stream positions. ( [#14881](https://github.com/matrix-org/synapse/issues/14881))
- Faster joins: allow the resync process more time to fetch `/state` ids. ( [#14912](https://github.com/matrix-org/synapse/issues/14912))
- Bump regex from 1.7.0 to 1.7.1. ( [#14848](https://github.com/matrix-org/synapse/issues/14848))
- Bump peaceiris/actions-gh-pages from 3.9.1 to 3.9.2. ( [#14861](https://github.com/matrix-org/synapse/issues/14861))
- Bump ruff from 0.0.215 to 0.0.224. ( [#14862](https://github.com/matrix-org/synapse/issues/14862))
- Bump types-pillow from 9.4.0.0 to 9.4.0.3. ( [#14863](https://github.com/matrix-org/synapse/issues/14863))
- Bump types-opentracing from 2.4.10 to 2.4.10.1. ( [#14896](https://github.com/matrix-org/synapse/issues/14896))
- Bump ruff from 0.0.224 to 0.0.230. ( [#14897](https://github.com/matrix-org/synapse/issues/14897))
- Bump types-requests from 2.28.11.7 to 2.28.11.8. ( [#14899](https://github.com/matrix-org/synapse/issues/14899))
- Bump types-psycopg2 from 2.9.21.2 to 2.9.21.4. ( [#14900](https://github.com/matrix-org/synapse/issues/14900))
- Bump types-commonmark from 0.9.2 to 0.9.2.1. ( [#14901](https://github.com/matrix-org/synapse/issues/14901))

---

# coturn docker/4.6.1-r1

> published at 2023-01-31 [source](https://github.com/coturn/coturn/releases/tag/docker%2F4.6.1-r1)

`4.6.1-r1` Docker image version of 4.6.1 Coturn release.

[Docker Hub](https://hub.docker.com/r/coturn/coturn) \| [GitHub Container Registry](https://github.com/orgs/coturn/packages/container/package/coturn) \| [Quay.io](https://quay.io/repository/coturn/coturn)

[Changelog](https://github.com/coturn/coturn/blob/docker/4.6.1-r1/docker/coturn/CHANGELOG.md#461-r1--2023-01-31)

---

# grafana 9.4.0-beta1 (2023-01-30)

> published at 2023-01-31 [source](https://github.com/grafana/grafana/releases/tag/v9.4.0-beta1)

[Download page](https://grafana.com/grafana/download/9.4.0-beta1)

[What's new highlights](https://grafana.com/docs/grafana/latest/whatsnew/)

### Features and enhancements

- **API:** Change how Cache-Control and related headers are set. [#62021](https://github.com/grafana/grafana/pull/62021), [@kylebrandt](https://github.com/kylebrandt)
- **AccessControl:** Add high availability support to access control seeder. (Enterprise)
- **Accessibility:** Make QueryEditorHelp examples keyboard interactive. [#59355](https://github.com/grafana/grafana/pull/59355), [@idastambuk](https://github.com/idastambuk)
- **Admin:** Combine org and admin user pages. [#59365](https://github.com/grafana/grafana/pull/59365), [@Clarity-89](https://github.com/Clarity-89)
- **Admin:** Remove navigation subheaders. [#61344](https://github.com/grafana/grafana/pull/61344), [@Clarity-89](https://github.com/Clarity-89)
- **AlertGroups:** Generate models.gen.ts from models.cue. [#61227](https://github.com/grafana/grafana/pull/61227), [@Clarity-89](https://github.com/Clarity-89)
- **Alerting:** Access query details of provisioned alerts. [#59626](https://github.com/grafana/grafana/pull/59626), [@konrad147](https://github.com/konrad147)
- **Alerting:** Add alert rule cloning action. [#59200](https://github.com/grafana/grafana/pull/59200), [@konrad147](https://github.com/konrad147)
- **Alerting:** Add dashboard and panel picker to the rule form. [#58304](https://github.com/grafana/grafana/pull/58304), [@konrad147](https://github.com/konrad147)
- **Alerting:** Add discord as a possible receiver in cloud rules. [#59366](https://github.com/grafana/grafana/pull/59366), [@soniaAguilarPeiron](https://github.com/soniaAguilarPeiron)
- **Alerting:** Add export button for exporting all alert rules in alert list view. [#62416](https://github.com/grafana/grafana/pull/62416), [@soniaAguilarPeiron](https://github.com/soniaAguilarPeiron)
- **Alerting:** Add header X-Grafana-Org-Id to evaluation requests. [#58972](https://github.com/grafana/grafana/pull/58972), [@yuri-tceretian](https://github.com/yuri-tceretian)
- **Alerting:** Add landing page. [#59050](https://github.com/grafana/grafana/pull/59050), [@konrad147](https://github.com/konrad147)
- **Alerting:** Add maxdatapoints in alert rule form. [#61904](https://github.com/grafana/grafana/pull/61904), [@soniaAguilarPeiron](https://github.com/soniaAguilarPeiron)
- **Alerting:** Add provisioning endpoint to fetch all rules. [#59989](https://github.com/grafana/grafana/pull/59989), [@alexweav](https://github.com/alexweav)
- **Alerting:** Add support for settings parse\_mode and disable\_notifications to Telegram receiver. [#60198](https://github.com/grafana/grafana/pull/60198), [@yuri-tceretian](https://github.com/yuri-tceretian)
- **Alerting:** Add support for tracing to alerting scheduler. [#61057](https://github.com/grafana/grafana/pull/61057), [@yuri-tceretian](https://github.com/yuri-tceretian)
- **Alerting:** Adds evaluation interval to group view. [#59974](https://github.com/grafana/grafana/pull/59974), [@gillesdemey](https://github.com/gillesdemey)
- **Alerting:** Alert rules search improvements. [#61398](https://github.com/grafana/grafana/pull/61398), [@konrad147](https://github.com/konrad147)
- **Alerting:** Allow state history to be disabled through configuration. [#61006](https://github.com/grafana/grafana/pull/61006), [@alexweav](https://github.com/alexweav)
- **Alerting:** Bump Prometheus Alertmanager to v0.25. [#60764](https://github.com/grafana/grafana/pull/60764), [@santihernandezc](https://github.com/santihernandezc)
- **Alerting:** Choose a previous valid AM configuration in case of error. [#58472](https://github.com/grafana/grafana/pull/58472), [@santihernandezc](https://github.com/santihernandezc)
- **Alerting:** Create endpoints for exporting in provisioning file format. [#58623](https://github.com/grafana/grafana/pull/58623), [@JacobsonMT](https://github.com/JacobsonMT)
- **Alerting:** Declare incident from a firing alert. [#61178](https://github.com/grafana/grafana/pull/61178), [@gillesdemey](https://github.com/gillesdemey)
- **Alerting:** Do not maintain Normal state. [#56336](https://github.com/grafana/grafana/pull/56336), [@yuri-tceretian](https://github.com/yuri-tceretian)
- **Alerting:** Improve UI for making more clear that evaluation interval belongs to the group. [#56397](https://github.com/grafana/grafana/pull/56397), [@soniaAguilarPeiron](https://github.com/soniaAguilarPeiron)
- **Alerting:** Improve legacy migration to include send reminder & frequency. [#60275](https://github.com/grafana/grafana/pull/60275), [@JacobsonMT](https://github.com/JacobsonMT)
- **Alerting:** PagerDuty receiver to let user configure fields Source, Client and Client URL. [#59895](https://github.com/grafana/grafana/pull/59895), [@yuri-tceretian](https://github.com/yuri-tceretian)
- **Alerting:** Recognise & change UI for OnCall notification policy + contact point. [#60259](https://github.com/grafana/grafana/pull/60259), [@soniaAguilarPeiron](https://github.com/soniaAguilarPeiron)
- **Alerting:** Rename contact point type to receiver in the user interface. [#59589](https://github.com/grafana/grafana/pull/59589), [@grobinson-grafana](https://github.com/grobinson-grafana)
- **Alerting:** Rule evaluator to get cached data source info. [#61305](https://github.com/grafana/grafana/pull/61305), [@yuri-tceretian](https://github.com/yuri-tceretian)
- **Alerting:** Support customizable timeout for screenshots. [#60981](https://github.com/grafana/grafana/pull/60981), [@grobinson-grafana](https://github.com/grobinson-grafana)
- **Alerting:** UI changes required to support v3 and Auth in Kafka Contact Point. [#61123](https://github.com/grafana/grafana/pull/61123), [@MohammadGhazanfar](https://github.com/MohammadGhazanfar)
- **Alerting:** Update Alerting and Alertmanager to v0.25.1. [#61233](https://github.com/grafana/grafana/pull/61233), [@grobinson-grafana](https://github.com/grobinson-grafana)
- **Alerting:** Upload images to Slack via files.upload. [#59163](https://github.com/grafana/grafana/pull/59163), [@grobinson-grafana](https://github.com/grobinson-grafana)
- **Auditing/Usage Insights:** Loki support for multi-tenancy. (Enterprise)
- **Auth forwarding:** Pass tokens without refresh. [#61634](https://github.com/grafana/grafana/pull/61634), [@Jguer](https://github.com/Jguer)
- **Auth:** Add expiry date for service accounts access tokens. [#58885](https://github.com/grafana/grafana/pull/58885), [@linoman](https://github.com/linoman)
- **Auth:** Add plugin roles to RolePicker. [#59667](https://github.com/grafana/grafana/pull/59667), [@linoman](https://github.com/linoman)
- **Auth:** Add skip\_org\_role\_sync for AzureAD OAuth. [#60322](https://github.com/grafana/grafana/pull/60322), [@eleijonmarck](https://github.com/eleijonmarck)
- **Auth:** Add skip\_org\_role\_sync for Okta. [#62106](https://github.com/grafana/grafana/pull/62106), [@eleijonmarck](https://github.com/eleijonmarck)
- **Auth:** Add skip\_org\_role\_sync setting for GrafanaCom. [#60553](https://github.com/grafana/grafana/pull/60553), [@eleijonmarck](https://github.com/eleijonmarck)
- **Auth:** Add skip\_org\_role\_sync setting for github. [#61673](https://github.com/grafana/grafana/pull/61673), [@eleijonmarck](https://github.com/eleijonmarck)
- **Auth:** Add skip\_org\_role\_sync setting to OAuth integration Google. [#61572](https://github.com/grafana/grafana/pull/61572), [@eleijonmarck](https://github.com/eleijonmarck)
- **Auth:** Add skip\_org\_role\_sync to GitLab OAuth. [#62055](https://github.com/grafana/grafana/pull/62055), [@eleijonmarck](https://github.com/eleijonmarck)
- **Auth:** Add sub claim check to JWT Auth pre-checks. [#61417](https://github.com/grafana/grafana/pull/61417), [@mgyongyosi](https://github.com/mgyongyosi)
- **Auth:** Disable team sync for JWT Authentication. [#62191](https://github.com/grafana/grafana/pull/62191), [@eleijonmarck](https://github.com/eleijonmarck)
- **Auth:** Display id Provider label in orgs/users view. [#58033](https://github.com/grafana/grafana/pull/58033), [@linoman](https://github.com/linoman)
- **Auth:** Implement skip org role sync for jwt. [#61647](https://github.com/grafana/grafana/pull/61647), [@linoman](https://github.com/linoman)
- **Auth:** Log a more useful msg if no OAuth provider configured. [#56722](https://github.com/grafana/grafana/pull/56722), [@someone-stole-my-name](https://github.com/someone-stole-my-name)
- **Auth:** Set service account access token limit on expiry dates. [#56467](https://github.com/grafana/grafana/pull/56467), [@linoman](https://github.com/linoman)
- **Azure Monitor:** Add variable function to list regions. [#62297](https://github.com/grafana/grafana/pull/62297), [@andresmgot](https://github.com/andresmgot)
- **Backend:** Consistently use context RemoteAddr function to determine remote address. [#60201](https://github.com/grafana/grafana/pull/60201), [@DanCech](https://github.com/DanCech)
- **BarChart:** Highlight bars option for easier interaction. [#60530](https://github.com/grafana/grafana/pull/60530), [@mdvictor](https://github.com/mdvictor)
- **BarChartPanel:** Custom tooltips. [#60148](https://github.com/grafana/grafana/pull/60148), [@mdvictor](https://github.com/mdvictor)
- **Canvas:** Add server element. [#61104](https://github.com/grafana/grafana/pull/61104), [@drew08t](https://github.com/drew08t)
- **Canvas:** Add support for basic arrows. [#57561](https://github.com/grafana/grafana/pull/57561), [@nmarrs](https://github.com/nmarrs)
- **Canvas:** Add tooltip for data links. [#61648](https://github.com/grafana/grafana/pull/61648), [@adela-almasan](https://github.com/adela-almasan)
- **Canvas:** Improve placement when adding an element via context menu. [#61071](https://github.com/grafana/grafana/pull/61071), [@adela-almasan](https://github.com/adela-almasan)
- **Canvas:** Update context menu actions for multiple elements selected. [#61108](https://github.com/grafana/grafana/pull/61108), [@adela-almasan](https://github.com/adela-almasan)
- **Canvas:** Update element(s) selection after action. [#61204](https://github.com/grafana/grafana/pull/61204), [@adela-almasan](https://github.com/adela-almasan)
- **Chore:** Add deprecation warnings for Sentry. [#60165](https://github.com/grafana/grafana/pull/60165), [@domasx2](https://github.com/domasx2)
- **CloudMonitor:** Improve detail of MQL series labels. [#59747](https://github.com/grafana/grafana/pull/59747), [@aangelisc](https://github.com/aangelisc)
- **CloudWatch Logs:** Set default logs query and disable button when empty. [#61956](https://github.com/grafana/grafana/pull/61956), [@iwysiu](https://github.com/iwysiu)
- **CloudWatch:** Add CloudWatchSynthetics dimension. [#60832](https://github.com/grafana/grafana/pull/60832), [@jangaraj](https://github.com/jangaraj)
- **CloudWatch:** Add MaxProvisionedTableReadCapacityUtilization AWS/DynamoDB metric name. [#60829](https://github.com/grafana/grafana/pull/60829), [@jangaraj](https://github.com/jangaraj)
- **CloudWatch:** Add RDS dimension. [#61027](https://github.com/grafana/grafana/pull/61027), [@jangaraj](https://github.com/jangaraj)
- **CloudWatch:** Add macro for resolving period in SEARCH expressions. [#60435](https://github.com/grafana/grafana/pull/60435), [@sunker](https://github.com/sunker)
- **Cloudwatch:** Add feedback labels to log groups selector. [#60619](https://github.com/grafana/grafana/pull/60619), [@sunker](https://github.com/sunker)
- **Cloudwatch:** Add run query button. [#60089](https://github.com/grafana/grafana/pull/60089), [@sunker](https://github.com/sunker)
- **Cloudwatch:** Add support for template variables in new log group picker. [#61243](https://github.com/grafana/grafana/pull/61243), [@sunker](https://github.com/sunker)
- **Cloudwatch:** Define and use getDefaultquery instead of calling onChange on mount. [#60221](https://github.com/grafana/grafana/pull/60221), [@idastambuk](https://github.com/idastambuk)
- **Cloudwatch:** Refactor log group model. [#60873](https://github.com/grafana/grafana/pull/60873), [@sunker](https://github.com/sunker)
- **Cloudwatch:** Set CloudwatchCrossAccountQuery feature to stable. [#62348](https://github.com/grafana/grafana/pull/62348), [@sarahzinger](https://github.com/sarahzinger)
- **Cloudwatch:** Use new log group picker also for non cross-account queries. [#60913](https://github.com/grafana/grafana/pull/60913), [@sunker](https://github.com/sunker)
- **CommandPalette:** Improve section header styling. [#61584](https://github.com/grafana/grafana/pull/61584), [@joshhunt](https://github.com/joshhunt)
- **CommandPalette:** Minor usability improvements. [#61567](https://github.com/grafana/grafana/pull/61567), [@joshhunt](https://github.com/joshhunt)
- **CommandPalette:** Search for dashboards using API. [#61090](https://github.com/grafana/grafana/pull/61090), [@joshhunt](https://github.com/joshhunt)
- **Config:** Support JSON list syntax. [#61288](https://github.com/grafana/grafana/pull/61288), [@alexanderzobnin](https://github.com/alexanderzobnin)
- **DataLinks:** Allow providing a dynamic data link builder. [#60452](https://github.com/grafana/grafana/pull/60452), [@dprokop](https://github.com/dprokop)
- **DataProxy:** Populate X-Grafana-Referer header. [#60040](https://github.com/grafana/grafana/pull/60040), [@neilfordyce](https://github.com/neilfordyce)
- **Database:** Adds support for enable/disable SQLite Write-Ahead Logging (WAL) via configuration. [#58268](https://github.com/grafana/grafana/pull/58268), [@marefr](https://github.com/marefr)
- **Dataplane:** Deprecate timeseries-many in favor of timeseries-multi. [#59070](https://github.com/grafana/grafana/pull/59070), [@bohandley](https://github.com/bohandley)
- **Datasource settings:** Add deprecation notice for database field. [#58647](https://github.com/grafana/grafana/pull/58647), [@zoltanbedi](https://github.com/zoltanbedi)
- **Datasources:** Add support for getDetDefaultQuery in variable editor. [#62026](https://github.com/grafana/grafana/pull/62026), [@idastambuk](https://github.com/idastambuk)
- **Devenv:** OpenLDAP-Mac improvements. [#60229](https://github.com/grafana/grafana/pull/60229), [@mgyongyosi](https://github.com/mgyongyosi)
- **Docs:** Rename Message templates to Notification templates. [#59477](https://github.com/grafana/grafana/pull/59477), [@grobinson-grafana](https://github.com/grobinson-grafana)
- **Docs:** Unified Alerting is now compatible with AWS Aurora. [#61001](https://github.com/grafana/grafana/pull/61001), [@alexweav](https://github.com/alexweav)
- **Elastic:** Remove experimental tag from v8.0+. [#61359](https://github.com/grafana/grafana/pull/61359), [@gwdawson](https://github.com/gwdawson)
- **Elasticsearch:** Deprecate raw document mode. [#62236](https://github.com/grafana/grafana/pull/62236), [@gabor](https://github.com/gabor)
- **Elasticsearch:** Support nested aggregation. [#62301](https://github.com/grafana/grafana/pull/62301), [@gabor](https://github.com/gabor)
- **Email:** Use MJML email template. (Enterprise)
- **Email:** Use MJML email templates. [#57751](https://github.com/grafana/grafana/pull/57751), [@gillesdemey](https://github.com/gillesdemey)
- **Explore:** Add feature to open log sample in split view. [#62097](https://github.com/grafana/grafana/pull/62097), [@ivanahuckova](https://github.com/ivanahuckova)
- **Explore:** Enable resize of split pane. [#58683](https://github.com/grafana/grafana/pull/58683), [@gelicia](https://github.com/gelicia)
- **Explore:** Implement logs sample in Explore. [#61864](https://github.com/grafana/grafana/pull/61864), [@ivanahuckova](https://github.com/ivanahuckova)
- **Explore:** Keyboard shortcut to go to explore. [#61837](https://github.com/grafana/grafana/pull/61837), [@torkelo](https://github.com/torkelo)
- **Explore:** Notify when compact URL is used. [#58684](https://github.com/grafana/grafana/pull/58684), [@gelicia](https://github.com/gelicia)
- **Explore:** Use Datasource Onboarding page when visiting without any datasource set up. [#60399](https://github.com/grafana/grafana/pull/60399), [@Elfo404](https://github.com/Elfo404)
- **FileDropzone:** Format the file size limit in the error message when the max file size is exceeded (1000 => 1 kB). [#62290](https://github.com/grafana/grafana/pull/62290), [@oscarkilhed](https://github.com/oscarkilhed)
- **Flame graph:** Search with uFuzzy. [#61748](https://github.com/grafana/grafana/pull/61748), [@joey-grafana](https://github.com/joey-grafana)
- **GRPC Server:** Add query service. [#55781](https://github.com/grafana/grafana/pull/55781), [@toddtreece](https://github.com/toddtreece)
- **Geomap panel:** Generate types. [#61636](https://github.com/grafana/grafana/pull/61636), [@Clarity-89](https://github.com/Clarity-89)
- **Geomap:** Add color gradients to route layer. [#59062](https://github.com/grafana/grafana/pull/59062), [@drew08t](https://github.com/drew08t)
- **Glue:** Validate target query in correlations page. [#57245](https://github.com/grafana/grafana/pull/57245), [@L-M-K-B](https://github.com/L-M-K-B)
- **Heatmap:** Remove legacy angular based implementation. [#59249](https://github.com/grafana/grafana/pull/59249), [@ryantxu](https://github.com/ryantxu)
- **InfluxDB:** Send retention policy with InfluxQL queries if its been specified. [#62149](https://github.com/grafana/grafana/pull/62149), [@brettbuddin](https://github.com/brettbuddin)
- **Influxdb:** Remove backend migration feature toggle. [#61308](https://github.com/grafana/grafana/pull/61308), [@itsmylife](https://github.com/itsmylife)
- **Internationalization:** Translate page headers and Search dashboard actions. [#60727](https://github.com/grafana/grafana/pull/60727), [@TaitChan](https://github.com/TaitChan)
- **LDAP:** Make LDAP attribute mapping case-insensitive. [#58992](https://github.com/grafana/grafana/pull/58992), [@markkrj](https://github.com/markkrj)
- **LoginAttempts:** Reset attempts on successfull password reset. [#59215](https://github.com/grafana/grafana/pull/59215), [@kalleep](https://github.com/kalleep)
- **Logs Panel:** Add support for keyboard interactions with log lines. [#60561](https://github.com/grafana/grafana/pull/60561), [@matyax](https://github.com/matyax)
- **Logs:** Add possibility to download logs in JSON format. [#61394](https://github.com/grafana/grafana/pull/61394), [@svennergr](https://github.com/svennergr)
- **Logs:** Make `no logs found` text more visible in Explore. [#61651](https://github.com/grafana/grafana/pull/61651), [@svennergr](https://github.com/svennergr)
- **Logs:** Unify detected fields and labels in Log Details. [#60448](https://github.com/grafana/grafana/pull/60448), [@svennergr](https://github.com/svennergr)
- **Loki Autocomplete:** Suggest only possible labels for unwrap. [#61411](https://github.com/grafana/grafana/pull/61411), [@matyax](https://github.com/matyax)
- **Loki Editor Autocomplete:** Suggest unique history items. [#60262](https://github.com/grafana/grafana/pull/60262), [@matyax](https://github.com/matyax)
- **Loki Query Editor:** Add support to display query parsing errors to users. [#59427](https://github.com/grafana/grafana/pull/59427), [@matyax](https://github.com/matyax)
- **Loki Query Editor:** Autocompletion and suggestions improvements (unwrap, parser, extracted labels). [#59103](https://github.com/grafana/grafana/pull/59103), [@matyax](https://github.com/matyax)
- **Loki Query Editor:** Update history items with successive queries. [#60327](https://github.com/grafana/grafana/pull/60327), [@matyax](https://github.com/matyax)
- **Loki:** Add format explanation to regex operations. [#60518](https://github.com/grafana/grafana/pull/60518), [@gwdawson](https://github.com/gwdawson)
- **Loki:** Add hints for query filters. [#60293](https://github.com/grafana/grafana/pull/60293), [@gwdawson](https://github.com/gwdawson)
- **Loki:** Add improvements to loki label browser. [#59387](https://github.com/grafana/grafana/pull/59387), [@gwdawson](https://github.com/gwdawson)
- **Loki:** Change format of query builder hints. [#60228](https://github.com/grafana/grafana/pull/60228), [@gwdawson](https://github.com/gwdawson)
- **Loki:** Make label browser accessible in query builder. [#58525](https://github.com/grafana/grafana/pull/58525), [@gwdawson](https://github.com/gwdawson)
- **Loki:** Push support for multi-tenancy mode. [#60866](https://github.com/grafana/grafana/pull/60866), [@joanlopez](https://github.com/joanlopez)
- **Loki:** Remove raw query toggle. [#59125](https://github.com/grafana/grafana/pull/59125), [@gwdawson](https://github.com/gwdawson)
- **Loki:** Rename "explain" toggle to "explain query". [#61150](https://github.com/grafana/grafana/pull/61150), [@gwdawson](https://github.com/gwdawson)
- **Loki:** Set custom width for modals in the loki query editor. [#59714](https://github.com/grafana/grafana/pull/59714), [@gwdawson](https://github.com/gwdawson)
- **Loki:** Show configured log line limit. [#61291](https://github.com/grafana/grafana/pull/61291), [@gwdawson](https://github.com/gwdawson)
- **Loki:** Show query size approximation. [#62109](https://github.com/grafana/grafana/pull/62109), [@gwdawson](https://github.com/gwdawson)
- **Middleware:** Add Custom Headers to HTTP responses. [#59018](https://github.com/grafana/grafana/pull/59018), [@jcalisto](https://github.com/jcalisto)
- **Navigation:** Open command palette from search box. [#61667](https://github.com/grafana/grafana/pull/61667), [@joshhunt](https://github.com/joshhunt)
- **NodeGraph:** Allow usage with single dataframe. [#58448](https://github.com/grafana/grafana/pull/58448), [@aocenas](https://github.com/aocenas)
- **OAuth:** Support pagination for GitHub orgs. [#58648](https://github.com/grafana/grafana/pull/58648), [@sdague](https://github.com/sdague)
- **PanelChrome:** Allow panel to be dragged if set as draggable from the outside. [#61698](https://github.com/grafana/grafana/pull/61698), [@axelavargas](https://github.com/axelavargas)
- **PanelChrome:** Refactor and refine items next to title. [#60514](https://github.com/grafana/grafana/pull/60514), [@axelavargas](https://github.com/axelavargas)
- **PanelRenderer:** Interpolate variables in applyFieldOverrides. [#59844](https://github.com/grafana/grafana/pull/59844), [@connorlindsey](https://github.com/connorlindsey)
- **Performance:** Preallocate slices. [#61580](https://github.com/grafana/grafana/pull/61580), [@peakle](https://github.com/peakle)
- **Phlare:** Reset flame graph after query is run. [#59609](https://github.com/grafana/grafana/pull/59609), [@joey-grafana](https://github.com/joey-grafana)
- **Phlare:** Transition from LogQL/PromQL to Phlare should keep the query. [#60217](https://github.com/grafana/grafana/pull/60217), [@joey-grafana](https://github.com/joey-grafana)
- **Plugins:** Add file permission error check when attempting to verify plugin signature. [#61860](https://github.com/grafana/grafana/pull/61860), [@wbrowne](https://github.com/wbrowne)
- **Plugins:** Automatically forward plugin request HTTP headers in outgoing HTTP requests. [#60417](https://github.com/grafana/grafana/pull/60417), [@marefr](https://github.com/marefr)
- **Plugins:** Forward user header (X-Grafana-User) in backend plugin requests. [#58646](https://github.com/grafana/grafana/pull/58646), [@marefr](https://github.com/marefr)
- **Plugins:** Pass through dashboard/contextual HTTP headers to plugins/datasources. [#60301](https://github.com/grafana/grafana/pull/60301), [@GiedriusS](https://github.com/GiedriusS)
- **Plugins:** Refactor forward of cookies, OAuth token and header modifications by introducing client middlewares. [#58132](https://github.com/grafana/grafana/pull/58132), [@marefr](https://github.com/marefr)
- **Plugins:** Remove connection/hop-by-hop request/response headers for call resource. [#60077](https://github.com/grafana/grafana/pull/60077), [@marefr](https://github.com/marefr)
- **Plugins:** Unsigned chromium file should not invalidate signature for Renderer plugin. [#59104](https://github.com/grafana/grafana/pull/59104), [@wbrowne](https://github.com/wbrowne)
- **Preferences:** Add pagination to org configuration page. [#60896](https://github.com/grafana/grafana/pull/60896), [@alexanderzobnin](https://github.com/alexanderzobnin)
- **Preferences:** Add theme preference to match system theme. [#61986](https://github.com/grafana/grafana/pull/61986), [@joshhunt](https://github.com/joshhunt)
- **Prometheus:** Kickstart your query, formerly query patterns. [#60718](https://github.com/grafana/grafana/pull/60718), [@bohandley](https://github.com/bohandley)
- **Prometheus:** New instant query results view in Explore. [#60479](https://github.com/grafana/grafana/pull/60479), [@gtk-grafana](https://github.com/gtk-grafana)
- **Prometheus:** Remove buffered client and feature toggle related to it. [#59898](https://github.com/grafana/grafana/pull/59898), [@itsmylife](https://github.com/itsmylife)
- **Public Dashboards:** Time range for public dashboard in NavToolbar. [#60689](https://github.com/grafana/grafana/pull/60689), [@juanicabanas](https://github.com/juanicabanas)
- **PublicDashboards:** A unique page for public dashboards. [#60744](https://github.com/grafana/grafana/pull/60744), [@juanicabanas](https://github.com/juanicabanas)
- **PublicDashboards:** Add react-hook-form for Public Dashboard modal. [#60249](https://github.com/grafana/grafana/pull/60249), [@juanicabanas](https://github.com/juanicabanas)
- **PublicDashboards:** Add share column to public dashboards table. [#61102](https://github.com/grafana/grafana/pull/61102), [@owensmallwood](https://github.com/owensmallwood)
- **PublicDashboards:** Adds middleware for email sharing. (Enterprise)
- **PublicDashboards:** Adds tables and models for email sharing. (Enterprise)
- **PublicDashboards:** Checkboxes list refactor. [#61947](https://github.com/grafana/grafana/pull/61947), [@juanicabanas](https://github.com/juanicabanas)
- **PublicDashboards:** Create API for sharing by email. (Enterprise)
- **PublicDashboards:** Enterprise service skeleton for public dashboards with feature flag. (Enterprise)
- **PublicDashboards:** Modal warns when using unsupported datasources. [#58926](https://github.com/grafana/grafana/pull/58926), [@owensmallwood](https://github.com/owensmallwood)
- **PublicDashboards:** Page to request access to protected pubdash. (Enterprise)
- **PublicDashboards:** Remove unnecessary css style in Audit Table. [#60546](https://github.com/grafana/grafana/pull/60546), [@juanicabanas](https://github.com/juanicabanas)
- **PublicDashboards:** Revert Time range setting added. [#60698](https://github.com/grafana/grafana/pull/60698), [@juanicabanas](https://github.com/juanicabanas)
- **PublicDashboards:** Time range conditionally shown. [#60425](https://github.com/grafana/grafana/pull/60425), [@juanicabanas](https://github.com/juanicabanas)
- **PublicDashboards:** Time range setting added. [#60487](https://github.com/grafana/grafana/pull/60487), [@juanicabanas](https://github.com/juanicabanas)
- **PublicDashboards:** Time range settings. [#61585](https://github.com/grafana/grafana/pull/61585), [@juanicabanas](https://github.com/juanicabanas)
- **PublicDashboards:** Time range switch added. [#60257](https://github.com/grafana/grafana/pull/60257), [@juanicabanas](https://github.com/juanicabanas)
- **PublicDashboards:** Time range wording changed. [#60473](https://github.com/grafana/grafana/pull/60473), [@juanicabanas](https://github.com/juanicabanas)
- **RBAC:** Add an endpoint to search through assignments. (Enterprise)
- **RBAC:** Add config option to reset basic roles on start up. [#59598](https://github.com/grafana/grafana/pull/59598), [@gamab](https://github.com/gamab)
- **RBAC:** Add config option to reset basic roles on start up. (Enterprise)
- **RBAC:** Add permission to get usage report preview. [#61570](https://github.com/grafana/grafana/pull/61570), [@alexanderzobnin](https://github.com/alexanderzobnin)
- **RBAC:** Permission check performance improvements for the new search. [#60729](https://github.com/grafana/grafana/pull/60729), [@IevaVasiljeva](https://github.com/IevaVasiljeva)
- **RBAC:** Register plugin permissions's action and its accepted scopes. (Enterprise)
- **RBAC:** Runtime plugin role registration and update. (Enterprise)
- **Report Settings:** Add UI to upload logo files. (Enterprise)
- **Reporting:** Allow to upload report branding images. (Enterprise)
- **RolePicker:** Align groupHeader to the list items horizontally. [#61060](https://github.com/grafana/grafana/pull/61060), [@mgyongyosi](https://github.com/mgyongyosi)
- **SAML:** Support auto login. [#61685](https://github.com/grafana/grafana/pull/61685), [@alexanderzobnin](https://github.com/alexanderzobnin)
- **SMTP:** Update SMTP TemplatesPatterns to do an 'or' filter instead of 'and'. [#61421](https://github.com/grafana/grafana/pull/61421), [@mmandrus](https://github.com/mmandrus)
- **SQL Datasources:** Move database setting to jsonData. [#58649](https://github.com/grafana/grafana/pull/58649), [@zoltanbedi](https://github.com/zoltanbedi)
- **SQL Datasources:** Use health check for config test. [#59867](https://github.com/grafana/grafana/pull/59867), [@zoltanbedi](https://github.com/zoltanbedi)
- **SQL:** Return no data response when no rows returned. [#59121](https://github.com/grafana/grafana/pull/59121), [@zoltanbedi](https://github.com/zoltanbedi)
- **Search:** Remember sorting preference between visits. [#62248](https://github.com/grafana/grafana/pull/62248), [@joshhunt](https://github.com/joshhunt)
- **Segment:** Individual segments are now keyboard accessible. [#60555](https://github.com/grafana/grafana/pull/60555), [@ashharrison90](https://github.com/ashharrison90)
- **Server:** Switch from separate server & cli to a unified grafana binary. [#58286](https://github.com/grafana/grafana/pull/58286), [@DanCech](https://github.com/DanCech)
- **SharePDF:** Add zoom select. (Enterprise)
- **Slugify:** Replace gosimple/slug with a simple function. [#59517](https://github.com/grafana/grafana/pull/59517), [@ryantxu](https://github.com/ryantxu)
- **Snapshots:** Add snapshot enable config. [#61587](https://github.com/grafana/grafana/pull/61587), [@leandro-deveikis](https://github.com/leandro-deveikis)
- **Table Panel:** Refactor Cell Options to Allow for Options per Cell Type. [#59363](https://github.com/grafana/grafana/pull/59363), [@codeincarnate](https://github.com/codeincarnate)
- **Table panel:** Use link elements instead of div elements with on click events to aid with keyboard accessibility. [#59393](https://github.com/grafana/grafana/pull/59393), [@oscarkilhed](https://github.com/oscarkilhed)
- **TablePanel:** Improve and align table styles with the rest of Grafana. [#60365](https://github.com/grafana/grafana/pull/60365), [@torkelo](https://github.com/torkelo)
- **Teams:** Support paginating and filtering more then 1000 teams. [#58761](https://github.com/grafana/grafana/pull/58761), [@kalleep](https://github.com/kalleep)
- **Teams:** Use generated TS types. [#60618](https://github.com/grafana/grafana/pull/60618), [@Clarity-89](https://github.com/Clarity-89)
- **Tempo:** Trace to logs custom query with interpolation. [#61702](https://github.com/grafana/grafana/pull/61702), [@aocenas](https://github.com/aocenas)
- **Tempo:** Update column width for Loki search. [#61924](https://github.com/grafana/grafana/pull/61924), [@joey-grafana](https://github.com/joey-grafana)
- **Tempo:** Update docs and default Tempo metrics query. [#62185](https://github.com/grafana/grafana/pull/62185), [@joey-grafana](https://github.com/joey-grafana)
- **TestDatasource:** Add scenario for generating trace data. [#59299](https://github.com/grafana/grafana/pull/59299), [@aocenas](https://github.com/aocenas)
- **TextPanel:** Refactor to functional component. [#60885](https://github.com/grafana/grafana/pull/60885), [@ryantxu](https://github.com/ryantxu)
- **Theme:** Use `Inter` font by default. [#59544](https://github.com/grafana/grafana/pull/59544), [@ashharrison90](https://github.com/ashharrison90)
- **Trace View:** Disallow a span colour that is the same or looks similar to previous colour. [#58146](https://github.com/grafana/grafana/pull/58146), [@joey-grafana](https://github.com/joey-grafana)
- **Tracing:** Add keyboard accessibility to SpanDetailRow. [#59412](https://github.com/grafana/grafana/pull/59412), [@joey-grafana](https://github.com/joey-grafana)
- **Transformations:** Add context parameter to transformDataFrame and operators. [#60694](https://github.com/grafana/grafana/pull/60694), [@torkelo](https://github.com/torkelo)
- **Transformations:** Extract JSON Paths. [#59400](https://github.com/grafana/grafana/pull/59400), [@NiklasCi](https://github.com/NiklasCi)
- **Transformations:** Grouping to matrix empty value option. [#55591](https://github.com/grafana/grafana/pull/55591), [@hugo082](https://github.com/hugo082)
- **UsageInsights:** Record events for Explore queries. [#59931](https://github.com/grafana/grafana/pull/59931), [@daniellee](https://github.com/daniellee)
- **Variables:** Support for colons in time variables custom format. [#61404](https://github.com/grafana/grafana/pull/61404), [@yesoreyeram](https://github.com/yesoreyeram)

### Bug fixes

- **Alerting:** Fix ConditionsCmd No Data for "has no value". [#58634](https://github.com/grafana/grafana/pull/58634), [@grobinson-grafana](https://github.com/grobinson-grafana)
- **Alerting:** Fix evaluation timeout. [#61303](https://github.com/grafana/grafana/pull/61303), [@yuri-tceretian](https://github.com/yuri-tceretian)
- **Alerting:** Redo refactoring from reverted fix in [#56812](https://github.com/grafana/grafana/pull/56812). [#61051](https://github.com/grafana/grafana/pull/61051), [@grobinson-grafana](https://github.com/grobinson-grafana)
- **Alerting:** Set Dashboard and Panel IDs on rule group replacement. [#60374](https://github.com/grafana/grafana/pull/60374), [@alexmobo](https://github.com/alexmobo)
- **Alerting:** Store alertmanager configuration history in a separate table in the database. [#60197](https://github.com/grafana/grafana/pull/60197), [@alexweav](https://github.com/alexweav)
- **Azure Monitor:** Fix health check for empty default subscription. [#60569](https://github.com/grafana/grafana/pull/60569), [@andresmgot](https://github.com/andresmgot)
- **Barchart:** Fix erroneous tooltip value. [#61455](https://github.com/grafana/grafana/pull/61455), [@mdvictor](https://github.com/mdvictor)
- **Candlestick:** Fix showing hidden legend values. [#60971](https://github.com/grafana/grafana/pull/60971), [@zoltanbedi](https://github.com/zoltanbedi)
- **CloudWatch:** Fix logs insights deeplink. [#59906](https://github.com/grafana/grafana/pull/59906), [@fridgepoet](https://github.com/fridgepoet)
- **Cloudmonitor:** Refactor query builder. [#61410](https://github.com/grafana/grafana/pull/61410), [@aangelisc](https://github.com/aangelisc)
- **Command Palette:** Links now work when grafana is served under a subpath. [#60033](https://github.com/grafana/grafana/pull/60033), [@ashharrison90](https://github.com/ashharrison90)
- **CommandPalette:** Fix long dashboard names freezing the browser. [#61278](https://github.com/grafana/grafana/pull/61278), [@joshhunt](https://github.com/joshhunt)
- **DataFrame:** Add explicit histogram frame type. [#61195](https://github.com/grafana/grafana/pull/61195), [@leeoniya](https://github.com/leeoniya)
- **Dropdown:** Make escape close a dropdown. [#62098](https://github.com/grafana/grafana/pull/62098), [@ashharrison90](https://github.com/ashharrison90)
- **Explore:** Fix a11y issue with show all series button in Graph. [#58943](https://github.com/grafana/grafana/pull/58943), [@Elfo404](https://github.com/Elfo404)
- **Explore:** Fixes explore page height and margin issues. [#59865](https://github.com/grafana/grafana/pull/59865), [@torkelo](https://github.com/torkelo)
- **Explore:** Re-initialize graph when number of series to show changes. [#60499](https://github.com/grafana/grafana/pull/60499), [@Elfo404](https://github.com/Elfo404)
- **Fix:** Unlocking the UI for AuthProxy users. [#59507](https://github.com/grafana/grafana/pull/59507), [@gamab](https://github.com/gamab)
- **GrafanaUI:** Checkbox description fix. [#61929](https://github.com/grafana/grafana/pull/61929), [@juanicabanas](https://github.com/juanicabanas)
- **LDAP:** Disable user in case it has been removed from LDAP directory. [#60231](https://github.com/grafana/grafana/pull/60231), [@mgyongyosi](https://github.com/mgyongyosi)
- **LibraryPanels:** Fix issue where viewer with folder edit permissions could not update library panel. [#58420](https://github.com/grafana/grafana/pull/58420), [@kaydelaney](https://github.com/kaydelaney)
- **Loki Query Builder:** Fix bug parsing range params. [#61678](https://github.com/grafana/grafana/pull/61678), [@matyax](https://github.com/matyax)
- **MultiSelect:** Fix `actionMeta` not available in `onChange` callback. [#62339](https://github.com/grafana/grafana/pull/62339), [@svennergr](https://github.com/svennergr)
- **Navigation:** Fix finding the active nav item for plugins. [#62123](https://github.com/grafana/grafana/pull/62123), [@leventebalogh](https://github.com/leventebalogh)
- **PanelChrome:** Allow hovering on description when status error is visible. [#61757](https://github.com/grafana/grafana/pull/61757), [@ivanortegaalba](https://github.com/ivanortegaalba)
- **PanelEditor:** Fixes issue where panel edit would show the panel plugin options of the previous edit panel. [#59861](https://github.com/grafana/grafana/pull/59861), [@torkelo](https://github.com/torkelo)
- **PublicDashboards:** Footer alignment fix for Firefox browser. [#62108](https://github.com/grafana/grafana/pull/62108), [@juanicabanas](https://github.com/juanicabanas)
- **PublicDashboards:** Footer position fix. [#61954](https://github.com/grafana/grafana/pull/61954), [@juanicabanas](https://github.com/juanicabanas)
- **RBAC:** Fix DeleteUserPermissions not being called on Upsert org sync. [#60531](https://github.com/grafana/grafana/pull/60531), [@Jguer](https://github.com/Jguer)
- **RBAC:** Handle edge case where there is duplicated acl entries for a role on a single dashboard. [#58079](https://github.com/grafana/grafana/pull/58079), [@kalleep](https://github.com/kalleep)
- **Resource Query Cache:** Do not store 207 status codes. (Enterprise)
- **SAML:** Do not register SAML support bundle collector when SAML is disabled. (Enterprise)
- **SSE:** Fix math expression to support NoData results. [#61721](https://github.com/grafana/grafana/pull/61721), [@yuri-tceretian](https://github.com/yuri-tceretian)
- **Security:** Fix XSS in runbook URL. [#59540](https://github.com/grafana/grafana/pull/59540), [@dsotirakis](https://github.com/dsotirakis)
- **TimeSeries:** Better y-axis ticks for IEC units. [#59984](https://github.com/grafana/grafana/pull/59984), [@leeoniya](https://github.com/leeoniya)
- **TimeSeries:** Fix rendering when zooming to time ranges between datapoints. [#59444](https://github.com/grafana/grafana/pull/59444), [@leeoniya](https://github.com/leeoniya)
- **TimeSeries:** Fix y-axis Yes/No and On/Off boolean units. [#61207](https://github.com/grafana/grafana/pull/61207), [@leeoniya](https://github.com/leeoniya)
- **Traces:** Fix for multiple $\_\_tags in trace to metrics. [#59641](https://github.com/grafana/grafana/pull/59641), [@joey-grafana](https://github.com/joey-grafana)
- **Variables:** Allow user to filter values in dropdown using white space. [#60622](https://github.com/grafana/grafana/pull/60622), [@yusuf-multhan](https://github.com/yusuf-multhan)

### Breaking changes

Removes the non-functional feature toggle `influxdbBackendMigration`. InfluxDB is working %100 with server access mode. You can keep using your dashboards, and data sources as you have been using. This won't affect them. If you are upgrading from older versions of Grafana please be sure to check your dashboard config and check for warnings. Issue [#61308](https://github.com/grafana/grafana/issues/61308)

Removes support for "detected fields" in the details of a log line, however all supported interactions (filter, statistics, visibility) are now supported for all fields. If you are using Loki you can get those fields back by using a parser operation like `logfmt` or `json`.

Issue [#60448](https://github.com/grafana/grafana/issues/60448)

### Deprecations

In the elasticsearch data source, the "Raw document" display mode is deprecated. We recommend using the "Raw Data" mode instead. Issue [#62236](https://github.com/grafana/grafana/issues/62236)

Sentry frontend logging provider will be removed with next major version. Issue [#60165](https://github.com/grafana/grafana/issues/60165)

### Plugin development fixes & changes

- **FileDropzone:** Display max file size. [#62334](https://github.com/grafana/grafana/pull/62334), [@oscarkilhed](https://github.com/oscarkilhed)
- **Chore:** Bump d3-color to 3.1.0. [#61609](https://github.com/grafana/grafana/pull/61609), [@jackw](https://github.com/jackw)
- **UI/Alert:** Infer the `role` property based on the `severity`. [#61242](https://github.com/grafana/grafana/pull/61242), [@leventebalogh](https://github.com/leventebalogh)
- **PanelChrome:** Menu is wrapped in a render prop for full outside control. [#60537](https://github.com/grafana/grafana/pull/60537), [@polibb](https://github.com/polibb)
- **Toolkit:** Deprecate all plugin related commands. [#60290](https://github.com/grafana/grafana/pull/60290), [@academo](https://github.com/academo)
- **Grafana UI:** Add experimental InteractiveTable component. [#58223](https://github.com/grafana/grafana/pull/58223), [@Elfo404](https://github.com/Elfo404)

---

# element v1.11.22

> published at 2023-01-31 [source](https://github.com/vector-im/element-web/releases/tag/v1.11.22)

## 🐛 Bug Fixes

- Bump version number to fix problems upgrading from v1.11.21-rc.1

---

# element v1.11.21

> published at 2023-01-31 [source](https://github.com/vector-im/element-web/releases/tag/v1.11.21)

## ✨ Features

- Move pin drop out of labs ( [#22993](https://github.com/vector-im/element-web/pull/22993)).
- Quotes for rich text editor (RTE) ( [#9932](https://github.com/matrix-org/matrix-react-sdk/pull/9932)). Contributed by [@alunturner](https://github.com/alunturner).
- Show the room name in the room header during calls ( [#9942](https://github.com/matrix-org/matrix-react-sdk/pull/9942)). Fixes [#24268](https://github.com/vector-im/element-web/issues/24268).
- Add code blocks to rich text editor ( [#9921](https://github.com/matrix-org/matrix-react-sdk/pull/9921)). Contributed by [@alunturner](https://github.com/alunturner).
- Add new style for inline code ( [#9936](https://github.com/matrix-org/matrix-react-sdk/pull/9936)). Contributed by [@florianduros](https://github.com/florianduros).
- Add disabled button state to rich text editor ( [#9930](https://github.com/matrix-org/matrix-react-sdk/pull/9930)). Contributed by [@alunturner](https://github.com/alunturner).
- Change the rageshake "app" for auto-rageshakes ( [#9909](https://github.com/matrix-org/matrix-react-sdk/pull/9909)).
- Device manager - tweak settings display ( [#9905](https://github.com/matrix-org/matrix-react-sdk/pull/9905)). Contributed by [@kerryarchibald](https://github.com/kerryarchibald).
- Add list functionality to rich text editor ( [#9871](https://github.com/matrix-org/matrix-react-sdk/pull/9871)). Contributed by [@alunturner](https://github.com/alunturner).

## 🐛 Bug Fixes

- Fix RTE focus behaviour in threads ( [#9969](https://github.com/matrix-org/matrix-react-sdk/pull/9969)). Fixes [#23755](https://github.com/vector-im/element-web/issues/23755). Contributed by [@florianduros](https://github.com/florianduros).
- [#22204](https://github.com/vector-im/element-web/issues/22204) Issue: Centered File info in lightbox ( [#9971](https://github.com/matrix-org/matrix-react-sdk/pull/9971)). Fixes [#22204](https://github.com/vector-im/element-web/issues/22204). Contributed by [@Spartan09](https://github.com/Spartan09).
- Fix seekbar position for zero length audio ( [#9949](https://github.com/matrix-org/matrix-react-sdk/pull/9949)). Fixes [#24248](https://github.com/vector-im/element-web/issues/24248).
- Allow thread panel to be closed after being opened from notification ( [#9937](https://github.com/matrix-org/matrix-react-sdk/pull/9937)). Fixes [#23764](https://github.com/vector-im/element-web/issues/23764) [#23852](https://github.com/vector-im/element-web/issues/23852) and [#24213](https://github.com/vector-im/element-web/issues/24213). Contributed by [@justjanne](https://github.com/justjanne).
- Only highlight focused menu item if focus is supposed to be visible ( [#9945](https://github.com/matrix-org/matrix-react-sdk/pull/9945)). Fixes [#23582](https://github.com/vector-im/element-web/issues/23582).
- Prevent call durations from breaking onto multiple lines ( [#9944](https://github.com/matrix-org/matrix-react-sdk/pull/9944)).
- Tweak call lobby buttons to more closely match designs ( [#9943](https://github.com/matrix-org/matrix-react-sdk/pull/9943)).
- Do not show a broadcast as live immediately after the recording has stopped ( [#9947](https://github.com/matrix-org/matrix-react-sdk/pull/9947)). Fixes [#24233](https://github.com/vector-im/element-web/issues/24233).
- Clear the RTE before sending a message ( [#9948](https://github.com/matrix-org/matrix-react-sdk/pull/9948)). Contributed by [@florianduros](https://github.com/florianduros).
- Fix {enter} press in RTE ( [#9927](https://github.com/matrix-org/matrix-react-sdk/pull/9927)). Contributed by [@florianduros](https://github.com/florianduros).
- Fix the problem that the password reset email has to be confirmed twice ( [#9926](https://github.com/matrix-org/matrix-react-sdk/pull/9926)). Fixes [#24226](https://github.com/vector-im/element-web/issues/24226).
- replace .at() with array.length-1 ( [#9933](https://github.com/matrix-org/matrix-react-sdk/pull/9933)). Fixes [matrix-org/element-web-rageshakes#19281](https://github.com/matrix-org/element-web-rageshakes/issues/19281).
- Fix broken threads list timestamp layout ( [#9922](https://github.com/matrix-org/matrix-react-sdk/pull/9922)). Fixes [#24243](https://github.com/vector-im/element-web/issues/24243) and [#24191](https://github.com/vector-im/element-web/issues/24191). Contributed by [@justjanne](https://github.com/justjanne).
- Disable multiple messages when {enter} is pressed multiple times ( [#9929](https://github.com/matrix-org/matrix-react-sdk/pull/9929)). Fixes [#24249](https://github.com/vector-im/element-web/issues/24249). Contributed by [@florianduros](https://github.com/florianduros).
- Fix logout devices when resetting the password ( [#9925](https://github.com/matrix-org/matrix-react-sdk/pull/9925)). Fixes [#24228](https://github.com/vector-im/element-web/issues/24228).
- Fix: Poll replies overflow when not enough space ( [#9924](https://github.com/matrix-org/matrix-react-sdk/pull/9924)). Fixes [#24227](https://github.com/vector-im/element-web/issues/24227). Contributed by [@kerryarchibald](https://github.com/kerryarchibald).
- State event updates are not forwarded to the widget from invitation room ( [#9802](https://github.com/matrix-org/matrix-react-sdk/pull/9802)). Contributed by [@maheichyk](https://github.com/maheichyk).
- Fix error when viewing source of redacted events ( [#9914](https://github.com/matrix-org/matrix-react-sdk/pull/9914)). Fixes [#24165](https://github.com/vector-im/element-web/issues/24165). Contributed by [@clarkf](https://github.com/clarkf).
- Replace outdated css attribute ( [#9912](https://github.com/matrix-org/matrix-react-sdk/pull/9912)). Fixes [#24218](https://github.com/vector-im/element-web/issues/24218). Contributed by [@justjanne](https://github.com/justjanne).
- Clear isLogin theme override when user is no longer viewing login screens ( [#9911](https://github.com/matrix-org/matrix-react-sdk/pull/9911)). Fixes [#23893](https://github.com/vector-im/element-web/issues/23893).
- Fix reply action in message context menu notif & file panels ( [#9895](https://github.com/matrix-org/matrix-react-sdk/pull/9895)). Fixes [#23970](https://github.com/vector-im/element-web/issues/23970).
- Fix issue where thread dropdown would not show up correctly ( [#9872](https://github.com/matrix-org/matrix-react-sdk/pull/9872)). Fixes [#24040](https://github.com/vector-im/element-web/issues/24040). Contributed by [@justjanne](https://github.com/justjanne).
- Fix unexpected composer growing ( [#9889](https://github.com/matrix-org/matrix-react-sdk/pull/9889)). Contributed by [@florianduros](https://github.com/florianduros).
- Fix misaligned timestamps for thread roots which are emotes ( [#9875](https://github.com/matrix-org/matrix-react-sdk/pull/9875)). Fixes [#23897](https://github.com/vector-im/element-web/issues/23897). Contributed by [@justjanne](https://github.com/justjanne).

---

# jitsi stable-8252: release

> published at 2023-01-31 [source](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-8252)

release

\\* [`076dbf7`](https://github.com/jitsi/docker-jitsi-meet/commit/076dbf7d17e9f39110fb0f7d89fdfc8df3284662) jibri: fix downloading new (>= 109) ChromeDriver

\\* [`8f40804`](https://github.com/jitsi/docker-jitsi-meet/commit/8f40804839b1f20c78e85cc551fb118c35d4292e) jibri: update Chrome to M109

\\* [`1cf8638`](https://github.com/jitsi/docker-jitsi-meet/commit/1cf86381055e18fc908818cf9fb5662e23cbd900) compose: fix whiteboard collab server variable name

\\* [`9e0305b`](https://github.com/jitsi/docker-jitsi-meet/commit/9e0305bc5900a420d182dc0f0db376ce7eb0cef7) prosody: set JWT\_ENABLE\_DOMAIN\_VERIFICATION to false by default

\\* [`ac551c3`](https://github.com/jitsi/docker-jitsi-meet/commit/ac551c300a849a4dc70aada5a828d3cfb83e358d) base: update tpl

\\* [`ec972ee`](https://github.com/jitsi/docker-jitsi-meet/commit/ec972eec2123840aaea7e9ed21984a87cec6f49a) base: update tpl

\\* [`8684b0b`](https://github.com/jitsi/docker-jitsi-meet/commit/8684b0b3d159712b6f786679b864b304a3fd6c67) misc: working on unstable

---

# dynamic-dns v3.10.0-ls108

> published at 2023-01-31 [source](https://github.com/linuxserver/docker-ddclient/releases/tag/v3.10.0-ls108)

**LinuxServer Changes:**

Update build instructions for 3.10.0. Update default `ddclient.conf`.

**ddclient Changes:**

**Note**

Please also read the changelog for v3.10.0\_2 and v3.10.0\_1 which contains dependency changes.

### New features

- Added support for domaindiscount24.com
- Added support for njal.la

---

# soft-serve v0.4.5

> published at 2023-01-30 [source](https://github.com/charmbracelet/soft-serve/releases/tag/v0.4.5)

## Changelog

### Bug fixes

- [`f227b4c`](https://github.com/charmbracelet/soft-serve/commit/f227b4c3e8e700de5f33246f9aab6a2f313d022c): fix(go): remove go.mod exclude directives ( [@aymanbagabas](https://github.com/aymanbagabas))

### Dependency updates

- [`7cfe48d`](https://github.com/charmbracelet/soft-serve/commit/7cfe48d706a87b98bcf79f41d0409c1bed18ff44): feat(deps): bump github.com/go-git/go-billy/v5 from 5.3.1 to 5.4.0 ( [@dependabot](https://github.com/dependabot)\[bot\])

* * *

[![The Charm logo](https://camo.githubusercontent.com/65459c24b86d0476085210fd0387503a161a1359b3cf5034324346f55907cbb8/68747470733a2f2f73747566662e636861726d2e73682f636861726d2d62616467652e6a7067)](https://charm.sh/)

Thoughts? Questions? We love hearing from you. Feel free to reach out on [Twitter](https://twitter.com/charmcli), [The Fediverse](https://mastodon.technology/@charm), or on [Discord](https://charm.sh/chat).

---

# ansible-core 2.14.2

> published at 2023-01-30 [source](https://pypi.org/project/ansible-core/2.14.2/)

> no description provided

---



---

# cinny v2.2.4

> published at 2023-01-30 [source](https://github.com/cinnyapp/cinny/releases/tag/v2.2.4)

## What's Changed

- Use relative paths for build by [@ajbura](https://github.com/ajbura) in [#1094](https://github.com/cinnyapp/cinny/pull/1094)
- Add node polyfills which fixes crypto related issues by [@ajbura](https://github.com/ajbura) in [#1093](https://github.com/cinnyapp/cinny/pull/1093)

**Full Changelog**: [`v2.2.3...v2.2.4`](https://github.com/cinnyapp/cinny/compare/v2.2.3...v2.2.4)

---

# miniflux Miniflux 2.0.42

> published at 2023-01-30 [source](https://github.com/miniflux/v2/releases/tag/2.0.42)

- Fix header items wrapping
- Add option to enable or disable double tap
- Improve PWA display mode label in settings page
- Bump `golang.org/x/*` dependencies
- Update translations
- Add scraping rule for `ilpost.it`
- Update reading time HTML element after fetching the original web page
- Add category feeds refresh feature

---

# mautrix-discord v0.1.0

> published at 2023-01-29 [source](https://github.com/mautrix/discord/releases/tag/v0.1.0)

Initial release.

---

# chatgpt v1.4.1

> published at 2023-01-27 [source](https://github.com/matrixgpt/matrix-chatgpt-bot/releases/tag/v1.4.1)

## What's Changed

- Optional threads by [@bertybuttface](https://github.com/bertybuttface) in [#67](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/67)

**Full Changelog**: [`v1.4.0...v1.4.1`](https://github.com/matrixgpt/matrix-chatgpt-bot/compare/v1.4.0...v1.4.1)

---

# grafana 9.3.6 (2023-01-26)

> published at 2023-01-27 [source](https://github.com/grafana/grafana/releases/tag/v9.3.6)

[Download page](https://grafana.com/grafana/download/9.3.6)

[What's new highlights](https://grafana.com/docs/grafana/latest/whatsnew/)

### Bug fixes

- **QueryEditorRow:** Fixes issue loading query editor when data source variable selected. [#61927](https://github.com/grafana/grafana/pull/61927), [@torkelo](https://github.com/torkelo)

---

# chatgpt v1.4.0

> published at 2023-01-27 [source](https://github.com/matrixgpt/matrix-chatgpt-bot/releases/tag/v1.4.0)

## What's Changed

- Add support for configurable context levels. by [@bertybuttface](https://github.com/bertybuttface) in [#66](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/66)

**Full Changelog**: [`v1.3.2...v1.4.0`](https://github.com/matrixgpt/matrix-chatgpt-bot/compare/v1.3.2...v1.4.0)

---

# prometheus 2.42.0-rc.0 / 2023-01-26

> published at 2023-01-27 [source](https://github.com/prometheus/prometheus/releases/tag/v2.42.0-rc.0)

- \[FEATURE\] Add 'keep\_firing\_for' field to alerting rules. [#11827](https://github.com/prometheus/prometheus/pull/11827)
- \[FEATURE\] Promtool: Add support of selecting timeseries for TSDB dump. [#11872](https://github.com/prometheus/prometheus/pull/11872)
- \[ENHANCEMENT\] Agent: Native histogram support. [#11842](https://github.com/prometheus/prometheus/pull/11842)
- \[ENHANCEMENT\] Histograms: Return actually useful counter reset hints. [#11864](https://github.com/prometheus/prometheus/pull/11864)
- \[ENHANCEMENT\] SD: Add container ID as a meta label for pod targets for Kubernetes. [#11844](https://github.com/prometheus/prometheus/pull/11844)
- \[ENHANCEMENT\] SD: Add VM size label to azure service discovery. [#11650](https://github.com/prometheus/prometheus/pull/11650)
- \[ENHANCEMENT\] Support native histograms in federation. [#11830](https://github.com/prometheus/prometheus/pull/11830)
- \[ENHANCEMENT\] TSDB: Add gauge histogram support. [#11783](https://github.com/prometheus/prometheus/pull/11783) [#11840](https://github.com/prometheus/prometheus/pull/11840)
- \[ENHANCEMENT\] TSDB: Support FloatHistogram. [#11522](https://github.com/prometheus/prometheus/pull/11522) [#11817](https://github.com/prometheus/prometheus/pull/11817)
- \[ENHANCEMENT\] UI: Show individual scrape pools on /targets page. [#11142](https://github.com/prometheus/prometheus/pull/11142)

---

# synapse v1.76.0rc2

> published at 2023-01-27 [source](https://github.com/matrix-org/synapse/releases/tag/v1.76.0rc2)

# Synapse 1.76.0rc2 (2023-01-27)

The 1.76 release is the first to enable faster joins ( [MSC3706](https://github.com/matrix-org/matrix-spec-proposals/pull/3706) and [MSC3902](https://github.com/matrix-org/matrix-spec-proposals/pull/3902)) by default. Admins can opt-out: see [the upgrade notes](https://github.com/matrix-org/synapse/blob/release-v1.76/upgrade.md#faster-joins-are-enabled-by-default) for more details.

The upgrade from 1.75 to 1.76 changes the account data replication streams in a backwards-incompatible manner. Server operators running a multi-worker deployment should consult [the upgrade notes](https://github.com/matrix-org/synapse/blob/release-v1.76/docs/upgrade.md#changes-to-the-account-data-replication-streams).

Those who are `poetry install` ing from source using our lockfile should ensure their poetry version is 1.3.2 or higher; [see upgrade notes](https://github.com/matrix-org/synapse/blob/release-v1.76/docs/upgrade.md#minimum-version-of-poetry-is-now-132).

## Bugfixes

- Faster joins: Fix a bug introduced in Synapse 1.69 where device list EDUs could fail to be handled after a restart when a faster join sync is in progress. ( [#14914](https://github.com/matrix-org/synapse/issues/14914))

## Internal Changes

- Faster joins: Improve performance of looking up partial-state status of rooms. ( [#14917](https://github.com/matrix-org/synapse/issues/14917))

---

# synapse-admin 0.8.6

> published at 2023-01-27 [source](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/0.8.6)

No content.

---

# grafana 8.5.16 (2023-01-25)

> published at 2023-01-27 [source](https://github.com/grafana/grafana/releases/tag/v8.5.16)

[Download page](https://grafana.com/grafana/download/8.5.16)

[What's new highlights](https://grafana.com/docs/grafana/latest/whatsnew/)

### Features and enhancements

- **Chore:** Upgrade Go to 1.19.3. [#58070](https://github.com/grafana/grafana/pull/58070), [@sakjur](https://github.com/sakjur)

---

# grafana 9.3.4 (2023-01-25)

> published at 2023-01-26 [source](https://github.com/grafana/grafana/releases/tag/v9.3.4)

[Download page](https://grafana.com/grafana/download/9.3.4)

[What's new highlights](https://grafana.com/docs/grafana/latest/whatsnew/)

### Features and enhancements

- **Prometheus:** Add default editor configuration. [#61510](https://github.com/grafana/grafana/pull/61510), [@itsmylife](https://github.com/itsmylife)
- **TextPanel:** Refactor to functional component ( [#60885](https://github.com/grafana/grafana/pull/60885)). [#61937](https://github.com/grafana/grafana/pull/61937), [@ryantxu](https://github.com/ryantxu)

### Bug fixes

- **Alerting:** Fix webhook to use correct key for decrypting token. [#61717](https://github.com/grafana/grafana/pull/61717), [@yuri-tceretian](https://github.com/yuri-tceretian)
- **Alerting:** Set error annotation on EvaluationError regardless of underlying error type. [#61506](https://github.com/grafana/grafana/pull/61506), [@alexweav](https://github.com/alexweav)
- **Datasources:** Fix Proxy by UID Failing for UIDs with a Hyphen. [#61723](https://github.com/grafana/grafana/pull/61723), [@csmarchbanks](https://github.com/csmarchbanks)
- **Elasticsearch:** Fix creating of span link with no tags. [#61753](https://github.com/grafana/grafana/pull/61753), [@ivanahuckova](https://github.com/ivanahuckova)
- **Elasticsearch:** Fix failing requests when using SigV4. [#61923](https://github.com/grafana/grafana/pull/61923), [@svennergr](https://github.com/svennergr)
- **Elasticsearch:** Fix toggle-settings are not shown correctly. [#61751](https://github.com/grafana/grafana/pull/61751), [@svennergr](https://github.com/svennergr)
- **Explore:** Be sure time range key bindings are mounted after clear. [#61892](https://github.com/grafana/grafana/pull/61892), [@gelicia](https://github.com/gelicia)
- **Explore:** Unsync time ranges when a pane is closed. [#61369](https://github.com/grafana/grafana/pull/61369), [@Elfo404](https://github.com/Elfo404)
- **Logs:** Lines with long words do not break properly. [#61707](https://github.com/grafana/grafana/pull/61707), [@svennergr](https://github.com/svennergr)
- **Loki:** Fix misaligned derived fields settings. [#61475](https://github.com/grafana/grafana/pull/61475), [@svennergr](https://github.com/svennergr)
- **Query Builder:** Fix max width of input component to prevent overflows. [#61798](https://github.com/grafana/grafana/pull/61798), [@matyax](https://github.com/matyax)
- **Search:** Auto focus input elements. [#61443](https://github.com/grafana/grafana/pull/61443), [@ryantxu](https://github.com/ryantxu)
- **Search:** Fix empty folder message showing when by starred dashboards. [#61610](https://github.com/grafana/grafana/pull/61610), [@eledobleefe](https://github.com/eledobleefe)
- **Table Panel:** Fix image of image cell overflowing table cell and cells ignoring text alignment setting when a data link is added. [#59392](https://github.com/grafana/grafana/pull/59392), [@oscarkilhed](https://github.com/oscarkilhed)

---

# synapse v1.76.0rc1

> published at 2023-01-25 [source](https://github.com/matrix-org/synapse/releases/tag/v1.76.0rc1)

# Synapse 1.76.0rc1 (2023-01-25)

## Features

- Update the default room version to [v10](https://spec.matrix.org/v1.5/rooms/v10/) ( [MSC 3904](https://github.com/matrix-org/matrix-spec-proposals/pull/3904)). Contributed by [@FSG-Cat](https://github.com/FSG-Cat). ( [#14111](https://github.com/matrix-org/synapse/issues/14111))
- Adds a `set_displayname()` method to the module API for setting a user's display name. ( [#14629](https://github.com/matrix-org/synapse/issues/14629))
- Add a dedicated listener configuration for `health` endpoint. ( [#14747](https://github.com/matrix-org/synapse/issues/14747))
- Implement support for [MSC3890](https://github.com/matrix-org/matrix-spec-proposals/pull/3890): Remotely silence local notifications. ( [#14775](https://github.com/matrix-org/synapse/issues/14775))
- Implement experimental support for [MSC3930](https://github.com/matrix-org/matrix-spec-proposals/pull/3930): Push rules for ( [MSC3381](https://github.com/matrix-org/matrix-spec-proposals/pull/3381)) Polls. ( [#14787](https://github.com/matrix-org/synapse/issues/14787))
- Per [MSC3925](https://github.com/matrix-org/matrix-spec-proposals/pull/3925), bundle the whole of the replacement with any edited events, and optionally inhibit server-side replacement. ( [#14811](https://github.com/matrix-org/synapse/issues/14811))
- Faster joins: always serve a partial join response to servers that request it with the stable query param. ( [#14839](https://github.com/matrix-org/synapse/issues/14839))
- Faster joins: allow non-lazy-loading ("eager") syncs to complete after a partial join by omitting partial state rooms until they become fully stated. ( [#14870](https://github.com/matrix-org/synapse/issues/14870))
- Faster joins: request partial joins by default. Admins can opt-out of this for the time being---see the upgrade notes. ( [#14905](https://github.com/matrix-org/synapse/issues/14905))

## Bugfixes

- Add index to improve performance of the `/timestamp_to_event` endpoint used for jumping to a specific date in the timeline of a room. ( [#14799](https://github.com/matrix-org/synapse/issues/14799))
- Fix a long-standing bug where Synapse would exhaust the stack when processing many federation requests where the remote homeserver has disconencted early. ( [#14812](https://github.com/matrix-org/synapse/issues/14812), [#14842](https://github.com/matrix-org/synapse/issues/14842))
- Fix rare races when using workers. ( [#14820](https://github.com/matrix-org/synapse/issues/14820))
- Fix a bug introduced in Synapse 1.64.0 when using room version 10 with frozen events enabled. ( [#14864](https://github.com/matrix-org/synapse/issues/14864))
- Fix a long-standing bug where the `populate_room_stats` background job could fail on broken rooms. ( [#14873](https://github.com/matrix-org/synapse/issues/14873))
- Faster joins: Fix a bug in worker deployments where the room stats and user directory would not get updated when finishing a fast join until another event is sent or received. ( [#14874](https://github.com/matrix-org/synapse/issues/14874))
- Faster joins: Fix incompatibility with joins into restricted rooms where no local users have the ability to invite. ( [#14882](https://github.com/matrix-org/synapse/issues/14882))
- Fix a regression introduced in Synapse 1.69.0 which can result in database corruption when database migrations are interrupted on sqlite. ( [#14910](https://github.com/matrix-org/synapse/issues/14910))

## Updates to the Docker image

- Bump default Python version in the Dockerfile from 3.9 to 3.11. ( [#14875](https://github.com/matrix-org/synapse/issues/14875))

## Improved Documentation

- Include `x_forwarded` entry in the HTTP listener example configs and remove the remaining `worker_main_http_uri` entries. ( [#14667](https://github.com/matrix-org/synapse/issues/14667))
- Remove duplicate commands from the Code Style documentation page; point to the Contributing Guide instead. ( [#14773](https://github.com/matrix-org/synapse/issues/14773))
- Add missing documentation for `tag` to `listeners` section. ( [#14803](https://github.com/matrix-org/synapse/issues/14803))
- Updated documentation in configuration manual for `user_directory.search_all_users`. ( [#14818](https://github.com/matrix-org/synapse/issues/14818))
- Add `worker_manhole` to configuration manual. ( [#14824](https://github.com/matrix-org/synapse/issues/14824))
- Fix the example config missing the `id` field in [application service documentation](https://matrix-org.github.io/synapse/latest/application_services.html). ( [#14845](https://github.com/matrix-org/synapse/issues/14845))
- Minor corrections to the logging configuration documentation. ( [#14868](https://github.com/matrix-org/synapse/issues/14868))
- Document the export user data command. Contributed by [@thezaidbintariq](https://github.com/thezaidbintariq). ( [#14883](https://github.com/matrix-org/synapse/issues/14883))

## Deprecations and Removals

- Poetry 1.3.2 or higher is now required when `poetry install` ing from source. ( [#14860](https://github.com/matrix-org/synapse/issues/14860))

## Internal Changes

- Faster remote room joins (worker mode): do not populate external hosts-in-room cache when sending events as this requires blocking for full state. ( [#14749](https://github.com/matrix-org/synapse/issues/14749))
- Enable Complement tests for Faster Remote Room Joins against worker-mode Synapse. ( [#14752](https://github.com/matrix-org/synapse/issues/14752))
- Add some clarifying comments and refactor a portion of the `Keyring` class for readability. ( [#14804](https://github.com/matrix-org/synapse/issues/14804))
- Add local poetry config files ( `poetry.toml`) to `.gitignore`. ( [#14807](https://github.com/matrix-org/synapse/issues/14807))
- Add missing type hints. ( [#14816](https://github.com/matrix-org/synapse/issues/14816), [#14885](https://github.com/matrix-org/synapse/issues/14885), [#14889](https://github.com/matrix-org/synapse/issues/14889))
- Refactor push tests. ( [#14819](https://github.com/matrix-org/synapse/issues/14819))
- Re-enable some linting that was disabled when we switched to ruff. ( [#14821](https://github.com/matrix-org/synapse/issues/14821))
- Add `cargo fmt` and `cargo clippy` to the lint script. ( [#14822](https://github.com/matrix-org/synapse/issues/14822))
- Drop unused table `presence`. ( [#14825](https://github.com/matrix-org/synapse/issues/14825))
- Merge the two account data and the two device list replication streams. ( [#14826](https://github.com/matrix-org/synapse/issues/14826), [#14833](https://github.com/matrix-org/synapse/issues/14833))
- Faster joins: use stable identifiers from [MSC3706](https://github.com/matrix-org/matrix-spec-proposals/pull/3706). ( [#14832](https://github.com/matrix-org/synapse/issues/14832), [#14841](https://github.com/matrix-org/synapse/issues/14841))
- Add a parameter to control whether the federation client performs a partial state join. ( [#14843](https://github.com/matrix-org/synapse/issues/14843))
- Add check to avoid starting duplicate partial state syncs. ( [#14844](https://github.com/matrix-org/synapse/issues/14844))
- Bump regex from 1.7.0 to 1.7.1. ( [#14848](https://github.com/matrix-org/synapse/issues/14848))
- Add an early return when handling no-op presence updates. ( [#14855](https://github.com/matrix-org/synapse/issues/14855))
- Fix `wait_for_stream_position` to correctly wait for the right instance to advance its token. ( [#14856](https://github.com/matrix-org/synapse/issues/14856), [#14872](https://github.com/matrix-org/synapse/issues/14872))
- Bump peaceiris/actions-gh-pages from 3.9.1 to 3.9.2. ( [#14861](https://github.com/matrix-org/synapse/issues/14861))
- Bump ruff from 0.0.215 to 0.0.224. ( [#14862](https://github.com/matrix-org/synapse/issues/14862))
- Bump types-pillow from 9.4.0.0 to 9.4.0.3. ( [#14863](https://github.com/matrix-org/synapse/issues/14863))
- Always notify replication when a stream advances automatically. ( [#14877](https://github.com/matrix-org/synapse/issues/14877))
- Reduce max time we wait for stream positions. ( [#14881](https://github.com/matrix-org/synapse/issues/14881))
- Bump types-opentracing from 2.4.10 to 2.4.10.1. ( [#14896](https://github.com/matrix-org/synapse/issues/14896))
- Bump ruff from 0.0.224 to 0.0.230. ( [#14897](https://github.com/matrix-org/synapse/issues/14897))
- Bump types-requests from 2.28.11.7 to 2.28.11.8. ( [#14899](https://github.com/matrix-org/synapse/issues/14899))
- Bump types-psycopg2 from 2.9.21.2 to 2.9.21.4. ( [#14900](https://github.com/matrix-org/synapse/issues/14900))
- Bump types-commonmark from 0.9.2 to 0.9.2.1. ( [#14901](https://github.com/matrix-org/synapse/issues/14901))
- Faster joins: allow the resync process more time to fetch `/state` ids. ( [#14912](https://github.com/matrix-org/synapse/issues/14912))

---

# grafana 9.3.2 (2022-12-16)

> published at 2023-01-25 [source](https://github.com/grafana/grafana/releases/tag/v9.3.2)

[Download page](https://grafana.com/grafana/download/9.3.2)

[What's new highlights](https://grafana.com/docs/grafana/latest/whatsnew/)

### Features and enhancements

- **Graphite:** Process multiple queries to Graphite plugin. [#59608](https://github.com/grafana/grafana/pull/59608), [@mmandrus](https://github.com/mmandrus)

### Bug fixes

- **API:** Fix delete user failure due to quota not enabled. [#59875](https://github.com/grafana/grafana/pull/59875), [@papagian](https://github.com/papagian)
- **Accessibility:** Improved keyboard accessibility in BarGauge. [#59382](https://github.com/grafana/grafana/pull/59382), [@lpskdl](https://github.com/lpskdl)
- **Accessibility:** Improved keyboard accessibility in BigValue. [#59830](https://github.com/grafana/grafana/pull/59830), [@lpskdl](https://github.com/lpskdl)
- **Alerting:** Use the QuotaTargetSrv instead of the QuotaTarget in quota check. [#60026](https://github.com/grafana/grafana/pull/60026), [@joeblubaugh](https://github.com/joeblubaugh)
- **AzureMonitor:** Automate location retrieval. [#59602](https://github.com/grafana/grafana/pull/59602), [@aangelisc](https://github.com/aangelisc)
- **AzureMonitor:** Fix bad request when setting dimensions. [#59700](https://github.com/grafana/grafana/pull/59700), [@andresmgot](https://github.com/andresmgot)
- **BarChart:** Fix value mappings. [#60066](https://github.com/grafana/grafana/pull/60066), [@leeoniya](https://github.com/leeoniya)
- **Build:** Streamline and sync dockerfiles. [#58101](https://github.com/grafana/grafana/pull/58101), [@DanCech](https://github.com/DanCech)
- **Build:** Unified dockerfile for all builds. [#59173](https://github.com/grafana/grafana/pull/59173), [@DanCech](https://github.com/DanCech)
- **CloudWatch:** Fix - make sure dimensions are propagated to alert query editor. [#58281](https://github.com/grafana/grafana/pull/58281), [@conorevans](https://github.com/conorevans)
- **Cloudwatch:** Fix deeplink with default region ( [#60260](https://github.com/grafana/grafana/pull/60260)). [#60274](https://github.com/grafana/grafana/pull/60274), [@iwysiu](https://github.com/iwysiu)
- **Command Palette:** Fix not being able to type if triggered whilst another modal is open. [#59728](https://github.com/grafana/grafana/pull/59728), [@ashharrison90](https://github.com/ashharrison90)
- **Command Palette:** Maintain page state when changing theme. [#59787](https://github.com/grafana/grafana/pull/59787), [@ashharrison90](https://github.com/ashharrison90)
- **Dashboards:** Fix 'Make Editable' button not working in Dashboard Settings. [#60306](https://github.com/grafana/grafana/pull/60306), [@joshhunt](https://github.com/joshhunt)
- **Dashboards:** Show error when data source is missing. [#60099](https://github.com/grafana/grafana/pull/60099), [@joshhunt](https://github.com/joshhunt)
- **Datasource:** Fix - apply default query also to queries in new panels. [#59625](https://github.com/grafana/grafana/pull/59625), [@sunker](https://github.com/sunker)
- **Dropdown:** Menu now closes correctly when selecting options on touch devices. [#60181](https://github.com/grafana/grafana/pull/60181), [@ashharrison90](https://github.com/ashharrison90)
- **Influx:** Query segment menus now position correctly near the bottom of the screen. [#60087](https://github.com/grafana/grafana/pull/60087), [@ashharrison90](https://github.com/ashharrison90)
- **Login:** Fix failure to login a new user via an external provider if quota are enabled. [#60015](https://github.com/grafana/grafana/pull/60015), [@papagian](https://github.com/papagian)
- **Loki/Prometheus:** Fix wrong queries executed in split view. [#60172](https://github.com/grafana/grafana/pull/60172), [@svennergr](https://github.com/svennergr)
- **Loki:** Fix wrongly escaped label values when using LabelFilter. [#59812](https://github.com/grafana/grafana/pull/59812), [@svennergr](https://github.com/svennergr)
- **Navigation:** Prevent app crash when importing a dashboard with a uid of `home`. [#59874](https://github.com/grafana/grafana/pull/59874), [@ashharrison90](https://github.com/ashharrison90)
- **Panel Edit:** Fix data links edit icons being off screen when provided title is too long. [#59829](https://github.com/grafana/grafana/pull/59829), [@dprokop](https://github.com/dprokop)
- **Prometheus:** Fix exemplar fill color to match series color in time series. [#59908](https://github.com/grafana/grafana/pull/59908), [@gtk-grafana](https://github.com/gtk-grafana)
- **Prometheus:** Fix exemplars not respecting corresponding series display status. [#59743](https://github.com/grafana/grafana/pull/59743), [@gtk-grafana](https://github.com/gtk-grafana)
- **StateTimeline:** Fix negative infinity legend/tooltip from thresholds. [#60279](https://github.com/grafana/grafana/pull/60279), [@leeoniya](https://github.com/leeoniya)
- **Table:** Fixes row border style not showing and colored rows blending together. [#59660](https://github.com/grafana/grafana/pull/59660), [@torkelo](https://github.com/torkelo)
- **Tempo:** Fix TraceQL autocomplete issues ( [#60058](https://github.com/grafana/grafana/issues/60058)). [#60125](https://github.com/grafana/grafana/pull/60125), [@CrypticSignal](https://github.com/CrypticSignal)
- **TimePicker:** Prevent TimePicker overflowing viewport on small screens. [#59808](https://github.com/grafana/grafana/pull/59808), [@ashharrison90](https://github.com/ashharrison90)
- **TimeRangePicker:** Fix recently ranges only not showing all recent ranges. [#59836](https://github.com/grafana/grafana/pull/59836), [@joshhunt](https://github.com/joshhunt)
- **TimeZonePicker:** Scroll menu correctly when using keyboard controls. [#60008](https://github.com/grafana/grafana/pull/60008), [@ashharrison90](https://github.com/ashharrison90)

---

# grafana 8.5.20

> published at 2023-01-25 [source](https://github.com/grafana/grafana/releases/tag/v8.5.20)

[Download page](https://grafana.com/grafana/download/8.5.20)

[What's new highlights](https://grafana.com/docs/grafana/latest/whatsnew/)

### Features and enhancements

- **Chore:** Upgrade Go to 1.19.4 \[v8.5.x\]. [#60824](https://github.com/grafana/grafana/pull/60824), [@sakjur](https://github.com/sakjur)

---

# grafana 9.2.10 (2023-01-25)

> published at 2023-01-25 [source](https://github.com/grafana/grafana/releases/tag/v9.2.10)

[Download page](https://grafana.com/grafana/download/9.2.10)

[What's new highlights](https://grafana.com/docs/grafana/latest/whatsnew/)

### Features and enhancements

- **TextPanel:** Refactor to functional component ( [#60885](https://github.com/grafana/grafana/pull/60885)). [#61940](https://github.com/grafana/grafana/pull/61940), [@ryantxu](https://github.com/ryantxu)
- **\[v9.2.x\] Chore:** Upgrade Go to 1.19.4. [#60826](https://github.com/grafana/grafana/pull/60826), [@sakjur](https://github.com/sakjur)

### Bug fixes

- **Live:** Fix `Subscription to the channel already exists` live streaming error. [#61420](https://github.com/grafana/grafana/pull/61420), [@grafanabot](https://github.com/grafanabot)
- **Live:** Fix `Subscription to the channel already exists` live streaming error. [#61419](https://github.com/grafana/grafana/pull/61419), [@grafanabot](https://github.com/grafanabot)
- **Live:** Fix `Subscription to the channel already exists` live streaming error. [#61406](https://github.com/grafana/grafana/pull/61406), [@ArturWierzbicki](https://github.com/ArturWierzbicki)

---

# chatgpt v1.3.2

> published at 2023-01-25 [source](https://github.com/matrixgpt/matrix-chatgpt-bot/releases/tag/v1.3.2)

## What's Changed

- Remove MATRIX\_THREADS as it is not currently used anywhere in the code [@bertybuttface](https://github.com/bertybuttface) in [#61](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/61)

**Full Changelog**: [`v1.3.1...v1.3.2`](https://github.com/matrixgpt/matrix-chatgpt-bot/compare/v1.3.1...v1.3.2)

---

# chatgpt v1.3.1

> published at 2023-01-24 [source](https://github.com/matrixgpt/matrix-chatgpt-bot/releases/tag/v1.3.1)

## What's Changed

- Update handlers.ts by [@bertybuttface](https://github.com/bertybuttface) in [#60](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/60)

**Full Changelog**: [`v1.3.0...v1.3.1`](https://github.com/matrixgpt/matrix-chatgpt-bot/compare/v1.3.0...v1.3.1)

---

# chatgpt v1.3.0

> published at 2023-01-24 [source](https://github.com/matrixgpt/matrix-chatgpt-bot/releases/tag/v1.3.0)

## What's Changed

- Descriptions added for all the env variables by [@jcromero](https://github.com/jcromero) in [#53](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/53)
- Code refactor by [@bertybuttface](https://github.com/bertybuttface) in [#56](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/56)
- Bump chatgpt from 3.3.13 to 3.5.1 by [@dependabot](https://github.com/dependabot) in [#58](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/58)

## New Contributors

- [@jcromero](https://github.com/jcromero) made their first contribution in [#53](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/53)

**Full Changelog**: [`v1.2.3...v1.3.0`](https://github.com/matrixgpt/matrix-chatgpt-bot/compare/v1.2.3...v1.3.0)

---

# element v1.11.21-rc.1

> published at 2023-01-24 [source](https://github.com/vector-im/element-web/releases/tag/v1.11.21-rc.1)

## ✨ Features

- Move pin drop out of labs ( [#22993](https://github.com/vector-im/element-web/pull/22993)).
- Quotes for rte ( [#9932](https://github.com/matrix-org/matrix-react-sdk/pull/9932)). Contributed by [@alunturner](https://github.com/alunturner).
- Show the room name in the room header during calls ( [#9942](https://github.com/matrix-org/matrix-react-sdk/pull/9942)). Fixes [#24268](https://github.com/vector-im/element-web/issues/24268).
- Add code blocks to rich text editor ( [#9921](https://github.com/matrix-org/matrix-react-sdk/pull/9921)). Contributed by [@alunturner](https://github.com/alunturner).
- Add new style for inline code ( [#9936](https://github.com/matrix-org/matrix-react-sdk/pull/9936)). Contributed by [@florianduros](https://github.com/florianduros).
- Add disabled button state to rich text editor ( [#9930](https://github.com/matrix-org/matrix-react-sdk/pull/9930)). Contributed by [@alunturner](https://github.com/alunturner).
- Change the rageshake "app" for auto-rageshakes ( [#9909](https://github.com/matrix-org/matrix-react-sdk/pull/9909)).
- Device manager - tweak settings display ( [#9905](https://github.com/matrix-org/matrix-react-sdk/pull/9905)). Contributed by [@kerryarchibald](https://github.com/kerryarchibald).
- Add list functionality to rich text editor ( [#9871](https://github.com/matrix-org/matrix-react-sdk/pull/9871)). Contributed by [@alunturner](https://github.com/alunturner).

## 🐛 Bug Fixes

- Fix RTE focus behaviour in threads ( [#9969](https://github.com/matrix-org/matrix-react-sdk/pull/9969)). Fixes [#23755](https://github.com/vector-im/element-web/issues/23755). Contributed by [@florianduros](https://github.com/florianduros).
- [#22204](https://github.com/vector-im/element-web/issues/22204) Issue: Centered File info in lightbox ( [#9971](https://github.com/matrix-org/matrix-react-sdk/pull/9971)). Fixes [#22204](https://github.com/vector-im/element-web/issues/22204). Contributed by [@Spartan09](https://github.com/Spartan09).
- Fix seekbar position for zero length audio ( [#9949](https://github.com/matrix-org/matrix-react-sdk/pull/9949)). Fixes [#24248](https://github.com/vector-im/element-web/issues/24248).
- Allow thread panel to be closed after being opened from notification ( [#9937](https://github.com/matrix-org/matrix-react-sdk/pull/9937)). Fixes [#23764](https://github.com/vector-im/element-web/issues/23764) [#23852](https://github.com/vector-im/element-web/issues/23852) and [#24213](https://github.com/vector-im/element-web/issues/24213). Contributed by [@justjanne](https://github.com/justjanne).
- Only highlight focused menu item if focus is supposed to be visible ( [#9945](https://github.com/matrix-org/matrix-react-sdk/pull/9945)). Fixes [#23582](https://github.com/vector-im/element-web/issues/23582).
- Prevent call durations from breaking onto multiple lines ( [#9944](https://github.com/matrix-org/matrix-react-sdk/pull/9944)).
- Tweak call lobby buttons to more closely match designs ( [#9943](https://github.com/matrix-org/matrix-react-sdk/pull/9943)).
- Do not show a broadcast as live immediately after the recording has stopped ( [#9947](https://github.com/matrix-org/matrix-react-sdk/pull/9947)). Fixes [#24233](https://github.com/vector-im/element-web/issues/24233).
- Clear the RTE before sending a message ( [#9948](https://github.com/matrix-org/matrix-react-sdk/pull/9948)). Contributed by [@florianduros](https://github.com/florianduros).
- Fix {enter} press in RTE ( [#9927](https://github.com/matrix-org/matrix-react-sdk/pull/9927)). Contributed by [@florianduros](https://github.com/florianduros).
- Fix the problem that the password reset email has to be confirmed twice ( [#9926](https://github.com/matrix-org/matrix-react-sdk/pull/9926)). Fixes [#24226](https://github.com/vector-im/element-web/issues/24226).
- replace .at() with array.length-1 ( [#9933](https://github.com/matrix-org/matrix-react-sdk/pull/9933)). Fixes [matrix-org/element-web-rageshakes#19281](https://github.com/matrix-org/element-web-rageshakes/issues/19281).
- Fix broken threads list timestamp layout ( [#9922](https://github.com/matrix-org/matrix-react-sdk/pull/9922)). Fixes [#24243](https://github.com/vector-im/element-web/issues/24243) and [#24191](https://github.com/vector-im/element-web/issues/24191). Contributed by [@justjanne](https://github.com/justjanne).
- Disable multiple messages when {enter} is pressed multiple times ( [#9929](https://github.com/matrix-org/matrix-react-sdk/pull/9929)). Fixes [#24249](https://github.com/vector-im/element-web/issues/24249). Contributed by [@florianduros](https://github.com/florianduros).
- Fix logout devices when resetting the password ( [#9925](https://github.com/matrix-org/matrix-react-sdk/pull/9925)). Fixes [#24228](https://github.com/vector-im/element-web/issues/24228).
- Fix: Poll replies overflow when not enough space ( [#9924](https://github.com/matrix-org/matrix-react-sdk/pull/9924)). Fixes [#24227](https://github.com/vector-im/element-web/issues/24227). Contributed by [@kerryarchibald](https://github.com/kerryarchibald).
- State event updates are not forwarded to the widget from invitation room ( [#9802](https://github.com/matrix-org/matrix-react-sdk/pull/9802)). Contributed by [@maheichyk](https://github.com/maheichyk).
- Fix error when viewing source of redacted events ( [#9914](https://github.com/matrix-org/matrix-react-sdk/pull/9914)). Fixes [#24165](https://github.com/vector-im/element-web/issues/24165). Contributed by [@clarkf](https://github.com/clarkf).
- Replace outdated css attribute ( [#9912](https://github.com/matrix-org/matrix-react-sdk/pull/9912)). Fixes [#24218](https://github.com/vector-im/element-web/issues/24218). Contributed by [@justjanne](https://github.com/justjanne).
- Clear isLogin theme override when user is no longer viewing login screens ( [#9911](https://github.com/matrix-org/matrix-react-sdk/pull/9911)). Fixes [#23893](https://github.com/vector-im/element-web/issues/23893).
- Fix reply action in message context menu notif & file panels ( [#9895](https://github.com/matrix-org/matrix-react-sdk/pull/9895)). Fixes [#23970](https://github.com/vector-im/element-web/issues/23970).
- Fix issue where thread dropdown would not show up correctly ( [#9872](https://github.com/matrix-org/matrix-react-sdk/pull/9872)). Fixes [#24040](https://github.com/vector-im/element-web/issues/24040). Contributed by [@justjanne](https://github.com/justjanne).
- Fix unexpected composer growing ( [#9889](https://github.com/matrix-org/matrix-react-sdk/pull/9889)). Contributed by [@florianduros](https://github.com/florianduros).
- Fix misaligned timestamps for thread roots which are emotes ( [#9875](https://github.com/matrix-org/matrix-react-sdk/pull/9875)). Fixes [#23897](https://github.com/vector-im/element-web/issues/23897). Contributed by [@justjanne](https://github.com/justjanne).

---

# ansible-core 2.14.2rc1

> published at 2023-01-23 [source](https://pypi.org/project/ansible-core/2.14.2rc1/)

> no description provided

---



---

# dendrite Dendrite 0.11.0

> published at 2023-01-20 [source](https://github.com/matrix-org/dendrite/releases/tag/v0.11.0)

The last three missing federation API Sytests have been fixed - bringing us to **100%** server-server Synapse parity, with client-server parity at **93%** 🎉

### Features

- Added `/_dendrite/admin/purgeRoom/{roomID}` to clean up the database
- The default room version was updated to 10 (contributed by [FSG-Cat](https://github.com/FSG-Cat))

### Fixes

- An oversight in the `create-config` binary, which now correctly sets the media path if specified (contributed by [BieHDC](https://github.com/BieHDC))
- The Helm chart now uses the `$.Chart.AppVersion` as the default image version to pull, with the possibility to override it (contributed by [genofire](https://github.com/genofire))

---

# dendrite helm-dendrite-0.11.0

> published at 2023-01-20 [source](https://github.com/matrix-org/dendrite/releases/tag/helm-dendrite-0.11.0)

Dendrite Matrix Homeserver

---

# hookshot 2.7.0 (2023-01-20)

> published at 2023-01-20 [source](https://github.com/matrix-org/matrix-hookshot/releases/tag/2.7.0)

## Features

- The room configuration widget now features an improved project search component, which now shows project avatars and descriptions. ( [#624](https://github.com/matrix-org/matrix-hookshot/issues/624))

---

# element v1.11.20

> published at 2023-01-20 [source](https://github.com/vector-im/element-web/releases/tag/v1.11.20)

## 🐛 Bug Fixes

- (Part 2) of prevent crash on older browsers (replace .at() with array.length-1)

---

# element v1.11.19

> published at 2023-01-18 [source](https://github.com/vector-im/element-web/releases/tag/v1.11.19)

## 🐛 Bug Fixes

- fix crash on browsers that don't support `Array.at` ( [#9935](https://github.com/matrix-org/matrix-react-sdk/pull/9935)). Contributed by [@andybalaam](https://github.com/andybalaam).

---

# kuma 1.19.6

> published at 2023-01-18 [source](https://github.com/louislam/uptime-kuma/releases/tag/1.19.6)

- Fix: Ping monitor is not working on Windows

---

# element v1.11.18

> published at 2023-01-18 [source](https://github.com/vector-im/element-web/releases/tag/v1.11.18)

## ✨ Features

- Switch threads on for everyone ( [#9879](https://github.com/matrix-org/matrix-react-sdk/pull/9879)).
- Make threads use new Unable to Decrypt UI ( [#9876](https://github.com/matrix-org/matrix-react-sdk/pull/9876)). Fixes [#24060](https://github.com/vector-im/element-web/issues/24060).
- Add edit and remove actions to link in RTE \[Labs\] ( [#9864](https://github.com/matrix-org/matrix-react-sdk/pull/9864)).
- Remove extensible events v1 experimental rendering ( [#9881](https://github.com/matrix-org/matrix-react-sdk/pull/9881)).
- Make create poll dialog scale better (PSG-929) ( [#9873](https://github.com/matrix-org/matrix-react-sdk/pull/9873)). Fixes [#21855](https://github.com/vector-im/element-web/issues/21855).
- Change RTE mode icons ( [#9861](https://github.com/matrix-org/matrix-react-sdk/pull/9861)).
- Device manager - prune client information events after remote sign out ( [#9874](https://github.com/matrix-org/matrix-react-sdk/pull/9874)).
- Check connection before starting broadcast ( [#9857](https://github.com/matrix-org/matrix-react-sdk/pull/9857)).
- Enable sent receipt for poll start events (PSG-962) ( [#9870](https://github.com/matrix-org/matrix-react-sdk/pull/9870)).
- Change clear notifications to have more readable copy ( [#9867](https://github.com/matrix-org/matrix-react-sdk/pull/9867)).
- combine search results when the query is present in multiple successive messages ( [#9855](https://github.com/matrix-org/matrix-react-sdk/pull/9855)). Fixes [#3977](https://github.com/vector-im/element-web/issues/3977). Contributed by [@grimhilt](https://github.com/grimhilt).
- Disable bubbles for broadcasts ( [#9860](https://github.com/matrix-org/matrix-react-sdk/pull/9860)). Fixes [#24140](https://github.com/vector-im/element-web/issues/24140).
- Enable reactions and replies for broadcasts ( [#9856](https://github.com/matrix-org/matrix-react-sdk/pull/9856)). Fixes [#24042](https://github.com/vector-im/element-web/issues/24042).
- Improve switching between rich and plain editing modes ( [#9776](https://github.com/matrix-org/matrix-react-sdk/pull/9776)).
- Redesign the picture-in-picture window ( [#9800](https://github.com/matrix-org/matrix-react-sdk/pull/9800)). Fixes [#23980](https://github.com/vector-im/element-web/issues/23980).
- User on-boarding tasks now appear in a static order. ( [#9799](https://github.com/matrix-org/matrix-react-sdk/pull/9799)). Contributed by [@GoodGuyMarco](https://github.com/GoodGuyMarco).
- Device manager - contextual menus ( [#9832](https://github.com/matrix-org/matrix-react-sdk/pull/9832)).
- If listening a non-live broadcast and changing the room, the broadcast will be paused ( [#9825](https://github.com/matrix-org/matrix-react-sdk/pull/9825)). Fixes [#24078](https://github.com/vector-im/element-web/issues/24078).
- Consider own broadcasts from other device as a playback ( [#9821](https://github.com/matrix-org/matrix-react-sdk/pull/9821)). Fixes [#24068](https://github.com/vector-im/element-web/issues/24068).
- Add link creation to rich text editor ( [#9775](https://github.com/matrix-org/matrix-react-sdk/pull/9775)).
- Add mark as read option in room setting ( [#9798](https://github.com/matrix-org/matrix-react-sdk/pull/9798)). Fixes [#24053](https://github.com/vector-im/element-web/issues/24053).
- Device manager - current device design and copy tweaks ( [#9801](https://github.com/matrix-org/matrix-react-sdk/pull/9801)).
- Unify notifications panel event design ( [#9754](https://github.com/matrix-org/matrix-react-sdk/pull/9754)).
- Add actions for integration manager to send and read certain events ( [#9740](https://github.com/matrix-org/matrix-react-sdk/pull/9740)).
- Device manager - design tweaks ( [#9768](https://github.com/matrix-org/matrix-react-sdk/pull/9768)).
- Change room list sorting to activity and unread first by default ( [#9773](https://github.com/matrix-org/matrix-react-sdk/pull/9773)). Fixes [#24014](https://github.com/vector-im/element-web/issues/24014).
- Add a config flag to enable the rust crypto-sdk ( [#9759](https://github.com/matrix-org/matrix-react-sdk/pull/9759)).
- Improve decryption error UI by consolidating error messages and providing instructions when possible ( [#9544](https://github.com/matrix-org/matrix-react-sdk/pull/9544)). Contributed by [@duxovni](https://github.com/duxovni).
- Honor font settings in Element Call ( [#9751](https://github.com/matrix-org/matrix-react-sdk/pull/9751)). Fixes [#23661](https://github.com/vector-im/element-web/issues/23661).
- Device manager - use deleteAccountData to prune device manager client information events ( [#9734](https://github.com/matrix-org/matrix-react-sdk/pull/9734)).

## 🐛 Bug Fixes

- Display rooms & threads as unread (bold) if threads have unread messages. ( [#9763](https://github.com/matrix-org/matrix-react-sdk/pull/9763)). Fixes [#23907](https://github.com/vector-im/element-web/issues/23907).
- Don't prefer STIXGeneral over the default font ( [#9711](https://github.com/matrix-org/matrix-react-sdk/pull/9711)). Fixes [#23899](https://github.com/vector-im/element-web/issues/23899).
- Use the same avatar colour when creating 1:1 DM rooms ( [#9850](https://github.com/matrix-org/matrix-react-sdk/pull/9850)). Fixes [#23476](https://github.com/vector-im/element-web/issues/23476).
- Fix space lock icon size ( [#9854](https://github.com/matrix-org/matrix-react-sdk/pull/9854)). Fixes [#24128](https://github.com/vector-im/element-web/issues/24128).
- Make calls automatically disconnect if the widget disappears ( [#9862](https://github.com/matrix-org/matrix-react-sdk/pull/9862)). Fixes [#23664](https://github.com/vector-im/element-web/issues/23664).
- Fix emoji in RTE editing ( [#9827](https://github.com/matrix-org/matrix-react-sdk/pull/9827)).
- Fix export with attachments on formats txt and json ( [#9851](https://github.com/matrix-org/matrix-react-sdk/pull/9851)). Fixes [#24130](https://github.com/vector-im/element-web/issues/24130). Contributed by [@grimhilt](https://github.com/grimhilt).
- Fixed empty `Content-Type` for encrypted uploads ( [#9848](https://github.com/matrix-org/matrix-react-sdk/pull/9848)). Contributed by [@K3das](https://github.com/K3das).
- Fix sign-in instead link on password reset page ( [#9820](https://github.com/matrix-org/matrix-react-sdk/pull/9820)). Fixes [#24087](https://github.com/vector-im/element-web/issues/24087).
- The seekbar now initially shows the current position ( [#9796](https://github.com/matrix-org/matrix-react-sdk/pull/9796)). Fixes [#24051](https://github.com/vector-im/element-web/issues/24051).
- Fix: Editing a poll will silently change it to a closed poll ( [#9809](https://github.com/matrix-org/matrix-react-sdk/pull/9809)). Fixes [#23176](https://github.com/vector-im/element-web/issues/23176).
- Make call tiles look less broken in the right panel ( [#9808](https://github.com/matrix-org/matrix-react-sdk/pull/9808)). Fixes [#23716](https://github.com/vector-im/element-web/issues/23716).
- Prevent unnecessary m.direct updates ( [#9805](https://github.com/matrix-org/matrix-react-sdk/pull/9805)). Fixes [#24059](https://github.com/vector-im/element-web/issues/24059).
- Fix checkForPreJoinUISI for thread roots ( [#9803](https://github.com/matrix-org/matrix-react-sdk/pull/9803)). Fixes [#24054](https://github.com/vector-im/element-web/issues/24054).
- Snap in PiP widget when content changed ( [#9797](https://github.com/matrix-org/matrix-react-sdk/pull/9797)). Fixes [#24050](https://github.com/vector-im/element-web/issues/24050).
- Load RTE components only when RTE labs is enabled ( [#9804](https://github.com/matrix-org/matrix-react-sdk/pull/9804)).
- Ensure that events are correctly updated when they are edited. ( [#9789](https://github.com/matrix-org/matrix-react-sdk/pull/9789)).
- When stopping a broadcast also stop the playback ( [#9795](https://github.com/matrix-org/matrix-react-sdk/pull/9795)). Fixes [#24052](https://github.com/vector-im/element-web/issues/24052).
- Prevent to start two broadcasts at the same time ( [#9744](https://github.com/matrix-org/matrix-react-sdk/pull/9744)). Fixes [#23973](https://github.com/vector-im/element-web/issues/23973).
- Correctly handle limited sync responses by resetting the thread timeline ( [#3056](https://github.com/matrix-org/matrix-js-sdk/pull/3056)). Fixes [#23952](https://github.com/vector-im/element-web/issues/23952).
- Fix failure to start in firefox private browser ( [#3058](https://github.com/matrix-org/matrix-js-sdk/pull/3058)). Fixes [#24216](https://github.com/vector-im/element-web/issues/24216).

---

# buscarron v1.3.1

> published at 2023-01-18 [source](https://gitlab.com/etke.cc/buscarron/-/tags/v1.3.1)

### Features ✨

- Shared rate limit across forms

### Bugfixes 🐛

- Do not ban internal IPs

### Misc 💤

- Updated deps
- Enabled vendoring

---

# postmoogle v0.9.11

> published at 2023-01-18 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.11)

bugfixes:

- improve banlist consistency
- resync rooms <-> mailboxes every 5 minutes
- adjust logging (more information on INFO level)
- react only to `m.text` events for commands
- handle multiple emails in the `To` header properly

---

# dendrite helm-dendrite-0.10.9

> published at 2023-01-18 [source](https://github.com/matrix-org/dendrite/releases/tag/helm-dendrite-0.10.9)

Dendrite Matrix Homeserver

---

# dendrite Dendrite 0.10.9

> published at 2023-01-17 [source](https://github.com/matrix-org/dendrite/releases/tag/v0.10.9)

### Features

- Stale device lists are now cleaned up on startup, removing entries for users the server doesn't share a room with anymore
- Dendrite now has its own Helm chart
- Guest access is now handled correctly (disallow joins, kick guests on revocation of guest access, as well as over federation)

### Fixes

- Push rules have seen several tweaks and fixes, which should, for example, fix notifications for `m.read_receipts`
- Outgoing presence will now correctly be sent to newly joined hosts
- Fixes the `/_dendrite/admin/resetPassword/{userID}` admin endpoint to use the correct variable
- Federated backfilling for medium/large rooms has been fixed
- `/login` causing wrong device list updates has been resolved
- `/sync` should now return the correct room summary heroes
- The default config options for `recaptcha_sitekey_class` and `recaptcha_form_field` are now set correctly
- `/messages` now omits empty `state` to be more spec compliant (contributed by [handlerug](https://github.com/handlerug))
- `/sync` has been optimised to only query state events for history visibility if they are really needed

---

# redis 6.2.10

> published at 2023-01-17 [source](https://github.com/redis/redis/releases/tag/6.2.10)

Upgrade urgency: MODERATE, a quick followup fix for a recently released 6.2.9.

# Bug Fixes

- Revert the change to KEYS in the recent client output buffer limit fix ( [#11676](https://github.com/redis/redis/pull/11676))

---

# redis 6.0.17

> published at 2023-01-17 [source](https://github.com/redis/redis/releases/tag/6.0.17)

Upgrade urgency: SECURITY, contains fixes to security issues.

Security Fixes:

- (CVE-2022-35977) Integer overflow in the Redis SETRANGE and SORT/SORT\_RO


  commands can drive Redis to OOM panic

# Bug Fixes

- Avoid hang when client issues long SRANDMEMBER command and gets


  disconnected by client output buffer limit ( [#11676](https://github.com/redis/redis/pull/11676))
- Lua: fix crash on a script call with many arguments, a regression in v6.0.16 ( [#9809](https://github.com/redis/redis/pull/9809))
- Lua: Add checks for min-slave-\* configs when evaluating Lua scripts ( [#10160](https://github.com/redis/redis/pull/10160))
- Fix BITFIELD overflow detection on some compilers due to undefined behavior ( [#9601](https://github.com/redis/redis/pull/9601))

---

# kuma 1.19.5

> published at 2023-01-17 [source](https://github.com/louislam/uptime-kuma/releases/tag/1.19.5)

- [#2575](https://github.com/louislam/uptime-kuma/pull/2575) Add: Splunk Notification Provider (Thanks [@Joseph-Irving](https://github.com/Joseph-Irving))
- Add: a `Help` button which links to the wiki
- Fix: Maintenance count is incorrect
- Fix: Ping monitor issues after 1.9.4
- Fix: Postgres monitor do not handle some error cases correctly
- [#2540](https://github.com/louislam/uptime-kuma/pull/2540) Fix: Allow more MQTT protocols (mqtt, mqtts, ws and wss) for MQTT monitor (Thanks [@twiggotronix](https://github.com/twiggotronix))
- [#2586](https://github.com/louislam/uptime-kuma/pull/2586) Fix: PromoSMS notification is not sent if the message is too long (Thanks [@PopcornPanda](https://github.com/PopcornPanda))
- Fix: Security patch from upstream dependencies

⚠️ I highly recommend that you should upgrade to this version if you are using any old versions of Uptime Kuma, due to the fix of [knex.js](https://github.com/advisories/GHSA-4jv9-3563-23j3)

Thank you all contributors! Bug fix, code refactoring, language files update, code review and more!

(Let me know if I forget to add your name if your pull request have been merged in this version)

[@DimitriDR](https://github.com/DimitriDR) [@Computroniks](https://github.com/Computroniks)

---

# synapse v1.75.0

> published at 2023-01-17 [source](https://github.com/matrix-org/synapse/releases/tag/v1.75.0)

# Synapse 1.75.0 (2023-01-17)

No significant changes since 1.75.0rc2.

# Synapse 1.75.0rc2 (2023-01-12)

## Bugfixes

- Fix a bug introduced in Synapse 1.75.0rc1 where device lists could be miscalculated with some sync filters. ( [#14810](https://github.com/matrix-org/synapse/issues/14810))
- Fix race where calling `/members` or `/state` with an `at` parameter could fail for newly created rooms, when using multiple workers. ( [#14817](https://github.com/matrix-org/synapse/issues/14817))

# Synapse 1.75.0rc1 (2023-01-10)

## Features

- Add a `cached` function to `synapse.module_api` that returns a decorator to cache return values of functions. ( [#14663](https://github.com/matrix-org/synapse/issues/14663))
- Add experimental support for [MSC3391](https://github.com/matrix-org/matrix-spec-proposals/pull/3391) (removing account data). ( [#14714](https://github.com/matrix-org/synapse/issues/14714))
- Support [RFC7636](https://datatracker.ietf.org/doc/html/rfc7636) Proof Key for Code Exchange for OAuth single sign-on. ( [#14750](https://github.com/matrix-org/synapse/issues/14750))
- Support non-OpenID compliant userinfo claims for subject and picture. ( [#14753](https://github.com/matrix-org/synapse/issues/14753))
- Improve performance of `/sync` when filtering all rooms, message types, or senders. ( [#14786](https://github.com/matrix-org/synapse/issues/14786))
- Improve performance of the `/hierarchy` endpoint. ( [#14263](https://github.com/matrix-org/synapse/issues/14263))

## Bugfixes

- Fix the _MAU Limits_ section of the Grafana dashboard relying on a specific `job` name for the workers of a Synapse deployment. ( [#14644](https://github.com/matrix-org/synapse/issues/14644))
- Fix a bug introduced in Synapse 1.70.0 which could cause spurious `UNIQUE constraint failed` errors in the `rotate_notifs` background job. ( [#14669](https://github.com/matrix-org/synapse/issues/14669))
- Ensure stream IDs are always updated after caches get invalidated with workers. Contributed by Nick @ Beeper ( [@Fizzadar](https://github.com/Fizzadar)). ( [#14723](https://github.com/matrix-org/synapse/issues/14723))
- Remove the unspecced `device` field from `/pushrules` responses. ( [#14727](https://github.com/matrix-org/synapse/issues/14727))
- Fix a bug introduced in Synapse 1.73.0 where the `picture_claim` configured under `oidc_providers` was unused (the default value of `"picture"` was used instead). ( [#14751](https://github.com/matrix-org/synapse/issues/14751))
- Unescape HTML entities in URL preview titles making use of oEmbed responses. ( [#14781](https://github.com/matrix-org/synapse/issues/14781))
- Disable sending confirmation email when 3pid is disabled. ( [#14725](https://github.com/matrix-org/synapse/issues/14725))

## Improved Documentation

- Declare support for Python 3.11. ( [#14673](https://github.com/matrix-org/synapse/issues/14673))
- Fix `target_memory_usage` being used in the description for the actual `cache_autotune` sub-option `target_cache_memory_usage`. ( [#14674](https://github.com/matrix-org/synapse/issues/14674))
- Move `email` to Server section in config file documentation. ( [#14730](https://github.com/matrix-org/synapse/issues/14730))
- Fix broken links in the Synapse documentation. ( [#14744](https://github.com/matrix-org/synapse/issues/14744))
- Add missing worker settings to shared configuration documentation. ( [#14748](https://github.com/matrix-org/synapse/issues/14748))
- Document using Twitter as a OAuth 2.0 authentication provider. ( [#14778](https://github.com/matrix-org/synapse/issues/14778))
- Fix Synapse 1.74 upgrade notes to correctly explain how to install pyICU when installing Synapse from PyPI. ( [#14797](https://github.com/matrix-org/synapse/issues/14797))
- Update link to towncrier in contribution guide. ( [#14801](https://github.com/matrix-org/synapse/issues/14801))
- Use `htmltest` to check links in the Synapse documentation. ( [#14743](https://github.com/matrix-org/synapse/issues/14743))

## Internal Changes

- Faster remote room joins: stream the un-partial-stating of events over replication. ( [#14545](https://github.com/matrix-org/synapse/issues/14545), [#14546](https://github.com/matrix-org/synapse/issues/14546))
- Use [ruff](https://github.com/charliermarsh/ruff/) instead of flake8. ( [#14633](https://github.com/matrix-org/synapse/issues/14633), [#14741](https://github.com/matrix-org/synapse/issues/14741))
- Change `handle_new_client_event` signature so that a 429 does not reach clients on `PartialStateConflictError`, and internally retry when needed instead. ( [#14665](https://github.com/matrix-org/synapse/issues/14665))
- Remove dependency on jQuery on reCAPTCHA page. ( [#14672](https://github.com/matrix-org/synapse/issues/14672))
- Faster joins: make `compute_state_after_events` consistent with other state-fetching functions that take a `StateFilter`. ( [#14676](https://github.com/matrix-org/synapse/issues/14676))
- Add missing type hints. ( [#14680](https://github.com/matrix-org/synapse/issues/14680), [#14681](https://github.com/matrix-org/synapse/issues/14681), [#14687](https://github.com/matrix-org/synapse/issues/14687))
- Improve type annotations for the helper methods on a `CachedFunction`. ( [#14685](https://github.com/matrix-org/synapse/issues/14685))
- Check that the SQLite database file exists before porting to PostgreSQL. ( [#14692](https://github.com/matrix-org/synapse/issues/14692))
- Add `.direnv/` directory to .gitignore to prevent local state generated by the [direnv](https://direnv.net/) development tool from being committed. ( [#14707](https://github.com/matrix-org/synapse/issues/14707))
- Batch up replication requests to request the resyncing of remote users's devices. ( [#14716](https://github.com/matrix-org/synapse/issues/14716))
- If debug logging is enabled, log the `msgid` s of any to-device messages that are returned over `/sync`. ( [#14724](https://github.com/matrix-org/synapse/issues/14724))
- Change GHA CI job to follow best practices. ( [#14772](https://github.com/matrix-org/synapse/issues/14772))
- Switch to our fork of `dh-virtualenv` to work around an upstream Python 3.11 incompatibility. ( [#14774](https://github.com/matrix-org/synapse/issues/14774))
- Skip testing built wheels for PyPy 3.7 on Linux x86\_64 as we lack new required dependencies in the build environment. ( [#14802](https://github.com/matrix-org/synapse/issues/14802))

### Dependabot updates

- Bump JasonEtco/create-an-issue from 2.8.1 to 2.8.2. ( [#14693](https://github.com/matrix-org/synapse/issues/14693))
- Bump anyhow from 1.0.66 to 1.0.68. ( [#14694](https://github.com/matrix-org/synapse/issues/14694))
- Bump blake2 from 0.10.5 to 0.10.6. ( [#14695](https://github.com/matrix-org/synapse/issues/14695))
- Bump serde\_json from 1.0.89 to 1.0.91. ( [#14696](https://github.com/matrix-org/synapse/issues/14696))
- Bump serde from 1.0.150 to 1.0.151. ( [#14697](https://github.com/matrix-org/synapse/issues/14697))
- Bump lxml from 4.9.1 to 4.9.2. ( [#14698](https://github.com/matrix-org/synapse/issues/14698))
- Bump types-jsonschema from 4.17.0.1 to 4.17.0.2. ( [#14700](https://github.com/matrix-org/synapse/issues/14700))
- Bump sentry-sdk from 1.11.1 to 1.12.0. ( [#14701](https://github.com/matrix-org/synapse/issues/14701))
- Bump types-setuptools from 65.6.0.1 to 65.6.0.2. ( [#14702](https://github.com/matrix-org/synapse/issues/14702))
- Bump minimum PyYAML to 3.13. ( [#14720](https://github.com/matrix-org/synapse/issues/14720))
- Bump JasonEtco/create-an-issue from 2.8.2 to 2.9.1. ( [#14731](https://github.com/matrix-org/synapse/issues/14731))
- Bump towncrier from 22.8.0 to 22.12.0. ( [#14732](https://github.com/matrix-org/synapse/issues/14732))
- Bump isort from 5.10.1 to 5.11.4. ( [#14733](https://github.com/matrix-org/synapse/issues/14733))
- Bump attrs from 22.1.0 to 22.2.0. ( [#14734](https://github.com/matrix-org/synapse/issues/14734))
- Bump black from 22.10.0 to 22.12.0. ( [#14735](https://github.com/matrix-org/synapse/issues/14735))
- Bump sentry-sdk from 1.12.0 to 1.12.1. ( [#14736](https://github.com/matrix-org/synapse/issues/14736))
- Bump setuptools from 65.3.0 to 65.5.1. ( [#14738](https://github.com/matrix-org/synapse/issues/14738))
- Bump serde from 1.0.151 to 1.0.152. ( [#14758](https://github.com/matrix-org/synapse/issues/14758))
- Bump ruff from 0.0.189 to 0.0.206. ( [#14759](https://github.com/matrix-org/synapse/issues/14759))
- Bump pydantic from 1.10.2 to 1.10.4. ( [#14760](https://github.com/matrix-org/synapse/issues/14760))
- Bump gitpython from 3.1.29 to 3.1.30. ( [#14761](https://github.com/matrix-org/synapse/issues/14761))
- Bump pillow from 9.3.0 to 9.4.0. ( [#14762](https://github.com/matrix-org/synapse/issues/14762))
- Bump types-requests from 2.28.11.5 to 2.28.11.7. ( [#14763](https://github.com/matrix-org/synapse/issues/14763))
- Bump dawidd6/action-download-artifact from 2.24.2 to 2.24.3. ( [#14779](https://github.com/matrix-org/synapse/issues/14779))
- Bump peaceiris/actions-gh-pages from 3.9.0 to 3.9.1. ( [#14791](https://github.com/matrix-org/synapse/issues/14791))
- Bump types-pillow from 9.3.0.4 to 9.4.0.0. ( [#14792](https://github.com/matrix-org/synapse/issues/14792))
- Bump pyopenssl from 22.1.0 to 23.0.0. ( [#14793](https://github.com/matrix-org/synapse/issues/14793))
- Bump types-setuptools from 65.6.0.2 to 65.6.0.3. ( [#14794](https://github.com/matrix-org/synapse/issues/14794))
- Bump importlib-metadata from 4.2.0 to 6.0.0. ( [#14795](https://github.com/matrix-org/synapse/issues/14795))
- Bump ruff from 0.0.206 to 0.0.215. ( [#14796](https://github.com/matrix-org/synapse/issues/14796))

---

# redis 6.2.9

> published at 2023-01-16 [source](https://github.com/redis/redis/releases/tag/6.2.9)

Upgrade urgency: SECURITY, contains fixes to security issues.

Security Fixes:

- (CVE-2022-35977) Integer overflow in the Redis SETRANGE and SORT/SORT\_RO


  commands can drive Redis to OOM panic
- (CVE-2023-22458) Integer overflow in the Redis HRANDFIELD and ZRANDMEMBER


  commands can lead to denial-of-service

# Bug Fixes

- Avoid possible hang when client issues long KEYS, SRANDMEMBER, HRANDFIELD,


  and ZRANDMEMBER commands and gets disconnected by client output buffer limit ( [#11676](https://github.com/redis/redis/pull/11676))
- Fix sentinel issue if replica changes IP ( [#11590](https://github.com/redis/redis/pull/11590))

---

# redis 7.0.8

> published at 2023-01-16 [source](https://github.com/redis/redis/releases/tag/7.0.8)

Upgrade urgency: SECURITY, contains fixes to security issues.

Security Fixes:

- (CVE-2022-35977) Integer overflow in the Redis SETRANGE and SORT/SORT\_RO


  commands can drive Redis to OOM panic
- (CVE-2023-22458) Integer overflow in the Redis HRANDFIELD and ZRANDMEMBER


  commands can lead to denial-of-service

# Bug Fixes

- Avoid possible hang when client issues long KEYS, SRANDMEMBER, HRANDFIELD,


  and ZRANDMEMBER commands and gets disconnected by client output buffer limit ( [#11676](https://github.com/redis/redis/pull/11676))
- Make sure that fork child doesn't do incremental rehashing ( [#11692](https://github.com/redis/redis/pull/11692))
- Fix a bug where blocking commands with a sub-second timeout would block forever ( [#11688](https://github.com/redis/redis/pull/11688))
- Fix sentinel issue if replica changes IP ( [#11590](https://github.com/redis/redis/pull/11590))

---

# hookshot 2.6.1 (2023-01-16)

> published at 2023-01-16 [source](https://github.com/matrix-org/matrix-hookshot/releases/tag/2.6.1)

## Features

- The message in the admin room when creating a webhook now also shows the name and links to the room. ( [#620](https://github.com/matrix-org/matrix-hookshot/issues/620))

## Bugfixes

- Fixed generic webhook 'user is already in the room' error ( [#627](https://github.com/matrix-org/matrix-hookshot/issues/627))
- Hookshot now handles `uk.half-shot.matrix-hookshot.generic.hook` state event updates ( [#628](https://github.com/matrix-org/matrix-hookshot/issues/628))

---

# mautrix-whatsapp v0.8.1

> published at 2023-01-16 [source](https://github.com/mautrix/whatsapp/releases/tag/v0.8.1)

- Added support for sending polls from Matrix to WhatsApp.
- Added config option for requesting more history from phone during login.
- Added support for WhatsApp chat with yourself.
- Fixed deleting portals not working correctly in some cases.

---

# cinny v2.2.3

> published at 2023-01-15 [source](https://github.com/cinnyapp/cinny/releases/tag/v2.2.3)

## What's Changed

- Remove MSC3244 use from restricted room creation by [@mjarr](https://github.com/mjarr) in [#892](https://github.com/cinnyapp/cinny/pull/892)
- Add `accept` attribute to `image/*` to the `ImageUpload` component by [@Steffo99](https://github.com/Steffo99) in [#989](https://github.com/cinnyapp/cinny/pull/989)
- Vite plugin to add svg as inline data by [@ajbura](https://github.com/ajbura) in [#1072](https://github.com/cinnyapp/cinny/pull/1072)
- Add jsdelivr cdn for twemoji

**Full Changelog**: [`v2.2.2...v2.2.3`](https://github.com/cinnyapp/cinny/compare/v2.2.2...v2.2.3)

---

# rageshake v1.8

> published at 2023-01-13 [source](https://github.com/matrix-org/rageshake/releases/tag/v1.8)

Features

Add config option to block unknown appplication names. ( [#67](https://github.com/matrix-org/rageshake/pull/67))

Internal Changes

Reimplement buildkite linting and changelog in GHA. ( [#64](https://github.com/matrix-org/rageshake/pull/64))

---

# hookshot 2.6.0 (2023-01-13)

> published at 2023-01-13 [source](https://github.com/matrix-org/matrix-hookshot/releases/tag/2.6.0)

## Features

- Add support for **experimental** end-to-bridge encryption via MSC3202. ( [#299](https://github.com/matrix-org/matrix-hookshot/issues/299))
- Add support for additional bot users called "service bots" which handle a particular connection type, so that different services can be used through different bot users. ( [#573](https://github.com/matrix-org/matrix-hookshot/issues/573))
- Add new GitHubRepo connection config setting `workflowRun.workflows` to filter run reports by workflow name. ( [#588](https://github.com/matrix-org/matrix-hookshot/issues/588))
- The GitHub/GitLab connection state configuration has changed. The configuration option `ignoreHooks` is now deprecated, and new connections may not use this options.


  Users should instead explicitly configure all the hooks they want to enable with the `enableHooks` option. Existing connections will continue to work with both options. ( [#592](https://github.com/matrix-org/matrix-hookshot/issues/592))
- A11y: Add alt tags to all images. ( [#602](https://github.com/matrix-org/matrix-hookshot/issues/602))

## Bugfixes

- Parent projects are now taken into account when calculating a user's access level to a GitLab project. ( [#539](https://github.com/matrix-org/matrix-hookshot/issues/539))
- Ensure bridge treats published and drafted GitHub releases as different events. ( [#582](https://github.com/matrix-org/matrix-hookshot/issues/582))
- Fix a bug where unknown keys in a connections state would be clobbered when updated via widget UI. ( [#587](https://github.com/matrix-org/matrix-hookshot/issues/587))
- Improve webhook code editor performance. ( [#601](https://github.com/matrix-org/matrix-hookshot/issues/601))
- Correctly apply CSS for recent RSS feed changes. ( [#604](https://github.com/matrix-org/matrix-hookshot/issues/604))
- Improve startup stability by not loading all room state at once. ( [#614](https://github.com/matrix-org/matrix-hookshot/issues/614))
- You can now add multiple GitLab connections to the same room with the same project path, if they are under different instances. ( [#617](https://github.com/matrix-org/matrix-hookshot/issues/617))

## Improved Documentation

- Clarify GitLab setup docs ( [#350](https://github.com/matrix-org/matrix-hookshot/issues/350))
- Change URL protocol in the ocumentation and sample configs to HTTPS. ( [#623](https://github.com/matrix-org/matrix-hookshot/issues/623))

## Deprecations and Removals

- Remove support for Pantalaimon-based encryption. ( [#299](https://github.com/matrix-org/matrix-hookshot/issues/299))

## Internal Changes

- RSS feed polling now uses cache headers sent by servers, which should mean we will be more conservative on resources. ( [#583](https://github.com/matrix-org/matrix-hookshot/issues/583))
- Only build ARM images when merging or releasing, due to slow ARM build times. ( [#589](https://github.com/matrix-org/matrix-hookshot/issues/589))
- Increase maximum size of incoming webhook payload from `100kb` to `10mb`. ( [#606](https://github.com/matrix-org/matrix-hookshot/issues/606))
- Mark encryption feature as experimental (config option is now `experimentalEncryption`). ( [#610](https://github.com/matrix-org/matrix-hookshot/issues/610))
- Cache yarn dependencies during Docker build. ( [#615](https://github.com/matrix-org/matrix-hookshot/issues/615))

---

# jitsi stable-8218: release

> published at 2023-01-13 [source](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-8218)

release

\\* [`8d7728b`](https://github.com/jitsi/docker-jitsi-meet/commit/8d7728b9dc2768ea75da85f621c2fe27b7b60da9) jibri: update Chrome to M108

\\* [`9cfbaf2`](https://github.com/jitsi/docker-jitsi-meet/commit/9cfbaf2d64026649bc971d2ad92a11d028a909fc) misc: drop JICOFO\_AUTH\_USER

\\* [`68751c2`](https://github.com/jitsi/docker-jitsi-meet/commit/68751c27f2e81c3ee7798cad60a7f6571af1eaab) prosody: add metadata component

\\* [`07f7054`](https://github.com/jitsi/docker-jitsi-meet/commit/07f7054bad786664e35944788c142488e0fc2ee1) jaas: pass the jitsi installation type at provisioning ( [#1456](https://github.com/jitsi/docker-jitsi-meet/pull/1456))

\\* [`e219bcf`](https://github.com/jitsi/docker-jitsi-meet/commit/e219bcfc21a3d00a0b79147a4955f17b640e5aaa) web: add ability to configure whiteboard

\\* [`ac12313`](https://github.com/jitsi/docker-jitsi-meet/commit/ac123134ea206d363d5949b3bc0177a4ef28293f) misc: working on unstable

---

# synapse v1.75.0rc2

> published at 2023-01-12 [source](https://github.com/matrix-org/synapse/releases/tag/v1.75.0rc2)

# Synapse 1.75.0rc2 (2023-01-12)

## Bugfixes

- Fix a bug introduced in Synapse 1.75.0rc1 where device lists could be miscalculated with some sync filters. ( [#14810](https://github.com/matrix-org/synapse/issues/14810))
- Fix race where calling `/members` or `/state` with an `at` parameter could fail for newly created rooms, when using multiple workers. ( [#14817](https://github.com/matrix-org/synapse/issues/14817))

---

# chatgpt v1.2.3

> published at 2023-01-12 [source](https://github.com/matrixgpt/matrix-chatgpt-bot/releases/tag/v1.2.3)

## What's Changed

- Add MATRIX\_PREFIX\_DM feature by [@bertybuttface](https://github.com/bertybuttface) in [#50](https://github.com/matrixgpt/matrix-chatgpt-bot/pull/50)

This will let you talk directly to the bot via DM without using a prefix. It defaults to false (no prefix required for DMs).

**Full Changelog**: [`v1.2.2...v1.2.3`](https://github.com/matrixgpt/matrix-chatgpt-bot/compare/v1.2.2...v1.2.3)

---

# registration v0.9.2.dev3

> published at 2023-01-11 [source](https://github.com/zeratax/matrix-registration/releases/tag/v0.9.2.dev3)

No content.

---

# mjolnir v1.6.4

> published at 2023-01-11 [source](https://github.com/matrix-org/mjolnir/releases/tag/v1.6.4)

This is a bugfix release.

# ChangeLog

## Bot

- Bugfix: In Mjölnir-for-all, make sure that config.bot.displayName is always set, by [@Yoric](https://github.com/Yoric) in [`9693149`](https://github.com/matrix-org/mjolnir/commit/9693149e1e0010812cbc2bb88a592ecd8dd921cb)

---

# mjolnir v1.6.3

> published at 2023-01-11 [source](https://github.com/matrix-org/mjolnir/releases/tag/v1.6.3)

# ChangeLog

## Bot

- Bugfix: `!mjolnir config get` was broken, should now be fixed by [@jesopo](https://github.com/jesopo) in [`5824539`](https://github.com/matrix-org/mjolnir/commit/58245394495dc79832d1c4da946b5a1b543f29f7).
- Feature: Support for decentralized abuse report (MSC3215). Use command `!mjolnir rooms setup <room alias/ID> reporting` to setup a room so that users can decide to send abuse reports to moderators (who can read the offending messages) _instead of_ homeserver administrators (who typically cannot) by [@Yoric](https://github.com/Yoric) in [`fa5fbee`](https://github.com/matrix-org/mjolnir/commit/fa5fbee229883397118fa2cbd963f4c01a58167c), [`5b509a2`](https://github.com/matrix-org/mjolnir/commit/5b509a226adc905c8b3763d397114d3a3f126c2b). Sending an abuse report this way currently requires Element Web Develop with Labs feature `report_to_moderator`.
- Feature: Mjölnir-for-all bot now has a nicer name and is easier to invite by [@Yoric](https://github.com/Yoric) in [`d83127e`](https://github.com/matrix-org/mjolnir/commit/d83127ea8c30aefdc086f162800c3f1936568baa).

## Hosting

- Feature: Early support for OpenMetrics/Prometheus. If you are hosting your Mjölnir, this will let you monitor e.g. CPU usage, memory usage, number of Matrix errors, ... Additional probes may be added in the future. Off by default. By [@Yoric](https://github.com/Yoric) in [`c3cb22b`](https://github.com/matrix-org/mjolnir/commit/c3cb22bf3673b981390285e71ea9b410d7d26cb6)

## Security

- Bumping up dependency `json5` to 1.0.2 by [@dependabot](https://github.com/dependabot) in [`1451ac9`](https://github.com/matrix-org/mjolnir/commit/1451ac99513976bf137ca6e54fd0dd6becaccce7)

---

# synapse v1.75.0rc1

> published at 2023-01-10 [source](https://github.com/matrix-org/synapse/releases/tag/v1.75.0rc1)

# Synapse 1.75.0rc1 (2023-01-10)

## Features

- Add a `cached` function to `synapse.module_api` that returns a decorator to cache return values of functions. ( [#14663](https://github.com/matrix-org/synapse/issues/14663))
- Add experimental support for [MSC3391](https://github.com/matrix-org/matrix-spec-proposals/pull/3391) (removing account data). ( [#14714](https://github.com/matrix-org/synapse/issues/14714))
- Support [RFC7636](https://datatracker.ietf.org/doc/html/rfc7636) Proof Key for Code Exchange for OAuth single sign-on. ( [#14750](https://github.com/matrix-org/synapse/issues/14750))
- Support non-OpenID compliant userinfo claims for subject and picture. ( [#14753](https://github.com/matrix-org/synapse/issues/14753))
- Improve performance of `/sync` when filtering all rooms, message types, or senders. ( [#14786](https://github.com/matrix-org/synapse/issues/14786))
- Improve performance of the `/hierarchy` endpoint. ( [#14263](https://github.com/matrix-org/synapse/issues/14263))

## Bugfixes

- Fix the _MAU Limits_ section of the Grafana dashboard relying on a specific `job` name for the workers of a Synapse deployment. ( [#14644](https://github.com/matrix-org/synapse/issues/14644))
- Fix a bug introduced in Synapse 1.70.0 which could cause spurious `UNIQUE constraint failed` errors in the `rotate_notifs` background job. ( [#14669](https://github.com/matrix-org/synapse/issues/14669))
- Ensure stream IDs are always updated after caches get invalidated with workers. Contributed by Nick @ Beeper ( [@Fizzadar](https://github.com/Fizzadar)). ( [#14723](https://github.com/matrix-org/synapse/issues/14723))
- Remove the unspecced `device` field from `/pushrules` responses. ( [#14727](https://github.com/matrix-org/synapse/issues/14727))
- Fix a bug introduced in Synapse 1.73.0 where the `picture_claim` configured under `oidc_providers` was unused (the default value of `"picture"` was used instead). ( [#14751](https://github.com/matrix-org/synapse/issues/14751))
- Unescape HTML entities in URL preview titles making use of oEmbed responses. ( [#14781](https://github.com/matrix-org/synapse/issues/14781))
- Disable sending confirmation email when 3pid is disabled. ( [#14725](https://github.com/matrix-org/synapse/issues/14725))

## Improved Documentation

- Declare support for Python 3.11. ( [#14673](https://github.com/matrix-org/synapse/issues/14673))
- Fix `target_memory_usage` being used in the description for the actual `cache_autotune` sub-option `target_cache_memory_usage`. ( [#14674](https://github.com/matrix-org/synapse/issues/14674))
- Move `email` to Server section in config file documentation. ( [#14730](https://github.com/matrix-org/synapse/issues/14730))
- Fix broken links in the Synapse documentation. ( [#14744](https://github.com/matrix-org/synapse/issues/14744))
- Add missing worker settings to shared configuration documentation. ( [#14748](https://github.com/matrix-org/synapse/issues/14748))
- Document using Twitter as a OAuth 2.0 authentication provider. ( [#14778](https://github.com/matrix-org/synapse/issues/14778))
- Fix Synapse 1.74 upgrade notes to correctly explain how to install pyICU when installing Synapse from PyPI. ( [#14797](https://github.com/matrix-org/synapse/issues/14797))
- Update link to towncrier in contribution guide. ( [#14801](https://github.com/matrix-org/synapse/issues/14801))
- Use `htmltest` to check links in the Synapse documentation. ( [#14743](https://github.com/matrix-org/synapse/issues/14743))

## Internal Changes

- Faster remote room joins: stream the un-partial-stating of events over replication. ( [#14545](https://github.com/matrix-org/synapse/issues/14545), [#14546](https://github.com/matrix-org/synapse/issues/14546))
- Use [ruff](https://github.com/charliermarsh/ruff/) instead of flake8. ( [#14633](https://github.com/matrix-org/synapse/issues/14633), [#14741](https://github.com/matrix-org/synapse/issues/14741))
- Change `handle_new_client_event` signature so that a 429 does not reach clients on `PartialStateConflictError`, and internally retry when needed instead. ( [#14665](https://github.com/matrix-org/synapse/issues/14665))
- Remove dependency on jQuery on reCAPTCHA page. ( [#14672](https://github.com/matrix-org/synapse/issues/14672))
- Faster joins: make `compute_state_after_events` consistent with other state-fetching functions that take a `StateFilter`. ( [#14676](https://github.com/matrix-org/synapse/issues/14676))
- Add missing type hints. ( [#14680](https://github.com/matrix-org/synapse/issues/14680), [#14681](https://github.com/matrix-org/synapse/issues/14681), [#14687](https://github.com/matrix-org/synapse/issues/14687))
- Improve type annotations for the helper methods on a `CachedFunction`. ( [#14685](https://github.com/matrix-org/synapse/issues/14685))
- Check that the SQLite database file exists before porting to PostgreSQL. ( [#14692](https://github.com/matrix-org/synapse/issues/14692))
- Add `.direnv/` directory to .gitignore to prevent local state generated by the [direnv](https://direnv.net/) development tool from being committed. ( [#14707](https://github.com/matrix-org/synapse/issues/14707))
- Batch up replication requests to request the resyncing of remote users's devices. ( [#14716](https://github.com/matrix-org/synapse/issues/14716))
- If debug logging is enabled, log the `msgid` s of any to-device messages that are returned over `/sync`. ( [#14724](https://github.com/matrix-org/synapse/issues/14724))
- Change GHA CI job to follow best practices. ( [#14772](https://github.com/matrix-org/synapse/issues/14772))
- Switch to our fork of `dh-virtualenv` to work around an upstream Python 3.11 incompatibility. ( [#14774](https://github.com/matrix-org/synapse/issues/14774))
- Skip testing built wheels for PyPy 3.7 on Linux x86\_64 as we lack new required dependencies in the build environment. ( [#14802](https://github.com/matrix-org/synapse/issues/14802))

### Dependabot updates

- Bump JasonEtco/create-an-issue from 2.8.1 to 2.8.2. ( [#14693](https://github.com/matrix-org/synapse/issues/14693))
- Bump anyhow from 1.0.66 to 1.0.68. ( [#14694](https://github.com/matrix-org/synapse/issues/14694))
- Bump blake2 from 0.10.5 to 0.10.6. ( [#14695](https://github.com/matrix-org/synapse/issues/14695))
- Bump serde\_json from 1.0.89 to 1.0.91. ( [#14696](https://github.com/matrix-org/synapse/issues/14696))
- Bump serde from 1.0.150 to 1.0.151. ( [#14697](https://github.com/matrix-org/synapse/issues/14697))
- Bump lxml from 4.9.1 to 4.9.2. ( [#14698](https://github.com/matrix-org/synapse/issues/14698))
- Bump types-jsonschema from 4.17.0.1 to 4.17.0.2. ( [#14700](https://github.com/matrix-org/synapse/issues/14700))
- Bump sentry-sdk from 1.11.1 to 1.12.0. ( [#14701](https://github.com/matrix-org/synapse/issues/14701))
- Bump types-setuptools from 65.6.0.1 to 65.6.0.2. ( [#14702](https://github.com/matrix-org/synapse/issues/14702))
- Bump minimum PyYAML to 3.13. ( [#14720](https://github.com/matrix-org/synapse/issues/14720))
- Bump JasonEtco/create-an-issue from 2.8.2 to 2.9.1. ( [#14731](https://github.com/matrix-org/synapse/issues/14731))
- Bump towncrier from 22.8.0 to 22.12.0. ( [#14732](https://github.com/matrix-org/synapse/issues/14732))
- Bump isort from 5.10.1 to 5.11.4. ( [#14733](https://github.com/matrix-org/synapse/issues/14733))
- Bump attrs from 22.1.0 to 22.2.0. ( [#14734](https://github.com/matrix-org/synapse/issues/14734))
- Bump black from 22.10.0 to 22.12.0. ( [#14735](https://github.com/matrix-org/synapse/issues/14735))
- Bump sentry-sdk from 1.12.0 to 1.12.1. ( [#14736](https://github.com/matrix-org/synapse/issues/14736))
- Bump setuptools from 65.3.0 to 65.5.1. ( [#14738](https://github.com/matrix-org/synapse/issues/14738))
- Bump serde from 1.0.151 to 1.0.152. ( [#14758](https://github.com/matrix-org/synapse/issues/14758))
- Bump ruff from 0.0.189 to 0.0.206. ( [#14759](https://github.com/matrix-org/synapse/issues/14759))
- Bump pydantic from 1.10.2 to 1.10.4. ( [#14760](https://github.com/matrix-org/synapse/issues/14760))
- Bump gitpython from 3.1.29 to 3.1.30. ( [#14761](https://github.com/matrix-org/synapse/issues/14761))
- Bump pillow from 9.3.0 to 9.4.0. ( [#14762](https://github.com/matrix-org/synapse/issues/14762))
- Bump types-requests from 2.28.11.5 to 2.28.11.7. ( [#14763](https://github.com/matrix-org/synapse/issues/14763))
- Bump dawidd6/action-download-artifact from 2.24.2 to 2.24.3. ( [#14779](https://github.com/matrix-org/synapse/issues/14779))
- Bump peaceiris/actions-gh-pages from 3.9.0 to 3.9.1. ( [#14791](https://github.com/matrix-org/synapse/issues/14791))
- Bump types-pillow from 9.3.0.4 to 9.4.0.0. ( [#14792](https://github.com/matrix-org/synapse/issues/14792))
- Bump pyopenssl from 22.1.0 to 23.0.0. ( [#14793](https://github.com/matrix-org/synapse/issues/14793))
- Bump types-setuptools from 65.6.0.2 to 65.6.0.3. ( [#14794](https://github.com/matrix-org/synapse/issues/14794))
- Bump importlib-metadata from 4.2.0 to 6.0.0. ( [#14795](https://github.com/matrix-org/synapse/issues/14795))
- Bump ruff from 0.0.206 to 0.0.215. ( [#14796](https://github.com/matrix-org/synapse/issues/14796))

---

# languagetool v6.0-dockerupdate-2

> published at 2023-01-10 [source](https://github.com/Erikvl87/docker-languagetool/releases/tag/v6.0-dockerupdate-2)

Fix missing libhunspell.so package for arm64

---

# kuma 1.19.4

> published at 2023-01-09 [source](https://github.com/louislam/uptime-kuma/releases/tag/1.19.4)

- [#2527](https://github.com/louislam/uptime-kuma/pull/2527) Fix: \[Docker Monitor\] Incorrect handling for container down (Thanks [@chakflying](https://github.com/chakflying))
- [#2528](https://github.com/louislam/uptime-kuma/pull/2528) Fix: Add tag issue (Thanks [@chakflying](https://github.com/chakflying))
- [#2569](https://github.com/louislam/uptime-kuma/pull/2569) Fix: Add validation of `Keep Monitor History Days` (Thanks [@Computroniks](https://github.com/Computroniks))
- [#2223](https://github.com/louislam/uptime-kuma/pull/2223) Improvement: Better support for ping monitor on different Linux distros (Thanks [@Computroniks](https://github.com/Computroniks))
- [#2543](https://github.com/louislam/uptime-kuma/pull/2543) Improvement: Remove redundant title in Pushover notification (Thanks [@SlothCroissant](https://github.com/SlothCroissant))
- Docker: Update Apprise from 1.2.0 to 1.2.1 ( [Changelog](https://github.com/caronc/apprise/releases/tag/v1.2.1))

Thank you all contributors! Bug fix, code refactoring, language files update, code review and more!

(Let me know if I forget to add your name if your pull request have been merged in this version)

[@furkanipek](https://github.com/furkanipek) [@SirMorfield](https://github.com/SirMorfield) [@Computroniks](https://github.com/Computroniks) [@deluxghost](https://github.com/deluxghost) [@MrEddX](https://github.com/MrEddX) [@DimitriDR](https://github.com/DimitriDR)

---

# dendrite helm-dendrite-0.10.8

> published at 2023-01-06 [source](https://github.com/matrix-org/dendrite/releases/tag/helm-dendrite-0.10.8)

Dendrite Matrix Homeserver

---

# heisenbridge v1.14.1

> published at 2023-01-06 [source](https://github.com/hifi/heisenbridge/releases/tag/v1.14.1)

No content.

---

# kuma 1.19.3

> published at 2023-01-03 [source](https://github.com/louislam/uptime-kuma/releases/tag/1.19.3)

- Add Hebrew translation (Thanks [@thefourcraft](https://github.com/thefourcraft))
- Add Zoho Cliq notification (Thanks [@pbaris](https://github.com/pbaris))
- Add Kook notification (Thanks @YehowahLiu)
- Fix MS SQL monitors interfere with each other
- Fix gRPC check throws errors when response data size > 50 chars (Thanks [@mathiash98](https://github.com/mathiash98))
- Fix Slack notification do not show message correctly (Thanks [@pruekk](https://github.com/pruekk))
- Fix same maintenance message be shown multiple times in a status page

Thank you all contributors! Bug fix, code refactoring, language files update, code review and more!

(Let me know if I forget to add your name if your pull request have been merged in this version)

[@DimitriDR](https://github.com/DimitriDR) [@mishankov](https://github.com/mishankov) [@5idereal](https://github.com/5idereal) [@Mikkel-T](https://github.com/Mikkel-T) [@lionep](https://github.com/lionep)

---

# dynamic-dns v3.10.0-ls107

> published at 2023-01-03 [source](https://github.com/linuxserver/docker-ddclient/releases/tag/v3.10.0-ls107)

**LinuxServer Changes:**

Update build instructions for 3.10.0. Update default `ddclient.conf`.

**ddclient Changes:**

**Note**

Please also read the changelog for v3.10.0\_2 and v3.10.0\_1 which contains dependency changes.

### New features

- Added support for domaindiscount24.com
- Added support for njal.la

---

# corporal 2.5.1

> published at 2023-01-02 [source](https://github.com/devture/matrix-corporal/releases/tag/2.5.1)

Release 2.5.1

---

# mjolnir v1.6.2

> published at 2023-01-02 [source](https://github.com/matrix-org/mjolnir/releases/tag/v1.6.2)

# Changelog

## Year

- We are now in 2023. Happy New Year to all!

## Bot

- **Change of behavior** The bot can now start even if it doesn't manage to resolve some policy rooms. Note that this situation is not ideal, because a broken policy room means that your protections are most likely broken, but at least users are now able to use Mjölnir to fix that list by [@Yoric](https://github.com/Yoric) in [`433ff7e`](https://github.com/matrix-org/mjolnir/commit/433ff7eadd025a286595a636a5f0672af1c3222b).
- **Change of behavior** Setting `autojoinOnlyIfManager` is now `true` by default by [@Gnuxie](https://github.com/Gnuxie) in [`1d3da94`](https://github.com/matrix-org/mjolnir/commit/1d3da94f384a05a7ade5f3ca9fe7b797fc4ef8da).
- **Performance improvement** Changing the policy sync algorithm means that we need fewer communications with the homeserver whenever there are policy list changes by [@Gnuxie](https://github.com/Gnuxie) in [`704bb66`](https://github.com/matrix-org/mjolnir/commit/704bb660c2d01be05d9c6c7fb787dee0730192e7).

## Monitoring

- Mjölnir now supports sending events and alerts to a Sentry server by [@Yoric](https://github.com/Yoric) in [`2915757`](https://github.com/matrix-org/mjolnir/commit/2915757b7d04308848061d4c048a9ee827fea9aa).

## Dependencies

- Bump express 4.17.3 in [`dafbd20`](https://github.com/matrix-org/mjolnir/commit/dafbd203934acfe9a3e6d7f98aea73e19dc28068)

---

# languagetool v6.0-dockerupdate-1

> published at 2023-01-01 [source](https://github.com/Erikvl87/docker-languagetool/releases/tag/v6.0-dockerupdate-1)

[#60](https://github.com/Erikvl87/docker-languagetool/issues/60) Fix error loading shared library ld-linux-x86-64.so.2

---

# languagetool v6.0

> published at 2022-12-30 [source](https://github.com/Erikvl87/docker-languagetool/releases/tag/v6.0)

[#26](https://github.com/Erikvl87/docker-languagetool/issues/26) Upgrade GitHub actions to run on Ubuntu 22.04

---

# kuma 1.19.2

> published at 2022-12-26 [source](https://github.com/louislam/uptime-kuma/releases/tag/1.19.2)

- Fix the UI broken after removed a monitor

---

# kuma 1.19.1

> published at 2022-12-26 [source](https://github.com/louislam/uptime-kuma/releases/tag/1.19.1)

- Fix HTTP Monitor issues occurred in 1.19.0
- Fix Docker monitor cannot send notifications (Known platform: Discord) ( [@zImPatrick](https://github.com/zImPatrick))

Thank you all contributors! Bug fix, code refactoring, language files update, code review and more!

(Let me know if I forget to add your name if your pull request have been merged in this version)

[@augustin64](https://github.com/augustin64)

---

# ntfy v1.30.1

> published at 2022-12-23 [source](https://github.com/binwiederhier/ntfy/releases/tag/v1.30.1)

This is a special holiday edition version of ntfy, with all sorts of holiday fun and games, and hidden quests.

Nahh, just kidding. This release is an intermediate release mainly to eliminate warnings in the logs, so I can

roll out the TLSv1.3, HTTP/2 and Unix mode changes on ntfy.sh (see [#552](https://github.com/binwiederhier/ntfy/issues/552)).

**Features:**

- Web: Generate random topic name button ( [#453](https://github.com/binwiederhier/ntfy/issues/453), thanks to [@yardenshoham](https://github.com/yardenshoham))
- Add [Gitpod config](https://github.com/binwiederhier/ntfy/blob/main/.gitpod.yml) ( [#540](https://github.com/binwiederhier/ntfy/pull/540), thanks to [@yardenshoham](https://github.com/yardenshoham))

**Bug fixes + maintenance:**

- Remove `--env-topic` option from `ntfy publish` as per [deprecation](/binwiederhier/ntfy/blob/v1.30.1/deprecations.md) (no ticket)
- Prepared statements for message cache writes ( [#542](https://github.com/binwiederhier/ntfy/pull/542), thanks to [@nicois](https://github.com/nicois))
- Do not warn about invalid IP address when behind proxy in unix socket mode (relates to [#552](https://github.com/binwiederhier/ntfy/issues/552))

---

# ntfy v1.29.1

> published at 2022-12-23 [source](https://github.com/binwiederhier/ntfy/releases/tag/v1.29.1)

This is mostly a bugfix release to address the high load on ntfy.sh. There are now two new options that allow

synchronous batch-writing of messages to the cache. This avoids database locking, and subsequent pileups of waiting

requests.

**Bug fixes:**

- High-load servers: Allow asynchronous batch-writing of messages to cache via `cache-batch-*` options ( [#498](https://github.com/binwiederhier/ntfy/issues/498)/ [#502](https://github.com/binwiederhier/ntfy/pull/502))
- Sender column in cache.db shows invalid IP ( [#503](https://github.com/binwiederhier/ntfy/issues/503))

**Documentation:**

- GitHub Actions example ( [#492](https://github.com/binwiederhier/ntfy/pull/492), thanks to [@ksurl](https://github.com/ksurl))
- UnifiedPush ACL clarification ( [#497](https://github.com/binwiederhier/ntfy/issues/497), thanks to [@bt90](https://github.com/bt90))
- Install instructions for Kustomize ( [#463](https://github.com/binwiederhier/ntfy/pull/463), thanks to [@l-maciej](https://github.com/l-maciej))

**Other things:**

- Put ntfy.sh docs on GitHub pages to reduce AWS outbound traffic cost ( [#491](https://github.com/binwiederhier/ntfy/issues/491))
- The ntfy.sh server hardware was upgraded to a bigger box. If you'd like to help out carrying the server cost, **[sponsorships and donations](https://github.com/sponsors/binwiederhier)** 💸 would be very much appreciated

---

# beeper-linkedin v0.5.4

> published at 2022-12-23 [source](https://github.com/beeper/linkedin/releases/tag/v0.5.4)

**Migrated away from Poetry**. The dependency management was getting very annoying, and it was quite different from the dependency management of all of the other mautrix Python bridges, so I switched to use `setup.py` and `requirements.txt`.

**Features**

- Added `login-manual` option to log in to LinkedIn using a manual login flow.

  You can now pull the cookies manually from within an incognito browser after logging in instead of using the (very unreliable) old login method.

- Added personal filtering space support.


**Internal**

- Updated to `mautrix>=0.18.7,<0.19`.
- Add support for SQLite.

---

# hydrogen v0.3.6

> published at 2022-12-20 [source](https://github.com/vector-im/hydrogen-web/releases/tag/v0.3.6)

- Fixes timeline not loading ( [#947](https://github.com/vector-im/hydrogen-web/issues/947) )

---

# prometheus 2.41.0 / 2022-12-20

> published at 2022-12-20 [source](https://github.com/prometheus/prometheus/releases/tag/v2.41.0)

- \[FEATURE\] Relabeling: Add `keepequal` and `dropequal` relabel actions. [#11564](https://github.com/prometheus/prometheus/pull/11564)
- \[FEATURE\] Add support for HTTP proxy headers. [#11712](https://github.com/prometheus/prometheus/pull/11712)
- \[ENHANCEMENT\] Reload private certificates when changed on disk. [#11685](https://github.com/prometheus/prometheus/pull/11685)
- \[ENHANCEMENT\] Add `max_version` to specify maximum TLS version in `tls_config`. [#11685](https://github.com/prometheus/prometheus/pull/11685)
- \[ENHANCEMENT\] Add `goos` and `goarch` labels to `prometheus_build_info`. [#11685](https://github.com/prometheus/prometheus/pull/11685)
- \[ENHANCEMENT\] SD: Add proxy support for EC2 and LightSail SDs [#11611](https://github.com/prometheus/prometheus/pull/11611)
- \[ENHANCEMENT\] SD: Add new metric `prometheus_sd_file_watcher_errors_total`. [#11066](https://github.com/prometheus/prometheus/pull/11066)
- \[ENHANCEMENT\] Remote Read: Use a pool to speed up marshalling. [#11357](https://github.com/prometheus/prometheus/pull/11357)
- \[ENHANCEMENT\] TSDB: Improve handling of tombstoned chunks in iterators. [#11632](https://github.com/prometheus/prometheus/pull/11632)
- \[ENHANCEMENT\] TSDB: Optimize postings offset table reading. [#11535](https://github.com/prometheus/prometheus/pull/11535)
- \[BUGFIX\] Scrape: Validate the metric name, label names, and label values after relabeling. [#11074](https://github.com/prometheus/prometheus/pull/11074)
- \[BUGFIX\] Remote Write receiver and rule manager: Fix error handling. [#11727](https://github.com/prometheus/prometheus/pull/11727)

---

# prometheus v0.41.0

> published at 2022-12-20 [source](https://github.com/prometheus/prometheus/releases/tag/v0.41.0)

v0.41.0

---

# mautrix-whatsapp v0.8.0

> published at 2022-12-16 [source](https://github.com/mautrix/whatsapp/releases/tag/v0.8.0)

- Added support for bridging polls from WhatsApp and votes in both directions.
  - Votes are only bridged if MSC3381 polls are enabled ( `extev_polls` in the config).
- Added support for bridging WhatsApp communities as spaces.
- Updated backfill logic to mark rooms as read if the only message is a notice about the disappearing message timer.
- Updated Docker image to Alpine 3.17.
- Fixed backfills starting at the wrong time and sending smaller batches than intended in some cases.
- Switched SQLite config from `sqlite3` to `sqlite3-fk-wal` to enforce foreign keys and WAL mode. Additionally, adding `_txlock=immediate` to the DB path is recommended, but not required.

---

# redis 7.0.7

> published at 2022-12-16 [source](https://github.com/redis/redis/releases/tag/7.0.7)

Upgrade urgency: MODERATE, Contains fix for a regression in Geo commands.

# Bug Fixes

- Fix regression from Redis 7.0.6 in distance replies of Geo commands ( [#11631](https://github.com/redis/redis/pull/11631))

---

# grafana 9.2.8 (unreleased)

> published at 2022-12-15 [source](https://github.com/grafana/grafana/releases/tag/v9.2.8)

[Download page](https://grafana.com/grafana/download/9.2.8)

[What's new highlights](https://grafana.com/docs/grafana/latest/whatsnew/)

### Bug fixes

- **AzureMonitor:** Fix panic from empty response in ARG. [#59691](https://github.com/grafana/grafana/pull/59691), [@andresmgot](https://github.com/andresmgot)
- **Cloudwatch:** Fix deeplink with default region. [#60260](https://github.com/grafana/grafana/pull/60260), [@iwysiu](https://github.com/iwysiu)
- **Cloudwatch:** Fix deeplink with default region ( [#60260](https://github.com/grafana/grafana/pull/60260)). [#60273](https://github.com/grafana/grafana/pull/60273), [@iwysiu](https://github.com/iwysiu)
- **DataSourcePermissions:** Handle licensing properly for ds permissions. [#59694](https://github.com/grafana/grafana/pull/59694), [@kalleep](https://github.com/kalleep)
- **DataSourcePermissions:** Handle licensing properly for ds permissions. (Enterprise)
- **FIX:** Remove service accounts from quota count. [#59878](https://github.com/grafana/grafana/pull/59878), [@gamab](https://github.com/gamab)

---

# prometheus 2.41.0-rc.0 / 2022-12-14

> published at 2022-12-15 [source](https://github.com/prometheus/prometheus/releases/tag/v2.41.0-rc.0)

- \[FEATURE\] Relabeling: Add `keepequal` and `dropequal` relabel actions. [#11564](https://github.com/prometheus/prometheus/pull/11564)
- \[FEATURE\] Add support for HTTP proxy headers. [#11712](https://github.com/prometheus/prometheus/pull/11712)
- \[ENHANCEMENT\] Reload private certificates when changed on disk. [#11685](https://github.com/prometheus/prometheus/pull/11685)
- \[ENHANCEMENT\] Add `max_version` to specify maximum TLS version in `tls_config`. [#11685](https://github.com/prometheus/prometheus/pull/11685)
- \[ENHANCEMENT\] Add `goos` and `goarch` labels to `prometheus_build_info`. [#11685](https://github.com/prometheus/prometheus/pull/11685)
- \[ENHANCEMENT\] SD: Add proxy support for EC2 and LightSail SDs [#11611](https://github.com/prometheus/prometheus/pull/11611)
- \[ENHANCEMENT\] SD: Add new metric `prometheus_sd_file_watcher_errors_total`. [#11066](https://github.com/prometheus/prometheus/pull/11066)
- \[ENHANCEMENT\] Remote Read: Use a pool to speed up marshalling. [#11357](https://github.com/prometheus/prometheus/pull/11357)
- \[ENHANCEMENT\] TSDB: Improve handling of tombstoned chunks in iterators. [#11632](https://github.com/prometheus/prometheus/pull/11632)
- \[ENHANCEMENT\] TSDB: Optimize postings offset table reading. [#11535](https://github.com/prometheus/prometheus/pull/11535)
- \[BUGFIX\] Scrape: Validate the metric name, label names, and label values after relabeling. [#11074](https://github.com/prometheus/prometheus/pull/11074)
- \[BUGFIX\] Remote Write receiver and rule manager: Fix error handling. [#11727](https://github.com/prometheus/prometheus/pull/11727)

---

# prometheus v0.41.0-rc.0

> published at 2022-12-15 [source](https://github.com/prometheus/prometheus/releases/tag/v0.41.0-rc.0)

v0.41.0-rc.0

---

# prometheus 2.40.7 / 2022-12-14

> published at 2022-12-14 [source](https://github.com/prometheus/prometheus/releases/tag/v2.40.7)

- \[BUGFIX\] Use Windows native DNS resolver. [#11704](https://github.com/prometheus/prometheus/pull/11704)
- \[BUGFIX\] TSDB: Fix queries involving negative buckets of native histograms. [#11699](https://github.com/prometheus/prometheus/pull/11699)

---

# mautrix-instagram v0.2.3

> published at 2022-12-13 [source](https://github.com/mautrix/instagram/releases/tag/v0.2.3)

- Added support for "mentioned in comment" messages.
- Added support for re-requesting 2FA SMS when logging in.
- Updated Docker image to Alpine 3.17.
- Fixed error in image bridging.
- Fixed logging in with phone/email in provisioning API.

---

# nginx-proxy release-1.23.3

> published at 2022-12-13 [source](https://github.com/nginx/nginx/releases/tag/release-1.23.3)

nginx-1.23.3-RELEASE

---

# jitsi stable-8138-1: release

> published at 2022-12-13 [source](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-8138-1)

release

\\* [`8923b72`](https://github.com/jitsi/docker-jitsi-meet/commit/8923b72d8ea5897c8f441edbc6ce79ed495b4fbc) web: fix missing quotes on config.js string

\\* [`6b11a89`](https://github.com/jitsi/docker-jitsi-meet/commit/6b11a89ab6835c3066923ea0fba59ddceadc52eb) misc: working on unstable

---

# wireguard Merge branch 'mptcp-fix-ipv6-reqsk-ops-and-some-netlink-error-codes'

> published at 2022-12-12 [source](https://git.zx2c4.com/wireguard-linux/commit/?id=a38a211e9e0ca20f5d65307c98571f40f78ad396)

Mat Martineau says:

====================
mptcp: Fix IPv6 reqsk ops and some netlink error codes

Patch 1 adds some missing error status values for MPTCP path management
netlink commands with invalid attributes.

Patches 2-4 make IPv6 subflows use the correct request\_sock\_ops
structure and IPv6-specific destructor. The first patch in this group is
a prerequisite change that simplifies the last two patches.
====================

Link: https://lore.kernel.org/r/20221210002810.289674-1-mathew.j.martineau@linux.intel.com
Signed-off-by: Jakub Kicinski

---

# jitsi stable-8138: release

> published at 2022-12-12 [source](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-8138)

release

\\* [`1e49d65`](https://github.com/jitsi/docker-jitsi-meet/commit/1e49d65e8c19ceab2cafe5ce2afdbc76b77e263c) web: simplify build

\\* [`dd399fe`](https://github.com/jitsi/docker-jitsi-meet/commit/dd399fee54a6e19a792167058335e03400a69c2e) web,jvb: remove ENABLE\_MULTISTREAM

\\* [`723d661`](https://github.com/jitsi/docker-jitsi-meet/commit/723d66156d0ab36b279b84979bb3e848d64e34f2) jibri: add single-use-mode config option

\\* [`ca14c52`](https://github.com/jitsi/docker-jitsi-meet/commit/ca14c522848a0ab3f8bd3c4cf531227b6cdd8b85) web: add more transcription config env vars

\\* [`ccc5746`](https://github.com/jitsi/docker-jitsi-meet/commit/ccc5746f919b22e8d4ccdb43b0935a81c0377ece) prosody: add ability to configure TURN server transports

\\* [`17d047a`](https://github.com/jitsi/docker-jitsi-meet/commit/17d047a53aabead044080d144085ce234d3d598c) misc: working on unstable

---

# redis 7.0.6

> published at 2022-12-12 [source](https://github.com/redis/redis/releases/tag/7.0.6)

Upgrade urgency: MODERATE, Contains fixes for a few non-critical or unlikely bugs,

and some dramatic optimizations to Geo, EVAL, and Sorted sets commands.

# Potentially Breaking Bug Fixes for new Redis 7.0 features

- RM\_ResetDataset module API should not clear the functions ( [#11268](https://github.com/redis/redis/pull/11268))
- RM\_Call module API used with the "C" flag to run scripts, would now cause


  the commands in the script to check ACL with the designated user ( [#10966](https://github.com/redis/redis/pull/10966))

# Performance and resource utilization improvements

- Geo commands speedups ( [#11535](https://github.com/redis/redis/pull/11535), [#11522](https://github.com/redis/redis/pull/11522), [#11552](https://github.com/redis/redis/pull/11552), [#11579](https://github.com/redis/redis/pull/11579))
- Fix EVAL command performance regression from Redis 7.0 ( [#11521](https://github.com/redis/redis/pull/11521), [#11541](https://github.com/redis/redis/pull/11541))
- Reduce EXPIRE commands performance regression from Redis 7.0 ( [#11602](https://github.com/redis/redis/pull/11602))
- Optimize commands returning double values, mainly affecting zset commands ( [#11093](https://github.com/redis/redis/pull/11093))
- Optimize Lua parsing of some command responses ( [#11556](https://github.com/redis/redis/pull/11556))
- Optimize client memory usage tracking operation while client eviction is disabled ( [#11348](https://github.com/redis/redis/pull/11348))

# Platform / toolchain support related improvements

- Fix compilation on Solaris ( [#11327](https://github.com/redis/redis/pull/11327))

# Module API changes

- RM\_SetContextUser, RM\_SetModuleUserACLString, RM\_GetModuleUserACLString ( [#10966](https://github.com/redis/redis/pull/10966))
- Fix crash in CLIENT\_CHANGE event, when the selected database is not 0 ( [#11500](https://github.com/redis/redis/pull/11500))

# Changes in CLI tools

- redis-benchmark avoid aborting on NOPERM from CONFIG GET ( [#11096](https://github.com/redis/redis/pull/11096))

# Bug Fixes

- Avoid hang of diskless replication fork child when parent crashes ( [#11463](https://github.com/redis/redis/pull/11463))
- Fix crash with module API of list iterator and RM\_ListDelete ( [#11383](https://github.com/redis/redis/pull/11383))
- Fix TLS error handling to avoid connection drops on timeouts ( [#11563](https://github.com/redis/redis/pull/11563))
- Fix runtime changes to cluster-announce-\*-port to take effect on the local node too ( [#10745](https://github.com/redis/redis/pull/10745))
- Fix sentinel function that compares hostnames if failed resolve ( [#11419](https://github.com/redis/redis/pull/11419))
- Fix MIGRATE with AUTH set to "keys" is getting wrong key names leading to MOVED or ACL errors ( [#11253](https://github.com/redis/redis/pull/11253))

## Fixes for issues in previous releases of Redis 7.0

- Fix command line startup --sentinel problem ( [#11591](https://github.com/redis/redis/pull/11591))
- Fis missing FCALL commands in monitor ( [#11510](https://github.com/redis/redis/pull/11510))
- Fix CLUSTER SHARDS showing empty hostname ( [#11297](https://github.com/redis/redis/pull/11297))
- Replica that asks for rdb-only could have missed the EOF and hang ( [#11296](https://github.com/redis/redis/pull/11296))

---

# redis 6.2.8

> published at 2022-12-12 [source](https://github.com/redis/redis/releases/tag/6.2.8)

Upgrade urgency: MODERATE, Contains fixes for a few non-critical or unlikely bugs

# Performance and resource utilization improvements

- Optimize zset conversion on large ZRANGESTORE ( [#10789](https://github.com/redis/redis/pull/10789))

# Module API changes

- Fix crash in CLIENT\_CHANGE event, when the selected database is not 0 ( [#11500](https://github.com/redis/redis/pull/11500))
- Fix RM\_SetAbsExpire and RM\_GetAbsExpire API registration ( [#11025](https://github.com/redis/redis/pull/11025), [#8564](https://github.com/redis/redis/pull/8564))

# Security improvements

- Sentinel: avoid logging auth-pass value ( [#9652](https://github.com/redis/redis/pull/9652))

# Bug Fixes

- Fix a crash when a Lua script returns a meta-table ( [#11032](https://github.com/redis/redis/pull/11032))
- Fix ZRANGESTORE crash when zset\_max\_listpack\_entries is 0 ( [#10767](https://github.com/redis/redis/pull/10767))
- Unpause clients after manual failover ends instead of waiting for timed ( [#9676](https://github.com/redis/redis/pull/9676))
- TLS: Notify clients on connection shutdown ( [#10931](https://github.com/redis/redis/pull/10931))
- Avoid hang of diskless replication fork child when parent crashes ( [#11463](https://github.com/redis/redis/pull/11463))
- Fix sentinel function that compares hostnames if failed resolve ( [#11419](https://github.com/redis/redis/pull/11419))
- Fix a hang when eviction is combined with lazy-free and maxmemory-eviction-tenacity


  is set to 100 ( [#11237](https://github.com/redis/redis/pull/11237))
- Fix bug with scripts ignoring client tracking NOLOOP ( [#11052](https://github.com/redis/redis/pull/11052))
- Fix client-side tracking breaking protocol when FLUSHDB / FLUSHALL / SWAPDB is


  used inside MULTI-EXEC ( [#11038](https://github.com/redis/redis/pull/11038))
- Fix BITFIELD overflow detection on some compilers due to undefined behavior ( [#9601](https://github.com/redis/redis/pull/9601))

---

# corporal 2.5.0

> published at 2022-12-11 [source](https://github.com/devture/matrix-corporal/releases/tag/2.5.0)

Release 2.5.0

---

# mailer 4.96-r1-0

> published at 2022-12-11 [source](https://github.com/devture/exim-relay/releases/tag/4.96-r1-0)

Replace Gitlab CI with Woodpecker CI

---

# wireguard stmmac: fix potential division by 0

> published at 2022-12-10 [source](https://git.zx2c4.com/wireguard-linux/commit/?id=ede5a389852d3640a28e7187fb32b7f204380901)

When the MAC is connected to a 10 Mb/s PHY and the PTP clock is derived
from the MAC reference clock (default), the clk\_ptp\_rate becomes too
small and the calculated sub second increment becomes 0 when computed by
the stmmac\_config\_sub\_second\_increment() function within
stmmac\_init\_tstamp\_counter().

Therefore, the subsequent div\_u64 in stmmac\_init\_tstamp\_counter()
operation triggers a divide by 0 exception as shown below.

\[ 95.062067\] socfpga-dwmac ff700000.ethernet eth0: Register MEM\_TYPE\_PAGE\_POOL RxQ-0
\[ 95.076440\] socfpga-dwmac ff700000.ethernet eth0: PHY \[stmmac-0:08\] driver \[NCN26000\] (irq=49)
\[ 95.095964\] dwmac1000: Master AXI performs any burst length
\[ 95.101588\] socfpga-dwmac ff700000.ethernet eth0: No Safety Features support found
\[ 95.109428\] Division by zero in kernel.
\[ 95.113447\] CPU: 0 PID: 239 Comm: ifconfig Not tainted 6.1.0-rc7-centurion3-1.0.3.0-01574-gb624218205b7-dirty #77
\[ 95.123686\] Hardware name: Altera SOCFPGA
\[ 95.127695\] unwind\_backtrace from show\_stack+0x10/0x14
\[ 95.132938\] show\_stack from dump\_stack\_lvl+0x40/0x4c
\[ 95.137992\] dump\_stack\_lvl from Ldiv0+0x8/0x10
\[ 95.142527\] Ldiv0 from \_\_aeabi\_uidivmod+0x8/0x18
\[ 95.147232\] \_\_aeabi\_uidivmod from div\_u64\_rem+0x1c/0x40
\[ 95.152552\] div\_u64\_rem from stmmac\_init\_tstamp\_counter+0xd0/0x164
\[ 95.158826\] stmmac\_init\_tstamp\_counter from stmmac\_hw\_setup+0x430/0xf00
\[ 95.165533\] stmmac\_hw\_setup from \_\_stmmac\_open+0x214/0x2d4
\[ 95.171117\] \_\_stmmac\_open from stmmac\_open+0x30/0x44
\[ 95.176182\] stmmac\_open from \_\_dev\_open+0x11c/0x134
\[ 95.181172\] \_\_dev\_open from \_\_dev\_change\_flags+0x168/0x17c
\[ 95.186750\] \_\_dev\_change\_flags from dev\_change\_flags+0x14/0x50
\[ 95.192662\] dev\_change\_flags from devinet\_ioctl+0x2b4/0x604
\[ 95.198321\] devinet\_ioctl from inet\_ioctl+0x1ec/0x214
\[ 95.203462\] inet\_ioctl from sock\_ioctl+0x14c/0x3c4
\[ 95.208354\] sock\_ioctl from vfs\_ioctl+0x20/0x38
\[ 95.212984\] vfs\_ioctl from sys\_ioctl+0x250/0x844
\[ 95.217691\] sys\_ioctl from ret\_fast\_syscall+0x0/0x4c
\[ 95.222743\] Exception stack(0xd0ee1fa8 to 0xd0ee1ff0)
\[ 95.227790\] 1fa0: 00574c4f be9aeca4 00000003 00008914 be9aeca4 be9aec50
\[ 95.235945\] 1fc0: 00574c4f be9aeca4 0059f078 00000036 be9aee8c be9aef7a 00000015 00000000
\[ 95.244096\] 1fe0: 005a01f0 be9aec38 004d7484 b6e67d74

Signed-off-by: Piergiorgio Beruto
Fixes: 91a2559c1dc5 ("net: stmmac: Fix sub-second increment")
Reviewed-by: Andrew Lunn
Link: https://lore.kernel.org/r/de4c64ccac9084952c56a06a8171d738604c4770.1670678513.git.piergiorgio.beruto@gmail.com
Signed-off-by: Jakub Kicinski

---

# miniflux Miniflux 2.0.41

> published at 2022-12-10 [source](https://github.com/miniflux/v2/releases/tag/2.0.41)

- Reverted PR [#1290](https://github.com/miniflux/v2/pull/1290) (follow the only link) because it leads to several panics/segfaults that prevent feed updates
- Disable double-tap mobile gesture if swipe gesture is disabled
- Skip integrations if there are no entries to push
- Enable TLS-ALPN-01 challenge for ACME
  - This type of challenge works purely at the TLS layer and is compatible


    with SNI proxies. The existing HTTP-01 challenge support has been left


    as-is.
- Preconfigure Miniflux for GitHub Codespaces
- Updated `golang.org/x/net/*` dependencies

---

# wireguard octeontx2-af: cn10k: mcs: Fix a resource leak in the probe and remove functions

> published at 2022-12-10 [source](https://git.zx2c4.com/wireguard-linux/commit/?id=87c978123ef1f346d7385eaccc141022d368166f)

In mcs\_register\_interrupts(), a call to request\_irq() is not balanced by a
corresponding free\_irq(), neither in the error handling path, nor in the
remove function.

Add the missing calls.

Fixes: 6c635f78c474 ("octeontx2-af: cn10k: mcs: Handle MCS block interrupts")
Signed-off-by: Christophe JAILLET
Link: https://lore.kernel.org/r/69f153db5152a141069f990206e7389f961d41ec.1670693669.git.christophe.jaillet@wanadoo.fr
Signed-off-by: Jakub Kicinski

---

# wireguard mptcp: use proper req destructor for IPv6

> published at 2022-12-10 [source](https://git.zx2c4.com/wireguard-linux/commit/?id=d3295fee3c756ece33ac0d935e172e68c0a4161b)

Before, only the destructor from TCP request sock in IPv4 was called
even if the subflow was IPv6.

It is important to use the right destructor to avoid memory leaks with
some advanced IPv6 features, e.g. when the request socks contain
specific IPv6 options.

Fixes: 79c0949e9a09 ("mptcp: Add key generation and token tree")
Reviewed-by: Mat Martineau
Cc: stable@vger.kernel.org
Signed-off-by: Matthieu Baerts
Signed-off-by: Mat Martineau
Signed-off-by: Jakub Kicinski

---

# wireguard mptcp: dedicated request sock for subflow in v6

> published at 2022-12-10 [source](https://git.zx2c4.com/wireguard-linux/commit/?id=34b21d1ddc8ace77a8fa35c1b1e06377209e0dae)

tcp\_request\_sock\_ops structure is specific to IPv4. It should then not
be used with MPTCP subflows on top of IPv6.

For example, it contains the 'family' field, initialised to AF\_INET.
This 'family' field is used by TCP FastOpen code to generate the cookie
but also by TCP Metrics, SELinux and SYN Cookies. Using the wrong family
will not lead to crashes but displaying/using/checking wrong things.

Note that 'send\_reset' callback from request\_sock\_ops structure is used
in some error paths. It is then also important to use the correct one
for IPv4 or IPv6.

The slab name can also be different in IPv4 and IPv6, it will be used
when printing some log messages. The slab pointer will anyway be the
same because the object size is the same for both v4 and v6. A
BUILD\_BUG\_ON() has also been added to make sure this size is the same.

Fixes: cec37a6e41aa ("mptcp: Handle MP\_CAPABLE options for outgoing connections")
Reviewed-by: Mat Martineau
Cc: stable@vger.kernel.org
Signed-off-by: Matthieu Baerts
Signed-off-by: Mat Martineau
Signed-off-by: Jakub Kicinski

---

# wireguard mptcp: remove MPTCP 'ifdef' in TCP SYN cookies

> published at 2022-12-10 [source](https://git.zx2c4.com/wireguard-linux/commit/?id=3fff88186f047627bb128d65155f42517f8e448f)

To ease the maintenance, it is often recommended to avoid having #ifdef
preprocessor conditions.

Here the section related to CONFIG\_MPTCP was quite short but the next
commit needs to add more code around. It is then cleaner to move
specific MPTCP code to functions located in net/mptcp directory.

Now that mptcp\_subflow\_request\_sock\_ops structure can be static, it can
also be marked as "read only after init".

Suggested-by: Paolo Abeni
Reviewed-by: Mat Martineau
Cc: stable@vger.kernel.org
Signed-off-by: Matthieu Baerts
Signed-off-by: Mat Martineau
Signed-off-by: Jakub Kicinski

---

# wireguard mptcp: netlink: fix some error return code

> published at 2022-12-10 [source](https://git.zx2c4.com/wireguard-linux/commit/?id=e0fe1123ab2b07d2cd5475660bd0b4e6993ffaa7)

Fix to return negative error code -EINVAL from some error handling
case instead of 0, as done elsewhere in those functions.

Fixes: 9ab4807c84a4 ("mptcp: netlink: Add MPTCP\_PM\_CMD\_ANNOUNCE")
Fixes: 702c2f646d42 ("mptcp: netlink: allow userspace-driven subflow establishment")
Cc: stable@vger.kernel.org
Reviewed-by: Matthieu Baerts
Signed-off-by: Wei Yongjun
Signed-off-by: Mat Martineau
Signed-off-by: Jakub Kicinski

---

# wireguard net: dsa: tag_8021q: avoid leaking ctx on dsa_tag_8021q_register() error path

> published at 2022-12-09 [source](https://git.zx2c4.com/wireguard-linux/commit/?id=e095493091e850d5292ad01d8fbf5cde1d89ac53)

If dsa\_tag\_8021q\_setup() fails, for example due to the inability of the
device to install a VLAN, the tag\_8021q context of the switch will leak.
Make sure it is freed on the error path.

Fixes: 328621f6131f ("net: dsa: tag\_8021q: absorb dsa\_8021q\_setup into dsa\_tag\_8021q\_{,un}register")
Signed-off-by: Vladimir Oltean
Link: https://lore.kernel.org/r/20221209235242.480344-1-vladimir.oltean@nxp.com
Signed-off-by: Jakub Kicinski

---

# wireguard i40e: Fix the inability to attach XDP program on downed interface

> published at 2022-12-09 [source](https://git.zx2c4.com/wireguard-linux/commit/?id=0c87b545a2ed5cd8a6318011f1c92b188c2d74bc)

Whenever trying to load XDP prog on downed interface, function i40e\_xdp
was passing vsi->rx\_buf\_len field to i40e\_xdp\_setup() which was equal 0.
i40e\_open() calls i40e\_vsi\_configure\_rx() which configures that field,
but that only happens when interface is up. When it is down, i40e\_open()
is not being called, thus vsi->rx\_buf\_len is not set.

Solution for this is calculate buffer length in newly created
function - i40e\_calculate\_vsi\_rx\_buf\_len() that return actual buffer
length. Buffer length is being calculated based on the same rules
applied previously in i40e\_vsi\_configure\_rx() function.

Fixes: 613142b0bb88 ("i40e: Log error for oversized MTU on device")
Fixes: 0c8493d90b6b ("i40e: add XDP support for pass and drop actions")
Signed-off-by: Bartosz Staszewski
Signed-off-by: Mateusz Palczewski
Tested-by: Shwetha Nagaraju
Reviewed-by: Maciej Fijalkowski
Signed-off-by: Tony Nguyen
Reviewed-by: Saeed Mahameed
Link: https://lore.kernel.org/r/20221209185411.2519898-1-anthony.l.nguyen@intel.com
Signed-off-by: Jakub Kicinski

---

# com.devture.ansible.role.traefik v2.9.6

> published at 2022-12-08 [source](https://github.com/traefik/traefik/releases/tag/v2.9.6)

**CVEs:**

- [https://www.cve.org/CVERecord?id=CVE-2022-23469](https://www.cve.org/CVERecord?id=CVE-2022-23469)
- [https://www.cve.org/CVERecord?id=CVE-2022-46153](https://www.cve.org/CVERecord?id=CVE-2022-46153)
- [https://www.cve.org/CVERecord?id=CVE-2022-41717](https://www.cve.org/CVERecord?id=CVE-2022-41717)

**Bug fixes:**

- **\[acme\]** Update go-acme/lego to v4.9.1 ( [#9550](https://github.com/traefik/traefik/pull/9550) by [@ldez](https://github.com/ldez))
- **\[k8s/crd\]** Support of allowEmptyServices in TraefikService ( [#9424](https://github.com/traefik/traefik/pull/9424) by [@jeromeguiard](https://github.com/jeromeguiard))
- **\[logs\]** Remove logs of the request ( [#9574](https://github.com/traefik/traefik/pull/9574) by [@ldez](https://github.com/ldez))
- **\[plugins\]** Increase the timeout on plugin download ( [#9529](https://github.com/traefik/traefik/pull/9529) by [@ldez](https://github.com/ldez))
- **\[server\]** Update golang.org/x/net ( [#9582](https://github.com/traefik/traefik/pull/9582) by [@ldez](https://github.com/ldez))
- **\[tls\]** Handle broken TLS conf better ( [#9572](https://github.com/traefik/traefik/pull/9572) by [@mpl](https://github.com/mpl))
- **\[tracing\]** Update DataDog tracing dependency to v1.43.1 ( [#9526](https://github.com/traefik/traefik/pull/9526) by [@rtribotte](https://github.com/rtribotte))
- **\[webui\]** Add missing serialNumber passTLSClientCert option to middleware panel ( [#9539](https://github.com/traefik/traefik/pull/9539) by [@rtribotte](https://github.com/rtribotte))

**Documentation:**

- **\[docker\]** Add networking example ( [#9542](https://github.com/traefik/traefik/pull/9542) by [@janik](https://github.com/janik))
- **\[hub\]** Add information about the Hub Agent ( [#9560](https://github.com/traefik/traefik/pull/9560) by [@nmengin](https://github.com/nmengin))
- **\[k8s/helm\]** Update Helm installation section ( [#9564](https://github.com/traefik/traefik/pull/9564) by [@mloiseleur](https://github.com/mloiseleur))
- **\[middleware\]** Clarify PathPrefix matcher greediness ( [#9519](https://github.com/traefik/traefik/pull/9519) by [@mpl](https://github.com/mpl))

---

# com.devture.ansible.role.traefik v3.0.0-beta2

> published at 2022-12-07 [source](https://github.com/traefik/traefik/releases/tag/v3.0.0-beta2)

**Enhancements:**

- **\[http3\]** Moves HTTP/3 outside the experimental section ( [#9570](https://github.com/traefik/traefik/pull/9570) by [@sdelicata](https://github.com/sdelicata))

**Bug fixes:**

- **\[logs\]** Change traefik cmd error log to error level ( [#9569](https://github.com/traefik/traefik/pull/9569) by [@tomMoulard](https://github.com/tomMoulard))
- **\[rules\]** Rework Host and HostRegexp matchers ( [#9559](https://github.com/traefik/traefik/pull/9559) by [@tomMoulard](https://github.com/tomMoulard))

**Misc:**

- Merge current v2.9 into master ( [#9586](https://github.com/traefik/traefik/pull/9586) by [@tomMoulard](https://github.com/tomMoulard))

---

# ansible 7.1.0

> published at 2022-12-07 [source](https://pypi.org/project/ansible/7.1.0/)

> no description provided

---



---

# ansible-core 2.14.1

> published at 2022-12-06 [source](https://pypi.org/project/ansible-core/2.14.1/)

> no description provided

---



---

# ansible 6.7.0

> published at 2022-12-06 [source](https://pypi.org/project/ansible/6.7.0/)

> no description provided

---



---

# com.devture.ansible.role.traefik v3.0.0-beta1

> published at 2022-12-06 [source](https://github.com/traefik/traefik/releases/tag/v3.0.0-beta1)

**Enhancements:**

- **\[ecs\]** Add option to keep only healthy ECS tasks ( [#8027](https://github.com/traefik/traefik/pull/8027) by [@Michampt](https://github.com/Michampt))
- **\[healthcheck\]** Support gRPC healthcheck ( [#8583](https://github.com/traefik/traefik/pull/8583) by [@jjacque](https://github.com/jjacque))
- **\[healthcheck\]** Add a status option to the service health check ( [#9463](https://github.com/traefik/traefik/pull/9463) by [@guoard](https://github.com/guoard))
- **\[http\]** Support custom headers when fetching configuration through HTTP ( [#9421](https://github.com/traefik/traefik/pull/9421) by [@kevinpollet](https://github.com/kevinpollet))
- **\[logs,performance\]** New logger for the Traefik logs ( [#9515](https://github.com/traefik/traefik/pull/9515) by [@ldez](https://github.com/ldez))
- **\[logs,plugins\]** Retry on plugin API calls ( [#9530](https://github.com/traefik/traefik/pull/9530) by [@ldez](https://github.com/ldez))
- **\[logs,provider\]** Improve provider logs ( [#9562](https://github.com/traefik/traefik/pull/9562) by [@ldez](https://github.com/ldez))
- **\[logs\]** Improve test logger assertions ( [#9533](https://github.com/traefik/traefik/pull/9533) by [@ldez](https://github.com/ldez))
- **\[metrics\]** Support gRPC and gRPC-Web protocol in metrics ( [#9483](https://github.com/traefik/traefik/pull/9483) by [@longit644](https://github.com/longit644))
- **\[middleware,accesslogs\]** Log TLS client subject ( [#9285](https://github.com/traefik/traefik/pull/9285) by [@xmessi](https://github.com/xmessi))
- **\[middleware,metrics,tracing\]** Add OpenTelemetry tracing and metrics support ( [#8999](https://github.com/traefik/traefik/pull/8999) by [@tomMoulard](https://github.com/tomMoulard))
- **\[middleware\]** Disable Content-Type auto-detection by default ( [#9546](https://github.com/traefik/traefik/pull/9546) by [@sdelicata](https://github.com/sdelicata))
- **\[middleware\]** Add gRPC-Web middleware ( [#9451](https://github.com/traefik/traefik/pull/9451) by [@juliens](https://github.com/juliens))
- **\[middleware\]** Add support for Brotli ( [#9387](https://github.com/traefik/traefik/pull/9387) by [@glinton](https://github.com/glinton))
- **\[middleware\]** Renaming IPWhiteList to IPAllowList ( [#9457](https://github.com/traefik/traefik/pull/9457) by [@wxmbugu](https://github.com/wxmbugu))
- **\[nomad\]** Support multiple namespaces in the Nomad Provider ( [#9332](https://github.com/traefik/traefik/pull/9332) by [@0teh](https://github.com/0teh))
- **\[rules\]** Update routing syntax ( [#9531](https://github.com/traefik/traefik/pull/9531) by [@skwair](https://github.com/skwair))
- **\[server\]** Rework servers load-balancer to use the WRR ( [#9431](https://github.com/traefik/traefik/pull/9431) by [@juliens](https://github.com/juliens))
- **\[server\]** Allow default entrypoints definition ( [#9100](https://github.com/traefik/traefik/pull/9100) by [@jilleJr](https://github.com/jilleJr))
- **\[tls,service\]** Support SPIFFE mTLS between Traefik and Backend servers ( [#9394](https://github.com/traefik/traefik/pull/9394) by [@jlevesy](https://github.com/jlevesy))
- **\[tls\]** Add Tailscale certificate resolver ( [#9237](https://github.com/traefik/traefik/pull/9237) by [@kevinpollet](https://github.com/kevinpollet))
- **\[tls\]** Support SNI routing with Postgres STARTTLS connections ( [#9377](https://github.com/traefik/traefik/pull/9377) by [@rtribotte](https://github.com/rtribotte))
- Remove deprecated options ( [#9527](https://github.com/traefik/traefik/pull/9527) by [@sdelicata](https://github.com/sdelicata))

**Bug fixes:**

- **\[logs\]** Fix log level ( [#9545](https://github.com/traefik/traefik/pull/9545) by [@ldez](https://github.com/ldez))
- **\[metrics\]** Fix ServerUp metric ( [#9534](https://github.com/traefik/traefik/pull/9534) by [@kevinpollet](https://github.com/kevinpollet))
- **\[tls,service\]** Enforce default servers transport SPIFFE config ( [#9444](https://github.com/traefik/traefik/pull/9444) by [@jlevesy](https://github.com/jlevesy))

**Documentation:**

- **\[metrics\]** Update and publish official Grafana Dashboard ( [#9493](https://github.com/traefik/traefik/pull/9493) by [@mloiseleur](https://github.com/mloiseleur))

**Misc:**

- Merge branch v2.9 into master ( [#9554](https://github.com/traefik/traefik/pull/9554) by [@ldez](https://github.com/ldez))
- Merge branch v2.9 into master ( [#9536](https://github.com/traefik/traefik/pull/9536) by [@ldez](https://github.com/ldez))
- Merge branch v2.9 into master ( [#9532](https://github.com/traefik/traefik/pull/9532) by [@ldez](https://github.com/ldez))
- Merge branch v2.9 into master ( [#9482](https://github.com/traefik/traefik/pull/9482) by [@kevinpollet](https://github.com/kevinpollet))
- Merge branch v2.9 into master ( [#9464](https://github.com/traefik/traefik/pull/9464) by [@ldez](https://github.com/ldez))
- Merge branch v2.9 into master ( [#9449](https://github.com/traefik/traefik/pull/9449) by [@kevinpollet](https://github.com/kevinpollet))
- Merge branch v2.9 into master ( [#9419](https://github.com/traefik/traefik/pull/9419) by [@kevinpollet](https://github.com/kevinpollet))
- Merge branch v2.9 into master ( [#9351](https://github.com/traefik/traefik/pull/9351) by [@rtribotte](https://github.com/rtribotte))

---

# ansible-core 2.13.7

> published at 2022-12-05 [source](https://pypi.org/project/ansible-core/2.13.7/)

> no description provided

---



---

# coturn docker/4.6.1-r0

> published at 2022-12-04 [source](https://github.com/coturn/coturn/releases/tag/docker%2F4.6.1-r0)

`4.6.1-r0` Docker image version of 4.6.1 Coturn release.

[Docker Hub](https://hub.docker.com/r/coturn/coturn) \| [GitHub Container Registry](https://github.com/orgs/coturn/packages/container/package/coturn) \| [Quay.io](https://quay.io/repository/coturn/coturn)

[Changelog](https://github.com/coturn/coturn/blob/docker/4.6.1-r0/docker/coturn/CHANGELOG.md#461-r0--2022-12-04)

---

# coturn 4.6.1

> published at 2022-12-03 [source](https://github.com/coturn/coturn/releases/tag/4.6.1)

Update version to 4.6.1

---

# mautrix-signal v0.4.2

> published at 2022-12-03 [source](https://github.com/mautrix/signal/releases/tag/v0.4.2)

Target signald version: [v0.23.0](https://gitlab.com/signald/signald/-/releases/0.23.0)

- Fixed database schema upgrade for users who used SQLite before it was stabilized in v0.4.1.
- Fixed error in commands that use phone numbers (like `!signal pm`).
- Fixed updating private chat portal metadata when Signal user info changes.
- Updated Docker image to Alpine 3.17.

---

# prometheus-blackbox-exporter 0.23.0 / 2022-12-02

> published at 2022-12-02 [source](https://github.com/prometheus/blackbox_exporter/releases/tag/v0.23.0)

- \[SECURITY\] Update Exporter Toolkit ( [CVE-2022-46146](https://github.com/advisories/GHSA-7rg2-cxvp-9p7p "CVE-2022-46146")) [#979](https://github.com/prometheus/blackbox_exporter/pull/979)
- \[FEATURE\] Support multiple Listen Addresses and systemd socket activation [#979](https://github.com/prometheus/blackbox_exporter/pull/979)
- \[FEATURE\] Add leaf certificate details in a new `probe_ssl_last_chain_info` metric. [#943](https://github.com/prometheus/blackbox_exporter/pull/943)
- \[FEATURE\] DNS: Add `Add probe_dns_query_succeeded` metric. [#990](https://github.com/prometheus/blackbox_exporter/pull/990)

---

# appservice-slack 2.0.2-rc2 (2022-11-30)

> published at 2022-12-02 [source](https://github.com/matrix-org/matrix-appservice-slack/releases/tag/2.0.2-rc2)

## Bugfixes

- Safely handle errors from admin commands. ( [#725](https://github.com/matrix-org/matrix-appservice-slack/issues/725))

## Internal Changes

- Docs: Docker container uses port 5858 unless explicitly modified in config.yaml ( [#718](https://github.com/matrix-org/matrix-appservice-slack/issues/718)). Contributed by [@mkarg](https://github.com/mkarg)
- Docs: Fixed numbering by fixing indentation ( [#719](https://github.com/matrix-org/matrix-appservice-slack/issues/719)). Contributed by [@mkarg](https://github.com/mkarg)
- Docs: Run bridge as daemon ( [#720](https://github.com/matrix-org/matrix-appservice-slack/issues/720)). Contributed by [@mkarg](https://github.com/mkarg)
- Docs: Merge [#718](https://github.com/matrix-org/matrix-appservice-slack/pull/718), [#719](https://github.com/matrix-org/matrix-appservice-slack/pull/719) and [#720](https://github.com/matrix-org/matrix-appservice-slack/pull/720) and reduce abbreviations. ( [#727](https://github.com/matrix-org/matrix-appservice-slack/issues/727))

---

# hookshot 2.5.0 (2022-12-02)

> published at 2022-12-02 [source](https://github.com/matrix-org/matrix-hookshot/releases/tag/2.5.0)

## Features

- GitHub assign command now automatically chooses the top issue and/or authenticated user if not provided. ( [#554](https://github.com/matrix-org/matrix-hookshot/issues/554))
- Display GitLab project paths with their true letter casing. ( [#556](https://github.com/matrix-org/matrix-hookshot/issues/556))
- Forbid creating multiple GitLab connections on the same path with different letter casing. ( [#557](https://github.com/matrix-org/matrix-hookshot/issues/557))
- Allow adding connections to GitLab projects even when Hookshot doesn't have permissions to automatically provision a webhook for it. When that occurs, tell the user to ask a project admin to add the webhook. ( [#567](https://github.com/matrix-org/matrix-hookshot/issues/567))

## Bugfixes

- Do not send a notice when a user replies to a GitLab comment, or when GitLab comment notices have been disabled. ( [#536](https://github.com/matrix-org/matrix-hookshot/issues/536))
- Don't announce error if a RSS feed request timed out. ( [#551](https://github.com/matrix-org/matrix-hookshot/issues/551))
- Don't ignore events from GitLab projects that have capital letters in their project path, and had their room connection set up by the configuration widget or provisioning API. ( [#553](https://github.com/matrix-org/matrix-hookshot/issues/553))
- Automatically JSON stringify values in a webhook payload that exceed the max depth/breadth supported by the parser. ( [#560](https://github.com/matrix-org/matrix-hookshot/issues/560))
- The bot no longer accepts invites from users who do not have permission to use it. ( [#561](https://github.com/matrix-org/matrix-hookshot/issues/561))
- Fix issue where GitLab connections couldn't be added via a bot command for projects on an instance URL configured with a trailing slash. ( [#563](https://github.com/matrix-org/matrix-hookshot/issues/563))
- Harden against unauthorized changes to room state for connection settings. ( [#565](https://github.com/matrix-org/matrix-hookshot/issues/565))
- Fixed a case where a bridge-created admin room would stop working on restart. ( [#578](https://github.com/matrix-org/matrix-hookshot/issues/578))

## Improved Documentation

- Improve navigability & legibility of some documentation pages. ( [#568](https://github.com/matrix-org/matrix-hookshot/issues/568))

## Internal Changes

- Increase default `feeds.pollTimeoutSeconds` from 10 seconds to 30. ( [#483](https://github.com/matrix-org/matrix-hookshot/issues/483))
- Don't provision a connection twice when using a bot command to create a connection. ( [#558](https://github.com/matrix-org/matrix-hookshot/issues/558))
- Change "setup" to "set up" where it's used as a verb. ( [#572](https://github.com/matrix-org/matrix-hookshot/issues/572))
- Fix misspellings of "occurred" in error messages. ( [#576](https://github.com/matrix-org/matrix-hookshot/issues/576))

---

# grafana 9.3.1 (2022-11-30)

> published at 2022-12-01 [source](https://github.com/grafana/grafana/releases/tag/v9.3.1)

[Download page](https://grafana.com/grafana/download/9.3.1)

[What's new highlights](https://grafana.com/docs/grafana/latest/whatsnew/)

### Features and enhancements

- **Connections:** Update "Your connections/Data sources" page. [#58589](https://github.com/grafana/grafana/pull/58589), [@mikkancso](https://github.com/mikkancso)

### Bug fixes

- **Accessibility:** Increase badge constrast to be WCAG AA compliant. [#59531](https://github.com/grafana/grafana/pull/59531), [@eledobleefe](https://github.com/eledobleefe)

---

# appservice-slack 2.0.2 (2022-11-30)

> published at 2022-12-01 [source](https://github.com/matrix-org/matrix-appservice-slack/releases/tag/2.0.2)

No changes since 2.0.2-rc2.

---

# prometheus_node_exporter 1.5.0 / 2022-11-29

> published at 2022-11-29 [source](https://github.com/prometheus/node_exporter/releases/tag/v1.5.0)

NOTE: This changes the Go runtime "GOMAXPROCS" to 1. This is done to limit the

concurrency of the exporter to 1 CPU thread at a time in order to avoid a

race condition problem in the Linux kernel ( [#2500](https://github.com/prometheus/node_exporter/issues/2500)) and parallel IO issues

on nodes with high numbers of CPUs/CPU threads ( [#1880](https://github.com/prometheus/node_exporter/issues/1880)).

- \[CHANGE\] Default GOMAXPROCS to 1 [#2530](https://github.com/prometheus/node_exporter/pull/2530)
- \[FEATURE\] Add multiple listeners and systemd socket listener activation [#2393](https://github.com/prometheus/node_exporter/pull/2393)
- \[ENHANCEMENT\] Add RTNL version of netclass collector [#2492](https://github.com/prometheus/node_exporter/pull/2492), [#2528](https://github.com/prometheus/node_exporter/pull/2528)
- \[BUGFIX\] Fix diskstats exclude flags [#2487](https://github.com/prometheus/node_exporter/pull/2487)
- \[BUGFIX\] Bump go/x/crypt and go/x/net [#2488](https://github.com/prometheus/node_exporter/pull/2488)
- \[BUGFIX\] Fix hwmon label sanitizer [#2504](https://github.com/prometheus/node_exporter/pull/2504)
- \[BUGFIX\] Use native endianness when encoding InetDiagMsg [#2508](https://github.com/prometheus/node_exporter/pull/2508)
- \[BUGFIX\] Fix btrfs device stats always being zero [#2516](https://github.com/prometheus/node_exporter/pull/2516)
- \[BUGFIX\] Security: Update exporter-toolkit ( [CVE-2022-46146](https://github.com/advisories/GHSA-7rg2-cxvp-9p7p "CVE-2022-46146")) [#2531](https://github.com/prometheus/node_exporter/pull/2531)

---

# prometheus_node_exporter 1.4.1 / 2022-11-29

> published at 2022-11-29 [source](https://github.com/prometheus/node_exporter/releases/tag/v1.4.1)

- \[BUGFIX\] Fix diskstats exclude flags [#2487](https://github.com/prometheus/node_exporter/pull/2487)
- \[BUGFIX\] Security: Update go/x/crypto and go/x/net ( [CVE-2022-27191](https://github.com/advisories/GHSA-8c26-wmh5-6g9v "CVE-2022-27191") [CVE-2022-27664](https://github.com/advisories/GHSA-69cg-p879-7622 "CVE-2022-27664")) [#2488](https://github.com/prometheus/node_exporter/pull/2488)
- \[BUGFIX\] Security: Update exporter-toolkit ( [CVE-2022-46146](https://github.com/advisories/GHSA-7rg2-cxvp-9p7p "CVE-2022-46146")) [#2531](https://github.com/prometheus/node_exporter/pull/2531)

---

# dendrite Dendrite 0.10.8

> published at 2022-11-29 [source](https://github.com/matrix-org/dendrite/releases/tag/v0.10.8)

### Features

- The built-in NATS Server has been updated to version 2.9.8
- A number of under-the-hood changes have been merged for future virtual hosting support in Dendrite (running multiple domain names on the same Dendrite deployment)

### Fixes

- Event auth handling of invites has been refactored, which should fix some edge cases being handled incorrectly
- Fix a bug when returning an empty protocol list, which could cause Element to display "The homeserver may be too old to support third party networks" when opening the public room directory
- The sync API will no longer filter out the user's own membership when using lazy-loading
- Dendrite will now correctly detect JetStream consumers being deleted, stopping the consumer goroutine as needed
- A panic in the federation API where the server list could go out of bounds has been fixed
- Blacklisted servers will now be excluded when querying joined servers, which improves CPU usage and performs less unnecessary outbound requests
- A database writer will now be used to assign state key NIDs when requesting NIDs that may not exist yet
- Dendrite will now correctly move local aliases for an upgraded room when the room is upgraded remotely
- Dendrite will now correctly move account data for an upgraded room when the room is upgraded remotely
- Missing state key NIDs will now be allocated on request rather than returning an error
- Guest access is now correctly denied on a number of endpoints
- Presence information will now be correctly sent for new private chats
- A number of unspecced fields have been removed from outbound `/send` transactions

---

# dynamic-dns v3.10.0-ls106

> published at 2022-11-29 [source](https://github.com/linuxserver/docker-ddclient/releases/tag/v3.10.0-ls106)

**LinuxServer Changes:**

Update build instructions for 3.10.0. Update default `ddclient.conf`.

**ddclient Changes:**

**Note**

Please also read the changelog for v3.10.0\_2 and v3.10.0\_1 which contains dependency changes.

### New features

- Added support for domaindiscount24.com
- Added support for njal.la

---

# ansible-core 2.14.1rc1

> published at 2022-11-28 [source](https://pypi.org/project/ansible-core/2.14.1rc1/)

> no description provided

---



---

# ansible-core 2.13.7rc1

> published at 2022-11-28 [source](https://pypi.org/project/ansible-core/2.13.7rc1/)

> no description provided

---



---

# appservice-slack 2.0.2-rc1 (2022-11-28)

> published at 2022-11-28 [source](https://github.com/matrix-org/matrix-appservice-slack/releases/tag/2.0.2-rc1)

## Bugfixes

- Correctly set "is\_falling\_back" flag in thread events so that events in threads are not also displayed as replies. ( [#709](https://github.com/matrix-org/matrix-appservice-slack/issues/709))
- Fix crash in `whoami` command when the team name of a Slack account is unavailable. ( [#714](https://github.com/matrix-org/matrix-appservice-slack/issues/714))
- Prevent admin commands from being unusable after an invalid command is issued. ( [#717](https://github.com/matrix-org/matrix-appservice-slack/issues/717))
- Improve the calculation of DM names & avatars. ( [#721](https://github.com/matrix-org/matrix-appservice-slack/issues/721))
- Ensure messages you send in a DM from Slack will appear in Matrix, even if the ghost of your Slack account was not initially present in the Matrix DM room. ( [#722](https://github.com/matrix-org/matrix-appservice-slack/issues/722))

## Internal Changes

- Update matrix-appservice-bridge to 6.0.0. ( [#710](https://github.com/matrix-org/matrix-appservice-slack/issues/710))
- Remove annoying noisy `Didn't handle event` log line. ( [#712](https://github.com/matrix-org/matrix-appservice-slack/issues/712))
- Remove IDE-related files from version control. ( [#715](https://github.com/matrix-org/matrix-appservice-slack/issues/715))

---

# mautrix-telegram v0.12.2

> published at 2022-11-26 [source](https://github.com/mautrix/telegram/releases/tag/v0.12.2)

### Added

- Added built-in custom emoji packs to allow reacting with any standard unicode emoji from Matrix (note that only premium users can use custom emojis).
- Added infinite backfill using [MSC2716](https://github.com/matrix-org/matrix-spec-proposals/pull/2716).

  - The new system includes a backwards compatibility mechanism which uses the old method of just sending events to the room. By default, MSC2716 is not enabled and the legacy method will be used.

### Improved

- Redacting reactions on Matrix no longer removes the user's other reactions to the same message (premium users can have up to 3 reactions per message).
- Changes to default user permissions on Telegram are now bridged.
- Added database index to make reaction polling more efficient (thanks to [@AndrewFerr](https://github.com/AndrewFerr) in [#862](https://github.com/mautrix/telegram/pull/862)).

### Fixed

- Fixed provisioning API not working with URL-encoded parameters.

---

# hydrogen v0.3.5

> published at 2022-11-25 [source](https://github.com/vector-im/hydrogen-web/releases/tag/v0.3.5)

- add sticky date headers in the timeline ( [#938](https://github.com/vector-im/hydrogen-web/pull/938) ). The stickyness is currently implemented with CSS which limits the design of the date header a bit, will improve on this later.
- fix [#451](https://github.com/vector-im/hydrogen-web/issues/451)
- restores active background of reaction button

---

# jitsi stable-8044-1: release

> published at 2022-11-25 [source](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-8044-1)

release

\\* [`fd70f04`](https://github.com/jitsi/docker-jitsi-meet/commit/fd70f045247e5f9eef08637bad493328598f424e) env: add note about JaaS account creation

\\* [`046bb79`](https://github.com/jitsi/docker-jitsi-meet/commit/046bb7927135fd5cf3f36d0a8129d978807e73dd) jaas: register JaaS account automatically

\\* [`c44c59e`](https://github.com/jitsi/docker-jitsi-meet/commit/c44c59e6cb5ccdadfe0e710b7c649545fb5904a8) misc: working on unstable

---

# mjolnir v1.6.1

> published at 2022-11-23 [source](https://github.com/matrix-org/mjolnir/releases/tag/v1.6.1)

## Changelog

### Bot:

- **Change of behaviour:** Mjolnir now supports specifying the config file with the argument `--mjolnir-config`. It is highly recommended that you do this as opposed to relying on the environment variable `NODE_ENV`. The documentation for running with [docker](https://github.com/matrix-org/mjolnir/blob/v1.6.1/docs/setup_docker.md) and from [source](https://github.com/matrix-org/mjolnir/blob/v1.6.1/docs/setup_selfbuild.md) have been updated accordingly.
- **Change of behaviour:** If you had run `latest` since `v1.5.0` or `v1.6.0`, briefly mjolnir would have loaded `default.yaml` if `NODE_ENV` was unset, whereas previously it would have loaded `development.yaml`. This led to highly confusing behaviour as the documentation for running from source had instructed users to create `development.yaml` without setting `NODE_ENV=development` while running mjolnir, which was how mjolnir understood to load `development.yaml` in all previous versions. Silently loading the default was an undocumented new behaviour that has now been removed.
- Fixed a bug where if `config.protectAllJoinedRooms` was enabled mjolnir would temporarily start protecting all watched policy lists at startup. by [@Gnuxie](https://github.com/Gnuxie) in [#431](https://github.com/matrix-org/mjolnir/pull/431). Thanks to [@cremesk](https://github.com/cremesk) for reporting.

**Full Changelog**: [`v1.6.0...v1.6.1`](https://github.com/matrix-org/mjolnir/compare/v1.6.0...v1.6.1)

---

# draupnir v1.6.1

> published at 2022-11-23 [source](https://github.com/Gnuxie/Draupnir/releases/tag/v1.6.1)

v1.6.1

---

# ansible 7.0.0

> published at 2022-11-22 [source](https://pypi.org/project/ansible/7.0.0/)

> no description provided

---



---

# mjolnir v1.6.0

> published at 2022-11-22 [source](https://github.com/matrix-org/mjolnir/releases/tag/v1.6.0)

## Changelog

### Bot:

#### Changes of behaviour:

- Use stable prefixes for policy recommendations ( `m.ban` instead of `org.matrix.mjolnir.ban`) by [@deepbluev7](https://github.com/deepbluev7) in [#329](https://github.com/matrix-org/mjolnir/pull/329)
- Always echo policy list changes. List changes are now always enabled, whereas before they where only shown with `config.verboseLogging`. Mjolnir now no longer hides changes made by the same mjolnir account, providing the user with feedback for changes they have made to policy lists after using the ban/unban commands. by [@Gnuxie](https://github.com/Gnuxie) in [#401](https://github.com/matrix-org/mjolnir/pull/401)
- Replaced `acceptInvitesFromGroup` with `acceptInvitesFromSpace`. by [@Gnuxie](https://github.com/Gnuxie) in [#338](https://github.com/matrix-org/mjolnir/pull/338)
- Policy lists created by mjolnir will now be done so with support for [MSC3784](https://github.com/matrix-org/matrix-spec-proposals/pull/3784). by [@Gnuxie](https://github.com/Gnuxie) in [#386](https://github.com/matrix-org/mjolnir/pull/386)

#### Everything else:

- Fix the WordList protection which was matching every message. by [@Gnuxie](https://github.com/Gnuxie) in [#427](https://github.com/matrix-org/mjolnir/pull/427)
- Rework the banning and unbanning of entities in PolicyLists by [@Gnuxie](https://github.com/Gnuxie) in [#345](https://github.com/matrix-org/mjolnir/pull/345):

  - Unbanning no longer searches for rules by `state_key` but by entity.
  - Users with longer mxids can now be banned.
  - Mjolnir no longer waits for the next client sync before applying server ACL and member bans after using the ban command
- Improve the clarity of the unban command. Users marked as banned by the `Flooding` and `FirstMessageIsMedia` protections can now be unbanned with the ban command. Using the unban command without `true` will now warn the user that room level bans will not be overridden. by [@Gnuxie](https://github.com/Gnuxie) in [#402](https://github.com/matrix-org/mjolnir/pull/402)
- Fix `config.protectAllJoinedRooms` leaking into explicitly protected rooms and mjolnir trying to protect watched lists in some circumstances. You would only have been impacted by this if you ran `latest`. by [@Gnuxie](https://github.com/Gnuxie) in [#385](https://github.com/matrix-org/mjolnir/pull/385)
- Add policy list shortcodes to status command output by [@tanriol](https://github.com/tanriol) in [#312](https://github.com/matrix-org/mjolnir/pull/312)
- Fix typo in `config set` command response by [@sharp-tailed](https://github.com/sharp-tailed) in [#382](https://github.com/matrix-org/mjolnir/pull/382)
- Fixed a bug where Mjolnir would temporarily apply a stale `m.room.server_acl` event to rooms during batching by [@Gnuxie](https://github.com/Gnuxie) in [#33](https://github.com/matrix-org/mjolnir/issues/33)

#### Documentation:

- Add section on Trusted Reporters to `docs/moderators.md` by [@jesopo](https://github.com/jesopo) in [#332](https://github.com/matrix-org/mjolnir/pull/332)
- Fix example to have correct incantation by [@erikjohnston](https://github.com/erikjohnston) in [#358](https://github.com/matrix-org/mjolnir/pull/358)
- Fixed typo in `docs/moderators.md` by [@mahdi1234](https://github.com/mahdi1234) in [#389](https://github.com/matrix-org/mjolnir/pull/389)
- Update `docs/setup_selfbuild.md` to explicitly provide a config and not rely on implicit behaviour from previous version. by [@Gnuxie](https://github.com/Gnuxie) in [#429](https://github.com/matrix-org/mjolnir/pull/429)

### New Contributors

- [@deepbluev7](https://github.com/deepbluev7) made their first contribution in [#329](https://github.com/matrix-org/mjolnir/pull/329)
- [@Half-Shot](https://github.com/Half-Shot) made their first contribution in [#337](https://github.com/matrix-org/mjolnir/pull/337)
- [@erikjohnston](https://github.com/erikjohnston) made their first contribution in [#358](https://github.com/matrix-org/mjolnir/pull/358)
- [@tanriol](https://github.com/tanriol) made their first contribution in [#312](https://github.com/matrix-org/mjolnir/pull/312)
- [@sharp-tailed](https://github.com/sharp-tailed) made their first contribution in [#382](https://github.com/matrix-org/mjolnir/pull/382)
- [@mahdi1234](https://github.com/mahdi1234) made their first contribution in [#389](https://github.com/matrix-org/mjolnir/pull/389)

**Full Changelog**: [`v1.5.0...v1.6.0`](https://github.com/matrix-org/mjolnir/compare/v1.5.0...v1.6.0)

### Minor changes impacting development

- Update tsconfig.json touse ES2021 by [@Half-Shot](https://github.com/Half-Shot) in [#337](https://github.com/matrix-org/mjolnir/pull/337)
- Towards opinions in PolicyLists. by [@Yoric](https://github.com/Yoric) in [#336](https://github.com/matrix-org/mjolnir/pull/336)
- Stop the config being global (in almost all contexts). by [@Gnuxie](https://github.com/Gnuxie) in [#334](https://github.com/matrix-org/mjolnir/pull/334)
- change report polling to read oldest first, so we paginate to end and wait by [@jesopo](https://github.com/jesopo) in [#353](https://github.com/matrix-org/mjolnir/pull/353)
- load config yaml manually, remove more references to static config by [@jesopo](https://github.com/jesopo) in [#347](https://github.com/matrix-org/mjolnir/pull/347)
- use requested path to configs by [@jesopo](https://github.com/jesopo) in [#357](https://github.com/matrix-org/mjolnir/pull/357)
- matrix.to urls for aliases don't need a `via` param by [@jesopo](https://github.com/jesopo) in [#348](https://github.com/matrix-org/mjolnir/pull/348)
- Missing awaits on floating promises in tests. by [@Gnuxie](https://github.com/Gnuxie) in [#359](https://github.com/matrix-org/mjolnir/pull/359)
- support compound consequences, switch WordList to consequences by [@jesopo](https://github.com/jesopo) in [#351](https://github.com/matrix-org/mjolnir/pull/351)
- Refactor protected rooms. by [@Gnuxie](https://github.com/Gnuxie) in [#371](https://github.com/matrix-org/mjolnir/pull/371)
- CI: Let's try and cache mx-tester installs by [@Yoric](https://github.com/Yoric) in [#383](https://github.com/matrix-org/mjolnir/pull/383)
- Update tests and instructions to intercept reports also with v3 endpoint by [@Yoric](https://github.com/Yoric) in [#388](https://github.com/matrix-org/mjolnir/pull/388)
- Access Control by [@Gnuxie](https://github.com/Gnuxie) in [#378](https://github.com/matrix-org/mjolnir/pull/378)
- Unbitrotting ruleserver tests by [@Yoric](https://github.com/Yoric) in [#418](https://github.com/matrix-org/mjolnir/pull/418)

---

# draupnir v1.6.0

> published at 2022-11-22 [source](https://github.com/Gnuxie/Draupnir/releases/tag/v1.6.0)

v1.6.0

---

# dynamic-dns v3.10.0-ls105

> published at 2022-11-22 [source](https://github.com/linuxserver/docker-ddclient/releases/tag/v3.10.0-ls105)

**LinuxServer Changes:**

Update build instructions for 3.10.0. Update default `ddclient.conf`.

**ddclient Changes:**

**Note**

Please also read the changelog for v3.10.0\_2 and v3.10.0\_1 which contains dependency changes.

### New features

- Added support for domaindiscount24.com
- Added support for njal.la

---

# soft-serve v0.4.4

> published at 2022-11-17 [source](https://github.com/charmbracelet/soft-serve/releases/tag/v0.4.4)

## Changelog

### New Features

- [`d3d2d63`](https://github.com/charmbracelet/soft-serve/commit/d3d2d63176323b2b89bbe40d4ad498ad164e518e): feat(ci): add code coverage ( [@aymanbagabas](https://github.com/aymanbagabas))

### Bug fixes

- [`7a7e73d`](https://github.com/charmbracelet/soft-serve/commit/7a7e73dda8cf32681cc31bedf64055f0094a2e21): fix(auth): wrong auth for anon users ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`97749ea`](https://github.com/charmbracelet/soft-serve/commit/97749eab1fbb05fc5b2e499ba315709d300c8911): fix(ci): run codecov on push ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`fdbbe08`](https://github.com/charmbracelet/soft-serve/commit/fdbbe08d6b168d5699daf4e0d941a75c825b8405): fix(config): set default anon-access to read-only ( [@aymanbagabas](https://github.com/aymanbagabas))

* * *

[![The Charm logo](https://camo.githubusercontent.com/65459c24b86d0476085210fd0387503a161a1359b3cf5034324346f55907cbb8/68747470733a2f2f73747566662e636861726d2e73682f636861726d2d62616467652e6a7067)](https://charm.sh/)

Thoughts? Questions? We love hearing from you. Feel free to reach out on [Twitter](https://twitter.com/charmcli), [The Fediverse](https://mastodon.technology/@charm), or on [Discord](https://charm.sh/chat).

---

# soft-serve v0.4.3

> published at 2022-11-17 [source](https://github.com/charmbracelet/soft-serve/releases/tag/v0.4.3)

## Changelog

### Bug fixes

- [`b41e50d`](https://github.com/charmbracelet/soft-serve/commit/b41e50d82c8de2ca78af96e4f350e746ee0d3909): fix(cmd): exit soft serve on error ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`8771223`](https://github.com/charmbracelet/soft-serve/commit/87712235de4d056c40ab6fa3cd1d4b616b6a8de8): fix(server): don't list unauthorized repos ( [@aymanbagabas](https://github.com/aymanbagabas))

* * *

[![The Charm logo](https://camo.githubusercontent.com/65459c24b86d0476085210fd0387503a161a1359b3cf5034324346f55907cbb8/68747470733a2f2f73747566662e636861726d2e73682f636861726d2d62616467652e6a7067)](https://charm.sh/)

Thoughts? Questions? We love hearing from you. Feel free to reach out on [Twitter](https://twitter.com/charmcli), [The Fediverse](https://mastodon.technology/@charm), or on [Discord](https://charm.sh/chat).

---

# soft-serve v0.4.2

> published at 2022-11-17 [source](https://github.com/charmbracelet/soft-serve/releases/tag/v0.4.2)

## Changelog

### New Features

- [`6133774`](https://github.com/charmbracelet/soft-serve/commit/6133774bb9e83f21aeab02ba50e8e40cdd6d09f8): feat(config): add auth tests ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`6669908`](https://github.com/charmbracelet/soft-serve/commit/66699089c0d29ccc2199f112fda9d4f750760fc8): feat(config): add repo collabs ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`9f8c09a`](https://github.com/charmbracelet/soft-serve/commit/9f8c09a5598280167742268ca9d07a788ff9b7c6): feat(config): per repo config ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`b079c14`](https://github.com/charmbracelet/soft-serve/commit/b079c14d4539e1ffa2ea4576cf2f073c9b96eb5d): feat(ui): back using right mouse click and backspace key ( [@aymanbagabas](https://github.com/aymanbagabas))

### Bug fixes

- [`8979f6b`](https://github.com/charmbracelet/soft-serve/commit/8979f6b7d78af74ac3cc664f25aed3f5d10d7e41): fix(git): disable pushing to subdirectories ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`cf2b319`](https://github.com/charmbracelet/soft-serve/commit/cf2b319914605fdb041ea5c13fd093ee22ad4854): fix(git): skip reading files ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`18d5989`](https://github.com/charmbracelet/soft-serve/commit/18d598959dbf8a192efab935b989a6a8bb9790d0): fix(session): unauthorized access to private repos ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`17bf584`](https://github.com/charmbracelet/soft-serve/commit/17bf5843492860d1d31b41f90b228534f9f59292): fix(ui): hide tabs while filtering ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`8bb68af`](https://github.com/charmbracelet/soft-serve/commit/8bb68af9d435fe8326b476d8ee621f9f0e549fcb): fix(ui): incorrect help while filtering ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`f65e532`](https://github.com/charmbracelet/soft-serve/commit/f65e5323180104c0610729e91fb030cc3c5ab5d1): fix(ui): move url style into its own ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`6dbdb43`](https://github.com/charmbracelet/soft-serve/commit/6dbdb434ea93d66b8dfacac8e615263c159356d1): fix(ui): quitting on filtering ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`f45c018`](https://github.com/charmbracelet/soft-serve/commit/f45c01885f617f96bb3f62966bae2e70a5cee3c6): fix: wait for ssh.Serve to terminate ( [@muesli](https://github.com/muesli))

### Dependency updates

- [`aed0130`](https://github.com/charmbracelet/soft-serve/commit/aed0130e70ab1835cfe05e2b2ee2f22f5a6be097): feat(deps): bump charmbracelet/wish ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`8c40f69`](https://github.com/charmbracelet/soft-serve/commit/8c40f695160d49ca2a811f75d1953c581b3ead42): feat(deps): bump github.com/charmbracelet/bubbletea ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`2c8a36c`](https://github.com/charmbracelet/soft-serve/commit/2c8a36c7c495ae532ccd50b4f8188afbe28236c8): feat(deps): bump github.com/charmbracelet/bubbletea ( [@dependabot](https://github.com/dependabot)\[bot\])

### Other work

- [`d88ccb9`](https://github.com/charmbracelet/soft-serve/commit/d88ccb97d3e756be489d680d29e7dc24b9792fb3): ref(config): clarify repo auth for key ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`c7a9618`](https://github.com/charmbracelet/soft-serve/commit/c7a9618c33d55eaa1491c0fa17df49ba59e87521): ref(ui): go back msg ( [@aymanbagabas](https://github.com/aymanbagabas))

* * *

[![The Charm logo](https://camo.githubusercontent.com/65459c24b86d0476085210fd0387503a161a1359b3cf5034324346f55907cbb8/68747470733a2f2f73747566662e636861726d2e73682f636861726d2d62616467652e6a7067)](https://charm.sh/)

Thoughts? Questions? We love hearing from you. Feel free to reach out on [Twitter](https://twitter.com/charmcli), [The Fediverse](https://mastodon.technology/@charm), or on [Discord](https://charm.sh/chat).

---

# com.devture.ansible.role.traefik v2.9.5

> published at 2022-11-17 [source](https://github.com/traefik/traefik/releases/tag/v2.9.5)

**Bug fixes:**

- **\[logs,middleware\]** Create a new capture instance for each incoming request ( [#9510](https://github.com/traefik/traefik/pull/9510) by [sdelicata](https://github.com/sdelicata))

**Documentation:**

- **\[k8s/helm\]** Update helm repository ( [#9506](https://github.com/traefik/traefik/pull/9506) by [charlie-haley](https://github.com/charlie-haley))
- Enhance wording of building-testing page ( [#9509](https://github.com/traefik/traefik/pull/9509) by [svx](https://github.com/svx))
- Add link descriptions and update wording ( [#9507](https://github.com/traefik/traefik/pull/9507) by [svx](https://github.com/svx))
- Removes the experimental tag on the Traefik Hub header ( [#9498](https://github.com/traefik/traefik/pull/9498) by [tfny](https://github.com/tfny))

---

# coturn docker/4.6.0-r1

> published at 2022-11-16 [source](https://github.com/coturn/coturn/releases/tag/docker%2F4.6.0-r1)

`4.6.0-r1` Docker image version of 4.6.0 Coturn release.

[Docker Hub](https://hub.docker.com/r/coturn/coturn) \| [GitHub Container Registry](https://github.com/orgs/coturn/packages/container/package/coturn) \| [Quay.io](https://quay.io/repository/coturn/coturn)

[Changelog](https://github.com/coturn/coturn/blob/docker/4.6.0-r1/docker/coturn/CHANGELOG.md#460-r1--2022-11-16)

---

# mautrix-whatsapp v0.7.2

> published at 2022-11-16 [source](https://github.com/mautrix/whatsapp/releases/tag/v0.7.2)

- Added option to handle all transactions asynchronously.
  - This may be useful for large instances, but using it means messages are no longer guaranteed to be sent to WhatsApp in the same order as Matrix.
- Fixed database error when backfilling disappearing messages on SQLite.
- Fixed incoming events blocking handling of incoming encryption keys.

---

# mautrix-googlechat v0.4.0

> published at 2022-11-15 [source](https://github.com/mautrix/googlechat/releases/tag/v0.4.0)

- Added support for bridging room mentions in both directions.
- Updated formatter to insert Matrix displayname into mentions when bridging from Google Chat. This ensures that the Matrix user gets mentioned correctly.
- Fixed images from Google Chat not being bridged with full resolution.
- Added SQLite support (thanks to [@durin42](https://github.com/durin42) in [#74](https://github.com/mautrix/googlechat/pull/74)).
- Updated Docker image to Alpine 3.16.
- Enabled appservice ephemeral events by default for new installations.
  - Existing bridges can turn it on by enabling `ephemeral_events` and disabling `sync_with_custom_puppets` in the config, then regenerating the registration file.
- Added options to make encryption more secure.
  - The `encryption` -\> `verification_levels` config options can be used to make the bridge require encrypted messages to come from cross-signed devices, with trust-on-first-use validation of the cross-signing master key.
  - The `encryption` -\> `require` option can be used to make the bridge ignore any unencrypted messages.
  - Key rotation settings can be configured with the `encryption` -\> `rotation` config.

---

# mautrix-hangouts v0.4.0

> published at 2022-11-15 [source](https://github.com/mautrix/googlechat/releases/tag/v0.4.0)

- Added support for bridging room mentions in both directions.
- Updated formatter to insert Matrix displayname into mentions when bridging from Google Chat. This ensures that the Matrix user gets mentioned correctly.
- Fixed images from Google Chat not being bridged with full resolution.
- Added SQLite support (thanks to [@durin42](https://github.com/durin42) in [#74](https://github.com/mautrix/googlechat/pull/74)).
- Updated Docker image to Alpine 3.16.
- Enabled appservice ephemeral events by default for new installations.
  - Existing bridges can turn it on by enabling `ephemeral_events` and disabling `sync_with_custom_puppets` in the config, then regenerating the registration file.
- Added options to make encryption more secure.
  - The `encryption` -\> `verification_levels` config options can be used to make the bridge require encrypted messages to come from cross-signed devices, with trust-on-first-use validation of the cross-signing master key.
  - The `encryption` -\> `require` option can be used to make the bridge ignore any unencrypted messages.
  - Key rotation settings can be configured with the `encryption` -\> `rotation` config.

---

# mautrix-facebook v0.4.1

> published at 2022-11-15 [source](https://github.com/mautrix/facebook/releases/tag/v0.4.1)

- Improved unsupported message fallbacks and added support for more message types from Facebook.
  - Powerup messages, descriptions in story attachments, facebook pay messages and group join links among other things should be bridged now.
- Added support for dynamically fetching a proxy URL.
- Added option to bypass startup script in docker image.

---

# ansible 7.0.0rc1

> published at 2022-11-15 [source](https://pypi.org/project/ansible/7.0.0rc1/)

> no description provided

---



---

# jitsi stable-8044: release

> published at 2022-11-15 [source](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-8044)

release

\\* [`b212dca`](https://github.com/jitsi/docker-jitsi-meet/commit/b212dca060e1c9649ae1846af9518e6714f7250a) web: fix parsing IPv6 reolver addresses

\\* [`53b2654`](https://github.com/jitsi/docker-jitsi-meet/commit/53b265455661fe0a6fc986ad589d6ecfd911847c) web: auto-detect nginx resolver

\\* [`9fbb5bd`](https://github.com/jitsi/docker-jitsi-meet/commit/9fbb5bd815e098d1ef15b99faec9372bd7cd3fea) jicofo: fix XMPP config (all moved to jicofo.conf)

\\* [`a2333b3`](https://github.com/jitsi/docker-jitsi-meet/commit/a2333b3e76d294e6d0a01ead69f4efa67fe12cd5) jicofo: remove JICOFO\_SHORT\_ID (removed upstream)

\\* [`d764db9`](https://github.com/jitsi/docker-jitsi-meet/commit/d764db93cee95124317faa34e79b1c0be480a18d) doc: update README

\\* [`c694a9e`](https://github.com/jitsi/docker-jitsi-meet/commit/c694a9e657627a2d31e0f10755caaa38d6d4f220) web: set charset as utf-8

\\* [`8660089`](https://github.com/jitsi/docker-jitsi-meet/commit/8660089eac016f3c484e2acd6bccf704780ba99b) misc: working on unstable

---

# dynamic-dns v3.10.0-ls104

> published at 2022-11-15 [source](https://github.com/linuxserver/docker-ddclient/releases/tag/v3.10.0-ls104)

**LinuxServer Changes:**

Update build instructions for 3.10.0. Update default `ddclient.conf`.

**ddclient Changes:**

**Note**

Please also read the changelog for v3.10.0\_2 and v3.10.0\_1 which contains dependency changes.

### New features

- Added support for domaindiscount24.com
- Added support for njal.la

---

# miniflux Miniflux 2.0.40

> published at 2022-11-14 [source](https://github.com/miniflux/v2/releases/tag/2.0.40)

- Update dependencies
- Pin Postgres image version in Docker Compose examples to avoid unexpected upgrades
- Make English and Spanish translation more consistent:
  - Use "Feed" everywhere instead of "Subscription"
  - Use "Entry" instead of "Article"
- Allow `Content-Type` and `Accept` headers in CORS policy
- Use dirs file for Debian package
- Use custom home page in PWA manifest
- Fix scraper rule that could be incorrect when there is a redirect
- Improve web scraper to fetch the only link present as workaround to some landing pages
- Add Matrix bot integration
- Proxify images in API responses
- Add new options in user preferences to configure sorting of entries in the category page
- Remove dependency on `github.com/mitchellh/go-server-timing`
- Add support for the `continuation` parameter and result for Google Reader API ID calls
- Use automatic variable for build target file names
- Add rewrite rule for `recalbox.com`
- Improve Dutch translation

---

# ntfy v1.29.0

> published at 2022-11-12 [source](https://github.com/binwiederhier/ntfy/releases/tag/v1.29.0)

This release adds the ability to add rate limit exemptions for IP ranges instead of just specific IP addresses. It also fixes a few bugs in the web app and the CLI and adds lots of new examples and install instructions.

Thanks to [some love on HN](https://news.ycombinator.com/item?id=33517944), we got so many new ntfy users trying out ntfy and joining the [chat rooms](https://github.com/binwiederhier/ntfy#chat--forum). **Welcome to the ntfy community to all of you!** We also got a ton of new **[sponsors and donations](https://github.com/sponsors/binwiederhier)** 💸, which is amazing. I'd like to thank all of you for believing in the project, and for helping me pay the server cost. The HN spike increased the AWS cost quite a bit.

**Features:**

- Allow IP CIDRs in `visitor-request-limit-exempt-hosts` ( [#423](https://github.com/binwiederhier/ntfy/issues/423), thanks to [@karmanyaahm](https://github.com/karmanyaahm))

**Bug fixes + maintenance:**

- Subscriptions can now have a display name ( [#370](https://github.com/binwiederhier/ntfy/issues/370), thanks to [@tfheen](https://github.com/tfheen) for reporting)
- Bump Go version to Go 18.x ( [#422](https://github.com/binwiederhier/ntfy/issues/422))
- Web: Strip trailing slash when subscribing ( [#428](https://github.com/binwiederhier/ntfy/issues/428), thanks to [@raining1123](https://github.com/raining1123) for reporting, and [@wunter8](https://github.com/wunter8) for fixing)
- Web: Strip trailing slash after server URL in publish dialog ( [#441](https://github.com/binwiederhier/ntfy/issues/441), thanks to [@wunter8](https://github.com/wunter8))
- Allow empty passwords in `client.yml` ( [#374](https://github.com/binwiederhier/ntfy/issues/374), thanks to [@cyqsimon](https://github.com/cyqsimon) for reporting, and [@wunter8](https://github.com/wunter8) for fixing)
- `ntfy pub` will now use default username and password from `client.yml` ( [#431](https://github.com/binwiederhier/ntfy/issues/431), thanks to [@wunter8](https://github.com/wunter8) for fixing)
- Make `ntfy sub` work with `NTFY_USER` env variable ( [#447](https://github.com/binwiederhier/ntfy/pull/447), thanks to [@SuperSandro2000](https://github.com/SuperSandro2000))
- Web: Disallow GET/HEAD requests with body in actions ( [#468](https://github.com/binwiederhier/ntfy/issues/468), thanks to [@ollien](https://github.com/ollien))

**Documentation:**

- Updated developer docs, bump nodejs and go version ( [#414](https://github.com/binwiederhier/ntfy/issues/414), thanks to [@YJSoft](https://github.com/YJSoft) for reporting)
- Officially document `?auth=..` query parameter ( [#433](https://github.com/binwiederhier/ntfy/pull/433), thanks to [@wunter8](https://github.com/wunter8))
- Added Rundeck example ( [#427](https://github.com/binwiederhier/ntfy/pull/427), thanks to [@demogorgonz](https://github.com/demogorgonz))
- Fix Debian installation instructions ( [#237](https://github.com/binwiederhier/ntfy/issues/237), thanks to [@Joeharrison94](https://github.com/Joeharrison94) for reporting)
- Updated [example](https://ntfy.sh/docs/examples/#gatus) with official [Gatus](https://github.com/TwiN/gatus) integration (thanks to [@TwiN](https://github.com/TwiN))
- Added [Kubernetes install instructions](https://ntfy.sh/docs/install/#kubernetes) ( [#452](https://github.com/binwiederhier/ntfy/pull/452), thanks to [@gmemstr](https://github.com/gmemstr))
- Added [additional NixOS links for self-hosting](https://ntfy.sh/docs/install/#nixos-nix) ( [#462](https://github.com/binwiederhier/ntfy/pull/462), thanks to [@wamserma](https://github.com/wamserma))
- Added additional [more secure nginx config example](https://ntfy.sh/docs/config/#nginxapache2caddy) ( [#451](https://github.com/binwiederhier/ntfy/pull/451), thanks to [@SuperSandro2000](https://github.com/SuperSandro2000))
- Minor fixes in the config table ( [#470](https://github.com/binwiederhier/ntfy/pull/470), thanks to [@snh](https://github.com/snh))
- Fix broken link ( [#476](https://github.com/binwiederhier/ntfy/pull/476), thanks to [@shuuji3](https://github.com/shuuji3))

**Additional translations:**

- Korean (thanks to [@YJSofta0f97461d82447ac](https://hosted.weblate.org/user/YJSofta0f97461d82447ac/))

**Sponsorships:**:

Thank you to the amazing folks who decided to [sponsor ntfy](https://github.com/sponsors/binwiederhier). Thank you for

helping carry the cost of the public server and developer licenses, and more importantly: Thank you for believing in ntfy!

You guys rock!

A list of all the sponsors can be found in the [README](https://github.com/binwiederhier/ntfy/blob/main/README.md).

---

# hydrogen v0.3.4

> published at 2022-11-10 [source](https://github.com/vector-im/hydrogen-web/releases/tag/v0.3.4)

- Fix message verification not working in rooms where we haven't sent a message yet ( [#920](https://github.com/vector-im/hydrogen-web/pull/920))
- Some smaller maintenance fixes ( [#916](https://github.com/vector-im/hydrogen-web/pull/916), [#917](https://github.com/vector-im/hydrogen-web/pull/917) and [#918](https://github.com/vector-im/hydrogen-web/pull/918))

---

# appservice-discord 3.1.1 (2022-11-10)

> published at 2022-11-10 [source](https://github.com/matrix-org/matrix-appservice-discord/releases/tag/v3.1.1)

## Bugfixes

- Fix a crash caused by processing metrics for Matrix events. ( [#869](https://github.com/matrix-org/matrix-appservice-discord/issues/869))

---

# ansible 7.0.0b1

> published at 2022-11-08 [source](https://pypi.org/project/ansible/7.0.0b1/)

> no description provided

---



---

# ansible 6.6.0

> published at 2022-11-08 [source](https://pypi.org/project/ansible/6.6.0/)

> no description provided

---



---

# com.devture.ansible.role.postgres REL_11_18

> published at 2022-11-07 [source](https://github.com/postgres/postgres/releases/tag/REL_11_18)

Stamp 11.18.

---

# com.devture.ansible.role.postgres REL_12_13

> published at 2022-11-07 [source](https://github.com/postgres/postgres/releases/tag/REL_12_13)

Stamp 12.13.

---

# com.devture.ansible.role.postgres REL_13_9

> published at 2022-11-07 [source](https://github.com/postgres/postgres/releases/tag/REL_13_9)

Stamp 13.9.

---

# com.devture.ansible.role.postgres REL_14_6

> published at 2022-11-07 [source](https://github.com/postgres/postgres/releases/tag/REL_14_6)

Stamp 14.6.

---

# com.devture.ansible.role.postgres REL_15_1

> published at 2022-11-07 [source](https://github.com/postgres/postgres/releases/tag/REL_15_1)

Stamp 15.1.

---

# ansible-core 2.13.6

> published at 2022-11-07 [source](https://pypi.org/project/ansible-core/2.13.6/)

> no description provided

---



---

# ansible-core 2.14.0

> published at 2022-11-07 [source](https://pypi.org/project/ansible-core/2.14.0/)

> no description provided

---



---

# dendrite Dendrite 0.10.7

> published at 2022-11-04 [source](https://github.com/matrix-org/dendrite/releases/tag/v0.10.7)

**NOTE:** Docker users may need to change the ownership of the Dendrite files (config, JetStream, logs, media and fulltext index path) to the `dendrite` user (UID `1337`) to complete this upgrade.

### Features

- Dendrite will now use a native SQLite port when building with `CGO_ENABLED=0`
- A number of `thirdparty` endpoints have been added, improving support for appservices

### Fixes

- The `"state"` section of the `/sync` response is no longer limited, so state events should not be dropped unexpectedly
- The deduplication of the `"timeline"` and `"state"` sections in `/sync` is now performed after applying history visibility, so state events should not be dropped unexpectedly
- The `prev_batch` token returned by `/sync` is now calculated after applying history visibility, so that the pagination boundaries are correct
- The room summary membership counts in `/sync` should now be calculated properly in more cases
- A false membership leave event should no longer be sent down `/sync` as a result of retiring an accepted invite (contributed by [tak-hntlabs](https://github.com/tak-hntlabs))
- Presence updates are now only sent to other servers for which the user shares rooms
- A bug which could cause a panic when converting events into the `ClientEvent` format has been fixed

---

# soft-serve v0.4.1

> published at 2022-11-04 [source](https://github.com/charmbracelet/soft-serve/releases/tag/v0.4.1)

## Changelog

### New Features

- [`7221051`](https://github.com/charmbracelet/soft-serve/commit/7221051867bcad32d58d3d3418909662b611a716): feat(config): support json config ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`b2c6401`](https://github.com/charmbracelet/soft-serve/commit/b2c6401e2fea9a83796c7a01bb94db602d701c1b): feat(server): use keyboard-interactive auth instead of password ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`db0c80b`](https://github.com/charmbracelet/soft-serve/commit/db0c80b34fd389a6636a83592af152dbc2dfd876): feat: add dependabot ( [@aymanbagabas](https://github.com/aymanbagabas))

### Bug fixes

- [`7baa99e`](https://github.com/charmbracelet/soft-serve/commit/7baa99e291629dce5b9e4c5e2096259185ba75be): fix(ci): use soft-serve goreleaser config ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`cf6870c`](https://github.com/charmbracelet/soft-serve/commit/cf6870c2d1e0c766836fbb7642a0b2f68e2199ff): fix(dep): exclude go-diff@v1.2.0 ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`216ccee`](https://github.com/charmbracelet/soft-serve/commit/216cceeb8e5e3e4015abe19dacd345398fb27303): fix: graceful shutdown ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`bccc716`](https://github.com/charmbracelet/soft-serve/commit/bccc71651ef0a1bbe31328759b25d8bb955421f2): fix: misleading authorized users comment ( [@aymanbagabas](https://github.com/aymanbagabas))

### Dependency updates

- [`ca2eb62`](https://github.com/charmbracelet/soft-serve/commit/ca2eb62ad6157a7f5dead2f4c6c0152a7c1da62c): feat(deps): bump github.com/aymanbagabas/go-osc52 from 1.0.3 to 1.2.1 ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`aaa9893`](https://github.com/charmbracelet/soft-serve/commit/aaa98938f03869da78e41cb052665aa926e6aa59): feat(deps): bump github.com/caarlos0/env/v6 from 6.10.0 to 6.10.1 ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`c6a3c34`](https://github.com/charmbracelet/soft-serve/commit/c6a3c3425123571f4bd5d11ee8fdb3e8adab8c33): feat(deps): bump github.com/caarlos0/env/v6 from 6.9.1 to 6.9.3 ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`880a761`](https://github.com/charmbracelet/soft-serve/commit/880a76125a928e0ec640e8014e3b3e0404d77d37): feat(deps): bump github.com/caarlos0/env/v6 from 6.9.3 to 6.10.0 ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`96588f9`](https://github.com/charmbracelet/soft-serve/commit/96588f9cfce69def6feaf9baa2da1cac770969be): feat(deps): bump github.com/charmbracelet/bubbles from 0.11.0 to 0.13.0 ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`12c2483`](https://github.com/charmbracelet/soft-serve/commit/12c2483744645a541a94c2af7a521514b7d9f917): feat(deps): bump github.com/charmbracelet/bubbles from 0.13.0 to 0.14.0 ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`8e69149`](https://github.com/charmbracelet/soft-serve/commit/8e691491b4e27850cbc9a63860c33ffd05d5554f): feat(deps): bump github.com/charmbracelet/bubbletea ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`8ae5f5a`](https://github.com/charmbracelet/soft-serve/commit/8ae5f5a2543247ad76d69f858dc30bdba66d41cb): feat(deps): bump github.com/charmbracelet/lipgloss from 0.5.0 to 0.6.0 ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`c9b88f4`](https://github.com/charmbracelet/soft-serve/commit/c9b88f4b8ecd30c0561694330b9507ea03aa8ae1): feat(deps): bump github.com/charmbracelet/wish from 0.5.0 to 0.6.0 ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`79ce212`](https://github.com/charmbracelet/soft-serve/commit/79ce2127cf468359709e6d85d8bce96709d31c6f): feat(deps): bump github.com/gliderlabs/ssh from 0.3.4 to 0.3.5 ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`7abfe3d`](https://github.com/charmbracelet/soft-serve/commit/7abfe3d7a58bec37cedcc55888bebbe3fd797a47): feat(deps): bump github.com/gogs/git-module from 1.6.1 to 1.7.0 ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`494ecb1`](https://github.com/charmbracelet/soft-serve/commit/494ecb10a492309cd5031d5bd7694ea365ca0cea): feat(deps): bump github.com/gogs/git-module from 1.7.0 to 1.7.1 ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`51ea0f8`](https://github.com/charmbracelet/soft-serve/commit/51ea0f8e0c02d1d584e117b6f833fb90da44cbe8): feat(deps): bump github.com/muesli/termenv from 0.12.0 to 0.13.0 ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`be4c80a`](https://github.com/charmbracelet/soft-serve/commit/be4c80af67cd0b619886b92907549761f7e7f9c9): feat(deps): bump github.com/spf13/cobra from 1.5.0 to 1.6.0 ( [@dependabot](https://github.com/dependabot)\[bot\])
- [`c0fcab4`](https://github.com/charmbracelet/soft-serve/commit/c0fcab4a7307ebed6c192392224155d08bdc801f): feat(deps): bump github.com/spf13/cobra from 1.6.0 to 1.6.1 ( [@dependabot](https://github.com/dependabot)\[bot\])

### Documentation updates

- [`b69881c`](https://github.com/charmbracelet/soft-serve/commit/b69881c24927ac525bbbe5ebbd0279c6aa19f225): docs: apt-key deprecation ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`b4ec8a4`](https://github.com/charmbracelet/soft-serve/commit/b4ec8a4805ac3042ac909742b56e62ed080f6b00): docs: fix debian/ubuntu install instructions ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`8851a0e`](https://github.com/charmbracelet/soft-serve/commit/8851a0e1c60f6714861367827167dfec7e75f526): docs: new readme footer ( [@muesli](https://github.com/muesli))
- [`e11cb71`](https://github.com/charmbracelet/soft-serve/commit/e11cb71bfd4792d407ab3bd1faa135a15df16f35): docs: remove duplicate comment ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`cc704b5`](https://github.com/charmbracelet/soft-serve/commit/cc704b53bec00e39b277a3e54f65570f90a77a5c): docs: show image as 100% and max-width 750px ( [@maaslalani](https://github.com/maaslalani))
- [`0da7a86`](https://github.com/charmbracelet/soft-serve/commit/0da7a8603813abba90f6446a5c2ce02d1781c5c7): docs: update apt/yum installation instructions ( [@aymanbagabas](https://github.com/aymanbagabas))

### Other work

- [`dc1f295`](https://github.com/charmbracelet/soft-serve/commit/dc1f295eb204efbeb6fc7e4295dd062a42f5059d): Improve port forwarding wording ( [@pingiun](https://github.com/pingiun))
- [`60f2f8f`](https://github.com/charmbracelet/soft-serve/commit/60f2f8fb9a31e237309db3286fec52b62c7334d4): Update issue templates ( [@aymanbagabas](https://github.com/aymanbagabas))

* * *

[![The Charm logo](https://camo.githubusercontent.com/65459c24b86d0476085210fd0387503a161a1359b3cf5034324346f55907cbb8/68747470733a2f2f73747566662e636861726d2e73682f636861726d2d62616467652e6a7067)](https://charm.sh/)

Thoughts? Questions? We love hearing from you. Feel free to reach out on [Twitter](https://twitter.com/charmcli), [The Fediverse](https://mastodon.technology/@charm), or on [Discord](https://charm.sh/chat).

---

# hydrogen v0.3.3

> published at 2022-11-04 [source](https://github.com/vector-im/hydrogen-web/releases/tag/v0.3.3)

- Provide basic UI to join room ( [#870](https://github.com/vector-im/hydrogen-web/pull/870))
- Fix an error that can stop sync ( [#885](https://github.com/vector-im/hydrogen-web/pull/885))
- Fetch missing e2ee devices when verifying message sender ( [#913](https://github.com/vector-im/hydrogen-web/pull/913))
- Lots of typescript conversion by [@ibeckermayer](https://github.com/ibeckermayer) 🙏

---

# appservice-discord 3.1.0 (2022-11-03)

> published at 2022-11-03 [source](https://github.com/matrix-org/matrix-appservice-discord/releases/tag/v3.1.0)

## Features

- Adds a config value, in order to disable forwarding room topic changes from Matrix to Discord ( `disableRoomTopicNotifications`, false by default). ( [#836](https://github.com/matrix-org/matrix-appservice-discord/issues/836))

## Bugfixes

- Include the domain name in the regular expression. ( [#834](https://github.com/matrix-org/matrix-appservice-discord/issues/834))
- Remove usage of unreliable field `age` on events, allowing the bridge to work with non-Synapse homeserver implementations. ( [#842](https://github.com/matrix-org/matrix-appservice-discord/issues/842))
- Prevent crashes when handling messages sent to voice channels. ( [#858](https://github.com/matrix-org/matrix-appservice-discord/issues/858))

---

# mautrix-instagram v0.2.2

> published at 2022-11-01 [source](https://github.com/mautrix/instagram/releases/tag/v0.2.2)

- Added option to send captions in the same message using [MSC2530](https://github.com/matrix-org/matrix-spec-proposals/pull/2530).
- Updated app version identifiers to bridge some new message types.
- Fixed race condition when backfilling chat with incoming message.

---

# dendrite Dendrite 0.10.6

> published at 2022-11-01 [source](https://github.com/matrix-org/dendrite/releases/tag/v0.10.6)

### Features

- History visibility checks have been optimised, which should speed up response times on a variety of endpoints (including `/sync`, `/messages`, `/context` and others) and reduce database load
- The built-in NATS Server has been updated to version 2.9.4
- Some other minor dependencies have been updated

### Fixes

- A panic has been fixed in the sync API PDU stream which could cause requests to fail
- The `/members` response now contains the `room_id` field, which may fix some E2EE problems with clients using the JS SDK (contributed by [ashkitten](https://github.com/ashkitten))
- The auth difference calculation in state resolution v2 has been tweaked for clarity (and moved into gomatrixserverlib with the rest of the state resolution code)

---

# dynamic-dns v3.10.0-ls103

> published at 2022-11-01 [source](https://github.com/linuxserver/docker-ddclient/releases/tag/v3.10.0-ls103)

**LinuxServer Changes:**

Update build instructions for 3.10.0. Update default `ddclient.conf`.

**ddclient Changes:**

**Note**

Please also read the changelog for v3.10.0\_2 and v3.10.0\_1 which contains dependency changes.

### New features

- Added support for domaindiscount24.com
- Added support for njal.la

---

# appservice-webhooks v1.0.4-beta.8

> published at 2022-10-31 [source](https://github.com/redoonetworks/matrix-appservice-webhooks/releases/tag/v1.0.4-beta.8)

No content.

---

# appservice-webhooks v1.0.4-beta.6

> published at 2022-10-31 [source](https://github.com/redoonetworks/matrix-appservice-webhooks/releases/tag/v1.0.4-beta.6)

No content.

---

# appservice-webhooks v1.0.4-beta.7

> published at 2022-10-31 [source](https://github.com/redoonetworks/matrix-appservice-webhooks/releases/tag/v1.0.4-beta.7)

Update WebhookStore.js

---

# appservice-webhooks v1.0.4-beta.5

> published at 2022-10-31 [source](https://github.com/redoonetworks/matrix-appservice-webhooks/releases/tag/v1.0.4-beta.5)

Update WebhookStore.js

adjust database.json to support sequalize and dbMigrate

---

# appservice-webhooks v1.0.4-beta.4

> published at 2022-10-31 [source](https://github.com/redoonetworks/matrix-appservice-webhooks/releases/tag/v1.0.4-beta.4)

No content.

---

# ansible-core 2.14.0rc2

> published at 2022-10-31 [source](https://pypi.org/project/ansible-core/2.14.0rc2/)

> no description provided

---



---

# ansible-core 2.13.6rc1

> published at 2022-10-31 [source](https://pypi.org/project/ansible-core/2.13.6rc1/)

> no description provided

---



---

# wireguard wireguard: timers: cast enum limits members to int in prints

> published at 2022-10-31 [source](https://git.zx2c4.com/wireguard-linux/commit/?id=2ef9b77e4ecbc4523ecc486644296bd7aeff2f18)

Since gcc13, each member of an enum has the same type as the enum. And
that is inherited from its members. Provided "REKEY\_AFTER\_MESSAGES =
1ULL << 60", the named type is unsigned long.

This generates warnings with gcc-13:
 error: format '%d' expects argument of type 'int', but argument 6 has type 'long unsigned int'

Cast those particular enum members to int when printing them.

Link: https://gcc.gnu.org/bugzilla/show\_bug.cgi?id=36113
Cc: Martin Liska
Signed-off-by: Jiri Slaby (SUSE)
Signed-off-by: Jason A. Donenfeld

---

# appservice-webhooks v1.0.4-beta.3

> published at 2022-10-31 [source](https://github.com/redoonetworks/matrix-appservice-webhooks/releases/tag/v1.0.4-beta.3)

- No changes to v1.0.4-beta.2

---

# appservice-webhooks v1.0.4-beta.2

> published at 2022-10-31 [source](https://github.com/redoonetworks/matrix-appservice-webhooks/releases/tag/v1.0.4-beta.2)

- Update dependencies
- Add support for postgres database. Default still remain sqlite

---

# ansible-core 2.14.0rc1.post0

> published at 2022-10-31 [source](https://pypi.org/project/ansible-core/2.14.0rc1.post0/)

> no description provided

---



---

# mautrix-instagram v0.2.1

> published at 2022-10-30 [source](https://github.com/mautrix/instagram/releases/tag/v0.2.1)

- Fixed login breaking due to an Instagram API change.
- Added support for SQLite as the bridge database.
- Added option to use [MSC2409](https://github.com/matrix-org/matrix-spec-proposals/pull/2409) and [MSC3202](https://github.com/matrix-org/matrix-spec-proposals/pull/3202) for end-to-bridge encryption. However, this may not work with the Synapse implementation as it hasn't been tested yet.
- The docker image now has an option to bypass the startup script by setting the `MAUTRIX_DIRECT_STARTUP` environment variable. Additionally, it will refuse to run as a non-root user if that variable is not set (and print an error message suggesting to either set the variable or use a custom command).
- Moved environment variable overrides for config fields to mautrix-python. The new system also allows loading JSON values to enable overriding maps like `login_shared_secret_map`.

---

# mautrix-signal v0.4.1

> published at 2022-10-28 [source](https://github.com/mautrix/signal/releases/tag/v0.4.1)

Target signald version: [v0.23.0](https://gitlab.com/signald/signald/-/releases/0.23.0)

- Dropped support for phone numbers as Signal user identifiers.
- Dropped support for v1 groups.
- Promoted SQLite support to non-experimental level.
- Fixed call notices not having a plaintext `body` field.
- "Implicit" messages from Signal (things like read receipts) that fail to decrypt will no longer send a notice to the Matrix room.
- The docker image now has an option to bypass the startup script by setting the `MAUTRIX_DIRECT_STARTUP` environment variable. Additionally, it will refuse to run as a non-root user if that variable is not set (and print an error message suggesting to either set the variable or use a custom command).

---

# com.devture.ansible.role.traefik v2.9.4

> published at 2022-10-27 [source](https://github.com/traefik/traefik/releases/tag/v2.9.4)

**Bug fixes:**

- **\[acme\]** Update go-acme/lego to v4.9.0 ( [#9413](https://github.com/traefik/traefik/pull/9413) by [tony-defa](https://github.com/tony-defa))
- **\[kv,redis\]** Fix Redis configuration type ( [#9435](https://github.com/traefik/traefik/pull/9435) by [ldez](https://github.com/ldez))
- **\[logs,middleware,metrics\]** Handle capture on redefined http.responseWriters ( [#9440](https://github.com/traefik/traefik/pull/9440) by [rtribotte](https://github.com/rtribotte))
- **\[middleware,k8s\]** Remove raw cert escape in PassTLSClientCert middleware ( [#9412](https://github.com/traefik/traefik/pull/9412) by [rtribotte](https://github.com/rtribotte))
- **\[plugins\]** Update Yaegi to v0.14.3 ( [#9468](https://github.com/traefik/traefik/pull/9468) by [ldez](https://github.com/ldez))
- Remove side effect on default transport tests ( [#9460](https://github.com/traefik/traefik/pull/9460) by [sdelicata](https://github.com/sdelicata))

**Documentation:**

- **\[k8s\]** Fix links to gateway API guides ( [#9445](https://github.com/traefik/traefik/pull/9445) by [kevinpollet](https://github.com/kevinpollet))
- Simplify dashboard rule example ( [#9454](https://github.com/traefik/traefik/pull/9454) by [sosoba](https://github.com/sosoba))
- Add v2.9 to release page ( [#9438](https://github.com/traefik/traefik/pull/9438) by [kevinpollet](https://github.com/kevinpollet))

---

# com.devture.ansible.role.traefik v2.9.3

> published at 2022-10-27 [source](https://github.com/traefik/traefik/releases/tag/v2.9.3)

Prepare release v2.9.3

---

# com.devture.ansible.role.traefik v2.9.2

> published at 2022-10-27 [source](https://github.com/traefik/traefik/releases/tag/v2.9.2)

Prepare release v2.9.2

---

# ansible 7.0.0a2

> published at 2022-10-25 [source](https://pypi.org/project/ansible/7.0.0a2/)

> no description provided

---



---

# appservice-irc 0.36.0 (2022-10-25)

> published at 2022-10-25 [source](https://github.com/matrix-org/matrix-appservice-irc/releases/tag/0.36.0)

## Bugfixes

- Disable metrics by default. ( [#1596](https://github.com/matrix-org/matrix-appservice-irc/issues/1596))
- Fix distribution of IrcServer.randomDomain() which tended to pick domains with a lower index when there are a lot of addresses for a server. ( [#1612](https://github.com/matrix-org/matrix-appservice-irc/issues/1612))
- Prevent runtime errors by provisisioning a room with a specific roomID. ( [#1614](https://github.com/matrix-org/matrix-appservice-irc/issues/1614))

## Improved Documentation

- Fix typos in the documentation. ( [#1599](https://github.com/matrix-org/matrix-appservice-irc/issues/1599))
- Update bridged networks doc to point towards GitHub issue [#1483](https://github.com/matrix-org/matrix-appservice-irc/issues/1483). ( [#1626](https://github.com/matrix-org/matrix-appservice-irc/issues/1626))

## Deprecations and Removals

- Breaking change: Raises the required NodeJS version to 16.


  Fixes Room visibility setting being broken for appservice directories. ( [#1616](https://github.com/matrix-org/matrix-appservice-irc/issues/1616))
- User activity tracking is now disabled by default, unless `userActivity` is enabled in the config. ( [#1638](https://github.com/matrix-org/matrix-appservice-irc/issues/1638))

## Internal Changes

- docker: prune dev dependencies from node\_modules. ( [#1541](https://github.com/matrix-org/matrix-appservice-irc/issues/1541))
- Logging: Use Map instead of object. ( [#1608](https://github.com/matrix-org/matrix-appservice-irc/issues/1608))
- Refactor Logging: Increase explicit TypeScript types and safer loops. ( [#1609](https://github.com/matrix-org/matrix-appservice-irc/issues/1609))
- Refactors: Reduce use of Bluebird, increase explicit types, use safer loops. ( [#1611](https://github.com/matrix-org/matrix-appservice-irc/issues/1611))
- Replace uses of the deprecated String.prototype.substr(). ( [#1615](https://github.com/matrix-org/matrix-appservice-irc/issues/1615))
- Add support for running tests against a real IRCd and Matrix homeserver in CI. ( [#1622](https://github.com/matrix-org/matrix-appservice-irc/issues/1622))
- Update to matrix-appservice-bridge 6.0.0. ( [#1631](https://github.com/matrix-org/matrix-appservice-irc/issues/1631))

---

# appservice-irc 0.35.1-uat-disable

> published at 2022-10-24 [source](https://github.com/matrix-org/matrix-appservice-irc/releases/tag/0.35.1-uat-disable)

0.35.1-uat-disable

---

# dynamic-dns v3.10.0-ls102

> published at 2022-10-22 [source](https://github.com/linuxserver/docker-ddclient/releases/tag/v3.10.0-ls102)

**LinuxServer Changes:**

Update build instructions for 3.10.0. Update default `ddclient.conf`.

**ddclient Changes:**

### New features

- Added support for domaindiscount24.com
- Added support for njal.la

---

# hookshot 2.4.0 (2022-10-21)

> published at 2022-10-21 [source](https://github.com/matrix-org/matrix-hookshot/releases/tag/2.4.0)

## Features

- Add support for notifying when a GitHub workflow completes. ( [#520](https://github.com/matrix-org/matrix-hookshot/issues/520))
- Disable GitHub workflow events by default. ( [#528](https://github.com/matrix-org/matrix-hookshot/issues/528))
- Support Jira version events. ( [#534](https://github.com/matrix-org/matrix-hookshot/issues/534))
- Allow multiple Jira connections at a time (either in the same room or across multiple rooms). ( [#540](https://github.com/matrix-org/matrix-hookshot/issues/540))

## Bugfixes

- Mention the `help` in AdminRooms if they send an invalid command. ( [#522](https://github.com/matrix-org/matrix-hookshot/issues/522))
- Fix an issue where `github status` would not respond with an error if your personal token had expired.


  Fix GitHub refresh tokens occasionally not working. ( [#523](https://github.com/matrix-org/matrix-hookshot/issues/523))
- Add support for notifying when a GitHub workflow completes. ( [#524](https://github.com/matrix-org/matrix-hookshot/issues/524))
- Fix a crash caused by invalid configuration in connection state events. ( [#537](https://github.com/matrix-org/matrix-hookshot/issues/537))
- Fix the Jira config widget to properly add listeners for issue creation events & expose support for issue update events. ( [#543](https://github.com/matrix-org/matrix-hookshot/issues/543))

## Internal Changes

- Use the `matrix-appservice-bridge` logging implementation. ( [#488](https://github.com/matrix-org/matrix-hookshot/issues/488))
- Increase network timeout for Docker builds, and fix Docker build OOMing in CI for arm64 builds. ( [#535](https://github.com/matrix-org/matrix-hookshot/issues/535))

---

# nginx-proxy release-1.22.1

> published at 2022-10-19 [source](https://github.com/nginx/nginx/releases/tag/release-1.22.1)

nginx-1.22.1-RELEASE

---

# nginx-proxy release-1.23.2

> published at 2022-10-19 [source](https://github.com/nginx/nginx/releases/tag/release-1.23.2)

nginx-1.23.2-RELEASE

---

# dynamic-dns v3.9.1-ls101

> published at 2022-10-18 [source](https://github.com/linuxserver/docker-ddclient/releases/tag/v3.9.1-ls101)

**LinuxServer Changes:**

Rebase to Alpine 3.15

**ddclient Changes:**

Yet again it's been a while but here is new release of ddclient. As usual,

there are some important changes and some documentation is modified.

A detailed overview can be found in git log but here's a quick overview:

- added support for Yandex.Mail for Domain DNS service
- added support for NearlyFreeSpeech.net
- added support for DNS Made Easy
- added systemd instructions
- added support for dondominio.com
- updated perl instruction
- updated fritzbox instructions
- fixed multidomain support for namecheap
- fixed support for Yandex

A very big thank you for everyone who created a pull request on github and

for everyone who helped to fix the little issues caused by the new providers.

And a very special thank you for [@DaveSophoServices](https://github.com/DaveSophoServices) who started to help with

the maintenance of ddclient.

---

# registration-bot v1.2.1

> published at 2022-10-18 [source](https://github.com/moan0s/matrix-registration-bot/releases/tag/v1.2.1)

Releasing version v1.2.1

---

# registration-bot v1.2.0

> published at 2022-10-18 [source](https://github.com/moan0s/matrix-registration-bot/releases/tag/v1.2.0)

Releasing version v1.2.0

---

# ansible-core 2.14.0rc1

> published at 2022-10-17 [source](https://pypi.org/project/ansible-core/2.14.0rc1/)

> no description provided

---



---

# miniflux Miniflux 2.0.39

> published at 2022-10-16 [source](https://github.com/miniflux/v2/releases/tag/2.0.39)

- Add support for date filtering in Google Reader API item ID calls
- Handle RSS entries with only a GUID permalink
- Go API Client: Accept endpoint URLs ending with `/v1/`
- CORS API headers: Allow `Basic` authorization header
- Log feed URL when submitting a subscription that returns an error
- Update `make run` command to execute migrations automatically
- Add option to send only the URL to Wallabag
- Do not convert anchors to absolute links
- Add config option to use a custom image proxy URL
- Allow zoom on mobile devices
- Add scraping rules for `theverge.com`, `royalroad.com`, `swordscomic.com`, and `smbc-comics.com`
- Add Ukrainian translation
- Update `golang.org/x/*` dependencies
- Bump `github.com/tdewolff/minify/v2` from `2.12.0` to `2.12.4`
- Bump `github.com/yuin/goldmark` from `1.4.13` to `1.5.2`
- Bump `github.com/lib/pq` from `1.10.6` to `1.10.7`

---

# mautrix-whatsapp v0.7.1

> published at 2022-10-16 [source](https://github.com/mautrix/whatsapp/releases/tag/v0.7.1)

- Added support for wa.me/qr links in `!wa resolve-link`.
- Added option to sync group members in parallel to speed up syncing large groups.
- Added initial support for WhatsApp message editing.
  - Sending edits will be disabled by default until official WhatsApp clients start rendering edits.
- Changed `private_chat_portal_meta` config option to be implicitly enabled in encrypted rooms, matching the behavior of other mautrix bridges.
- Updated media bridging to check homeserver media size limit before downloading media to avoid running out of memory.
  - The bridge may still run out of ram when bridging files if your homeserver has a large media size limit and a low bridge memory limit.

---

# ansible 6.5.0

> published at 2022-10-12 [source](https://pypi.org/project/ansible/6.5.0/)

> no description provided

---



---

# postmoogle v0.9.7

> published at 2022-10-12 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.7)

- fix room account data retrieving

---

# postmoogle v0.9.6

> published at 2022-10-11 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.6)

- show recipient's email (header `TO`)
- add `norecipient` option
- add anti-spam options ( `spamcheck:` and `spamlist:`)
- fix bugs

---

# ansible-core 2.14.0b3

> published at 2022-10-11 [source](https://pypi.org/project/ansible-core/2.14.0b3/)

> no description provided

---



---

# ansible-core 2.13.5

> published at 2022-10-11 [source](https://pypi.org/project/ansible-core/2.13.5/)

> no description provided

---



---

# ansible-core 2.12.10

> published at 2022-10-11 [source](https://pypi.org/project/ansible-core/2.12.10/)

> no description provided

---



---

# jitsi stable-7882: release

> published at 2022-10-07 [source](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-7882)

release

\\* [`4fcba2c`](https://github.com/jitsi/docker-jitsi-meet/commit/4fcba2c6aadfa1a3bcafe3cfa8b2064ab07003ab) jibri: update Chrome to M106

\\* [`957a225`](https://github.com/jitsi/docker-jitsi-meet/commit/957a225d3cdcde5496973da10d7b19680b3fadd2) misc: working on unstable

---

# postmoogle v0.9.5

> published at 2022-10-06 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.5)

- fix `!pm send` parsing with one-word subject and body
- fix `CTE_8BIT_MISMATCH`
- add account data encryption (keys and values) support
- add catch-all mailbox support

---

# appservice-slack 2.0.1 (2022-10-06)

> published at 2022-10-06 [source](https://github.com/matrix-org/matrix-appservice-slack/releases/tag/2.0.1)

This release fixes a bug in our build system that prevented Docker images from being built. Users not using Docker on `2.0.0`

are not required to upgrade.

---

# appservice-slack 2.0.0-testing-release-fix

> published at 2022-10-06 [source](https://github.com/matrix-org/matrix-appservice-slack/releases/tag/2.0.0-testing-release-fix)

2.0.0-testing-release-fix

---

# appservice-slack 2.0.0 (2022-10-05)

> published at 2022-10-06 [source](https://github.com/matrix-org/matrix-appservice-slack/releases/tag/2.0.0)

Note this release requires **Node.16** or greater.

## Features

- New releases will now include Docker images built on the `linux/amd64` platform in addition to x86\_64. ( [#656](https://github.com/matrix-org/matrix-appservice-slack/issues/656))

## Bugfixes

- Add missing `reactions:write` scope to oauth scopes. ( [#696](https://github.com/matrix-org/matrix-appservice-slack/issues/696))

- Fix PostgreSQL schema 14 not applying cleanly if `events` containing NULL columns. ( [#701](https://github.com/matrix-org/matrix-appservice-slack/issues/701))

- Fix `/metrics` and `/ready` paths requiring authentication. ( [#704](https://github.com/matrix-org/matrix-appservice-slack/issues/704))

- Disallowed the accidental behaviour of puppeting two or more Slack accounts from the same team to the same Matrix user. This would cause all puppeting for the


  user to stop working. The bridge will now **automatically delete** any puppeted connections which violate this rule. You will be notified via the bridge when


  this happens, so you can relink your accounts. ( [#705](https://github.com/matrix-org/matrix-appservice-slack/issues/705))

- Improve performance of removing deleted Slack users from rooms. ( [#649](https://github.com/matrix-org/matrix-appservice-slack/issues/649))


## Improved Documentation

- Use the matrix-appservice-bridge contributing guide. ( [#686](https://github.com/matrix-org/matrix-appservice-slack/issues/686))

## Deprecations and Removals

- The bridge now requires Node.JS 16 or greater. ( [#690](https://github.com/matrix-org/matrix-appservice-slack/issues/690))

## Internal Changes

- Don't allow NULL in SQL columns of events, matching the expectation of the models. ( [#670](https://github.com/matrix-org/matrix-appservice-slack/issues/670))
- Fix towncrier script for summarising the newsfiles ( [#677](https://github.com/matrix-org/matrix-appservice-slack/issues/677))
- Switch from `npm` to `yarn` for dependency management. ( [#685](https://github.com/matrix-org/matrix-appservice-slack/issues/685))
- Move CI to GitHub Actions. ( [#688](https://github.com/matrix-org/matrix-appservice-slack/issues/688))
- Add new CI workflow to check for signoffs. ( [#693](https://github.com/matrix-org/matrix-appservice-slack/issues/693))

---

# hookshot 2.3.0 (2022-10-05)

> published at 2022-10-05 [source](https://github.com/matrix-org/matrix-hookshot/releases/tag/2.3.0)

## Features

- Added `create-confidential` GitLab connection command. ( [#496](https://github.com/matrix-org/matrix-hookshot/issues/496))
- Add new GitLab connection flag `includeCommentBody`, to enable including the body of comments on MR notifications. ( [#500](https://github.com/matrix-org/matrix-hookshot/issues/500), [#517](https://github.com/matrix-org/matrix-hookshot/issues/517))
- Add room configuration widget for Jira. ( [#502](https://github.com/matrix-org/matrix-hookshot/issues/502))
- Add bot commands to list and remove Jira connections. ( [#503](https://github.com/matrix-org/matrix-hookshot/issues/503))
- Reorganize the GitHub widget to allow searching for repositories by organization. ( [#508](https://github.com/matrix-org/matrix-hookshot/issues/508))
- Print a notice message after successfully logging in to GitHub when conversing with the bot in a DM. ( [#512](https://github.com/matrix-org/matrix-hookshot/issues/512))

## Bugfixes

- Give a warning if the user attempts to add a configuration widget to the room without giving the bot permissions. ( [#491](https://github.com/matrix-org/matrix-hookshot/issues/491))
- Improve formatting of help commands and Jira's `whoami` command. ( [#504](https://github.com/matrix-org/matrix-hookshot/issues/504))
- Add a configuration widget for Jira. ( [#507](https://github.com/matrix-org/matrix-hookshot/issues/507))
- Fix inactive "Command Prefix" field in configuration widgets. ( [#515](https://github.com/matrix-org/matrix-hookshot/issues/515))
- Fix support for the "Labeled" event in the GitHub widget. ( [#519](https://github.com/matrix-org/matrix-hookshot/issues/519))

## Internal Changes

- Improve some type-checking in the codebase. ( [#505](https://github.com/matrix-org/matrix-hookshot/issues/505))
- Refactor the Vite component's `tsconfig.json` file to make it compatible with the TypeScript project settings & the TypeScript language server. ( [#506](https://github.com/matrix-org/matrix-hookshot/issues/506))
- Don't send empty query string in some widget API requests. ( [#518](https://github.com/matrix-org/matrix-hookshot/issues/518))

---

# ansible-core 2.14.0b2

> published at 2022-10-03 [source](https://pypi.org/project/ansible-core/2.14.0b2/)

> no description provided

---



---

# ansible-core 2.13.5rc1

> published at 2022-10-03 [source](https://pypi.org/project/ansible-core/2.13.5rc1/)

> no description provided

---



---

# ansible-core 2.12.10rc1

> published at 2022-10-03 [source](https://pypi.org/project/ansible-core/2.12.10rc1/)

> no description provided

---



---

# com.devture.ansible.role.traefik v2.9.1

> published at 2022-10-03 [source](https://github.com/traefik/traefik/releases/tag/v2.9.1)

**Enhancements:**

- **\[acme,tls\]** ACME Default Certificate ( [#9189](https://github.com/traefik/traefik/pull/9189) by [rtribotte](https://github.com/rtribotte))
- **\[consul,etcd,zk,kv,redis\]** Update valkeyrie to v1.0.0 ( [#9316](https://github.com/traefik/traefik/pull/9316) by [ldez](https://github.com/ldez))
- **\[consulcatalog,nomad\]** Support Nomad canary deployment ( [#9216](https://github.com/traefik/traefik/pull/9216) by [rtribotte](https://github.com/rtribotte))
- **\[consulcatalog\]** Move consulcatalog provider to only use health apis ( [#9140](https://github.com/traefik/traefik/pull/9140) by [kevinpollet](https://github.com/kevinpollet))
- **\[docker\]** Add support for reaching containers using host networking on Podman ( [#9190](https://github.com/traefik/traefik/pull/9190) by [freundTech](https://github.com/freundTech))
- **\[docker\]** Use IPv6 address ( [#9183](https://github.com/traefik/traefik/pull/9183) by [tomMoulard](https://github.com/tomMoulard))
- **\[docker\]** Add allowEmptyServices for Docker provider ( [#8690](https://github.com/traefik/traefik/pull/8690) by [jvasseur](https://github.com/jvasseur))
- **\[ecs\]** Add support for ECS Anywhere ( [#9324](https://github.com/traefik/traefik/pull/9324) by [tuxpower](https://github.com/tuxpower))
- **\[healthcheck\]** Add a method option to the service Health Check ( [#9165](https://github.com/traefik/traefik/pull/9165) by [ddtmachado](https://github.com/ddtmachado))
- **\[http3\]** Upgrade quic-go to v0.28.0 ( [#9187](https://github.com/traefik/traefik/pull/9187) by [tomMoulard](https://github.com/tomMoulard))
- **\[http\]** Start polling HTTP provider at the beginning ( [#9116](https://github.com/traefik/traefik/pull/9116) by [moutoum](https://github.com/moutoum))
- **\[k8s/crd,plugins\]** Load plugin configuration field value from Kubernetes Secret ( [#9103](https://github.com/traefik/traefik/pull/9103) by [rtribotte](https://github.com/rtribotte))
- **\[logs,tcp\]** Quiet down TCP RST packet error on read operation ( [#9007](https://github.com/traefik/traefik/pull/9007) by [rtribotte](https://github.com/rtribotte))
- **\[metrics\]** Add traffic size metrics ( [#9208](https://github.com/traefik/traefik/pull/9208) by [tomMoulard](https://github.com/tomMoulard))
- **\[middleware,pilot\]** Remove Pilot support ( [#9330](https://github.com/traefik/traefik/pull/9330) by [ldez](https://github.com/ldez))
- **\[rules,tcp\]** Support ALPN for TCP + TLS routers ( [#8913](https://github.com/traefik/traefik/pull/8913) by [sh7dm](https://github.com/sh7dm))
- **\[tcp,service,udp\]** Make the loadbalancers servers order random ( [#9037](https://github.com/traefik/traefik/pull/9037) by [qmloong](https://github.com/qmloong))
- **\[tls\]** Change default TLS options for more security ( [#8951](https://github.com/traefik/traefik/pull/8951) by [ddtmachado](https://github.com/ddtmachado))
- **\[tracing\]** Add Datadog GlobalTags support ( [#9266](https://github.com/traefik/traefik/pull/9266) by [sdelicata](https://github.com/sdelicata))

**Bug fixes:**

- **\[acme\]** Fix ACME panic ( [#9365](https://github.com/traefik/traefik/pull/9365) by [ldez](https://github.com/ldez))

**Documentation:**

- Prepare release v2.9.0 ( [#9409](https://github.com/traefik/traefik/pull/9409) by [tomMoulard](https://github.com/tomMoulard))
- **\[metrics\]** Rework metrics overview page ( [#9366](https://github.com/traefik/traefik/pull/9366) by [ddtmachado](https://github.com/ddtmachado))
- Prepare release v2.9.0-rc5 ( [#9402](https://github.com/traefik/traefik/pull/9402) by [ldez](https://github.com/ldez))
- Prepare release v2.9.0-rc4 ( [#9372](https://github.com/traefik/traefik/pull/9372) by [kevinpollet](https://github.com/kevinpollet))
- Prepare release v2.9.0-rc3 ( [#9344](https://github.com/traefik/traefik/pull/9344) by [kevinpollet](https://github.com/kevinpollet))
- Prepare release v2.9.0-rc2 ( [6c2c561](https://github.com/traefik/traefik/commit/6c2c561d8f935d76ccd07d28e1455c7768adc153) by [ldez](https://github.com/ldez))
- Prepare release v2.9.0-rc1 ( [#9334](https://github.com/traefik/traefik/pull/9334) by [rtribotte](https://github.com/rtribotte))

**Misc:**

- Merge current v2.8 into v2.9 ( [#9400](https://github.com/traefik/traefik/pull/9400) by [ldez](https://github.com/ldez))
- Merge current v2.8 into v2.9 ( [#9371](https://github.com/traefik/traefik/pull/9371) by [ldez](https://github.com/ldez))
- Merge current v2.8 into v2.9 ( [#9367](https://github.com/traefik/traefik/pull/9367) by [ldez](https://github.com/ldez))
- Merge current v2.8 into v2.9 ( [#9350](https://github.com/traefik/traefik/pull/9350) by [ldez](https://github.com/ldez))
- Merge current v2.8 into v2.9 ( [#9343](https://github.com/traefik/traefik/pull/9343) by [kevinpollet](https://github.com/kevinpollet))
- Merge v2.8.5 into master ( [#9329](https://github.com/traefik/traefik/pull/9329) by [rtribotte](https://github.com/rtribotte))
- Merge current v2.8 into master ( [#9291](https://github.com/traefik/traefik/pull/9291) by [rtribotte](https://github.com/rtribotte))
- Merge current v2.8 into master ( [#9265](https://github.com/traefik/traefik/pull/9265) by [kevinpollet](https://github.com/kevinpollet))
- Merge current v2.8 into master ( [#9209](https://github.com/traefik/traefik/pull/9209) by [kevinpollet](https://github.com/kevinpollet))
- Merge current v2.8 into master ( [#9146](https://github.com/traefik/traefik/pull/9146) by [kevinpollet](https://github.com/kevinpollet))
- Merge current v2.8 into master ( [#9135](https://github.com/traefik/traefik/pull/9135) by [kevinpollet](https://github.com/kevinpollet))

---

# corporal 2.4.0

> published at 2022-10-02 [source](https://github.com/devture/matrix-corporal/releases/tag/2.4.0)

Release 2.4.0

---

# corporal Release 2.3.2

> published at 2022-10-02 [source](https://github.com/devture/matrix-corporal/releases/tag/2.3.2)

This is pretty much the same as 2.3.0 - reverting [`0d4b597`](https://github.com/devture/matrix-corporal/commit/0d4b597c1a1fabe1da42c63020fc4217c3db70ca).

---

# mautrix-telegram v0.12.1

> published at 2022-10-01 [source](https://github.com/mautrix/telegram/releases/tag/v0.12.1)

### Added

- Support for custom emojis in reactions.
  - Like other bridges with custom emoji reactions, they're bridged as `mxc://` URIs, so client support is required to render them properly.

### Improved

- The bridge will now poll for reactions to 20 most recent messages when receiving a read receipt. This works around Telegram's bad protocol that doesn't notify clients on reactions to other users' messages.
- The docker image now has an option to bypass the startup script by setting the `MAUTRIX_DIRECT_STARTUP` environment variable. Additionally, it will refuse to run as a non-root user if that variable is not set (and print an error message suggesting to either set the variable or use a custom command).
- Moved environment variable overrides for config fields to mautrix-python. The new system also allows loading JSON values to enable overriding maps like `login_shared_secret_map`.

### Fixed

- `ChatParticipantsForbidden` is handled properly when syncing non-supergroup info.
- Fixed some bugs with file transfers when using SQLite.
- Fixed error when attempting to log in again after logging out.
- Fixed QR login not working.
- Fixed error syncing chats if bridging a message had previously been interrupted.

---

# corporal 2.3.1

> published at 2022-09-30 [source](https://github.com/devture/matrix-corporal/releases/tag/2.3.1)

Release 2.3.1

---

# ansible 7.0.0a1

> published at 2022-09-29 [source](https://pypi.org/project/ansible/7.0.0a1/)

> no description provided

---



---

# jitsi stable-7830: release

> published at 2022-09-28 [source](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-7830)

release

\\* [`dd95b3d`](https://github.com/jitsi/docker-jitsi-meet/commit/dd95b3d29caef827c26923a4c5f102d19a038d14) prosody: fix arm64 build

\\* [`acb2f4e`](https://github.com/jitsi/docker-jitsi-meet/commit/acb2f4eb03c60813914dc81cbeb062e96f747edb) misc: update stale.yml

\\* [`02e32e5`](https://github.com/jitsi/docker-jitsi-meet/commit/02e32e5294f655137f2c470514403efb8d4fcf2c) jibri: update Chrome to M105

\\* [`c53de72`](https://github.com/jitsi/docker-jitsi-meet/commit/c53de728cedf54edcbb94e104b3a45fd029f8b9d) jvb: add JVB\_ADVERTISE\_IPS, deprecating DOCKER\_HOST\_ADDRESS

\\* [`723acc2`](https://github.com/jitsi/docker-jitsi-meet/commit/723acc20ef0466d232337d912bae522cc5cac038) web: add ability to configure the room password digit size

\\* [`a1e82ea`](https://github.com/jitsi/docker-jitsi-meet/commit/a1e82ea582ef95eb1b9593154601f0a211ef30f3) jvb: migrate config to secure octo

\\* [`91043c5`](https://github.com/jitsi/docker-jitsi-meet/commit/91043c581dedcf7776a18932a36013f93c71f9bc) prosody: upgrade UVS module to be compatible with Prosody 0.12 and luajwtjitsi 3.0

\\* [`dc5b6a1`](https://github.com/jitsi/docker-jitsi-meet/commit/dc5b6a1ef558dc2c9241e37ace9df3d035a62605) fix: multi tenant setup ( [#1401](https://github.com/jitsi/docker-jitsi-meet/pull/1401))

\\* [`47804d0`](https://github.com/jitsi/docker-jitsi-meet/commit/47804d0b9e1657debf0200472e3eb22563a26100) prosody: add JWT\_ENABLE\_DOMAIN\_VERIFICATION to compose file

\\* [`832b178`](https://github.com/jitsi/docker-jitsi-meet/commit/832b178d64fa7ae79262669201eef97ae6d354d5) prosody: make GC options configurable

\\* [`bf6a68b`](https://github.com/jitsi/docker-jitsi-meet/commit/bf6a68b17468d488bcdd7cee1ce4e6f534a5a296) web: fix setting prefix for subdomains

\\* [`5fabec9`](https://github.com/jitsi/docker-jitsi-meet/commit/5fabec93aac83037903795c25c7322370b4dec5b) prosody: add end conference

\\* [`7f7a9b4`](https://github.com/jitsi/docker-jitsi-meet/commit/7f7a9b4ee97228714693db2f3fbdaf65ffe8efe7) misc: working on unstable

---

# languagetool v5.9

> published at 2022-09-28 [source](https://github.com/Erikvl87/docker-languagetool/releases/tag/v5.9)

Update languagetool to 5.9

---

# cinny v2.2.1

> published at 2022-09-28 [source](https://github.com/cinnyapp/cinny/releases/tag/v2.2.1)

## 🔑 Security

- Update dependency [matrix-js-sdk to v19.7.0](https://matrix.org/blog/2022/09/28/upgrade-now-to-address-encryption-vulns-in-matrix-sdks-and-clients)

## 🐞 Bugs

- Handle nested lists by [@ginnyTheCat](https://github.com/ginnyTheCat) in [#853](https://github.com/cinnyapp/cinny/pull/853)
- Fix crash on space leave by [@ajbura](https://github.com/ajbura)
- Wrap view source text by [@ajbura](https://github.com/ajbura)

**Full Changelog**: [`v2.2.0...v2.2.1`](https://github.com/cinnyapp/cinny/compare/v2.2.0...v2.2.1)

---

# cinny v2.2.2

> published at 2022-09-28 [source](https://github.com/cinnyapp/cinny/releases/tag/v2.2.2)

## 🐞 Bugs

- Bump IDB [crypto store version](https://github.com/cinnyapp/cinny/pull/879).

**Full Changelog**: [`v2.2.1...v2.2.2`](https://github.com/cinnyapp/cinny/compare/v2.2.1...v2.2.2)

---

# ntfy v1.28.0

> published at 2022-09-27 [source](https://github.com/binwiederhier/ntfy/releases/tag/v1.28.0)

This release primarily adds icon support for the Android app, and adds a display name to subscriptions in the web app. Aside from that, we fixed a few random bugs, most importantly the `Priority` header bug that allows the use behind Cloudflare. We also added a ton of documentation. Most prominently, an [integrations + projects page](https://ntfy.sh/docs/integrations/).

As of now, I also have started accepting **[donations and sponsorships](https://github.com/sponsors/binwiederhier)** 💸. I would be very humbled if you consider donating.

**Features:**

- Subscription display name for the web app ( [#348](https://github.com/binwiederhier/ntfy/pull/348))
- Allow setting socket permissions via `--listen-unix-mode` ( [#356](https://github.com/binwiederhier/ntfy/pull/356), thanks to [@koro666](https://github.com/koro666))
- Icons can be set for each individual notification ( [#126](https://github.com/binwiederhier/ntfy/issues/126), thanks to [@wunter8](https://github.com/wunter8))
- CLI: Allow default username/password in `client.yml` ( [#372](https://github.com/binwiederhier/ntfy/pull/372), thanks to [@wunter8](https://github.com/wunter8))
- Build support for other Unix systems ( [#393](https://github.com/binwiederhier/ntfy/pull/393), thanks to [@la-ninpre](https://github.com/la-ninpre))

**Bugs:**

- `ntfy user` commands don't work with `auth_file` but works with `auth-file` ( [#344](https://github.com/binwiederhier/ntfy/issues/344), thanks to [@Histalek](https://github.com/Histalek) for reporting)
- Ignore new draft HTTP `Priority` header ( [#351](https://github.com/binwiederhier/ntfy/issues/351), thanks to [@ksurl](https://github.com/ksurl) for reporting)
- Delete expired attachments based on mod time instead of DB entry to avoid races (no ticket)
- Better logging for Matrix push key errors ( [#384](https://github.com/binwiederhier/ntfy/pull/384), thanks to [@christophehenry](https://github.com/christophehenry))
- Web: Switched "Pop" and "Pop Swoosh" sounds ( [#352](https://github.com/binwiederhier/ntfy/issues/352), thanks to [@coma-toast](https://github.com/coma-toast) for reporting)

**Documentation:**

- Added [integrations + projects page](https://ntfy.sh/docs/integrations/) ( **so many integrations, whoa!**)
- Added example for [UptimeRobot](https://ntfy.sh/docs/examples/#uptimerobot)
- Fix some PowerShell publish docs ( [#345](https://github.com/binwiederhier/ntfy/pull/345), thanks to [@noahpeltier](https://github.com/noahpeltier))
- Clarified Docker install instructions ( [#361](https://github.com/binwiederhier/ntfy/issues/361), thanks to [@barart](https://github.com/barart) for reporting)
- Mismatched quotation marks ( [#392](https://github.com/binwiederhier/ntfy/pull/392)\], thanks to [@connorlanigan](https://github.com/connorlanigan))

**Additional translations:**

- Ukranian (thanks to [@v.kopitsa](https://hosted.weblate.org/user/v.kopitsa/))
- Polish (thanks to [@Namax0r](https://hosted.weblate.org/user/Namax0r/))

---

# prometheus_node_exporter 1.4.0 / 2022-09-24

> published at 2022-09-26 [source](https://github.com/prometheus/node_exporter/releases/tag/v1.4.0)

- \[CHANGE\] Merge metrics descriptions in textfile collector [#2475](https://github.com/prometheus/node_exporter/pull/2475)
- \[FEATURE\] \[node-mixin\] Add darwin dashboard to mixin [#2351](https://github.com/prometheus/node_exporter/pull/2351)
- \[FEATURE\] Add "isolated" metric on cpu collector on linux [#2251](https://github.com/prometheus/node_exporter/pull/2251)
- \[FEATURE\] Add cgroup summary collector [#2408](https://github.com/prometheus/node_exporter/pull/2408)
- \[FEATURE\] Add selinux collector [#2205](https://github.com/prometheus/node_exporter/pull/2205)
- \[FEATURE\] Add slab info collector [#2376](https://github.com/prometheus/node_exporter/pull/2376)
- \[FEATURE\] Add sysctl collector [#2425](https://github.com/prometheus/node_exporter/pull/2425)
- \[FEATURE\] Also track the CPU Spin time for OpenBSD systems [#1971](https://github.com/prometheus/node_exporter/pull/1971)
- \[FEATURE\] Add support for MacOS version [#2471](https://github.com/prometheus/node_exporter/pull/2471)
- \[ENHANCEMENT\] \[node-mixin\] Add missing selectors [#2426](https://github.com/prometheus/node_exporter/pull/2426)
- \[ENHANCEMENT\] \[node-mixin\] Change current datasource to grafana's default [#2281](https://github.com/prometheus/node_exporter/pull/2281)
- \[ENHANCEMENT\] \[node-mixin\] Change disk graph to disk table [#2364](https://github.com/prometheus/node_exporter/pull/2364)
- \[ENHANCEMENT\] \[node-mixin\] Change io time units to %util [#2375](https://github.com/prometheus/node_exporter/pull/2375)
- \[ENHANCEMENT\] Ad user\_wired\_bytes and laundry\_bytes on \*bsd [#2266](https://github.com/prometheus/node_exporter/pull/2266)
- \[ENHANCEMENT\] Add additional vm\_stat memory metrics for darwin [#2240](https://github.com/prometheus/node_exporter/pull/2240)
- \[ENHANCEMENT\] Add device filter flags to arp collector [#2254](https://github.com/prometheus/node_exporter/pull/2254)
- \[ENHANCEMENT\] Add diskstats include and exclude device flags [#2417](https://github.com/prometheus/node_exporter/pull/2417)
- \[ENHANCEMENT\] Add node\_softirqs\_total metric [#2221](https://github.com/prometheus/node_exporter/pull/2221)
- \[ENHANCEMENT\] Add rapl zone name label option [#2401](https://github.com/prometheus/node_exporter/pull/2401)
- \[ENHANCEMENT\] Add slabinfo collector [#1799](https://github.com/prometheus/node_exporter/issues/1799)
- \[ENHANCEMENT\] Allow user to select port on NTP server to query [#2270](https://github.com/prometheus/node_exporter/pull/2270)
- \[ENHANCEMENT\] collector/diskstats: Add labels and metrics from udev [#2404](https://github.com/prometheus/node_exporter/pull/2404)
- \[ENHANCEMENT\] Enable builds against older macOS SDK [#2327](https://github.com/prometheus/node_exporter/pull/2327)
- \[ENHANCEMENT\] qdisk-linux: Add exclude and include flags for interface name [#2432](https://github.com/prometheus/node_exporter/pull/2432)
- \[ENHANCEMENT\] systemd: Expose systemd minor version [#2282](https://github.com/prometheus/node_exporter/pull/2282)
- \[ENHANCEMENT\] Use netlink for tcpstat collector [#2322](https://github.com/prometheus/node_exporter/pull/2322)
- \[ENHANCEMENT\] Use netlink to get netdev stats [#2074](https://github.com/prometheus/node_exporter/pull/2074)
- \[ENHANCEMENT\] Add additional perf counters for stalled frontend/backend cycles [#2191](https://github.com/prometheus/node_exporter/pull/2191)
- \[ENHANCEMENT\] Add btrfs device error stats [#2193](https://github.com/prometheus/node_exporter/pull/2193)
- \[BUGFIX\] \[node-mixin\] Fix fsSpaceAvailableCriticalThreshold and fsSpaceAvailableWarning [#2352](https://github.com/prometheus/node_exporter/pull/2352)
- \[BUGFIX\] Fix concurrency issue in ethtool collector [#2289](https://github.com/prometheus/node_exporter/pull/2289)
- \[BUGFIX\] Fix concurrency issue in netdev collector [#2267](https://github.com/prometheus/node_exporter/pull/2267)
- \[BUGFIX\] Fix diskstat reads and write metrics for disks with different sector sizes [#2311](https://github.com/prometheus/node_exporter/pull/2311)
- \[BUGFIX\] Fix iostat on macos broken by deprecation warning [#2292](https://github.com/prometheus/node_exporter/pull/2292)
- \[BUGFIX\] Fix NodeFileDescriptorLimit alerts [#2340](https://github.com/prometheus/node_exporter/pull/2340)
- \[BUGFIX\] Sanitize rapl zone names [#2299](https://github.com/prometheus/node_exporter/issues/2299)
- \[BUGFIX\] Add file descriptor close safely in test [#2447](https://github.com/prometheus/node_exporter/pull/2447)
- \[BUGFIX\] Fix race condition in os\_release.go [#2454](https://github.com/prometheus/node_exporter/pull/2454)
- \[BUGFIX\] Skip ZFS IO metrics if their paths are missing [#2451](https://github.com/prometheus/node_exporter/pull/2451)

---

# ansible-core 2.14.0b1

> published at 2022-09-26 [source](https://pypi.org/project/ansible-core/2.14.0b1/)

> no description provided

---



---

# appservice-irc 0.35.1 (2022-09-26)

> published at 2022-09-26 [source](https://github.com/matrix-org/matrix-appservice-irc/releases/tag/0.35.1)

# 🔒 Security

This release addresses a security vulnerability in the bridge. **Please update as a matter of urgency.** A matrix.org blog post detailing the specifics of the bugs will be available soon.

### Mitigation

A new security vulnerability was found in the matrix-appservice-irc bridge, for which we are releasing 0.35.1 as a fix. If you have the provisioning API enabled, this is potentially exploitable, so we advise you to upgrade immediately.

In case you cannot upgrade at the moment, we advise to update your IRC bridge configuration as a mitigation as follows:

- [Change user permissions](https://github.com/matrix-org/matrix-appservice-irc/blob/develop/config.sample.yaml#L607-L610) to prevent untrusted users from issuing !plumb commands.
- [Disable provisioning](https://github.com/matrix-org/matrix-appservice-irc/blob/develop/config.sample.yaml#L607-L610) if enabled.

You may revert these configuration changes after patching.

* * *

## Bugfixes

- Prevent possible attack by provisisioning a room with a specific roomID. ( [#1619](https://github.com/matrix-org/matrix-appservice-irc/issues/1619))

---

# appservice-irc sec-fix-26-09-22

> published at 2022-09-26 [source](https://github.com/matrix-org/matrix-appservice-irc/releases/tag/sec-fix-26-09-22)

Prevent SQL injection from roomId values when checking room visibility

---

# honoroit v0.9.15

> published at 2022-09-24 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.15)

> **NOTE**: tag was re-created to include missed changes

do not fail on disabled presence

---

# honoroit v0.9.14

> published at 2022-09-23 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.14)

- add `HONOROIT_ALLOWEDUSERS`

---

# postmoogle v0.9.4

> published at 2022-09-23 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.4)

- send emails in UTF-8 👋
- fix options descriptions
- add SMTP auth
- allow sending emails from your apps and scripts using postmoogle as email provider

---

# redis 7.0.5

> published at 2022-09-21 [source](https://github.com/redis/redis/releases/tag/7.0.5)

Upgrade urgency: SECURITY, contains fixes to security issues.

Security Fixes:

- (CVE-2022-35951) Executing a XAUTOCLAIM command on a stream key in a specific


  state, with a specially crafted COUNT argument, may cause an integer overflow,


  a subsequent heap overflow, and potentially lead to remote code execution.


  The problem affects Redis versions 7.0.0 or newer


  \[reported by Xion (SeungHyun Lee) of KAIST GoN\].

# Module API changes

- Fix RM\_Call execution of scripts when used with M/W/S flags to properly


  handle script flags ( [#11159](https://github.com/redis/redis/pull/11159))
- Fix RM\_SetAbsExpire and RM\_GetAbsExpire API registration ( [#11025](https://github.com/redis/redis/pull/11025), [#8564](https://github.com/redis/redis/pull/8564))

# Bug Fixes

- Fix a hang when eviction is combined with lazy-free and maxmemory-eviction-tenacity is set to 100 ( [#11237](https://github.com/redis/redis/pull/11237))
- Fix a crash when a replica may attempt to set itself as its master as a result of a manual failover ( [#11263](https://github.com/redis/redis/pull/11263))
- Fix a bug where a cluster-enabled replica node may permanently set its master's hostname to '?' ( [#10696](https://github.com/redis/redis/pull/10696))
- Fix a crash when a Lua script returns a meta-table ( [#11032](https://github.com/redis/redis/pull/11032))

## Fixes for issues in previous releases of Redis 7.0

- Fix redis-cli to do DNS lookup before sending CLUSTER MEET ( [#11151](https://github.com/redis/redis/pull/11151))
- Fix crash when a key is lazy expired during cluster key migration ( [#11176](https://github.com/redis/redis/pull/11176))
- Fix AOF rewrite to fsync the old AOF file when a new one is created ( [#11004](https://github.com/redis/redis/pull/11004))
- Fix some crashes involving a list containing entries larger than 1GB ( [#11242](https://github.com/redis/redis/pull/11242))
- Correctly handle scripts with a non-read-only shebang on a cluster replica ( [#11223](https://github.com/redis/redis/pull/11223))
- Fix memory leak when unloading a module ( [#11147](https://github.com/redis/redis/pull/11147))
- Fix bug with scripts ignoring client tracking NOLOOP ( [#11052](https://github.com/redis/redis/pull/11052))
- Fix client-side tracking breaking protocol when FLUSHDB / FLUSHALL / SWAPDB is used inside MULTI-EXEC ( [#11038](https://github.com/redis/redis/pull/11038))
- Fix ACL: BITFIELD with GET and also SET / INCRBY can be executed with read-only key permission ( [#11086](https://github.com/redis/redis/pull/11086))
- Fix missing sections for INFO ALL when also requesting a module info section ( [#11291](https://github.com/redis/redis/pull/11291))

---

# postmoogle v0.9.3

> published at 2022-09-21 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.3)

- update deps
- add read receipts
- add typing notifications

---

# mautrix-signal v0.4.0

> published at 2022-09-17 [source](https://github.com/mautrix/signal/releases/tag/v0.4.0)

Target signald version: [v0.21.1](https://gitlab.com/signald/signald/-/releases/0.21.1)

**N.B.** This release requires a homeserver with Matrix v1.1 support, which bumps up the minimum homeserver versions to Synapse 1.54 and Dendrite 0.8.7. Minimum Conduit version remains at 0.4.0.

### Added

- Added provisioning API for checking if a phone number is registered on Signal
- Added admin command for linking to an existing account in signald.
- Added Matrix -> Signal bridging for invites, kicks, bans and unbans (thanks to [@maltee1](https://github.com/maltee1) in [#246](https://github.com/mautrix/signal/pull/246) and [#257](https://github.com/mautrix/signal/pull/257)).
- Added command to create Signal group for existing Matrix room (thanks to [@maltee1](https://github.com/maltee1) in [#250](https://github.com/mautrix/signal/pull/250)).
- Added Matrix -> Signal power level change bridging (thanks to [@maltee1](https://github.com/maltee1) in [#260](https://github.com/mautrix/signal/pull/260) and [#263](https://github.com/mautrix/signal/pull/263)).
- Added join rule bridging in both directions (thanks to [@maltee1](https://github.com/maltee1) in [#268](https://github.com/mautrix/signal/pull/268)).
- Added Matrix -> Signal bridging of location messages (thanks to [@maltee1](https://github.com/maltee1) in [#287](https://github.com/mautrix/signal/pull/287)).

  - Since Signal doesn't have actual location messages, they're just bridged as map links. The link template is configurable.
- Added command to link devices when the bridge is the primary device (thanks to [@Craeckie](https://github.com/Craeckie) in [#221](https://github.com/mautrix/signal/pull/221)).
- Added command to bridge existing Matrix rooms to existing Signal groups (thanks to [@MaximilianGaedig](https://github.com/MaximilianGaedig) in [#288](https://github.com/mautrix/signal/pull/288)).
- Added config option to auto-enable relay mode when a specific user is invited (thanks to [@maltee1](https://github.com/maltee1) in [#293](https://github.com/mautrix/signal/pull/293)).
- Added options to make encryption more secure.
  - The `encryption` -\> `verification_levels` config options can be used to make the bridge require encrypted messages to come from cross-signed devices, with trust-on-first-use validation of the cross-signing master key.
  - The `encryption` -\> `require` option can be used to make the bridge ignore any unencrypted messages.
  - Key rotation settings can be configured with the `encryption` -\> `rotation` config.

### Improved

- Improved/fixed handling of disappearing message timer changes.
- Improved handling profile/contact names and prevented them from being downgraded (switching from profile name to contact name or phone number).
- Updated contact list provisioning API to not block if signald needs to update profiles.
- Trying to start a direct chat with a non-existent phone number will now reply with a proper error message instead of throwing an exception (thanks to [@maltee1](https://github.com/maltee1) in [#265](https://github.com/mautrix/signal/pull/265)).
- Syncing chat members will no longer be interrupted if one of the member profiles is unavailable (thanks to \[ [@maltee1](https://github.com/maltee1)\] in \[ [#270](https://github.com/mautrix/signal/pull/270)\]).
- Group metadata changes are now bridged based on the event itself rather than resyncing the whole group, which means changes will use the correct ghost user instead of always using the bridge bot (thanks to [@maltee1](https://github.com/maltee1) in [#283](https://github.com/mautrix/signal/pull/283)).
- Added proper captcha error handling when registering (thanks to [@maltee1](https://github.com/maltee1) in [#280](https://github.com/mautrix/signal/pull/280)).
- Added user's phone number as topic in private chat portals (thanks to [@maltee1](https://github.com/maltee1) in [#282](https://github.com/mautrix/signal/pull/282)).

### Fixed

- Call start notices work again

---

# cinny v2.2.0

> published at 2022-09-17 [source](https://github.com/cinnyapp/cinny/releases/tag/v2.2.0)

## Features

- Open image in lightbox by [@ajbura](https://github.com/ajbura) in [#767](https://github.com/cinnyapp/cinny/pull/767)
- Update sidebar on room/space switch by [@ajbura](https://github.com/ajbura) in [#768](https://github.com/cinnyapp/cinny/pull/768)
- Edit last message with up arrow key by [@ajbura](https://github.com/ajbura) in [#774](https://github.com/cinnyapp/cinny/pull/774)
- Improve commands by [@ajbura](https://github.com/ajbura) in [#791](https://github.com/cinnyapp/cinny/pull/791)
  - Commands now works as they are sent
  - Add /plain text command
  - Add /myroomnick and /myroomavatar
  - Add /converttodm and /converttoroom
  - Add /me and /shrug
  - Add /help command
  - /startdm /ban /unban /kick /invite /disinvite /ingnore /unignore /join now accept multiple targets
- Notification settings by [@ajbura](https://github.com/ajbura) in [#807](https://github.com/cinnyapp/cinny/pull/807)
  - Add notification on favicon in browser tab
  - Add Global notification settings in user settings
  - Can manage notification keywords
- Manage ignored users by [@ajbura](https://github.com/ajbura) in [#814](https://github.com/cinnyapp/cinny/pull/814)
- Rework Markdown parsing by [@ginnyTheCat](https://github.com/ginnyTheCat) in [#719](https://github.com/cinnyapp/cinny/pull/719), [#805](https://github.com/cinnyapp/cinny/pull/805), [#847](https://github.com/cinnyapp/cinny/pull/847)
  - Fix Emojis rendering in code-blocks
  - Strip excessive whitespace in HTML formatted messages
  - Support sending spoilers with reason `||spoiler||(reason)`
  - Fix everything between two $ rendering as TeX math
  - Fix Numbered lists are numbered wrongly
  - Preserve Markdown newlines
  - Send room address as matrix.to links
- Delete notifications after messages have been read or deleted by [@ginnyTheCat](https://github.com/ginnyTheCat) in [#830](https://github.com/cinnyapp/cinny/pull/830)
- Improve MIME type handling on File Upload and in Message Component by [@Thumbscrew](https://github.com/Thumbscrew) in [#688](https://github.com/cinnyapp/cinny/pull/688)
- Cancel edit-message on `Esc` Key press by [@jameskitt616](https://github.com/jameskitt616) in [#765](https://github.com/cinnyapp/cinny/pull/765)
- Cancel replyto on `Esc` key press by [@jameskitt616](https://github.com/jameskitt616) in [#777](https://github.com/cinnyapp/cinny/pull/777)
- Add Clear cache and reload button by [@morguldir](https://github.com/morguldir) in [#793](https://github.com/cinnyapp/cinny/pull/793)

## Bugs

- Fix room not selecting with bad emoji data by [@ajbura](https://github.com/ajbura) ( [#772](https://github.com/cinnyapp/cinny/issues/772))
- Fix emojiboard icon background by [@ajbura](https://github.com/ajbura)
- Fix crash in manage device by [@ajbura](https://github.com/ajbura)
- Fix crash with unknown mime type by [@ajbura](https://github.com/ajbura)
- Fix wrong notification count by [@ajbura](https://github.com/ajbura)
- Copy Olm directly from node modules by [@kfiven](https://github.com/kfiven) in [#817](https://github.com/cinnyapp/cinny/pull/817)
- Handle messages with invalid body by [@ginnyTheCat](https://github.com/ginnyTheCat) in [#833](https://github.com/cinnyapp/cinny/pull/833)
- Ignore mimetype parameters in safety check by [@ginnyTheCat](https://github.com/ginnyTheCat) in [#808](https://github.com/cinnyapp/cinny/pull/808)
- Change "Join public room" label to "Explore public rooms" by [@ginnyTheCat](https://github.com/ginnyTheCat) in [#832](https://github.com/cinnyapp/cinny/pull/832)
- Fix grammar of read receipt text by [@anoadragon453](https://github.com/anoadragon453) in [#744](https://github.com/cinnyapp/cinny/pull/744)

## Docker Image

- Push Docker image to ghcr registry by [@kfiven](https://github.com/kfiven) in [#764](https://github.com/cinnyapp/cinny/pull/764)

**Full Changelog**: [`v2.1.2...v2.2.0`](https://github.com/cinnyapp/cinny/compare/v2.1.2...v2.2.0)

---

# mautrix-whatsapp v0.7.0

> published at 2022-09-16 [source](https://github.com/mautrix/whatsapp/releases/tag/v0.7.0)

- Bumped minimum Go version to 1.18.
- Added hidden option to use appservice login for double puppeting.
  - This can be used by adding everyone to a non-exclusive namespace in the registration, and setting the login shared secret to the string `appservice`.
- Enabled appservice ephemeral events by default for new installations.
  - Existing bridges can turn it on by enabling `ephemeral_events` and disabling `sync_with_custom_puppets` in the config, then regenerating the registration file.
- Updated sticker bridging to send actual sticker messages to WhatsApp rather than sending as image. This includes converting stickers to webp and adding transparent padding to make the aspect ratio 1:1.
- Added automatic webm -> mp4 conversion when sending videos to WhatsApp.
- Started rejecting unsupported mime types when sending media to WhatsApp.
- Added option to use [MSC2409](https://github.com/matrix-org/matrix-spec-proposals/pull/2409) and [MSC3202](https://github.com/matrix-org/matrix-spec-proposals/pull/3202) for end-to-bridge encryption. However, this may not work with the Synapse implementation as it hasn't been tested yet.
- Added error notice if the bridge is started twice.

---

# hookshot 2.2.0 (2022-09-16)

> published at 2022-09-16 [source](https://github.com/matrix-org/matrix-hookshot/releases/tag/2.2.0)

## Features

- Ready/draft state changes for GitLab merge requests are now reported. ( [#480](https://github.com/matrix-org/matrix-hookshot/issues/480))
- Merge GitLab MR approvals and comments into one message. ( [#484](https://github.com/matrix-org/matrix-hookshot/issues/484))

## Bugfixes

- Log noisy "Got GitHub webhook event" log line at debug level. ( [#473](https://github.com/matrix-org/matrix-hookshot/issues/473))
- Fix Figma service not being able to create new webhooks on startup, causing a crash. ( [#481](https://github.com/matrix-org/matrix-hookshot/issues/481))
- Fix a bug where the bridge can crash when JSON logging is enabled. ( [#478](https://github.com/matrix-org/matrix-hookshot/issues/478))

## Internal Changes

- Update codemirror and remove unused font. ( [#489](https://github.com/matrix-org/matrix-hookshot/issues/489))

---

# ansible 6.4.0

> published at 2022-09-15 [source](https://pypi.org/project/ansible/6.4.0/)

> no description provided

---



---

# postmoogle v0.9.2

> published at 2022-09-14 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.2)

- bugfixes
- speed up email delivery
- StartTLS enabled automatically if TLS cert and key are provided
- set `msgtype` by attachment's mime-type (received image -> got rendered image in your matrix client; received audio -> got audio player in your matrix client; etc)

---

# coturn docker/4.6.0-r0

> published at 2022-09-13 [source](https://github.com/coturn/coturn/releases/tag/docker%2F4.6.0-r0)

`4.6.0-r0` Docker image version of 4.6.0 Coturn release.

[Docker Hub](https://hub.docker.com/r/coturn/coturn) \| [GitHub Container Registry](https://github.com/orgs/coturn/packages/container/package/coturn) \| [Quay.io](https://quay.io/repository/coturn/coturn)

[Changelog](https://github.com/coturn/coturn/blob/docker/4.6.0-r0/docker/coturn/CHANGELOG.md#460-r0--2022-08-13)

---

# coturn docker/4.5.2-r14

> published at 2022-09-13 [source](https://github.com/coturn/coturn/releases/tag/docker%2F4.5.2-r14)

[Changelog](https://github.com/coturn/coturn/blob/docker/4.5.2-r14/docker/coturn/CHANGELOG.md#452-r14--2022-08-10)

---

# appservice-irc 0.35.0 (2022-09-13)

> published at 2022-09-13 [source](https://github.com/matrix-org/matrix-appservice-irc/releases/tag/0.35.0)

# 🔒 Security

This release addresses security vulnerabilities in the bridge. **Please update as a matter of urgency.** A matrix.org blog post detailing the specifics of the bugs will be available soon.

## Features

- Add new Debug API `/warnReapUsers` which allows bridges to send a warning to users when they are going to be idle reaped. ( [#1571](https://github.com/matrix-org/matrix-appservice-irc/issues/1571))

## Bugfixes

- Truncated messages now default to wrapping URLs in angle brackets. ( [#1573](https://github.com/matrix-org/matrix-appservice-irc/issues/1573))

## Internal Changes

- Include the bridge version and homeserver in the `CTCP VERSION` response body. ( [#1559](https://github.com/matrix-org/matrix-appservice-irc/issues/1559))
- BREAKING: Remove (IRC) as a default displayName suffix. ( [#1567](https://github.com/matrix-org/matrix-appservice-irc/issues/1567))
- Update CONTRIBUTING.md ( [#1570](https://github.com/matrix-org/matrix-appservice-irc/issues/1570))
- Add new CI workflow to check for signoffs. ( [#1585](https://github.com/matrix-org/matrix-appservice-irc/issues/1585))
- Strongly type emitted events from the IRC client. ( [#1604](https://github.com/matrix-org/matrix-appservice-irc/issues/1604))

---

# coturn Increase version to 4.6.0 (#976)

> published at 2022-09-13 [source](https://github.com/coturn/coturn/releases/tag/4.6.0)

Increase the version number for the 4.6.0 release.

It uses the codename Gorst.

---

# ansible-core 2.12.9

> published at 2022-09-12 [source](https://pypi.org/project/ansible-core/2.12.9/)

> no description provided

---



---

# ansible-core 2.13.4

> published at 2022-09-12 [source](https://pypi.org/project/ansible-core/2.13.4/)

> no description provided

---



---

# postmoogle v0.9.1

> published at 2022-09-09 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.1)

- bugfixes
- removed migrations
- smart auto joins according to ACL
- send emails from matrix
- send emails with proper DKIM signature
- Secure SMTP (with TLS) for incoming emails

---

# ansible-core 2.12.9rc1

> published at 2022-09-06 [source](https://pypi.org/project/ansible-core/2.12.9rc1/)

> no description provided

---



---

# ansible-core 2.13.4rc1

> published at 2022-09-06 [source](https://pypi.org/project/ansible-core/2.13.4rc1/)

> no description provided

---



---

# hookshot 2.1.2 (2022-09-03)

> published at 2022-09-03 [source](https://github.com/matrix-org/matrix-hookshot/releases/tag/2.1.2)

## Bugfixes

- Fix a bug where reading RSS feeds could crash the process. ( [#469](https://github.com/matrix-org/matrix-hookshot/issues/469))

---

# hookshot 2.1.1 (2022-09-02)

> published at 2022-09-02 [source](https://github.com/matrix-org/matrix-hookshot/releases/tag/2.1.1)

## Bugfixes

- Fixed issue where log lines would only be outputted when the `logging.level` is `debug`. ( [#467](https://github.com/matrix-org/matrix-hookshot/issues/467))

---

# hookshot 2.1.0 (2022-09-02)

> published at 2022-09-02 [source](https://github.com/matrix-org/matrix-hookshot/releases/tag/2.1.0)

## Features

- Add support for ARM64 docker images. ( [#458](https://github.com/matrix-org/matrix-hookshot/issues/458))
- Added new config option `feeds.pollTimeoutSeconds` to explictly set how long to wait for a feed response. ( [#459](https://github.com/matrix-org/matrix-hookshot/issues/459))
- JSON logging output now includes new keys such as `error` and `args`. ( [#463](https://github.com/matrix-org/matrix-hookshot/issues/463))

## Bugfixes

- Fix error when responding to a provisioning request for a room that the Hookshot bot isn't yet a member of. ( [#457](https://github.com/matrix-org/matrix-hookshot/issues/457))
- Fix a bug users without "login" permissions could run login commands for GitHub/GitLab/JIRA, but get an error when attempting to store the token. Users now have their permissions checked earlier. ( [#461](https://github.com/matrix-org/matrix-hookshot/issues/461))
- Hookshot now waits for Redis to be ready before handling traffic. ( [#462](https://github.com/matrix-org/matrix-hookshot/issues/462))
- Fix room membership going stale for rooms used in the permissions config. ( [#464](https://github.com/matrix-org/matrix-hookshot/issues/464))

## Improved Documentation

- Be explicit that identifiers in the permissions yaml config need to be wrapped in quotes, because they start with the characters @ and !. ( [#453](https://github.com/matrix-org/matrix-hookshot/issues/453))

## Internal Changes

- Track coverage of tests. ( [#351](https://github.com/matrix-org/matrix-hookshot/issues/351))

---

# cinny v2.1.3

> published at 2022-08-31 [source](https://github.com/cinnyapp/cinny/releases/tag/v2.1.3)

## What's Changed

- **Security release**: Bump matrix-js-sdk from 19.2.0 to 19.4.0 ( [Read more](https://matrix.org/blog/2022/08/31/security-releases-matrix-js-sdk-19-4-0-and-matrix-react-sdk-3-53-0)).

**Full Changelog**: [`v2.1.2...v2.1.3`](https://github.com/cinnyapp/cinny/compare/v2.1.2...v2.1.3)

---

# postmoogle v0.9.0

> published at 2022-08-30 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.0)

this releases contains all migrations

---

# hydrogen v0.3.2

> published at 2022-08-30 [source](https://github.com/vector-im/hydrogen-web/releases/tag/v0.3.2)

- Fixes [#779](https://github.com/vector-im/hydrogen-web/issues/779), [#627](https://github.com/vector-im/hydrogen-web/issues/627), [#798](https://github.com/vector-im/hydrogen-web/issues/798)
- Progress on typescript conversion thanks to [@ibeckermayer](https://github.com/ibeckermayer)

---

# prometheus_postgres_exporter 0.12.0-rc.0 / 2022-08-26

> published at 2022-08-29 [source](https://github.com/prometheus-community/postgres_exporter/releases/tag/v0.12.0-rc.0)

BREAKING CHANGES:

This release changes support for multiple postgres servers to use the

multi-target exporter pattern. This makes it much easier to monitor multiple

PostgreSQL servers from a single exporter by passing the target via URL

params. See the Multi-Target Support section of the README.

- \[CHANGE\] Add multi-target support [#618](https://github.com/prometheus-community/postgres_exporter/pull/618)
- \[BUGFIX\] Add dsn type for handling datasources [#678](https://github.com/prometheus-community/postgres_exporter/pull/678)

---

# mautrix-instagram v0.2.0

> published at 2022-08-26 [source](https://github.com/mautrix/instagram/releases/tag/v0.2.0)

- Added handling for rate limit errors when connecting to Instagram.
- Added option to not bridge `m.notice` messages (thanks to [@bramenn](https://github.com/bramenn) in [#55](https://github.com/mautrix/instagram/pull/55)).
- Fixed bridging voice messages to Instagram (broke due to server-side changes).
- Made Instagram message processing synchronous so messages are bridged in order.
- Updated Docker image to Alpine 3.16.
- Enabled appservice ephemeral events by default for new installations.
  - Existing bridges can turn it on by enabling `ephemeral_events` and disabling `sync_with_custom_puppets` in the config, then regenerating the registration file.
- Added options to make encryption more secure.
  - The `encryption` -\> `verification_levels` config options can be used to make the bridge require encrypted messages to come from cross-signed devices, with trust-on-first-use validation of the cross-signing master key.
  - The `encryption` -\> `require` option can be used to make the bridge ignore any unencrypted messages.
  - Key rotation settings can be configured with the `encryption` -\> `rotation` config.

---

# jitsi stable-7648-4: release

> published at 2022-08-26 [source](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-7648-4)

release

\\* [`6449c60`](https://github.com/jitsi/docker-jitsi-meet/commit/6449c6087548ec1489db48fe51a0ed1b05c33171) prosody: fix installation of lua inspect module

\\* [`6664c89`](https://github.com/jitsi/docker-jitsi-meet/commit/6664c89db68e0d1a1ffdf47f4edd36cea08dd625) prosody: add missing lua-inspect dependency

\\* [`755bd3f`](https://github.com/jitsi/docker-jitsi-meet/commit/755bd3fc89e39dcd7d45064a7eee140757325194) prosody: add jigasi and jibri users as admins

\\* [`8c5fba1`](https://github.com/jitsi/docker-jitsi-meet/commit/8c5fba13df9057b970f90deb4b372d13199b7d36) jigasi: add ability to disable SIP

\\* [`4fa0a2f`](https://github.com/jitsi/docker-jitsi-meet/commit/4fa0a2f7a5636a136be4d7eab97743cfedf28ce7) jvb: allow configuration of videobridge.ice.advertise-private-candidates

\\* [`74e5942`](https://github.com/jitsi/docker-jitsi-meet/commit/74e594242feb1e25085520d360a6b32d34c1d891) misc: working on unstable

---

# mautrix-telegram v0.12.0

> published at 2022-08-26 [source](https://github.com/mautrix/telegram/releases/tag/v0.12.0)

**N.B.** This release requires a homeserver with Matrix v1.1 support, which bumps up the minimum homeserver versions to Synapse 1.54 and Dendrite 0.8.7. Minimum Conduit version remains at 0.4.0.

### Added

- Added provisioning API for resolving Telegram identifiers (like usernames).
- Added support for bridging Telegram custom emojis to Matrix.
- Added option to not bridge chats with lots of members.
- Added option to include captions in the same message as the media to implement [MSC2530](https://github.com/matrix-org/matrix-spec-proposals/pull/2530). Sending captions the same way is also supported and enabled by default.
- Added commands to kick or ban relaybot users from Telegram.
- Added support for Telegram's disappearing messages.
- Added support for bridging forwarded messages as forwards on Telegram.
  - Forwarding is not allowed in relay mode as the bot wouldn't be able to specify who sent the message.
  - Matrix doesn't have real forwarding (there's no forwarding metadata), so only messages bridged from Telegram can be forwarded.
  - Double puppeted messages from Telegram currently can't be forwarded without removing the `fi.mau.double_puppet_source` key from the content.
  - If forwarding fails (e.g. due to it being blocked in the source chat), the bridge will automatically fall back to sending it as a normal new message.
- Added options to make encryption more secure.
  - The `encryption` -\> `verification_levels` config options can be used to make the bridge require encrypted messages to come from cross-signed devices, with trust-on-first-use validation of the cross-signing master key.
  - The `encryption` -\> `require` option can be used to make the bridge ignore any unencrypted messages.
  - Key rotation settings can be configured with the `encryption` -\> `rotation` config.

### Improved

- Improved handling the bridge user leaving chats on Telegram, and new users being added on Telegram.
- Improved animated sticker conversion options: added support for animated webp and added option to convert video stickers (webm) to the specified image format.
- Audio and video metadata is now bridged properly to Telegram.
- Added database index on Telegram usernames (used when bridging username @-mentions in messages).
- Changed `/login/send_code` provisioning API to return a proper error when the phone number is not registered on Telegram.

  - The same login code can be used for registering an account, but registering is not currently supported in the provisioning API.
- Removed `plaintext_highlights` config option (the code using it was already removed in v0.11.0).
- Enabled appservice ephemeral events by default for new installations.
  - Existing bridges can turn it on by enabling `ephemeral_events` and disabling `sync_with_custom_puppets` in the config, then regenerating the registration file.
- Updated to API layer 144 so that Telegram would send new message types like premium stickers to the bridge.
- Updated Docker image to Alpine 3.16 and made it smaller.

### Fixed

- Fixed command prefix in game and poll messages (thanks to [@cynhr](https://github.com/cynhr) in [#804](https://github.com/mautrix/telegram/pull/804)).

---

# mautrix-twitter v0.1.5

> published at 2022-08-23 [source](https://github.com/mautrix/twitter/releases/tag/v0.1.5)

Bump version to 0.1.5

---

# ansible 6.3.0

> published at 2022-08-23 [source](https://pypi.org/project/ansible/6.3.0/)

> no description provided

---



---

# honoroit v0.9.13

> published at 2022-08-23 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.13)

- update deps

---

# buscarron v1.2.1

> published at 2022-08-23 [source](https://gitlab.com/etke.cc/buscarron/-/tags/v1.2.1)

- update deps

---

# jitsi stable-7648-3: release

> published at 2022-08-22 [source](https://github.com/jitsi/docker-jitsi-meet/releases/tag/stable-7648-3)

release

\\* [`7890183`](https://github.com/jitsi/docker-jitsi-meet/commit/7890183b9fc9dc7819fbdb659e58fdd36a754a4a) jibri: fix ENABLE\_RECORDING issue

\\* [`a2b86a0`](https://github.com/jitsi/docker-jitsi-meet/commit/a2b86a0f9a7f9508c60ec5127e39c50943ea12ef) fix: Fixes undefined variable $ENABLE\_JAAS\_COMPONENTS. Fixes [#1377](https://github.com/jitsi/docker-jitsi-meet/issues/1377).

\\* [`9f3c81f`](https://github.com/jitsi/docker-jitsi-meet/commit/9f3c81fab91169c528fc8085bddc524519f8ed70) misc: working on unstable

---

# beeper-linkedin v0.5.3

> published at 2022-08-19 [source](https://github.com/beeper/linkedin/releases/tag/v0.5.3)

**Migrated to GitHub**. You should change all of your Docker images to point to ghcr rather than registry.gitlab.com. For example:

```
registry.gitlab.com/beeper/linkedin:latest

```

should become

```
ghcr.io/beeper/linkedin:latest

```

You'll also need to change your git commit URLs.

**Features**

- Implemented typing notifications
- Implemented read receipts

**Other changes:**

- Switched to GitHub Actions for CI/CD
- Added pre-commit config to help prevent bad pushes
- Upgraded `mautrix` to `^0.17.6`
- Upgraded `linkedin-messaging` to `^0.5.2`
- Converted to use `isort` instead of `flake8-import-order`

---

# beeper-linkedin v0.5.2

> published at 2022-08-19 [source](https://github.com/beeper/linkedin/releases/tag/v0.5.2)

- Upgraded mautrix to ^0.14.0
- Major improvements across the board for message send status reporting via native Matrix notices and via message send checkpoints.

---

# heisenbridge v1.14.0

> published at 2022-08-19 [source](https://github.com/hifi/heisenbridge/releases/tag/v1.14.0)

No content.

---

# prometheus_postgres_exporter 0.11.1 / 2022-08-01

> published at 2022-08-18 [source](https://github.com/prometheus-community/postgres_exporter/releases/tag/v0.11.1)

- \[BUGFIX\] Fix checkpoint\_write\_time value type [#666](https://github.com/prometheus-community/postgres_exporter/pull/666)
- \[BUGFIX\] Fix checkpoint\_sync\_time value type [#667](https://github.com/prometheus-community/postgres_exporter/pull/667)

---

# mautrix-whatsapp v0.6.1

> published at 2022-08-16 [source](https://github.com/mautrix/whatsapp/releases/tag/v0.6.1)

- Added support for "Delete for me" and deleting private chats from WhatsApp.
- Added support for admin deletions in groups.
- Document with caption messages should work with the bridge as soon as WhatsApp enables them in their apps.

---

# ansible-core 2.13.3

> published at 2022-08-15 [source](https://pypi.org/project/ansible-core/2.13.3/)

> no description provided

---



---

# ansible-core 2.12.8

> published at 2022-08-15 [source](https://pypi.org/project/ansible-core/2.12.8/)

> no description provided

---



---

# miniflux Miniflux 2.0.38

> published at 2022-08-14 [source](https://github.com/miniflux/v2/releases/tag/2.0.38)

Make sure to use the new Debian and RPM repositories instead of the old ones:

- [https://miniflux.app/docs/howto.html#apt-repo](https://miniflux.app/docs/howto.html#apt-repo)
- [https://miniflux.app/docs/howto.html#rpm-repo](https://miniflux.app/docs/howto.html#rpm-repo)

List of changes:

- Rename default branch from master to main
- Update GitHub Actions
- Bump `github.com/prometheus/client_golang` from `1.12.2` to `1.13.0`
- Fix some linter issues
- Handle Atom links with a text/html type defined
- Add `parse_markdown` rewrite function
- Build RPM and Debian packages automatically using GitHub Actions
- Add `explosm.net` scraper rule
- Make default home page configurable
- Add title attribute to entry links because text could be truncated
- Highlight categories with unread entries
- Allow option to order by title and author in API entry endpoint
- Update Russian translation
- Make reading speed user-configurable
- Added translation for Hindi language used in India
- Add rewrite rules for article URL before fetching content
- Bump `github.com/tdewolff/minify/v2` from `2.11.7` to `2.12.0`
- Support other repo owners in GitHub Docker Action
- Proxify empty URL should not crash
- Avoid stretched image if specified width is larger than Miniflux's layout
- Add support for OPML files with several nested outlines
- sanitizer: handle image URLs in `srcset` attribute with comma
- Allow `width` and `height` attributes for `img` tags
- Document that `-config-dump` command line argument shows sensitive info
- Add System-V init service in contrib folder
- Fix syntax error in `RequestBuilder.getCsrfToken()` method

---

# appservice-discord 3.0.0 (2022-08-12)

> published at 2022-08-11 [source](https://github.com/matrix-org/matrix-appservice-discord/releases/tag/v3.0.0)

## Bugfixes

- Make sure we don't lose errors thrown when checking usage limits. ( [#823](https://github.com/matrix-org/matrix-appservice-discord/issues/823))
- Fix Docker instances not starting due to being unable to load a dynamic library in the latest unstable image. ( [#828](https://github.com/matrix-org/matrix-appservice-discord/issues/828))
- Remove matrix.to hyperlinks when relaying non-Discord user mentions to Discord.


  Fix mentioning Matrix users in Discord. ( [#829](https://github.com/matrix-org/matrix-appservice-discord/issues/829))

## Deprecations and Removals

- Minimum required Node.js version is now 16. ( [#825](https://github.com/matrix-org/matrix-appservice-discord/issues/825))

## Internal Changes

- Remove unused variables. ( [#657](https://github.com/matrix-org/matrix-appservice-discord/issues/657))
- Add workflow for building docker images, and push new docker images to ghcr.io. ( [#826](https://github.com/matrix-org/matrix-appservice-discord/issues/826))
- Remove `git config` workaround to pull a dependency from github.com. ( [#830](https://github.com/matrix-org/matrix-appservice-discord/issues/830))

---

# cinny v2.1.2

> published at 2022-08-11 [source](https://github.com/cinnyapp/cinny/releases/tag/v2.1.2)

## What's Changed

- Escape html with markdown off ( [#732](https://github.com/cinnyapp/cinny/issues/732))
- Fix image not loading without h/w data ( [#738](https://github.com/cinnyapp/cinny/issues/738))
- Don't drop animation of GIF emojis/stickers ( [#731](https://github.com/cinnyapp/cinny/issues/731))
- Pre-fill file name while uploading emojis/stickers
- Add navigation bar to sticker board
- Fix logout not working when server offline
- Only render mxc images in markdown

**Full Changelog**: [`v2.1.1...v2.1.2`](https://github.com/cinnyapp/cinny/compare/v2.1.1...v2.1.2)

---

# soft-serve v0.4.0

> published at 2022-08-10 [source](https://github.com/charmbracelet/soft-serve/releases/tag/v0.4.0)

## New Release, New Look

🍦 This fresh release of Soft Serve contains:

- A completely new look and UI overhaul
- Mouse support
- Copying text over SSH

Give it a go with:

```
ssh git.charm.sh
```

[![landing](https://user-images.githubusercontent.com/3187948/183486654-eab9c4e6-b58a-4073-99e8-63d8f0d58ed0.gif)](https://user-images.githubusercontent.com/3187948/183486654-eab9c4e6-b58a-4073-99e8-63d8f0d58ed0.gif)

**Full Changelog**: [`v0.3.0...v0.4.0`](https://github.com/charmbracelet/soft-serve/compare/v0.3.0...v0.4.0)

* * *

[![The Charm logo](https://camo.githubusercontent.com/65459c24b86d0476085210fd0387503a161a1359b3cf5034324346f55907cbb8/68747470733a2f2f73747566662e636861726d2e73682f636861726d2d62616467652e6a7067)](https://charm.sh/)

Thoughts? Questions? We love hearing from you. Feel free to reach out on [Twitter](https://twitter.com/charmcli), [The Fediverse](https://mastodon.technology/@charm), or on [Slack](https://charm.sh/slack).

---

# languagetool v5.8-dockerupdate-2: Upgrade CodeQL Action v1 to v2

> published at 2022-08-10 [source](https://github.com/Erikvl87/docker-languagetool/releases/tag/v5.8-dockerupdate-2)

CodeQL Action v1 will be deprecated on December 7th, 2022.

[https://github.blog/changelog/2022-04-27-code-scanning-deprecation-of-codeql-action-v1/](https://github.blog/changelog/2022-04-27-code-scanning-deprecation-of-codeql-action-v1/)

---

# ansible-core 2.12.8rc1

> published at 2022-08-08 [source](https://pypi.org/project/ansible-core/2.12.8rc1/)

> no description provided

---



---

# ansible-core 2.13.3rc1

> published at 2022-08-08 [source](https://pypi.org/project/ansible-core/2.13.3rc1/)

> no description provided

---



---

# email2matrix 1.1.0

> published at 2022-08-08 [source](https://github.com/devture/email2matrix/releases/tag/1.1.0)

Bump version (1.0.3 -> 1.1.0)

---

# cinny v2.1.1

> published at 2022-08-07 [source](https://github.com/cinnyapp/cinny/releases/tag/v2.1.1)

## What's Changed

- Fix blurhash visible under transparent img by [@ajbura](https://github.com/ajbura) in [#721](https://github.com/cinnyapp/cinny/issues/721)
- Update Olm from v3.2.8 to v3.2.12 by [@ajbura](https://github.com/ajbura)

**Full Changelog**: [`v2.1.0...v2.1.1`](https://github.com/cinnyapp/cinny/compare/v2.1.0...v2.1.1)

---

# cinny v2.1.0: Custom emojis and stickers (Birthday edition)

> published at 2022-08-07 [source](https://github.com/cinnyapp/cinny/releases/tag/v2.1.0)

Last week on July 28 Cinny marked its first birthday. 🍰🎉

Over the year we have gained ~800 Github stars, ~44k DockerHub pulls, and ~14k downloads from Github releases. Thank you everyone for these milestones!

## Features

- Custom emoji and sticker support using [MSC2545](https://github.com/matrix-org/matrix-spec-proposals/pull/2545) by [@ajbura](https://github.com/ajbura) in [#686](https://github.com/cinnyapp/cinny/pull/686)
  - Option to add packs to room, space, and user account
  - Option to use any pack globally and one place to remove them
  - Option to delete existing packs
  - Option to rename, delete entire pack or individual image
  - Option to change pack usages
  - Space packs works in all it's children including subspaces
  - Render stickers nicely
  - Animated emojis and stickers work
  - New sticker board
  - Reaction with custom emojis using [MSC3746](https://github.com/matrix-org/matrix-spec-proposals/pull/3746)
- Blurhash support for Images and Videos by [@ginnyTheCat](https://github.com/ginnyTheCat) in [#701](https://github.com/cinnyapp/cinny/pull/701)
- Add "Mark as read" button to space options by [@exodrifter](https://github.com/exodrifter) [@Mapledv](https://github.com/Mapledv) in [#667](https://github.com/cinnyapp/cinny/pull/667)
- Send user pills when mentioning users by [@ajbura](https://github.com/ajbura) in [#686](https://github.com/cinnyapp/cinny/pull/686)
- Add toggle to show password on login/register pages by [@ajbura](https://github.com/ajbura) in [#73](https://github.com/cinnyapp/cinny/issues/73)
- Accept MXID on login by [@ajbura](https://github.com/ajbura) in [#187](https://github.com/cinnyapp/cinny/issues/187)
- Follow system theme by default by [@ajbura](https://github.com/ajbura) in [`5c0eb20`](https://github.com/cinnyapp/cinny/commit/5c0eb20cb446f5027d91f334d0cf6a669011e41c)
- Support RTL text in messages by [@ajbura](https://github.com/ajbura) in [#717](https://github.com/cinnyapp/cinny/issues/717)
- Add emoji name fallback; means Unicode 14 emojis are now in Cinny by [@ginnyTheCat](https://github.com/ginnyTheCat) in [#658](https://github.com/cinnyapp/cinny/pull/658)
- Show full timestamp on hovering over message date/time by [@ginnyTheCat](https://github.com/ginnyTheCat) in [#714](https://github.com/cinnyapp/cinny/pull/714)
- Add support to play .mov files by [@Thumbscrew](https://github.com/Thumbscrew) in [#672](https://github.com/cinnyapp/cinny/pull/672)
- Support mark as read by ESC while in room input by [@deanveloper](https://github.com/deanveloper) in [#669](https://github.com/cinnyapp/cinny/pull/669)

## Bugs

- Fix parsing encoded matrix.to URL by [@chuangzhu](https://github.com/chuangzhu) in [#660](https://github.com/cinnyapp/cinny/pull/660)
- Fix captcha loop issue in registration form by [@ajbura](https://github.com/ajbura) in [#664](https://github.com/cinnyapp/cinny/issues/664)
- Fix wrong power level in room permission by [@ajbura](https://github.com/ajbura) in [`009966a`](https://github.com/cinnyapp/cinny/commit/009966a5c7151a7e1941fee76341330e40afd91f)
- Allow removing the room name by [@ginnyTheCat](https://github.com/ginnyTheCat) in [#702](https://github.com/cinnyapp/cinny/pull/702)
- Fix keyboard shortcuts on non-QWERTY keyboards by [@ginnyTheCat](https://github.com/ginnyTheCat) in [#715](https://github.com/cinnyapp/cinny/pull/715)
- Don't enable e2ee for bridge users from user profile by [@kfiven](https://github.com/kfiven) in [#666](https://github.com/cinnyapp/cinny/pull/666)

**Full Changelog**: [`v2.0.4...v2.1.0`](https://github.com/cinnyapp/cinny/compare/v2.0.4...v2.1.0)

---

# appservice-discord v2.0.0 (2022-08-05)

> published at 2022-08-04 [source](https://github.com/matrix-org/matrix-appservice-discord/releases/tag/v2.0.0)

## Improved Documentation

- Update `CONTRIBUTING.md` guide to reference the newly-updated guide for all of the matrix.org bridge repos. ( [#794](https://github.com/matrix-org/matrix-appservice-discord/issues/794))

## Deprecations and Removals

- Node.JS 12 is now unsupported, please upgrade to Node.JS 14 or later. Node.JS 16 becomes the new default version. ( [#811](https://github.com/matrix-org/matrix-appservice-discord/issues/811))

## Internal Changes

- Add automatic changelog generation via [Towncrier](https://github.com/twisted/towncrier). ( [#787](https://github.com/matrix-org/matrix-appservice-discord/issues/787))
- Use `yarn` instead of `npm` for package management and scripts. ( [#796](https://github.com/matrix-org/matrix-appservice-discord/issues/796))
- Add new CI workflow to check for signoffs. ( [#818](https://github.com/matrix-org/matrix-appservice-discord/issues/818))

---

# honoroit v0.9.12

> published at 2022-08-04 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.12)

- add `!ho note` command
- add `HONOROIT_IGNORENOTHREAD` config var

---

# soft-serve v0.3.3

> published at 2022-08-03 [source](https://github.com/charmbracelet/soft-serve/releases/tag/v0.3.3)

- Use spf13/cobra
- Fix showing Home readme for anon users

## Changelog

### New Features

- [`2ac11c0`](https://github.com/charmbracelet/soft-serve/commit/2ac11c0ad46d13f761e65639ca90964e45f4c58b): feat(cd): sign nfpm packages ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`748f21e`](https://github.com/charmbracelet/soft-serve/commit/748f21e49181981222f0e336fe2eac47e4de4266): feat(ci): publish package to aur ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`d128631`](https://github.com/charmbracelet/soft-serve/commit/d1286319653832d82931f16b1e071dc89b1e8194): feat: generate man pages and completions ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`3165828`](https://github.com/charmbracelet/soft-serve/commit/3165828420ab94f7bdcd64822f52905d82c2ea46): feat: update git-module ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`5ae15b5`](https://github.com/charmbracelet/soft-serve/commit/5ae15b5114bf7ac2151a32f187e2f6609a13b218): feat: use spf13/cobra ( [@aymanbagabas](https://github.com/aymanbagabas))

### Bug fixes

- [`dc138cd`](https://github.com/charmbracelet/soft-serve/commit/dc138cd8a8bf3e30c41c63aa6eae1f23466049c2): fix(cd): don't publish aur package ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`ecfaee9`](https://github.com/charmbracelet/soft-serve/commit/ecfaee9ade66d3cd9a5ec419eb1da1ebe99361df): fix(ci): add fury token ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`2cabbbc`](https://github.com/charmbracelet/soft-serve/commit/2cabbbc222578a534f884763d2fceaa436566d28): fix(ci): don't publish nightly builds to fury.io ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`20c33e0`](https://github.com/charmbracelet/soft-serve/commit/20c33e0d54be1d13f09c638ddd1371f7f5a52c40): fix(server): disable cobra completions over ssh ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`8316558`](https://github.com/charmbracelet/soft-serve/commit/83165588aa36d68a58c4fe74ff6e7440b3b4ad34): fix: show config repo for anon users ( [@aymanbagabas](https://github.com/aymanbagabas))

### Others

- [`b6f1053`](https://github.com/charmbracelet/soft-serve/commit/b6f1053e508172d8f4741b2de648ea6eccfb4e65): Fix example commands ( [@levidurfee](https://github.com/levidurfee))
- [`2ca6979`](https://github.com/charmbracelet/soft-serve/commit/2ca6979112161773b07b14a63e405ea09efad382): Fixes [#133](https://github.com/charmbracelet/soft-serve/issues/133) ( [@wissam](https://github.com/wissam))

* * *

[![The Charm logo](https://camo.githubusercontent.com/65459c24b86d0476085210fd0387503a161a1359b3cf5034324346f55907cbb8/68747470733a2f2f73747566662e636861726d2e73682f636861726d2d62616467652e6a7067)](https://charm.sh/)

Thoughts? Questions? We love hearing from you. Feel free to reach out on [Twitter](https://twitter.com/charmcli), [The Fediverse](https://mastodon.technology/@charm), or on [Slack](https://charm.sh/slack).

---

# ansible 6.2.0

> published at 2022-08-02 [source](https://pypi.org/project/ansible/6.2.0/)

> no description provided

---



---

# prometheus-blackbox-exporter 0.22.0 / 2022-08-02

> published at 2022-08-02 [source](https://github.com/prometheus/blackbox_exporter/releases/tag/v0.22.0)

- \[FEATURE\] HTTP: Add `skip_resolve_phase_with_proxy` option. [#944](https://github.com/prometheus/blackbox_exporter/pull/944)
- \[ENHANCEMENT\] OAuth: Use Blackbox Exporter user agent when doing OAuth2


  authenticated requests. [#948](https://github.com/prometheus/blackbox_exporter/pull/948)
- \[ENHANCEMENT\] Print usage and help to stdout instead of stderr. [#928](https://github.com/prometheus/blackbox_exporter/pull/928)

---

# hydrogen v0.3.1

> published at 2022-08-02 [source](https://github.com/vector-im/hydrogen-web/releases/tag/v0.3.1)

No functional changes in the app, but fixes several issues with the build system:

- [#823](https://github.com/vector-im/hydrogen-web/pull/823)
- [#824](https://github.com/vector-im/hydrogen-web/pull/824)
- [#825](https://github.com/vector-im/hydrogen-web/pull/825)

---

# honoroit v0.9.11

> published at 2022-07-30 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.11)

- add `HONOROIT_IGNOREDROOMS` config var
- add customers' display names on membership and request events

---

# hydrogen v0.3.0

> published at 2022-07-29 [source](https://github.com/vector-im/hydrogen-web/releases/tag/v0.3.0)

This release includes the following changes:

- the composer is now hidden in rooms where you don't have permission to write ( [#788](https://github.com/vector-im/hydrogen-web/pull/788))
- the /join command was added, thanks to [@Kaki-In](https://github.com/Kaki-In)! ( [#809](https://github.com/vector-im/hydrogen-web/pull/809))
- the last location is now restored again when opening the app, thanks to [@Kaki-In](https://github.com/Kaki-In). ( [#816](https://github.com/vector-im/hydrogen-web/pull/816))
- room keys are now shared according to the room history visibility, for example with invited members where appropriate ( [#811](https://github.com/vector-im/hydrogen-web/pull/811))
- runtime themes are now supported, you can now change the colors (including the icons!) of one of the built-in themes with just a json file, no rebuild needed! See [the docs](https://github.com/vector-im/hydrogen-web/blob/master/doc/THEMING.md#derived-themecollection) for more info ( [#769](https://github.com/vector-im/hydrogen-web/pull/769))

In addition to this, we've converted some parts of the code to typescript ( [#676](https://github.com/vector-im/hydrogen-web/pull/676), [#777](https://github.com/vector-im/hydrogen-web/pull/777) thanks to [@ibeckermayer](https://github.com/ibeckermayer)!) and we've improved the documentation ( [#813](https://github.com/vector-im/hydrogen-web/pull/813), [#812](https://github.com/vector-im/hydrogen-web/pull/812), [#758](https://github.com/vector-im/hydrogen-web/pull/758), [#752](https://github.com/vector-im/hydrogen-web/pull/752) and [#778](https://github.com/vector-im/hydrogen-web/pull/778)).

---

# appservice-slack 2.0.0-rc1 (2022-07-29)

> published at 2022-07-29 [source](https://github.com/matrix-org/matrix-appservice-slack/releases/tag/2.0.0-rc1)

This release requires **Node.16** or greater.

## Features

- New releases will now include Docker images built on the `linux/amd64` platform in addition to x86\_64. ( [#656](https://github.com/matrix-org/matrix-appservice-slack/issues/656))

## Bugfixes

- Improve performance of removing deleted Slack users from rooms. ( [#649](https://github.com/matrix-org/matrix-appservice-slack/issues/649))

## Improved Documentation

- Use the matrix-appservice-bridge contributing guide. ( [#686](https://github.com/matrix-org/matrix-appservice-slack/issues/686))

## Deprecations and Removals

- The bridge now requires Node.JS 16 or greater. ( [#690](https://github.com/matrix-org/matrix-appservice-slack/issues/690))

## Internal Changes

- Don't allow NULL in SQL columns of events, matching the expectation of the models. ( [#670](https://github.com/matrix-org/matrix-appservice-slack/issues/670))
- Fix towncrier script for summarising the newsfiles ( [#677](https://github.com/matrix-org/matrix-appservice-slack/issues/677))
- Switch from `npm` to `yarn` for dependency management. ( [#685](https://github.com/matrix-org/matrix-appservice-slack/issues/685))
- Move CI to GitHub Actions. ( [#688](https://github.com/matrix-org/matrix-appservice-slack/issues/688))
- Add new CI workflow to check for signoffs. ( [#693](https://github.com/matrix-org/matrix-appservice-slack/issues/693))

---

# prometheus_postgres_exporter 0.11.0 / 2022-07-28

> published at 2022-07-28 [source](https://github.com/prometheus-community/postgres_exporter/releases/tag/v0.11.0)

NOTE: pg\_stat\_bgwriter counter metrics had the `_total` suffix added [#556](https://github.com/prometheus-community/postgres_exporter/pull/556)

- \[CHANGE\] refactor pg\_stat\_bgwriter metrics into standalone collector [#556](https://github.com/prometheus-community/postgres_exporter/pull/556)
- \[FEATURE\] Add pg\_database collector [#613](https://github.com/prometheus-community/postgres_exporter/pull/613)
- \[ENHANCEMENT\] Add pg\_database\_size\_bytes metric [#613](https://github.com/prometheus-community/postgres_exporter/pull/613)
- \[BUGFIX\] Avoid parsing error from bogus Azure Flexible Server custom GUC [#587](https://github.com/prometheus-community/postgres_exporter/pull/587)
- \[BUGFIX\] Fix pg\_stat\_archiver error in 9.4 and earlier. [#599](https://github.com/prometheus-community/postgres_exporter/pull/599)
- \[BUGFIX\] Sanitize setting values because of Aurora irregularity [#620](https://github.com/prometheus-community/postgres_exporter/pull/620)

---

# prometheus_node_exporter 1.4.0-rc.0 / 2022-07-27

> published at 2022-07-27 [source](https://github.com/prometheus/node_exporter/releases/tag/v1.4.0-rc.0)

- \[BUGFIX\] \[node-mixin\] Fix fsSpaceAvailableCriticalThreshold and fsSpaceAvailableWarning ( [#2352](https://github.com/prometheus/node_exporter/pull/2352))
- \[BUGFIX\] Fix concurrency issue in ethtool collector ( [#2289](https://github.com/prometheus/node_exporter/pull/2289))
- \[BUGFIX\] Fix concurrency issue in netdev collector ( [#2267](https://github.com/prometheus/node_exporter/pull/2267))
- \[BUGFIX\] Fix diskstat reads and write metrics for disks with different sector sizes ( [#2311](https://github.com/prometheus/node_exporter/pull/2311))
- \[BUGFIX\] Fix iostat on macos broken by deprecation warning ( [#2292](https://github.com/prometheus/node_exporter/pull/2292))
- \[BUGFIX\] Fix NodeFileDescriptorLimit alerts ( [#2340](https://github.com/prometheus/node_exporter/pull/2340))
- \[BUGFIX\] Sanitize rapl zone names ( [#2299](https://github.com/prometheus/node_exporter/issues/2299))
- \[ENHANCEMENT\] \[node-mixin\] Add missing selectors ( [#2426](https://github.com/prometheus/node_exporter/pull/2426))
- \[ENHANCEMENT\] \[node-mixin\] Change current datasource to grafana's default ( [#2281](https://github.com/prometheus/node_exporter/pull/2281))
- \[ENHANCEMENT\] \[node-mixin\] Change disk graph to disk table [#2364](https://github.com/prometheus/node_exporter/pull/2364)
- \[ENHANCEMENT\] \[node-mixin\] Change io time units to %util ( [#2375](https://github.com/prometheus/node_exporter/pull/2375)))
- \[ENHANCEMENT\] Ad user\_wired\_bytes and laundry\_bytes on \*bsd ( [#2266](https://github.com/prometheus/node_exporter/pull/2266))
- \[ENHANCEMENT\] Add additional vm\_stat memory metrics for darwin ( [#2240](https://github.com/prometheus/node_exporter/pull/2240))
- \[ENHANCEMENT\] Add device filter flags to arp collector ( [#2254](https://github.com/prometheus/node_exporter/pull/2254))
- \[ENHANCEMENT\] Add diskstats include and exclude device flags ( [#2417](https://github.com/prometheus/node_exporter/pull/2417))
- \[ENHANCEMENT\] Add node\_softirqs\_total metric ( [#2221](https://github.com/prometheus/node_exporter/pull/2221))
- \[ENHANCEMENT\] Add rapl zone name label option ( [#2401](https://github.com/prometheus/node_exporter/pull/2401))
- \[ENHANCEMENT\] Add slabinfo collector ( [#1799](https://github.com/prometheus/node_exporter/issues/1799))
- \[ENHANCEMENT\] Allow user to select port on NTP server to query ( [#2270](https://github.com/prometheus/node_exporter/pull/2270))
- \[ENHANCEMENT\] collector/diskstats: Add labels and metrics from udev ( [#2404](https://github.com/prometheus/node_exporter/pull/2404))
- \[ENHANCEMENT\] Enable builds against older macOS SDK ( [#2327](https://github.com/prometheus/node_exporter/pull/2327))
- \[ENHANCEMENT\] qdisk-linux: Add exclude and include flags for interface name [#2432](https://github.com/prometheus/node_exporter/pull/2432)
- \[ENHANCEMENT\] systemd: Expose systemd minor version ( [#2282](https://github.com/prometheus/node_exporter/pull/2282))
- \[ENHANCEMENT\] Use netlink for tcpstat collector ( [#2322](https://github.com/prometheus/node_exporter/pull/2322))
- \[ENHANCEMENT\] Use netlink to get netdev stats ( [#2074](https://github.com/prometheus/node_exporter/pull/2074))
- \[FEATURE\] \[node-mixin\] Add darwin dashboard to mixin ( [#2351](https://github.com/prometheus/node_exporter/pull/2351))
- \[FEATURE\] Add "isolated" metric on cpu collector on linux ( [#2251](https://github.com/prometheus/node_exporter/pull/2251))
- \[FEATURE\] Add cgroup summary collector ( [#2408](https://github.com/prometheus/node_exporter/pull/2408))
- \[FEATURE\] Add selinux collector ( [#2205](https://github.com/prometheus/node_exporter/pull/2205))
- \[FEATURE\] Add slab info collector ( [#2376](https://github.com/prometheus/node_exporter/pull/2376))
- \[FEATURE\] Add sysctl collector ( [#2425](https://github.com/prometheus/node_exporter/pull/2425))
- \[FEATURE\] Also track the CPU Spin time for OpenBSD systems ( [#1971](https://github.com/prometheus/node_exporter/pull/1971))

---

# coturn docker/4.5.2-r13

> published at 2022-07-19 [source](https://github.com/coturn/coturn/releases/tag/docker%2F4.5.2-r13)

[Changelog](https://github.com/coturn/coturn/blob/docker/4.5.2-r13/docker/coturn/CHANGELOG.md#452-r13--2022-07-19)

---

# nginx-proxy release-1.23.1

> published at 2022-07-19 [source](https://github.com/nginx/nginx/releases/tag/release-1.23.1)

nginx-1.23.1-RELEASE

---

# languagetool v5.8-dockerupdate-1: Add a workflow_dispatch to the release action with the option to skip…

> published at 2022-07-19 [source](https://github.com/Erikvl87/docker-languagetool/releases/tag/v5.8-dockerupdate-1)

… the tests

---

# honoroit v0.9.10

> published at 2022-07-18 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.10)

- add a special message to encrypted chats when encryption is disabled

---

# ansible-core 2.13.2

> published at 2022-07-18 [source](https://pypi.org/project/ansible-core/2.13.2/)

> no description provided

---



---

# redis 7.0.4

> published at 2022-07-18 [source](https://github.com/redis/redis/releases/tag/7.0.4)

Upgrade urgency: SECURITY, contains fixes to security issues.

Security Fixes:

- (CVE-2022-31144) A specially crafted XAUTOCLAIM command on a stream


  key in a specific state may result with heap overflow, and potentially


  remote code execution. The problem affects Redis versions 7.0.0 or newer.

---

# mautrix-whatsapp v0.6.0

> published at 2022-07-16 [source](https://github.com/mautrix/whatsapp/releases/tag/v0.6.0)

- Started requiring homeservers to advertise Matrix v1.1 support.
  - This bumps up the minimum homeserver versions to Synapse 1.54 and Dendrite 0.8.7. Minimum Conduit version remains at 0.4.0.
  - The bridge will also refuse to start if backfilling is enabled in the config, but the homeserver isn't advertising support for MSC2716. Only Synapse supports backfilling at the moment.
- Added options to make encryption more secure.
  - The `encryption` -\> `verification_levels` config options can be used to make the bridge require encrypted messages to come from cross-signed devices, with trust-on-first-use validation of the cross-signing master key.
  - The `encryption` -\> `require` option can be used to make the bridge ignore any unencrypted messages.
  - Key rotation settings can be configured with the `encryption` -\> `rotation` config.
- Added config validation to make the bridge refuse to start if critical fields like homeserver or database address haven't been changed from the defaults.
- Added option to include captions in the same message as the media to implement [MSC2530](https://github.com/matrix-org/matrix-spec-proposals/pull/2530). Sending captions the same way is also supported and enabled by default.
- Added basic support for fancy business messages (template and list messages).
- Added periodic background sync of user and group avatars.
- Added maximum message handling duration config options to prevent messages getting stuck and blocking everything.
- Changed message send error notices to be replies to the errored message.
- Changed dimensions of stickers bridged from WhatsApp to match WhatsApp web.
- Changed attachment bridging to find the Matrix `msgtype` based on the WhatsApp message type instead of the file mimetype.
- Updated Docker image to Alpine 3.16.
- Fixed backfill queue on SQLite.

---

# ansible 6.1.0

> published at 2022-07-12 [source](https://pypi.org/project/ansible/6.1.0/)

> no description provided

---



---

# ansible-core 2.13.2rc1

> published at 2022-07-11 [source](https://pypi.org/project/ansible-core/2.13.2rc1/)

> no description provided

---



---

# redis 7.0.3

> published at 2022-07-11 [source](https://github.com/redis/redis/releases/tag/7.0.3)

Upgrade urgency: MODERATE, specifically if you're using a previous release of

Redis 7.0, contains fixes for bugs in previous 7.0 releases.

# Performance and resource utilization improvements

- Optimize zset conversion on large ZRANGESTORE ( [#10789](https://github.com/redis/redis/pull/10789))
- Optimize the performance of sending PING on large clusters ( [#10624](https://github.com/redis/redis/pull/10624))
- Allow for faster restart of Redis in cluster mode ( [#10912](https://github.com/redis/redis/pull/10912))

# INFO fields and introspection changes

- Add missing sharded pubsub keychannel count to CLIENT LIST ( [#10895](https://github.com/redis/redis/pull/10895))
- Add missing pubsubshard\_channels field in INFO STATS ( [#10929](https://github.com/redis/redis/pull/10929))

# Module API changes

- Add RM\_StringToULongLong and RM\_CreateStringFromULongLong ( [#10889](https://github.com/redis/redis/pull/10889))
- Add RM\_SetClientNameById and RM\_GetClientNameById ( [#10839](https://github.com/redis/redis/pull/10839))

# Changes in CLI tools

- Add missing cluster-port support to redis-cli --cluster ( [#10344](https://github.com/redis/redis/pull/10344))

# Other General Improvements

- Account sharded pubsub channels memory consumption ( [#10925](https://github.com/redis/redis/pull/10925))
- Allow ECHO in loading and stale modes ( [#10853](https://github.com/redis/redis/pull/10853))
- Cluster: Throw -TRYAGAIN instead of -ASK on migrating nodes for multi-key


  commands when the node only has some of the keys ( [#9526](https://github.com/redis/redis/pull/9526))

# Bug Fixes

- TLS: Notify clients on connection shutdown ( [#10931](https://github.com/redis/redis/pull/10931))
- Fsync directory while persisting AOF manifest, RDB file, and config file ( [#10737](https://github.com/redis/redis/pull/10737))
- Script that made modification will not break with unexpected NOREPLICAS error ( [#10855](https://github.com/redis/redis/pull/10855))
- Cluster: Fix a bug where nodes may not acknowledge a CLUSTER FAILOVER TAKEOVER


  after a replica reboots ( [#10798](https://github.com/redis/redis/pull/10798))
- Cluster: Fix crash during handshake and cluster shards call ( [#10942](https://github.com/redis/redis/pull/10942))

## Fixes for issues in previous releases of Redis 7.0

- TLS: Fix issues with large replies ( [#10909](https://github.com/redis/redis/pull/10909))
- Correctly report the startup warning for vm.overcommit\_memory ( [#10841](https://github.com/redis/redis/pull/10841))
- redis-server command line allow passing config name and value in the same argument ( [#10866](https://github.com/redis/redis/pull/10866))
- Support --save command line argument with no value for backwards compatibility ( [#10866](https://github.com/redis/redis/pull/10866))
- Fix CLUSTER RESET command regression requiring an argument ( [#10898](https://github.com/redis/redis/pull/10898))

---

# cactus-comments-2 v0.13.0

> published at 2022-07-08 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.13.0)

# Cactus Comments Web Client v0.13.0

- Fix for missing end tokens in sync responses from Synapse >= v1.61.0 (Thanks to Tom Price for [!20](/cactus-comments/cactus-client/-/merge_requests/20 "Make end parameter optional in message response and room.")).
- A prettier animation while loading comments.

**IPFS CID**QmSiWN27KZZ1XE32jKwifBnS3nWTUcFGNArKzur2nmDgoL**JS URL**[https://gateway.pinata.cloud/ipfs/QmSiWN27KZZ1XE32jKwifBnS3nWTUcFGNArKzur2nmDgoL/v0.13.0/cactus.js](https://gateway.pinata.cloud/ipfs/QmSiWN27KZZ1XE32jKwifBnS3nWTUcFGNArKzur2nmDgoL/v0.13.0/cactus.js)**CSS URL**[https://gateway.pinata.cloud/ipfs/QmSiWN27KZZ1XE32jKwifBnS3nWTUcFGNArKzur2nmDgoL/v0.13.0/style.css](https://gateway.pinata.cloud/ipfs/QmSiWN27KZZ1XE32jKwifBnS3nWTUcFGNArKzur2nmDgoL/v0.13.0/style.css)

---

# mjolnir v1.5.0

> published at 2022-07-07 [source](https://github.com/matrix-org/mjolnir/releases/tag/v1.5.0)

## Changelog

**Bot**:

- **Change of behaviour:** The minimum supported node version is now **node 16**.
- Add the option for reports to be polled via the synapse admin API (rather than configuring proxy pass-through). by [@jesopo](https://github.com/jesopo) in [#259](https://github.com/matrix-org/mjolnir/pull/259)
- Add the option to disable the displaying new reports in moderation room (so that you can use the [TrustedReporters](https://github.com/matrix-org/mjolnir/blob/main/src/protections/TrustedReporters.ts) protection without the abuse reports features) by [@jesopo](https://github.com/jesopo) in [#320](https://github.com/matrix-org/mjolnir/pull/320)
- Add a `!mjolnir rules matching <entity>` command to search watched lists. by [@Gnuxie](https://github.com/Gnuxie) in [#307](https://github.com/matrix-org/mjolnir/pull/307)
- Add Glob support to the kick command by [@jae1911](https://github.com/jae1911) in [#291](https://github.com/matrix-org/mjolnir/pull/291)
- A background queue for kicking (to reduce the load of large glob kicks) by [@Yoric](https://github.com/Yoric) in [#296](https://github.com/matrix-org/mjolnir/pull/296)
- Improve the performance of the redact command. by [@Gnuxie](https://github.com/Gnuxie) in [#297](https://github.com/matrix-org/mjolnir/pull/297)
- Improve documentation (including dedicated setup documentation) by [@ShadowJonathan](https://github.com/ShadowJonathan) in [#277](https://github.com/matrix-org/mjolnir/pull/277)
- Add `!mjolnir since 1day mute 100` command: adding the ability to mute by [@Yoric](https://github.com/Yoric) in [#272](https://github.com/matrix-org/mjolnir/pull/272)

## New Contributors

- [@jae1911](https://github.com/jae1911) made their first contribution in [#291](https://github.com/matrix-org/mjolnir/pull/291)

**Full Changelog**: [`v1.4.2...v1.5.0`](https://github.com/matrix-org/mjolnir/compare/v1.4.2...v1.5.0)

---

# draupnir v1.5.0

> published at 2022-07-07 [source](https://github.com/Gnuxie/Draupnir/releases/tag/v1.5.0)

v1.5.0

---

# languagetool Update to v5.8

> published at 2022-07-06 [source](https://github.com/Erikvl87/docker-languagetool/releases/tag/v5.8)

[https://github.com/languagetool-org/languagetool/releases/tag/v5.8](https://github.com/languagetool-org/languagetool/releases/tag/v5.8)

---

# sygnal v0.12.0

> published at 2022-07-04 [source](https://github.com/matrix-org/sygnal/releases/tag/v0.12.0)

# Sygnal 0.12.0 (2022-07-04)

## Features

- Add a new `push_type` configuration option for APNs apps, to control the value of the `apns-push-type` header when sending requests. ( [#309](https://github.com/matrix-org/sygnal/issues/309))

## Bugfixes

- Fix a bug introduced in Sygnal 0.7.0 where a malformed `default_payload` could cause an internal server error. ( [#292](https://github.com/matrix-org/sygnal/issues/292))

## Improved Documentation

- Document the use of an iOS Notification Service Extension and the Push Gateway API as a workaround to trigger VoIP notifications on iOS. ( [#285](https://github.com/matrix-org/sygnal/issues/285))
- Add a link to the docker image in the README. ( [#297](https://github.com/matrix-org/sygnal/issues/297))

## Internal Changes

- Avoid a breaking change in aioapns 2.1 by requiring an earlier version of that package. ( [#294](https://github.com/matrix-org/sygnal/issues/294))
- Fix test failures by using the latest versions of TLS in the TLS proxy tests. ( [#306](https://github.com/matrix-org/sygnal/issues/306))
- Update the `black` code formatter to 22.3.0. ( [#307](https://github.com/matrix-org/sygnal/issues/307))

---

# hydrogen v0.2.33

> published at 2022-06-30 [source](https://github.com/vector-im/hydrogen-web/releases/tag/v0.2.33)

- Fixes [#765](https://github.com/vector-im/hydrogen-web/issues/765)
- Adds option do download media on video and image messages in the context menu [#767](https://github.com/vector-im/hydrogen-web/pull/767)

---

# ansible 5.10.0

> published at 2022-06-28 [source](https://pypi.org/project/ansible/5.10.0/)

> no description provided

---



---

# mailer 4.95-r0-4

> published at 2022-06-25 [source](https://github.com/devture/exim-relay/releases/tag/4.95-r0-4)

Upgrade Alpine base (3.15.4 -> 3.16.0)

---

# heisenbridge v1.13.1

> published at 2022-06-24 [source](https://github.com/hifi/heisenbridge/releases/tag/v1.13.1)

No content.

---

# sms v0.5.8

> published at 2022-06-24 [source](https://github.com/benkuly/matrix-sms-bridge/releases/tag/v0.5.8)

- Adding multi-bridge capabilities

thanks to [@erikhb](https://github.com/erikhb)

---

# ntfy v1.27.2

> published at 2022-06-23 [source](https://github.com/binwiederhier/ntfy/releases/tag/v1.27.2)

**Features:**

- Add `cache-startup-queries` option to allow custom [SQLite performance tuning](https://ntfy.sh/docs/config/#wal-for-message-cache) (no ticket)
- ntfy CLI can now [wait for a command or PID](https://ntfy.sh/docs/subscribe/cli/#wait-for-pidcommand) before publishing ( [#263](https://github.com/binwiederhier/ntfy/issues/263), thanks to the [original ntfy](https://github.com/dschep/ntfy) for the idea)
- Trace: Log entire HTTP request to simplify debugging (no ticket)
- Allow setting user password via `NTFY_PASSWORD` env variable ( [#327](https://github.com/binwiederhier/ntfy/pull/327), thanks to [@Kenix3](https://github.com/Kenix3))

**Bugs:**

- Fix slow requests due to excessive locking ( [#338](https://github.com/binwiederhier/ntfy/issues/338))
- Return HTTP 500 for `GET /_matrix/push/v1/notify` when `base-url` is not configured (no ticket)
- Disallow setting `upstream-base-url` to the same value as `base-url` ( [#334](https://github.com/binwiederhier/ntfy/issues/334), thanks to [@oester](https://github.com/oester) for reporting)
- Fix `since=<id>` implementation for multiple topics ( [#336](https://github.com/binwiederhier/ntfy/issues/336), thanks to [@karmanyaahm](https://github.com/karmanyaahm) for reporting)
- Simple parsing in `Actions` header now supports settings Android `intent=` key ( [#341](https://github.com/binwiederhier/ntfy/pull/341), thanks to [@wunter8](https://github.com/wunter8))

**Deprecations:**

- The `ntfy publish --env-topic` option is deprecated as of now (see [deprecations](https://ntfy.sh/docs/deprecations) for details)

---

# ntfy v1.27.1

> published at 2022-06-23 [source](https://github.com/binwiederhier/ntfy/releases/tag/v1.27.1)

Fix intermittent test failure; bump version

---

# prometheus-blackbox-exporter 0.21.1 / 2022-06-17

> published at 2022-06-23 [source](https://github.com/prometheus/blackbox_exporter/releases/tag/v0.21.1)

- \[BUGFIX\] Fix a data race in HTTP probes. [#929](https://github.com/prometheus/blackbox_exporter/pull/929)

---

# ansible 6.0.0

> published at 2022-06-21 [source](https://pypi.org/project/ansible/6.0.0/)

> no description provided

---



---

# nginx-proxy release-1.23.0

> published at 2022-06-21 [source](https://github.com/nginx/nginx/releases/tag/release-1.23.0)

nginx-1.23.0-RELEASE

---

# buscarron v1.2.0

> published at 2022-06-20 [source](https://gitlab.com/etke.cc/buscarron/-/tags/v1.2.0)

### Features ✨

- Spam checker can work with with email localpart list
- Add optional domain validation on the forms level
- Add `NOENCRYPTION` option

### Bugfixes 🐛

- Added CORS headers

### Misc 💤

- Updated deps

---

# ansible-core 2.12.7

> published at 2022-06-20 [source](https://pypi.org/project/ansible-core/2.12.7/)

> no description provided

---



---

# ansible-core 2.13.1

> published at 2022-06-20 [source](https://pypi.org/project/ansible-core/2.13.1/)

> no description provided

---



---

# mautrix-whatsapp v0.5.0

> published at 2022-06-16 [source](https://github.com/mautrix/whatsapp/releases/tag/v0.5.0)

- Moved a lot of code to mautrix-go.
- Improved handling edge cases in backfill system.
- Improved handling errors in Matrix->WhatsApp message bridging.
- Disallowed sending status broadcast messages by default, as it breaks with big contact lists. Sending can be re-enabled in the config.
- Fixed some cases where the first outgoing message was undecryptable for WhatsApp users.
- Fixed chats not being marked as read when sending a message from another WhatsApp client after receiving a call.
- Fixed other bridge users being added to status broadcasts rooms through double puppeting.
- Fixed edge cases in the deferred backfill queue.

---

# soft-serve v0.3.2

> published at 2022-06-15 [source](https://github.com/charmbracelet/soft-serve/releases/tag/v0.3.2)

This release fixes bugs related to running Soft Serve on Windows and respecting private repos on the TUI.

## Changelog

### New Features

- [`038537d`](https://github.com/charmbracelet/soft-serve/commit/038537d53431312920c8fb9a53c827158861a73c): feat: add running soft-serve as non-root using setgid/setuid ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`4a5d488`](https://github.com/charmbracelet/soft-serve/commit/4a5d4889a081abcda1cef6bea8a5d67ebddf0b3e): feat: use flag and add comment ( [@aymanbagabas](https://github.com/aymanbagabas))

### Bug fixes

- [`1a871c8`](https://github.com/charmbracelet/soft-serve/commit/1a871c89dad3a0b9ad8adb8d27bb132f295a5702): fix(config): use errors.Is instead of os.IsNotExist ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`af13e15`](https://github.com/charmbracelet/soft-serve/commit/af13e15ccab9930d117508afe954228cc944e044): fix: default config repo first commit time ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`4a5c387`](https://github.com/charmbracelet/soft-serve/commit/4a5c387bb045cb2285cc090a069509330cc7ec68): fix: respect private repos ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`ece7523`](https://github.com/charmbracelet/soft-serve/commit/ece7523ade23d86d6ec4a7d71fc13dae739547ff): fix: set allow keyless when no public keys provided ( [@aymanbagabas](https://github.com/aymanbagabas))

### Others

- [`ce28819`](https://github.com/charmbracelet/soft-serve/commit/ce288190e0485aae0c6ecc92f0957b86559579cc): chore: bump deps ( [@aymanbagabas](https://github.com/aymanbagabas))

* * *

[![The Charm logo](https://camo.githubusercontent.com/65459c24b86d0476085210fd0387503a161a1359b3cf5034324346f55907cbb8/68747470733a2f2f73747566662e636861726d2e73682f636861726d2d62616467652e6a7067)](https://charm.sh/)

Thoughts? Questions? We love hearing from you. Feel free to reach out on [Twitter](https://twitter.com/charmcli), [The Fediverse](https://mastodon.technology/@charm), or on [Slack](https://charm.sh/slack).

---

# registration-bot v1.1.6

> published at 2022-06-15 [source](https://github.com/moan0s/matrix-registration-bot/releases/tag/v1.1.6)

Releasing version v1.1.6

---

# corporal 2.3.0

> published at 2022-06-14 [source](https://github.com/devture/matrix-corporal/releases/tag/2.3.0)

Release 2.3.0

---

# ansible-core 2.12.7rc1

> published at 2022-06-13 [source](https://pypi.org/project/ansible-core/2.12.7rc1/)

> no description provided

---



---

# ansible-core 2.13.1rc1

> published at 2022-06-13 [source](https://pypi.org/project/ansible-core/2.13.1rc1/)

> no description provided

---



---

# ansible 6.0.0rc1

> published at 2022-06-07 [source](https://pypi.org/project/ansible/6.0.0rc1/)

> no description provided

---



---

# ansible 5.9.0

> published at 2022-06-07 [source](https://pypi.org/project/ansible/5.9.0/)

> no description provided

---



---

# mautrix-googlechat v0.3.3

> published at 2022-06-03 [source](https://github.com/mautrix/googlechat/releases/tag/v0.3.3)

- Switched to using native Matrix threads for bridging Google Chat threads.
- Removed web login interface and added support for logging in inside Matrix.
  - The provisioning API is still available, but it has moved from `/login/api` to `/_matrix/provision/v1`.
- Added error messages and optionally custom status events to detect when a message fails to bridge.

---

# mautrix-hangouts v0.3.3

> published at 2022-06-03 [source](https://github.com/mautrix/googlechat/releases/tag/v0.3.3)

- Switched to using native Matrix threads for bridging Google Chat threads.
- Removed web login interface and added support for logging in inside Matrix.
  - The provisioning API is still available, but it has moved from `/login/api` to `/_matrix/provision/v1`.
- Added error messages and optionally custom status events to detect when a message fails to bridge.

---

# ansible 6.0.0b2

> published at 2022-06-01 [source](https://pypi.org/project/ansible/6.0.0b2/)

> no description provided

---



---

# soft-serve v0.3.1

> published at 2022-06-01 [source](https://github.com/charmbracelet/soft-serve/releases/tag/v0.3.1)

## Changelog

### Bug fixes

- [`ccbbd0c`](https://github.com/charmbracelet/soft-serve/commit/ccbbd0c5aa505168d81b0dcfc9970c35c4dcfee7): fix(server): commands host & port ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`b6f20d7`](https://github.com/charmbracelet/soft-serve/commit/b6f20d723ec05c43bad19204c30eb0a03064e887): fix(server): logging middleware should come first ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`875ff2b`](https://github.com/charmbracelet/soft-serve/commit/875ff2b69e90de7ff02abf64fd3e423404b9188c): fix(server): use session stderr on ssh cli interface ( [@aymanbagabas](https://github.com/aymanbagabas))
- [`30d217f`](https://github.com/charmbracelet/soft-serve/commit/30d217fdd3e66aa0c1365916be75904581433a52): fix: fix typo in name ( [@bashbunni](https://github.com/bashbunni))
- [`9aa196a`](https://github.com/charmbracelet/soft-serve/commit/9aa196aedba8dbd1e33ec6aebfb3d811ed4652a0): fix: upgrade go-yaml to v3 ( [@aymanbagabas](https://github.com/aymanbagabas))

* * *

[![The Charm logo](https://camo.githubusercontent.com/65459c24b86d0476085210fd0387503a161a1359b3cf5034324346f55907cbb8/68747470733a2f2f73747566662e636861726d2e73682f636861726d2d62616467652e6a7067)](https://charm.sh/)

Thoughts? Questions? We love hearing from you. Feel free to reach out on [Twitter](https://twitter.com/charmcli), [The Fediverse](https://mastodon.technology/@charm), or on [Slack](https://charm.sh/slack).

---

# honoroit v0.9.9

> published at 2022-05-31 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.9)

- fix the `!ho start` command with `HONOROIT_NOENCRYPTION=1`

---

# honoroit v0.9.8

> published at 2022-05-31 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.8)

- add `NOENCRYPTION` option
- fix race condition on thread start
- fix greetings messages
- updated deps

---

# prometheus-blackbox-exporter 0.21.0 / 2022-05-30

> published at 2022-05-30 [source](https://github.com/prometheus/blackbox_exporter/releases/tag/v0.21.0)

This Prometheus release is built with go1.18, which contains two noticeable

changes related to TLS and HTTP:

1. [TLS 1.0 and 1.1 disabled by default client-side](https://go.dev/doc/go1.18#tls10).


   Blackbox Exporter users can override this with the `min_version` parameter of

   [tls\_config](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#tls_config).
2. [Certificates signed with the SHA-1 hash function are rejected](https://go.dev/doc/go1.18#sha1).


   This doesn't apply to self-signed root certificates.

- \[BUGFIX\] Prevent setting negative timeouts when using a small scrape interval. [#869](https://github.com/prometheus/blackbox_exporter/pull/869)

---

# cinny v2.0.4

> published at 2022-05-29 [source](https://github.com/cinnyapp/cinny/releases/tag/v2.0.4)

## What's Changed

- Don't show verify button if Cross-Signing is not enabled by [@ajbura](https://github.com/ajbura)
- Fix emoji autocomplete in some cases by [@ajbura](https://github.com/ajbura) ( [#565](https://github.com/cinnyapp/cinny/issues/565))
- Add support for building docker image for linux/arm64 by [@kfiven](https://github.com/kfiven) in [#494](https://github.com/cinnyapp/cinny/pull/494)

**Full Changelog**: [`v2.0.3...v2.0.4`](https://github.com/cinnyapp/cinny/compare/v2.0.3...v2.0.4)

### Desktop app

- Fix drag-n-drop not working by [@kfiven](https://github.com/kfiven)
- Add dev-tools to production builds by [@kfiven](https://github.com/kfiven)
- Fix copy-paste not working on macOS by [@kfiven](https://github.com/kfiven) ( [#531](https://github.com/cinnyapp/cinny/issues/531))
- [Flatpak package](https://flathub.org/apps/details/in.cinny.Cinny) for of the desktop app by [@kfiven](https://github.com/kfiven) ( [#527](https://github.com/cinnyapp/cinny/issues/527))

**Full Changelog**: [cinnyapp/cinny-desktop@ `v2.0.3...v2.0.4`](https://github.com/cinnyapp/cinny-desktop/compare/v2.0.3...v2.0.4)

---

# miniflux Miniflux 2.0.37

> published at 2022-05-28 [source](https://github.com/miniflux/v2/releases/tag/2.0.37)

- Add rewrite rule to decode base64 content
- Add Linkding integration
- Add comment button to Telegram message
- Add API endpoint to fetch unread and read counters
- Fixes logic bug in Google Reader API sanity check
- Reduce number of CORS preflight check to save network brandwidth
- Add Espial integration
- Allow API search for entries which are not starred
- Try to use outermost element text when title is empty
- Make swipe gestures feel more natural
  - Removes opacity transition when swiping an article read/unread
  - Adds "resistance" to the swiped entry when the 75px threshold is


    reached
  - Fixes an issue in which a swiped article couldn't be moved <15px
- Add support for feed streams to Google Reader API IDs API
- Fix invalid parsing of icon data URL
- Add Traditional Chinese translation
- Add distroless Docker image variant
- Add Go 1.18 to GitHub Action
- Bump `github.com/tdewolff/minify/v2` from `2.10.0` to `2.11`
- Bump `github.com/prometheus/client_golang` from `1.12.1` to `1.12.2`
- Bump `github.com/lib/pq` from `1.10.4` to `1.10.6`

---

# ansible 6.0.0b1

> published at 2022-05-27 [source](https://pypi.org/project/ansible/6.0.0b1/)

> no description provided

---



---

# languagetool v5.7-dockerupdate-1: fix: CORS settings

> published at 2022-05-24 [source](https://github.com/Erikvl87/docker-languagetool/releases/tag/v5.7-dockerupdate-1)

Adding an NGINX image acting as reverse proxy

in order to setting CORS directives and port expected by FF extension.

---

# coturn docker/4.5.2-r12

> published at 2022-05-24 [source](https://github.com/coturn/coturn/releases/tag/docker%2F4.5.2-r12)

[Changelog](https://github.com/coturn/coturn/blob/docker/4.5.2-r12/docker/coturn/CHANGELOG.md#452-r12--2022-05-24)

---

# nginx-proxy release-1.22.0

> published at 2022-05-23 [source](https://github.com/nginx/nginx/releases/tag/release-1.22.0)

nginx-1.22.0-RELEASE

---

# ansible-core 2.11.12

> published at 2022-05-23 [source](https://pypi.org/project/ansible-core/2.11.12/)

> no description provided

---



---

# ansible-core 2.12.6

> published at 2022-05-23 [source](https://pypi.org/project/ansible-core/2.12.6/)

> no description provided

---



---

# ansible 5.8.0

> published at 2022-05-18 [source](https://pypi.org/project/ansible/5.8.0/)

> no description provided

---



---

# ansible 6.0.0a3

> published at 2022-05-18 [source](https://pypi.org/project/ansible/6.0.0a3/)

> no description provided

---



---

# heisenbridge v1.13.0

> published at 2022-05-18 [source](https://github.com/hifi/heisenbridge/releases/tag/v1.13.0)

- Read default port and listen address from config url ( [@BtbN](https://github.com/BtbN))
- Improvements to pillifying IRC nicks, again
- Fixes for AUTOQUERY not always working correctly
- Allow anyone to use STATUS command to get their own status
- Filter control characters only for plumbs so people can send garbage to IRC if they wish from Matrix
- Support for converting IRC color codes to Matrix ( [@tjaderxyz](https://github.com/tjaderxyz))
- Fixed compose docker Synapse configuration for registration
- Improved Python 3.10 compatibliy ( [@BtbN](https://github.com/BtbN))
- Hidden room to hide joins using restricted rooms join rule ( [@BtbN](https://github.com/BtbN))

Enabling hidden room functionality additionally requires either re-joining channels or having them already at v9 and using the UPGRADE command.

---

# ansible-core 2.12.6rc1

> published at 2022-05-16 [source](https://pypi.org/project/ansible-core/2.12.6rc1/)

> no description provided

---



---

# mautrix-whatsapp v0.4.0

> published at 2022-05-16 [source](https://github.com/mautrix/whatsapp/releases/tag/v0.4.0)

- Switched from `/r0` to `/v3` paths everywhere.

  - The new `v3` paths are implemented since Synapse 1.48, Dendrite 0.6.5, and Conduit 0.4.0. Servers older than these are no longer supported.
- Added new deferred backfill system to allow backfilling historical messages later instead of doing everything at login.
- Added option to automatically request old media from phone after backfilling.
- Added experimental provisioning API to check if a phone number is registered on WhatsApp.
- Added automatic retrying if the websocket dies while sending a message.
- Added experimental support for sending status broadcast messages.
- Added command to change disappearing message timer in chats.
- Improved error handling if Postgres dies while the bridge is running.
- Fixed bridging stickers sent from WhatsApp web.
- Fixed registration generation not regex-escaping user ID namespaces.

---

# ansible-core 2.11.12rc1

> published at 2022-05-16 [source](https://pypi.org/project/ansible-core/2.11.12rc1/)

> no description provided

---



---

# mx-puppet-slack v0.1.2

> published at 2022-05-16 [source](https://gitlab.com/mx-puppet/slack/mx-puppet-slack/-/tags/v0.1.2)

- Force rebuild

---

# ansible-core 2.13.0

> published at 2022-05-16 [source](https://pypi.org/project/ansible-core/2.13.0/)

> no description provided

---



---

# mx-puppet-slack v0.1.1

> published at 2022-05-14 [source](https://gitlab.com/mx-puppet/slack/mx-puppet-slack/-/tags/v0.1.1)

- Upgrade matrix-slack-parser to a functional version

---

# etherpad 1.8.18

> published at 2022-05-12 [source](https://github.com/ether/etherpad-lite/releases/tag/1.8.18)

### Notable enhancements and fixes

- Upgraded ueberDB to fix a regression with CouchDB ( [#5532](https://github.com/ether/etherpad-lite/issues/5532)).

---

# prometheus-blackbox-exporter 0.21.0-rc.0 / 2022-05-10

> published at 2022-05-11 [source](https://github.com/prometheus/blackbox_exporter/releases/tag/v0.21.0-rc.0)

This Prometheus release is built with go1.18, which contains two noticeable

changes related to TLS and HTTP:

1. [TLS 1.0 and 1.1 disabled by default client-side](https://go.dev/doc/go1.18#tls10).


   Blackbox Exporter users can override this with the `min_version` parameter of

   [tls\_config](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#tls_config).
2. [Certificates signed with the SHA-1 hash function are rejected](https://go.dev/doc/go1.18#sha1).


   This doesn't apply to self-signed root certificates.

- \[BUGFIX\] Prevent setting negative timeouts when using a small scrape interval. [#869](https://github.com/prometheus/blackbox_exporter/pull/869)

---

# buscarron v1.1.0

> published at 2022-05-10 [source](https://gitlab.com/etke.cc/buscarron/-/tags/v1.1.0)

- automatic confirmation emails with postmark
- templates support for redirect URLs
- automatically ban spammers, scanners and bots

---

# mailer 4.95-r0-3

> published at 2022-05-08 [source](https://github.com/devture/exim-relay/releases/tag/4.95-r0-3)

Upgrade Alpine base (3.15.0 -> 3.15.4)

---

# mjolnir v1.4.2

> published at 2022-05-06 [source](https://github.com/matrix-org/mjolnir/releases/tag/v1.4.2)

## Changelog

**Bot**:

- Added new `JoinWaveShortCircuit` protection, thanks to [@ShadowJonathan](https://github.com/ShadowJonathan) with [#280](https://github.com/matrix-org/mjolnir/pull/280). This protection can be used to detect a mass-join scenario and set the room to invite-only.
- **Change of behaviour:** Mjolnir will now apply server ACL and member bans to the most recently active rooms first (while syncing). The order was random before.
- The causes of errors at startup (e.g. via misconfiguration) have been made more clear.
- The image with the `latest` tag on [dockerhub](https://hub.docker.com/r/matrixdotorg/mjolnir/tags) is now correctly in sync with the `main` branch.

**Synapse Module**:

- The [python module version](https://github.com/matrix-org/mjolnir/blob/main/synapse_antispam/setup.py#L5) is now in sync with the current git tag.
- The Readme's example config has been fixed (thanks to [@maranda](https://github.com/maranda) ).

---

# draupnir v1.4.2

> published at 2022-05-06 [source](https://github.com/Gnuxie/Draupnir/releases/tag/v1.4.2)

v1.4.2

---

# mx-puppet-discord v0.1.1

> published at 2022-05-05 [source](https://gitlab.com/mx-puppet/discord/mx-puppet-discord/-/tags/v0.1.1)

- Build the images with node:12-alpine

---

# ansible 6.0.0a2

> published at 2022-05-04 [source](https://pypi.org/project/ansible/6.0.0a2/)

> no description provided

---



---

# ansible 5.7.1

> published at 2022-05-04 [source](https://pypi.org/project/ansible/5.7.1/)

> no description provided

---



---

# appservice-irc 0.34.0 (2022-05-04)

> published at 2022-05-04 [source](https://github.com/matrix-org/matrix-appservice-irc/releases/tag/0.34.0)

This release fixes a High severity security vulnerability. See [the matrix blog](https://matrix.org/blog/2022/05/04/0-34-0-security-release-for-matrix-appservice-irc-high-severity) for more details.

# Internal Changes

- Updated node-irc to 1.2.1

---

# ansible 5.7.0

> published at 2022-04-26 [source](https://pypi.org/project/ansible/5.7.0/)

> no description provided

---



---

# buscarron v1.0.0

> published at 2022-04-23 [source](https://gitlab.com/etke.cc/buscarron/-/tags/v1.0.0)

### Features ✨

- Initial release

### Bugfixes 🐛

N/A _that's the first version, bugs are creating on that step!_

### Breaking changes ⚠

Buscarron has been created.

---

# heisenbridge v1.12.0

> published at 2022-04-22 [source](https://github.com/hifi/heisenbridge/releases/tag/v1.12.0)

- Make MAXLINES and PASTEBIN option available to every room & global defaults (thanks [@BtbN](https://github.com/BtbN))
- Fix 'NoneType' is not iterable error (thanks [@BtbN](https://github.com/BtbN))
- Upgrade to Mautrix 0.16

Not much to see here. A small contribution release which is always a nice thing when people come around and help 🤗.

---

# radicale 3.1.2.0

> published at 2022-04-22 [source](https://github.com/tomsquest/docker-radicale/releases/tag/3.1.2.0)

[Docker-Radicale changelog](https://github.com/tomsquest/docker-radicale/blob/master/CHANGELOG.md)

[Radicale changelog](https://github.com/Kozea/Radicale/blob/master/CHANGELOG.md)

**Full Changelog**: [`3.1.0.0...3.1.2.0`](https://github.com/tomsquest/docker-radicale/compare/3.1.0.0...3.1.2.0)

---

# radicale 3.1.3.0

> published at 2022-04-22 [source](https://github.com/tomsquest/docker-radicale/releases/tag/3.1.3.0)

[Docker-Radicale changelog](https://github.com/tomsquest/docker-radicale/blob/master/CHANGELOG.md)

[Radicale changelog](https://github.com/Kozea/Radicale/blob/master/CHANGELOG.md)

**Full Changelog**: [`3.1.2.0...3.1.3.0`](https://github.com/tomsquest/docker-radicale/compare/3.1.2.0...3.1.3.0)

---

# radicale 3.1.4.0

> published at 2022-04-22 [source](https://github.com/tomsquest/docker-radicale/releases/tag/3.1.4.0)

[Docker-Radicale changelog](https://github.com/tomsquest/docker-radicale/blob/master/CHANGELOG.md)

[Radicale changelog](https://github.com/Kozea/Radicale/blob/master/CHANGELOG.md)

**Full Changelog**: [`3.1.3.0...3.1.4.0`](https://github.com/tomsquest/docker-radicale/compare/3.1.3.0...3.1.4.0)

---

# radicale 3.1.5.0

> published at 2022-04-22 [source](https://github.com/tomsquest/docker-radicale/releases/tag/3.1.5.0)

[Docker-Radicale changelog](https://github.com/tomsquest/docker-radicale/blob/master/CHANGELOG.md)

[Radicale changelog](https://github.com/Kozea/Radicale/blob/master/CHANGELOG.md)

**Full Changelog**: [`3.1.4.0...3.1.5.0`](https://github.com/tomsquest/docker-radicale/compare/3.1.4.0...3.1.5.0)

---

# radicale 3.1.5.1

> published at 2022-04-22 [source](https://github.com/tomsquest/docker-radicale/releases/tag/3.1.5.1)

## What's Changed

- fix: add openssh for git ssh remotes by [@tionis](https://github.com/tionis) in [#117](https://github.com/tomsquest/docker-radicale/pull/117)

## New Contributors

- [@tionis](https://github.com/tionis) made their first contribution in [#117](https://github.com/tomsquest/docker-radicale/pull/117)

[Docker-Radicale changelog](https://github.com/tomsquest/docker-radicale/blob/master/CHANGELOG.md)

[Radicale changelog](https://github.com/Kozea/Radicale/blob/master/CHANGELOG.md)

**Full Changelog**: [`3.1.5.0...3.1.5.1`](https://github.com/tomsquest/docker-radicale/compare/3.1.5.0...3.1.5.1)

---

# radicale 3.1.7.0

> published at 2022-04-21 [source](https://github.com/tomsquest/docker-radicale/releases/tag/3.1.7.0)

[Docker-Radicale changelog](https://github.com/tomsquest/docker-radicale/blob/master/CHANGELOG.md)

[Radicale changelog](https://github.com/Kozea/Radicale/blob/master/CHANGELOG.md)

**Full Changelog**: [`3.1.6.0...3.1.7.0`](https://github.com/tomsquest/docker-radicale/compare/3.1.6.0...3.1.7.0)

---

# radicale 3.1.6.0

> published at 2022-04-21 [source](https://github.com/tomsquest/docker-radicale/releases/tag/3.1.6.0)

[Docker-Radicale changelog](https://github.com/tomsquest/docker-radicale/blob/master/CHANGELOG.md)

[Radicale changelog](https://github.com/Kozea/Radicale/blob/master/CHANGELOG.md)

**Full Changelog**: [`3.1.5.1...3.1.6.0`](https://github.com/tomsquest/docker-radicale/compare/3.1.5.1...3.1.6.0)

---

# registration-bot v1.1.5

> published at 2022-04-20 [source](https://github.com/moan0s/matrix-registration-bot/releases/tag/v1.1.5)

This is a small feature release that adds the option to specifiy a config file via an environment variable. Very useful for docker deployment

---

# mautrix-signal v0.3.0

> published at 2022-04-20 [source](https://github.com/mautrix/signal/releases/tag/v0.3.0)

Target signald version: [v0.18.0](https://gitlab.com/signald/signald/-/releases/0.18.0)

### Important changes

- Both the signald and mautrix-signal docker images have been changed to run as UID 1337 by default. The migration should work automatically as long as you update both containers at the same time.
  - Also note that the `finn/signald` image is deprecated, you should use `signald/signald`. [https://signald.org/articles/install/docker/](https://signald.org/articles/install/docker/)
- Old homeservers which don't support the new `/v3` API endpoints are no longer supported. Synapse 1.48+, Dendrite 0.6.5+ and Conduit 0.4.0+ are supported. Legacy `r0` API support can be temporarily re-enabled with `pip install mautrix==0.16.0`. However, this option will not be available in future releases.

### Added

- Support for creating DM portals by inviting user (i.e. just making a DM the normal Matrix way).
- Leaving groups is now bridged to Signal (thanks to [@maltee1](https://github.com/maltee1) in [#245](https://github.com/mautrix/signal/pull/245)).
- Signal group descriptions are now bridged into Matrix room topics.
- Signal announcement group status is now bridged into power levels on Matrix (the group will be read-only for everyone except admins).
- Added optional parameter to `mark-trusted` command to set trust level (instead of always using `TRUSTED_VERIFIED`).
- Added option to use [MSC2246](https://github.com/matrix-org/matrix-spec-proposals/pull/2246) async media uploads.
- Added provisioning API for listing contacts and starting private chats.

### Improved

- Dropped Python 3.7 support.
- Files bridged to Matrix now include the `size` field in the file `info`.
- Removed redundant `msgtype` field in sticker events sent to Matrix.
- Users who have left the group on Signal will now be removed from Matrix too.

### Fixed

- Logging into the bridge with double puppeting no longer removes your Signal user's Matrix ghost from DM portals with other bridge users.
- Fixed identity failure error message always saying "while sending message to None" instead of specifying the problematic phone number.
- Fixed `channel` -\> `id` field in `m.bridge` events.

---

# mautrix-googlechat v0.3.2

> published at 2022-04-19 [source](https://github.com/mautrix/googlechat/releases/tag/v0.3.2)

**N.B.** This release drops support for old homeservers which don't support the new `/v3` API endpoints. Synapse 1.48+, Dendrite 0.6.5+ and Conduit 0.4.0+ are supported. Legacy `r0` API support can be temporarily re-enabled with `pip install mautrix==0.16.0`. However, this option will not be available in future releases.

- Added option to use [MSC2246](https://github.com/matrix-org/matrix-spec-proposals/pull/2246) async media uploads.
- Added support for syncing read state from Google Chat after backfilling.
- Updated user avatar sync to store hashes and check them before updating avatar on Matrix (thanks to [@kpfleming](https://github.com/kpfleming) in [#66](https://github.com/mautrix/googlechat/pull/66)).

  - Usually avatar URLs are stable, but it seems that they aren't in some cases. This change should prevent unnecessary avatar change events on Matrix.
- Changed event handling to work synchronously to make sure incoming messages are bridged in the correct order.
- Fixed bug where messages being sent while the bridge is reconnecting to Google Chat would fail completely.
- Removed unnecessary warning log about `COMPASS` cookies.

---

# mautrix-hangouts v0.3.2

> published at 2022-04-19 [source](https://github.com/mautrix/googlechat/releases/tag/v0.3.2)

**N.B.** This release drops support for old homeservers which don't support the new `/v3` API endpoints. Synapse 1.48+, Dendrite 0.6.5+ and Conduit 0.4.0+ are supported. Legacy `r0` API support can be temporarily re-enabled with `pip install mautrix==0.16.0`. However, this option will not be available in future releases.

- Added option to use [MSC2246](https://github.com/matrix-org/matrix-spec-proposals/pull/2246) async media uploads.
- Added support for syncing read state from Google Chat after backfilling.
- Updated user avatar sync to store hashes and check them before updating avatar on Matrix (thanks to [@kpfleming](https://github.com/kpfleming) in [#66](https://github.com/mautrix/googlechat/pull/66)).

  - Usually avatar URLs are stable, but it seems that they aren't in some cases. This change should prevent unnecessary avatar change events on Matrix.
- Changed event handling to work synchronously to make sure incoming messages are bridged in the correct order.
- Fixed bug where messages being sent while the bridge is reconnecting to Google Chat would fail completely.
- Removed unnecessary warning log about `COMPASS` cookies.

---

# honoroit v0.9.7

> published at 2022-04-18 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.7)

- fix membership events
- `!ho done` will try to save previous thread topic
- add notifications about user invite/join/leave events

---

# mautrix-telegram v0.11.3

> published at 2022-04-17 [source](https://github.com/mautrix/telegram/releases/tag/v0.11.3)

**N.B.** This release drops support for old homeservers which don't support the new `/v3` API endpoints. Synapse 1.48+, Dendrite 0.6.5+ and Conduit 0.4.0+ are supported. Legacy `r0` API support can be temporarily re-enabled with `pip install mautrix==0.16.0`. However, this option will not be available in future releases.

### Added

- Added `list-invite-links` command to list invite links in a chat.
- Added option to use [MSC2246](https://github.com/matrix-org/matrix-spec-proposals/pull/2246) async media uploads.
- Provisioning API for listing contacts and starting private chats.

### Improved

- Dropped Python 3.7 support.
- Telegram->Matrix message formatter will now replace `t.me/c/chatid/messageid` style links with a link to the bridged Matrix event (in addition to the previously supported `t.me/username/messageid` links).
- Updated formatting converter to keep newlines in code blocks as `\n` instead of converting them to `<br/>`.
- Removed `max_document_size` option. The bridge will now fetch the max size automatically using the media repo config endpoint.
- Removed redundant `msgtype` field in sticker events sent to Matrix.
- Disabled file logging in Docker image by default.
  - If you want to enable it, set the `filename` in the file log handler to a path that is writable, then add `"file"` back to `logging.root.handlers`.
- Reactions are now marked as read when bridging read receipts from Matrix.

### Fixed

- Fixed `!tg bridge` throwing error if the parameter is not an integer
- Fixed `!tg bridge` failing if the command had been previously run with an incorrectly prefixed chat ID (e.g. `!tg bridge -1234567` followed by `!tg bridge -1001234567`).
- Fixed `bridge_matrix_leave` config option not actually being used correctly.
- Fixed public channel mentions always bridging into a user mention on Matrix rather than a room mention.
  - The bridge will now make room mentions if the portal exists and fall back to user mentions otherwise.
- Fixed newlines being lost in unformatted forwarded messages.

---

# registration-bot v1.1.4

> published at 2022-04-15 [source](https://github.com/moan0s/matrix-registration-bot/releases/tag/v1.1.4)

Fix pip install

---

# registration-bot v1.1.3

> published at 2022-04-15 [source](https://github.com/moan0s/matrix-registration-bot/releases/tag/v1.1.3)

A small release, adding a `README.rst ` that is compatible with PyPi. This release has no effect on the end user.

---

# registration-bot v1.1.2

> published at 2022-04-15 [source](https://github.com/moan0s/matrix-registration-bot/releases/tag/v1.1.2)

This release focuses on build&deployment. It contains

- A build with `pyproject.toml` that fixes dependency problems
- An ansible role to easily install the bot, written by [https://github.com/bziemons](https://github.com/bziemons)
- The option to run the bot via `matrix-registration-bot` after installation

---

# rageshake v1.7

> published at 2022-04-14 [source](https://github.com/matrix-org/rageshake/releases/tag/v1.7)

# 1.7 (2022-04-14)

## Features

- Pass the prefix as a unique ID for the rageshake to the generic webhook mechanism. ( [#54](https://github.com/matrix-org/rageshake/issues/54))

---

# ansible 6.0.0a1

> published at 2022-04-13 [source](https://pypi.org/project/ansible/6.0.0a1/)

> no description provided

---



---

# honoroit v0.9.6

> published at 2022-04-12 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.6)

- support stable threads

---

# mautrix-instagram v0.1.3

> published at 2022-04-06 [source](https://github.com/mautrix/instagram/releases/tag/v0.1.3)

- Added support for Matrix->Instagram replies.
- Added support for sending clickable links with previews to Instagram.
- Added support for creating DMs from Matrix (by starting a chat with a ghost).
- Added option to use [MSC2246](https://github.com/matrix-org/matrix-spec-proposals/pull/2246) async media uploads.
- Added support for logging in with a Facebook token in the provisioning API.
- Added support for sending giphy gifs (requires client support).
- Changed some fields to stop the user from showing up as online on Instagram all the time.
- Fixed messages on Instagram not being marked as read if last event on Matrix is not a normal message.
- Fixed incoming messages not being deduplicated properly in some cases.
- Removed legacy `community_id` config option.
- Stopped running as root in Docker image (default user is now `1337`).
- Disabled file logging in Docker image by default.
  - If you want to enable it, set the `filename` in the file log handler to a path that is writable, then add `"file"` back to `logging.root.handlers`.
- Dropped Python 3.7 support.

---

# mautrix-facebook v0.4.0

> published at 2022-04-06 [source](https://github.com/mautrix/facebook/releases/tag/v0.4.0)

- Dropped Python 3.7 support.
- Added optional support for bridging presence from Facebook to Matrix (thanks to [@JakuJ](https://github.com/JakuJ) in [#189](https://github.com/mautrix/facebook/pull/189)).
- Added option to not resync chats on startup and instead ask the server to just send missed messages.
- Added option to use [MSC2246](https://github.com/matrix-org/matrix-spec-proposals/pull/2246) async media uploads.
- Changed some fields to stop the user from showing up as online on Facebook all the time.
- Changed incoming message handling to preserve order from Facebook to Matrix.
- Fixed calculating mention offsets (mentioning users in messages with complicated unicode characters like emojis).
  - This will break message rendering that involves mentions and emojis in the Messenger web app, but it works everywhere else. The issue on web happens even with messages sent from the official apps.
- Fixed bridging reactions from Facebook in certain cases.
- Fixed bridging files from Matrix with non-ascii names.

---

# ansible 5.6.0

> published at 2022-04-05 [source](https://pypi.org/project/ansible/5.6.0/)

> no description provided

---



---

# appservice-slack 1.11.0

> published at 2022-04-05 [source](https://github.com/matrix-org/matrix-appservice-slack/releases/tag/1.11.0)

No significant changes since 1.11.0-rc1.

## Features

- Support bridging Slack message threads with new m.thread relations (MSC3440) ( [#634](https://github.com/matrix-org/matrix-appservice-slack/issues/634), [#673](https://github.com/matrix-org/matrix-appservice-slack/issues/673))
- Add check to verify if the homeserver <-> bridge connection is working on startup. ( [#666](https://github.com/matrix-org/matrix-appservice-slack/issues/666))

## Bugfixes

- Fix: Don't create an internal ping room on every restart ( [#669](https://github.com/matrix-org/matrix-appservice-slack/issues/669))

## Internal Changes

- Upgrade dependency axios to 0.26.0 to close a possible vulnerability ( [#664](https://github.com/matrix-org/matrix-appservice-slack/issues/664))
- Remove uses of the deprecated JavaScript function substr() ( [#665](https://github.com/matrix-org/matrix-appservice-slack/issues/665))

---

# heisenbridge v1.11.0

> published at 2022-04-03 [source](https://github.com/hifi/heisenbridge/releases/tag/v1.11.0)

- Fixed retry behavior on startup to wait for HS startup
- Ignore TAGMSG messages from IRC server
- Fixed HTML messages not working as commands
- Fixed room aliases in messages dropping the message completely
- Upgrade to Mautrix 0.15

This release also breaks support for homeservers not supporting the "v3" API path so if you run Synapse 1.47 or older the bridge will not start.

---

# appservice-irc 0.33.1 (2022-03-30)

> published at 2022-03-30 [source](https://github.com/matrix-org/matrix-appservice-irc/releases/tag/0.33.1)

**This release fixes a critical bug which would cause bans across the bridge when using the new ban list feature.**

## Bugfixes

- Fix an issue where synchronising a ban list would cause all users to get banned. ( [#1551](https://github.com/matrix-org/matrix-appservice-irc/issues/1551))

## Deprecations and Removals

- Remove several scripts in `scripts/` which were unmaintained and obsolete. ( [#1531](https://github.com/matrix-org/matrix-appservice-irc/issues/1531))

## Internal Changes

- Fix towncrier script for summarising the newsfiles. ( [#1549](https://github.com/matrix-org/matrix-appservice-irc/issues/1549))

---

# languagetool v5.7: Update the build with the appropriate LANGUAGETOOL_DIST_VERSION

> published at 2022-03-30 [source](https://github.com/Erikvl87/docker-languagetool/releases/tag/v5.7)

[languagetool-org/languagetool#6198 (comment)](https://github.com/languagetool-org/languagetool/issues/6198#issuecomment-1083071258)

---

# mautrix-twitter v0.1.4

> published at 2022-03-30 [source](https://github.com/mautrix/twitter/releases/tag/v0.1.4)

Bump version to 0.1.4

---

# appservice-irc hs-fix-evil-bans

> published at 2022-03-29 [source](https://github.com/matrix-org/matrix-appservice-irc/releases/tag/hs-fix-evil-bans)

Fix an issue causing users to be banned chaotically

---

# mjolnir v1.4.0

> published at 2022-03-22 [source](https://github.com/matrix-org/mjolnir/releases/tag/v1.4.0)

## Changelog

**Antispam module**:

- Fix incorrect type declaration in check\_username\_for\_spam by [@Gnuxie](https://github.com/Gnuxie) in [#250](https://github.com/matrix-org/mjolnir/pull/250) (thanks [@squahtx](https://github.com/squahtx) for [reporting](https://github.com/matrix-org/mjolnir/issues/245))
- Fix block\_usernames config option by [@DMRobertson](https://github.com/DMRobertson) in [#246](https://github.com/matrix-org/mjolnir/pull/246) (issue [#244](https://github.com/matrix-org/mjolnir/issues/244))
- Move message\_limit into antispam by [@Gnuxie](https://github.com/Gnuxie), a new protection for limiting the length of messages on your homeserver [#243](https://github.com/matrix-org/mjolnir/pull/243)

**Bot**:

- A room Protection designed to measure & detect federation lag in a room as an early warning for mass spam by [@Yoric](https://github.com/Yoric) in [#217](https://github.com/matrix-org/mjolnir/pull/217)
- Reduce stackspam in logs from http errors by [@Yoric](https://github.com/Yoric) in [#237](https://github.com/matrix-org/mjolnir/pull/237)
- send whole channel shutdown reason, not just the first word by [@jesopo](https://github.com/jesopo) in [#242](https://github.com/matrix-org/mjolnir/pull/242) (thanks [@verymilan](https://github.com/verymilan) for reporting issue [#239](https://github.com/matrix-org/mjolnir/issues/239))
- Add command to elevate a user (or the bot) as room administrator using Synapse's [make\_room\_admin](https://matrix-org.github.io/synapse/latest/admin_api/rooms.html#make-room-admin-api) by [@maranda](https://github.com/maranda) in [#219](https://github.com/matrix-org/mjolnir/pull/219)
- A command to show when users in a given room have joined by [@Yoric](https://github.com/Yoric) in [#225](https://github.com/matrix-org/mjolnir/pull/225)
- standard protection consequences by [@jesopo](https://github.com/jesopo), making it easier to write new protections in [#232](https://github.com/matrix-org/mjolnir/pull/232)
- New command `!mjolnir since <date or duration> <kick | ban | show> <limit> [reason] [...rooms]`, to filter through recent joiners by [@Yoric](https://github.com/Yoric) in [#238](https://github.com/matrix-org/mjolnir/pull/238)

**Full Changelog**: [`v1.3.2...v1.4.0`](https://github.com/matrix-org/mjolnir/compare/v1.3.2...v1.4.0)

---

# appservice-slack 1.11.0-rc1 (2022-03-21)

> published at 2022-03-21 [source](https://github.com/matrix-org/matrix-appservice-slack/releases/tag/1.11.0-rc1)

## Features

- Support bridging Slack message threads with new m.thread relations (MSC3440) ( [#634](https://github.com/matrix-org/matrix-appservice-slack/issues/634), [#673](https://github.com/matrix-org/matrix-appservice-slack/issues/673))
- Add check to verify if the homeserver <-> bridge connection is working on startup. ( [#666](https://github.com/matrix-org/matrix-appservice-slack/issues/666))

## Bugfixes

- Fix: Don't create an internal ping room on every restart ( [#669](https://github.com/matrix-org/matrix-appservice-slack/issues/669))

## Internal Changes

- Upgrade dependency axios to 0.26.0 to close a possible vulnerability ( [#664](https://github.com/matrix-org/matrix-appservice-slack/issues/664))
- Remove uses of the deprecated JavaScript function substr() ( [#665](https://github.com/matrix-org/matrix-appservice-slack/issues/665))

---

# mjolnir v1.4.1

> published at 2022-03-21 [source](https://github.com/matrix-org/mjolnir/releases/tag/v1.4.1)

## Bot

- remove line leftover from debugging that caused BasicFloodingProtection to issue kicks by [@jesopo](https://github.com/jesopo) in [#254](https://github.com/matrix-org/mjolnir/pull/254) (thanks [@JokerGermany](https://github.com/JokerGermany) for reporting)
- show room ID when logging protection consequences by [@jesopo](https://github.com/jesopo) in [#255](https://github.com/matrix-org/mjolnir/pull/255)

**Full Changelog**: [`v1.4.0...v1.4.1`](https://github.com/matrix-org/mjolnir/compare/v1.4.0...v1.4.1)

---

# draupnir v1.4.1

> published at 2022-03-21 [source](https://github.com/Gnuxie/Draupnir/releases/tag/v1.4.1)

v1.4.1

---

# draupnir v1.4.0

> published at 2022-03-21 [source](https://github.com/Gnuxie/Draupnir/releases/tag/v1.4.0)

v1.4.0

---

# prometheus-blackbox-exporter 0.20.0 / 2022-03-16

> published at 2022-03-16 [source](https://github.com/prometheus/blackbox_exporter/releases/tag/v0.20.0)

- \[FEATURE\] Add support for grpc health check. [#835](https://github.com/prometheus/blackbox_exporter/pull/835)
- \[FEATURE\] Add hostname parameter. [#823](https://github.com/prometheus/blackbox_exporter/pull/823)
- \[ENHANCEMENT\] Add body\_size\_limit option to http module. [#836](https://github.com/prometheus/blackbox_exporter/pull/836)
- \[ENHANCEMENT\] Change default user agent. [#557](https://github.com/prometheus/blackbox_exporter/pull/557)
- \[ENHANCEMENT\] Add control of recursion desired flag for DNS probes. [#859](https://github.com/prometheus/blackbox_exporter/pull/859)
- \[ENHANCEMENT\] Delay init of http phase values. [#865](https://github.com/prometheus/blackbox_exporter/pull/865)
- \[BUGFIX\] Fix IP hash. [#863](https://github.com/prometheus/blackbox_exporter/pull/863)

---

# mautrix-googlechat v0.3.1

> published at 2022-03-16 [source](https://github.com/mautrix/googlechat/releases/tag/v0.3.1)

- Ignored errors getting `COMPASS` cookie as Google appears to have changed something.
- Improved attachment bridging support.
  - Drive and YouTube links will be bridged even when they're sent as attachments (rather than text messages).
  - Bridging big files uses less memory now (only ~1-2x file size rather than 2-4x).
  - Link preview metadata is now included in the Matrix events (in a custom field).
- Disabled file logging in Docker image by default.
  - If you want to enable it, set the `filename` in the file log handler to a path that is writable, then add `"file"` back to `logging.root.handlers`.
- Formatted all code using [black](https://github.com/psf/black) and [isort](https://github.com/PyCQA/isort).

---

# mautrix-hangouts v0.3.1

> published at 2022-03-16 [source](https://github.com/mautrix/googlechat/releases/tag/v0.3.1)

- Ignored errors getting `COMPASS` cookie as Google appears to have changed something.
- Improved attachment bridging support.
  - Drive and YouTube links will be bridged even when they're sent as attachments (rather than text messages).
  - Bridging big files uses less memory now (only ~1-2x file size rather than 2-4x).
  - Link preview metadata is now included in the Matrix events (in a custom field).
- Disabled file logging in Docker image by default.
  - If you want to enable it, set the `filename` in the file log handler to a path that is writable, then add `"file"` back to `logging.root.handlers`.
- Formatted all code using [black](https://github.com/psf/black) and [isort](https://github.com/PyCQA/isort).

---

# ansible 5.5.0

> published at 2022-03-15 [source](https://pypi.org/project/ansible/5.5.0/)

> no description provided

---



---

# miniflux Miniflux 2.0.36

> published at 2022-03-09 [source](https://github.com/miniflux/v2/releases/tag/2.0.36)

- Gray out pagination buttons when they are not applicable
- Use truncated entry description as title if unavailable
- Do not fallback to InnerXML if XHTML title is empty
- Add `+` keyboard shortcut for new subscription page
- Add `(+)` action next to Feeds to quickly add new feeds
- Fix unstar not working via Google Reader API
- Remove circles in front of page header list items
- Fix CSS hover style for links styled as buttons
- Avoid showing `undefined` when clicking on read/unread
- Add new keyboard shortcut `M` to toggle read/unread, and go to previous item
- Add several icons to menus according to their roles
- Add missing event argument to `onClick()` function call
- Add links to scraper/rewrite/filtering docs when editing feeds
- Add a rewrite rule for Castopod episodes
- Fix regression: reset touch-item if not in `/unread` page
- Add API endpoint to fetch original article
- Show the category first in feed settings
- Add pagination on top of all entries
- Display Go version in "About" page
- Bump `mvdan.cc/xurls/v2` from 2.3.0 to 2.4.0
- Bump `github.com/prometheus/client_golang` from 1.11.0 to 1.12.1
- Bump `github.com/tdewolff/minify/v2` from 2.9.28 to 2.10.0

---

# appservice-irc 0.33.0 (2022-03-02)

> published at 2022-03-02 [source](https://github.com/matrix-org/matrix-appservice-irc/releases/tag/0.33.0)

## Features

- Support splitting users from different homeservers into different IPv6 blocks. ( [#1514](https://github.com/matrix-org/matrix-appservice-irc/issues/1514))
- Added a new metric `clientpool_by_homeserver` which lists the states of IRC clients, by the top 25 homeservers. ( [#1517](https://github.com/matrix-org/matrix-appservice-irc/issues/1517))
- Add support for subscribing to moderation policy. See [http://matrix-org.github.io/matrix-appservice-irc/administrators\_guide.html#subscribing-to-moderation-policies](http://matrix-org.github.io/matrix-appservice-irc/administrators_guide.html#subscribing-to-moderation-policies) for more information. ( [#1532](https://github.com/matrix-org/matrix-appservice-irc/issues/1532))

## Bugfixes

- Matrix message edits no longer bridge as a diff if it's longer than the new message ( [#1477](https://github.com/matrix-org/matrix-appservice-irc/issues/1477))
- Fix a duplicate metric that would prevent the bridge from starting. ( [#1534](https://github.com/matrix-org/matrix-appservice-irc/issues/1534))

## Improved Documentation

- Update the list of bridged networks after hackint started offering a bridge once again. ( [#1501](https://github.com/matrix-org/matrix-appservice-irc/issues/1501))
- Removed freenode from bridged networks. ( [#1523](https://github.com/matrix-org/matrix-appservice-irc/issues/1523))

## Deprecations and Removals

- The bridge will no longer treat invites without a `is_direct: true` as DM invites (and will henceforth reject group room invites). This may break some Matrix


  clients that do not supply this metadata when creating a room. ( [#1506](https://github.com/matrix-org/matrix-appservice-irc/issues/1506))
- **Minimum required Node version is now 14**. Users on Node 12 are advised to update to newer versions. ( [#1515](https://github.com/matrix-org/matrix-appservice-irc/issues/1515))

## Internal Changes

- Check changelog.d entries in CI. ( [#1527](https://github.com/matrix-org/matrix-appservice-irc/issues/1527))
- Update various packages that were out of date. ( [#1530](https://github.com/matrix-org/matrix-appservice-irc/issues/1530))

---

# etherpad 1.8.17

> published at 2022-02-27 [source](https://github.com/ether/etherpad-lite/releases/tag/1.8.17)

### Security fixes

- Fixed a vunlerability in the `CHANGESET_REQ` message handler that allowed a user with any access to read any pad if the pad ID is known.

### Notable enhancements and fixes

- Fixed a bug that caused all pad edit messages received at the server to go through a single queue. Now there is a separate queue per pad as intended, which should reduce message processing latency when many pads are active at the same time.

---

# honoroit v0.9.5

> published at 2022-02-24 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.5)

- changed cache algorithm to LRU
- added cache size config var
- deeper sentry integration

---

# mjolnir v1.3.2

> published at 2022-02-23 [source](https://github.com/matrix-org/mjolnir/releases/tag/v1.3.2)

## Changelog

**Bot**:

- Added experimental protection to take action on reports from trusted users.
- Fixed missing CORS headers for Report API responses (thanks to [@maranda](https://github.com/maranda)).
- Batch updates from watched lists together so that changes to server bans will result in less server ACL events being sent to protected rooms.
- Legacy rules will be checked for and removed when unbanning (thanks to [@heftig](https://github.com/heftig) for reporting [#220](https://github.com/matrix-org/mjolnir/issues/220))

---

# draupnir v1.3.2

> published at 2022-02-23 [source](https://github.com/Gnuxie/Draupnir/releases/tag/v1.3.2)

v1.3.2

---

# registration-bot v1.1.0

> published at 2022-02-22 [source](https://github.com/moan0s/matrix-registration-bot/releases/tag/v1.1.0)

# Releasing version 1.1.0

This release mainly aims to make this project docker ready and ease configuration.

**Changelog**

- Added command -line option `--config` to specify a configuration file to use
- Add a config class that can deal with configuration via environment variables

---

# ansible 5.4.0

> published at 2022-02-22 [source](https://pypi.org/project/ansible/5.4.0/)

> no description provided

---



---

# rageshake v1.6

> published at 2022-02-22 [source](https://github.com/matrix-org/rageshake/releases/tag/v1.6)

# 1.6 (2022-02-22)

## Features

- Provide ?format=tar.gz option on directory listings to download tarball. ( [#53](https://github.com/matrix-org/rageshake/issues/53))

---

# prometheus-blackbox-exporter 0.20.0-rc.0 / 2022-02-17

> published at 2022-02-17 [source](https://github.com/prometheus/blackbox_exporter/releases/tag/v0.20.0-rc.0)

- \[FEATURE\] Add hostname parameter. [#823](https://github.com/prometheus/blackbox_exporter/pull/823)
- \[FEATURE\] Add support for grpc health check. [#835](https://github.com/prometheus/blackbox_exporter/pull/835)
- \[ENHANCEMENT\] Change default user agent. [#557](https://github.com/prometheus/blackbox_exporter/pull/557)
- \[ENHANCEMENT\] Add control of recursion desired flag for DNS probes. [#859](https://github.com/prometheus/blackbox_exporter/pull/859)
- \[ENHANCEMENT\] Add body\_size\_limit option to http module. [#836](https://github.com/prometheus/blackbox_exporter/pull/836)
- \[ENHANCEMENT\] Delay init of http phase values. [#865](https://github.com/prometheus/blackbox_exporter/pull/865)
- \[BUGFIX\] Fix IP hash. [#863](https://github.com/prometheus/blackbox_exporter/pull/863)

---

# synapse-admin 0.8.5

> published at 2022-02-17 [source](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/0.8.5)

No content.

---

# mautrix-signal v0.2.3

> published at 2022-02-17 [source](https://github.com/mautrix/signal/releases/tag/v0.2.3)

Target signald version: [v0.17.0](https://gitlab.com/signald/signald/-/releases/0.17.0)

**N.B.** This will be the last release to support Python 3.7. Future versions will require Python 3.8 or higher. In general, the mautrix bridges will only support the lowest Python version in the latest Debian or Ubuntu LTS.

### Added

- New v2 link API to provide immediate feedback after the QR code is scanned.

### Improved

- Added automatic retrying for failed Matrix->Signal reactions.
- Commands using phone numbers will now try to resolve the UUID first (especially useful for `pm` so the portal is created with the correct ghost immediately)
- Improved signald socket handling to catch weird errors and reconnect.

### Fixed

- Fixed catching errors when connecting to Signal (e.g. if the account was deleted from signald's database, but not the bridge's).
- Fixed handling message deletions from Signal.
- Fixed race condition in incoming message deduplication.

---

# registration-bot v1.0.0

> published at 2022-02-17 [source](https://github.com/moan0s/matrix-registration-bot/releases/tag/v1.0.0)

# Releasing version 1.0.0 🎉

This release is mostly a stability/bugfix release. The big change in the version number tries to communicate the stability of the project which now feels stable enough to be used in production.

There are no breaking changes. Updates to this version can be done via `pip install matrix-registration-bot==1.0.0`

Changelog:

- Add password based login: This should not only make it easier to get started but also helps users with old versions of simple-matrix-bot-lib [#2](https://github.com/moan0s/matrix-registration-bot/issues/2)
- Add logging: Helps debug and provides insight for admins in case of misuse
- Improved Feedback to user
- Help now encourages users to join a matrix room to ask questions, not to message [@moan0s](https://github.com/moan0s) directly

**Full Changelog**: [`v0.0.2...v1.0.0`](https://github.com/moan0s/matrix-registration-bot/compare/v0.0.2...v1.0.0)

---

# mautrix-telegram v0.11.2

> published at 2022-02-14 [source](https://github.com/mautrix/telegram/releases/tag/v0.11.2)

**N.B.** This will be the last release to support Python 3.7. Future versions will require Python 3.8 or higher. In general, the mautrix bridges will only support the lowest Python version in the latest Debian or Ubuntu LTS.

### Added

- Added simple fallback message for live location and venue messages from Telegram.
- Added support for `t.me/+code` style invite links in `!tg join`.
- Added support for showing channel profile when users send messages as a channel.
- Added "user joined Telegram" message when Telegram auto-creates a DM chat for a new user.

### Improved

- Added option for adding a random prefix to relayed user displaynames to help distinguish them on the Telegram side.
- Improved syncing profile info to room info when using encryption and/or the `private_chat_profile_meta` config option.
- Removed legacy `community_id` config option.

### Fixed

- Fixed newlines disappearing when bridging channel messages with signatures.
- Fixed login throwing an error if a previous login code expired.
- Fixed bug in v0.11.0 that broke `!tg create`.

---

# cactus-comments-2 v0.12.0

> published at 2022-02-12 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.12.0)

# Cactus Comments Web Client v0.12.0

- Re-structure login form to more clearly separate matrix.to links from direct login.
- Require user id (e.g. `@alice:example.com`) instead of username. Only show homeserver url if .well-known lookup fails.
- Add top-right "X" button to close login modal.
- Login modal now closes when you press outside it.
- Fix some CSS bugs related to positioning the login form in viewport center.
- Make `LoginForm` a stateful component.

**IPFS CID**QmQpbFFnivpkLukkU1x5kPck5CvLtAkLpcZevAWQLoXFUy**JS URL**[https://gateway.pinata.cloud/ipfs/QmQpbFFnivpkLukkU1x5kPck5CvLtAkLpcZevAWQLoXFUy/v0.12.0/cactus.js](https://gateway.pinata.cloud/ipfs/QmQpbFFnivpkLukkU1x5kPck5CvLtAkLpcZevAWQLoXFUy/v0.12.0/cactus.js)**CSS URL**[https://gateway.pinata.cloud/ipfs/QmQpbFFnivpkLukkU1x5kPck5CvLtAkLpcZevAWQLoXFUy/v0.12.0/style.css](https://gateway.pinata.cloud/ipfs/QmQpbFFnivpkLukkU1x5kPck5CvLtAkLpcZevAWQLoXFUy/v0.12.0/style.css)

---

# rageshake v1.5

> published at 2022-02-08 [source](https://github.com/matrix-org/rageshake/releases/tag/v1.5)

# 1.5 (2022-02-08)

## Features

- Allow upload of Files with a .json postfix. ( [#52](https://github.com/matrix-org/rageshake/issues/52))

---

# email2matrix 1.0.3

> published at 2022-02-05 [source](https://github.com/devture/email2matrix/releases/tag/1.0.3)

Release 1.0.3

---

# email2matrix 1.0.2

> published at 2022-02-05 [source](https://github.com/devture/email2matrix/releases/tag/1.0.2)

Release 1.0.2

---

# heisenbridge v1.10.1

> published at 2022-02-04 [source](https://github.com/hifi/heisenbridge/releases/tag/v1.10.1)

Add twitch.tv/membership capability

Signed-off-by: Filip Kszczot <filip@kszczot.pl>

---

# rageshake v1.4

> published at 2022-02-03 [source](https://github.com/matrix-org/rageshake/releases/tag/v1.4)

# 1.4 (2022-02-01)

## Features

- Allow forwarding of a request to a webhook endpoint. ( [#50](https://github.com/matrix-org/rageshake/issues/50))

---

# ansible 5.3.0

> published at 2022-02-01 [source](https://pypi.org/project/ansible/5.3.0/)

> no description provided

---



---

# mailer 4.95-r0-2

> published at 2022-02-01 [source](https://github.com/devture/exim-relay/releases/tag/4.95-r0-2)

Ensure /etc/exim/exim.conf permissions are what Exim will be happy with

---

# mailer 4.95-r0-1: Add Gitlab CI/CD integration

> published at 2022-02-01 [source](https://github.com/devture/exim-relay/releases/tag/4.95-r0-1)

Supersedes [#16](https://github.com/devture/exim-relay/pull/16)

---

# corporal 2.2.3

> published at 2022-01-31 [source](https://github.com/devture/matrix-corporal/releases/tag/2.2.3)

Release 2.2.3

---

# registration-bot v0.0.2

> published at 2022-01-31 [source](https://github.com/moan0s/matrix-registration-bot/releases/tag/v0.0.2)

# Changelog

**No action needed**

This release adds support for [allow/blocklists](https://simple-matrix-bot-lib.readthedocs.io/en/latest/manual.html#allowlist) that regulate which users can interact with the bot (for restricted commands). The file to control the behavior will be created automatically.

---

# honoroit v0.9.4

> published at 2022-01-29 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.4)

Added in-memory cache for relations (both threads and reply-to fallback).
That change significantly decreases amount of requests to resolve thread relation

---

# mautrix-facebook v0.3.3

> published at 2022-01-29 [source](https://github.com/mautrix/facebook/releases/tag/v0.3.3)

- Added relay mode.
- Added automatic conversion of voice messages in both directions (mp4/aac to facebook and ogg/opus to Matrix).
- Added external URLs to unsupported attachment messages and story reply messages.
- Added support for typing notifications in both directions.
- Added Python 3.10 support.
- Removed legacy community features.
- Changed example config to disable temporary disconnect notices by default.
- Updated Docker image to Alpine 3.15.
- Formatted all code using [black](https://github.com/psf/black) and [isort](https://github.com/PyCQA/isort).

---

# nginx-proxy release-1.21.6

> published at 2022-01-25 [source](https://github.com/nginx/nginx/releases/tag/release-1.21.6)

nginx-1.21.6-RELEASE

---

# rageshake v1.3

> published at 2022-01-25 [source](https://github.com/matrix-org/rageshake/releases/tag/v1.3)

# 1.3 (2022-01-25)

## Features

- Add support for creating GitLab issues. Contributed by [@tulir](https://github.com/tulir). ( [#37](https://github.com/matrix-org/rageshake/issues/37))
- Support element-android submitting logs with .gz suffix. ( [#40](https://github.com/matrix-org/rageshake/issues/40))

## Bugfixes

- Prevent timestamp collisions when reports are submitted within 1 second of each other. ( [#39](https://github.com/matrix-org/rageshake/issues/39))

## Internal Changes

- Update minimum Go version to 1.16. ( [#37](https://github.com/matrix-org/rageshake/issues/37), [#42](https://github.com/matrix-org/rageshake/issues/42))
- Add documentation on the types and formats of files submitted to the rageshake server. ( [#44](https://github.com/matrix-org/rageshake/issues/44))
- Build and push a multi-arch Docker image on the GitHub Container Registry. ( [#47](https://github.com/matrix-org/rageshake/issues/47))
- Add a /health endpoint that always replies with a 200 OK. ( [#48](https://github.com/matrix-org/rageshake/issues/48))

---

# miniflux Miniflux 2.0.35

> published at 2022-01-22 [source](https://github.com/miniflux/v2/releases/tag/2.0.35)

- Set `read-all` permission to `GITHUB_TOKEN` for GitHub Actions
- Pin `jshint` version in linter job
- Fix incorrect conversion between integer types
- Add new GitHub Actions workflows: CodeQL and Scorecards analysis
- Handle Atom feeds with space around CDATA
- Bump `github.com/tdewolff/minify/v2` from 2.9.22 to 2.9.28
- Add Documentation directive to Systemd service
- Do not reset `touch-item` if successfully swiped
- Add support for multiple authors in Atom feeds
- Omit `User-Agent` header in image proxy to avoid being blocked
- Use custom feed user agent to fetch website icon
- Make default Invidious instance configurable
- Add new rewrite rule `add_youtube_video_from_id` to add Youtube videos in Quanta articles
- Add scrape and rewrite rules for `quantamagazine.org`
- Expose entry unshare link in the entry and list views
- Add Google Reader API implementation (experimental)
- Add `Content-Security-Policy` header to feed icon and image proxy endpoints

  - SVG images could contain Javascript. This CSP blocks inline script.
  - Feed icons are served using `<img>` tag and Javascript is not interpreted.
- Add Finnish translation
- Add scraper rule for `ikiwiki.iki.fi`
- Remove `SystemCallFilter` from `miniflux.service`
- Fix minor typo in French translation

---

# honoroit v0.9.3

> published at 2022-01-19 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.3)

- Bugfix commands parsing in reply-to mode
- Allow prefixes of threads topics

---

# prometheus_postgres_exporter 0.10.1 / 2022-01-14

> published at 2022-01-19 [source](https://github.com/prometheus-community/postgres_exporter/releases/tag/v0.10.1)

- \[BUGFIX\] Fix broken log-level for values other than debug. [#560](https://github.com/prometheus-community/postgres_exporter/pull/560)

---

# honoroit v0.9.2

> published at 2022-01-16 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.2)

added fallback reply-to mode

---

# mautrix-instagram v0.1.2

> published at 2022-01-15 [source](https://github.com/mautrix/instagram/releases/tag/v0.1.2)

- Added relay mode (see [docs](https://docs.mau.fi/bridges/general/relay-mode.html) for more info).
- Added notices for unsupported incoming message types.
- Added support for more message types:
  - "Tagged in post" messages
  - Reel clip shares
  - Profile shares
- Updated Docker image to Alpine 3.15.
- Formatted all code using [black](https://github.com/psf/black) and [isort](https://github.com/PyCQA/isort).

---

# mautrix-twitter v0.1.3

> published at 2022-01-15 [source](https://github.com/mautrix/twitter/releases/tag/v0.1.3)

Fix setup.py classifiers

---

# mautrix-signal v0.2.2

> published at 2022-01-15 [source](https://github.com/mautrix/signal/releases/tag/v0.2.2)

Target signald version: [v0.16.1](https://gitlab.com/signald/signald/-/releases/0.16.1)

### Added

- Support for disappearing messages.
  - Disabled by default in group chats, as there's no way to delete messages from the view of a single Matrix user. For single-user bridges, it's safe to enable the `enable_disappearing_messages_in_groups` config option.
- Notifications about incoming calls.
- Support for voice messages with [MSC3245](https://github.com/matrix-org/matrix-doc/pull/3245).
- Support for incoming contact share messages.
- Support for long text messages from Signal.

### Improved

- Formatted all code using [black](https://github.com/psf/black) and [isort](https://github.com/PyCQA/isort).
- Moved most relay mode code to mautrix-python to be shared with other bridges.
- The bridge will now queue messages temporarily if signald is down while sending.
- Removed legacy community-related features.
- Updated remaining things to use signald's v1 API.

### Fixed

- Fixed empty DM rooms being unnecessarily created when receiving non-bridgeable events (e.g. profile key updates).
- Fixed duplicate rooms being created in certain cases due to the room mapping cache not working.
- Fixed replies to attachments not rendering on Signal iOS properly.

---

# heisenbridge v1.10.0

> published at 2022-01-14 [source](https://github.com/hifi/heisenbridge/releases/tag/v1.10.0)

- RELAYMSG sending support 🚀
- Allow forwarding all IRC noise to network room
- Support ZNC self-message caps ✅
- Support CHGHOST caps (prevents leave+join on host change) ✅
- Fix owner auto-registration (regression) 🐛
- Upgrade to Mautrix 0.14 ⬆️

Not a breaking change but the caps support will cause connections to networks that ignore CAP requests take a few seconds longer unless you remove all the default caps for said network and it will never try requesting them again.

Mautrix 0.14 upgrade bumps the minimum version as well so packages beware.

---

# ansible 5.2.0

> published at 2022-01-12 [source](https://pypi.org/project/ansible/5.2.0/)

> no description provided

---



---

# mautrix-telegram v0.11.1

> published at 2022-01-10 [source](https://github.com/mautrix/telegram/releases/tag/v0.11.1)

- Added support for message reactions.
- Added support for spoiler text.
- Improved support for voice messages.
- Improved color of blue text from Telegram to be more readable on dark themes.
- Fixed syncing contacts throwing an error for new accounts.
- Fixed migrating pre-v0.11 legacy databases if the database schema had been corrupted (e.g. by using 3rd party tools for SQLite -> Postgres migration).
- Fixed converting animated stickers to webm with >33 FPS.
- Fixed a bug in v0.11.0 that broke mentioning users in groups (thanks to [@dfuchss](https://github.com/dfuchss) in [#724](https://github.com/mautrix/telegram/pull/724)).

---

# honoroit v0.9.1

> published at 2022-01-09 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.1)

Multi-arch support: arm32, arm64, amd64

---

# honoroit v0.9.0

> published at 2022-01-06 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.0)

Functionality considered final for 1.0.0 release, but there is no unit tests yet, so consider v0.9.0 as preview/unstable release candidate before actual v1.0.0.

---

# appservice-webhooks ARM64 compatiblity

> published at 2022-01-03 [source](https://github.com/redoonetworks/matrix-appservice-webhooks/releases/tag/v1.0.3-01)

Make build compatible to ARM64

---

# appservice-webhooks v1.0.4-beta.1: Merge pull request #6 from redoonetworks/arm-compatibility

> published at 2022-01-03 [source](https://github.com/redoonetworks/matrix-appservice-webhooks/releases/tag/v1.0.4-beta.1)

Update Dockerfile with npm self upgrade

---

# cactus-comments-2 v0.11.0

> published at 2022-01-02 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.11.0)

# Cactus Comments Web Client v0.11.0

- Relicense from GPLv3 to LGPLv3.
- Rewrite large parts of the stylesheet to use flexbox (Thanks to asdfkcdk for [!8](/cactus-comments/cactus-client/-/merge_requests/8 "Rewrite CSS using Flexbox")).
- Introduce CSS variables to the stylesheet (Thanks to asdfkcdk for [!9](/cactus-comments/cactus-client/-/merge_requests/9 "Add Dark Mode")).
- `.dark` and `.light` CSS classes with default values for dark/light mode (Thanks to asdfkcdk for [!9](/cactus-comments/cactus-client/-/merge_requests/9 "Add Dark Mode")).
- Bugfix: "View More" button no longer blinks when auto-refreshing short comment sections.

**IPFS CID**Qmc3ngaLj3WpCVM8Fshj1Mzmk9j1nQeD7fmyqP46f1sgP2**JS URL**[https://gateway.pinata.cloud/ipfs/Qmc3ngaLj3WpCVM8Fshj1Mzmk9j1nQeD7fmyqP46f1sgP2/v0.11.0/cactus.js](https://gateway.pinata.cloud/ipfs/Qmc3ngaLj3WpCVM8Fshj1Mzmk9j1nQeD7fmyqP46f1sgP2/v0.11.0/cactus.js)**CSS URL**[https://gateway.pinata.cloud/ipfs/Qmc3ngaLj3WpCVM8Fshj1Mzmk9j1nQeD7fmyqP46f1sgP2/v0.11.0/style.css](https://gateway.pinata.cloud/ipfs/Qmc3ngaLj3WpCVM8Fshj1Mzmk9j1nQeD7fmyqP46f1sgP2/v0.11.0/style.css)

---

# etherpad 1.8.16

> published at 2022-01-01 [source](https://github.com/ether/etherpad-lite/releases/tag/1.8.16)

### Security fixes

This release includes fixes for [GHSA-w3g3-qf3g-2mqc](https://github.com/ether/etherpad-lite/security/advisories/GHSA-w3g3-qf3g-2mqc) ( [CVE-2021-43802](https://www.cve.org/CVERecord?id=CVE-2021-43802)).

If you cannot upgrade to v1.8.16 for some reason, you are encouraged to try cherry-picking the fixes to the version you are running:

```
git cherry-pick b7065eb9a0ec..77bcb507b30e
```

- Maliciously crafted `.etherpad` files can no longer overwrite arbitrary non-pad database records when imported.
- Imported `.etherpad` files are now subject to numerous consistency checks before any records are written to the database. This should help avoid denial-of-service attacks via imports of malformed `.etherpad` files.

### Notable enhancements and fixes

- Fixed several `.etherpad` import bugs.
- Improved support for large `.etherpad` imports.

---

# heisenbridge v1.9.0

> published at 2021-12-29 [source](https://github.com/hifi/heisenbridge/releases/tag/v1.9.0)

- Spaces support 🌃
- Proper SASL external with CertFP with mechanism override option (see notes)
- Disconnect and cleanup from networks that have no rooms open ♻️
- Reply (and reject) DM requests to ghosts with QUERY command ↪️
- Try to keep IRC users in the room at all costs if they are on the IRC channel
- Prevent accidental namespace changes to cause mayhem
- Conduit support was broken in 1.8.x but fixed again in 1.9.0, sorry

**Upgrade warning**: CertFP SASL has been updated to do SASL external flow by default. If you are upgrading and have used CertFP with OFTC you need to run `SASL --mechanism=none` for it to connect again.

---

# nginx-proxy release-1.21.5

> published at 2021-12-28 [source](https://github.com/nginx/nginx/releases/tag/release-1.21.5)

nginx-1.21.5-RELEASE

---

# mautrix-telegram v0.11.0

> published at 2021-12-28 [source](https://github.com/mautrix/telegram/releases/tag/v0.11.0)

- Switched from SQLAlchemy to asyncpg/aiosqlite.
  - The default database is now Postgres. If using SQLite, make sure you install the `sqlite` [optional dependency](https://docs.mau.fi/bridges/python/optional-dependencies.html).
  - **Alembic is no longer used**, schema migrations happen automatically on startup.
  - **The automatic database migration requires you to be on the latest legacy database version.** If you were running any v0.10.x version, you should be on the latest version already. Otherwise, update to v0.10.2 first, upgrade the database with `alembic`, then upgrade to v0.11.0 (or higher).
- Added support for contact messages.
- Added support for Telegram sponsored messages in channels.
  - Only applies to broadcast channels with 1000+ members (as per [https://t.me/durov/172](https://t.me/durov/172)).
  - Only applies if you're using puppeting with a normal user account, because bots can't get sponsored messages.
- Fixed non-supergroup member sync incorrectly kicking one user from the Matrix side if there was no limit on the number of members to sync (broke in v0.10.2).
- Updated animated sticker conversion to support [lottieconverter r0.2](https://github.com/sot-tech/LottieConverter/releases/tag/r0.2) (thanks to [@sot-tech](https://github.com/sot-tech) in [#694](https://github.com/mautrix/telegram/pull/694)).
- Updated Docker image to Alpine 3.15.
- Formatted all code using [black](https://github.com/psf/black) and [isort](https://github.com/PyCQA/isort).

---

# appservice-slack 1.10.0 (2021-12-22)

> published at 2021-12-22 [source](https://github.com/matrix-org/matrix-appservice-slack/releases/tag/1.10.0)

## Bugfixes

- Improve reliability of Matrix users being bridged with correct displayname and avatar ( [#628](https://github.com/matrix-org/matrix-appservice-slack/issues/628))
- Fix an issue where Slack users sometimes have stale displaynames ( [#631](https://github.com/matrix-org/matrix-appservice-slack/issues/631))
- Fix inverted check causing Slack messages to not be relayed to Matrix. Thanks to [@ewilderj](https://github.com/ewilderj) ( [#648](https://github.com/matrix-org/matrix-appservice-slack/issues/648))

## Internal Changes

- Use the MembershipQueue for handling joins and leaves in the TeamSyncer. ( [#633](https://github.com/matrix-org/matrix-appservice-slack/issues/633))
- Docker images now use Node 16 ( [#639](https://github.com/matrix-org/matrix-appservice-slack/issues/639))
- Update dependencies, remove unused winston dependencies. ( [#640](https://github.com/matrix-org/matrix-appservice-slack/issues/640))
- Switch to using Debian as a base for Docker images. ( [#645](https://github.com/matrix-org/matrix-appservice-slack/issues/645))
- Fix a regression in Node 16+ environments where the bridge may occasionally crash when a message wasn't handled successfully. ( [#646](https://github.com/matrix-org/matrix-appservice-slack/issues/646))

---

# ansible 5.1.0

> published at 2021-12-21 [source](https://pypi.org/project/ansible/5.1.0/)

> no description provided

---



---

# mautrix-googlechat v0.3.0

> published at 2021-12-18 [source](https://github.com/mautrix/googlechat/releases/tag/v0.3.0)

Initial stable-ish Google Chat release

---

# mautrix-hangouts v0.3.0

> published at 2021-12-18 [source](https://github.com/mautrix/googlechat/releases/tag/v0.3.0)

Initial stable-ish Google Chat release

---

# synapse-admin 0.8.4

> published at 2021-12-17 [source](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/0.8.4)

No content.

---

# miniflux Miniflux 2.0.34

> published at 2021-12-17 [source](https://github.com/miniflux/v2/releases/tag/2.0.34)

- Add rewrite rule for comics website [http://monkeyuser.com](http://monkeyuser.com)
- Add `<head>` tag to OPML export
- Tighten Systemd sandboxing and update comments in `miniflux.service`
- Add `RuntimeDirectory` to Systemd service
- Order disabled feeds at the end of the list
- Add support for theme color based on preferred color scheme of OS
- Bump `github.com/lib/pq` from 1.10.3 to 1.10.4
- Bump `github.com/PuerkitoBio/goquery` from 1.7.1 to 1.8.0
- Fix typos in `model/icon.go`
- Add `data-srcset` support to `add_dynamic_image rewrite` rewrite rule
- Fix Docker Compose example files compatibility to v3
- Added the `role="article"` to `<article>` elements for better accessibility with screen readers
- Redact secrets shown on the about page
- Handle `srcset` images with no space after comma
- Hide the logout link when using auth proxy
- Fix wrong CSS variable
- Change `-config-dump` command to use `KEY=VALUE` format

---

# sygnal v0.11.0

> published at 2021-12-15 [source](https://github.com/matrix-org/sygnal/releases/tag/v0.11.0)

## Bugfixes

- Fix a bug introduced in Sygnal 0.5.0 where pushkin names would match substrings of app IDs and treat dots as wildcards. ( [#269](https://github.com/matrix-org/sygnal/issues/269))
- Fix a bug introduced in Sygnal 0.5.0 where GCM pushes would always fail when configured to handle an app ID glob. ( [#270](https://github.com/matrix-org/sygnal/issues/270))
- Treat more APNs errors as permanent rejections. ( [#280](https://github.com/matrix-org/sygnal/issues/280))
- Fix a bug introduced in Sygnal 0.9.1 where web pushkeys with missing endpoints would cause an error. ( [#288](https://github.com/matrix-org/sygnal/issues/288))

## Improved Documentation

- Document that the `topic` is most commonly the Bundle Identifier for the iOS application. ( [#284](https://github.com/matrix-org/sygnal/issues/284))
- Add troubleshooting documentation for when you receive 'Could not deserialize key data' when using APNs with key files. ( [#286](https://github.com/matrix-org/sygnal/issues/286))

## Internal Changes

- Fix the changelog CI check when running on a fork of the Sygnal repository, rather than a branch. ( [#254](https://github.com/matrix-org/sygnal/issues/254))
- Configure [@matrix-org/synapse-core](https://github.com/orgs/matrix-org/teams/synapse-core) to be the code owner for the repository. ( [#259](https://github.com/matrix-org/sygnal/issues/259))
- Improve static type checking. ( [#264](https://github.com/matrix-org/sygnal/issues/264))
- Use absolute imports for consistency. ( [#265](https://github.com/matrix-org/sygnal/issues/265))
- Remove explicit inheritance from `object` that was left over from Python 2. ( [#266](https://github.com/matrix-org/sygnal/issues/266))
- Use Python 3-style super calls. ( [#267](https://github.com/matrix-org/sygnal/issues/267))
- Add type hints to most of the code. ( [#271](https://github.com/matrix-org/sygnal/issues/271), [#273](https://github.com/matrix-org/sygnal/issues/273), [#274](https://github.com/matrix-org/sygnal/issues/274), [#275](https://github.com/matrix-org/sygnal/issues/275), [#276](https://github.com/matrix-org/sygnal/issues/276))
- Convert the README to use markdown rather than reStructuredText for consistency and familiarity. ( [#278](https://github.com/matrix-org/sygnal/issues/278))
- Move `glob_to_regex` to `matrix-python-common`. ( [#281](https://github.com/matrix-org/sygnal/issues/281))
- Add `opentracing-types` to the dev dependencies. ( [#287](https://github.com/matrix-org/sygnal/issues/287))
- Add missing dependencies to `setup.py`. ( [#290](https://github.com/matrix-org/sygnal/issues/290))

---

# ansible 4.10.0

> published at 2021-12-14 [source](https://pypi.org/project/ansible/4.10.0/)

> no description provided

---



---

# com.devture.ansible.role.traefik_certs_dumper v2.8.1

> published at 2021-12-12 [source](https://github.com/ldez/traefik-certs-dumper/releases/tag/v2.8.1)

## Changelog

---

# com.devture.ansible.role.traefik_certs_dumper v2.8.0

> published at 2021-12-11 [source](https://github.com/ldez/traefik-certs-dumper/releases/tag/v2.8.0)

chore: update seihon

---

# prometheus_node_exporter 1.3.1 / 2021-12-01

> published at 2021-12-05 [source](https://github.com/prometheus/node_exporter/releases/tag/v1.3.1)

- \[BUGFIX\] Handle nil CPU thermal power status on M1 [#2218](https://github.com/prometheus/node_exporter/issues/2218)
- \[BUGFIX\] bsd: Ignore filesystems flagged as MNT\_IGNORE. [#2227](https://github.com/prometheus/node_exporter/pull/2227)
- \[BUGFIX\] Sanitize UTF-8 in dmi collector [#2229](https://github.com/prometheus/node_exporter/pull/2229)

---

# ansible 5.0.1

> published at 2021-12-02 [source](https://pypi.org/project/ansible/5.0.1/)

> no description provided

---



---

# corporal 2.2.2

> published at 2021-12-01 [source](https://github.com/devture/matrix-corporal/releases/tag/2.2.2)

Release 2.2.2

---

# mautrix-signal v0.2.1

> published at 2021-11-28 [source](https://github.com/mautrix/signal/releases/tag/v0.2.1)

Target signald version: [v0.15.0](https://gitlab.com/signald/signald/-/releases/0.15.0)

### Added

- Support for Matrix->Signal redactions.
- Error messages to Matrix when sending to Signal fails.
- Custom flag to invite events that will be auto-accepted with double puppeting.
- Command to get group invite link.
- Support for custom bridge bot welcome messages (thanks to [@justinbot](https://github.com/justinbot) in [#146](https://github.com/mautrix/signal/pull/146)).
- Option to disable federation in portal rooms.
- Option to prevent users from registering the bridge as their primary device (thanks to [@tadzik](https://github.com/tadzik) in [#153](https://github.com/mautrix/signal/pull/153)).
- Extremely experimental support for SQLite. It's probably broken in some cases, so don't use it.

### Improved

- Increased line length limit for signald socket (was causing the connection to fail when there was too much data going through).
- Improved Signal disconnection detection (mostly affects prometheus metrics).
- Updated provisioning API `/link/wait` endpoint to return HTTP 400 if signald loses connection to Signal.

### Fixed

- Fixed bugs with automatic migration of Matrix ghosts from phone number to UUID format.
- Fixed handling empty Signal avatar files.

---

# mailer 4.95-r0

> published at 2021-11-25 [source](https://github.com/devture/exim-relay/releases/tag/4.95-r0)

Upgrade Alpine base (3.14.3 -> 3.15.0)

---

# ansible 4.9.0

> published at 2021-11-24 [source](https://pypi.org/project/ansible/4.9.0/)

> no description provided

---



---

# ansible 5.0.0rc1

> published at 2021-11-23 [source](https://pypi.org/project/ansible/5.0.0rc1/)

> no description provided

---



---

# etherpad 1.8.15

> published at 2021-11-21 [source](https://github.com/ether/etherpad-lite/releases/tag/1.8.15)

### Security fixes

- Fixed leak of the writable pad ID when exporting from the pad's read-only ID. This only matters if you treat the writeable pad IDs as secret (e.g., you are not using [ep\_padlist2](https://www.npmjs.com/package/ep_padlist2)) and you share the pad's read-only ID with untrusted users. Instead of treating writeable pad IDs as secret, you are encouraged to take advantage of Etherpad's authentication and authorization mechanisms (e.g., use [ep\_openid\_connect](https://www.npmjs.com/package/ep_openid_connect) with [ep\_readonly\_guest](https://www.npmjs.com/package/ep_readonly_guest), or write your own [authentication](https://etherpad.org/doc/v1.8.14/#index_authenticate) and [authorization](https://etherpad.org/doc/v1.8.14/#index_authorize) plugins).
- Updated dependencies.

### Compatibility changes

- The `logconfig` setting is deprecated.

#### For plugin authors

- Etherpad now uses [jsdom](https://github.com/jsdom/jsdom) instead of [cheerio](https://cheerio.js.org/) for processing HTML imports. There are two consequences of this change:

  - `require('ep_etherpad-lite/node_modules/cheerio')` no longer works. To fix, your plugin should directly depend on `cheerio` and do `require('cheerio')`.
  - The `collectContentImage` hook's `node` context property is now an [`HTMLImageElement`](https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement) object rather than a Cheerio Node-like object, so the API is slightly different. See [citizenos/ep\_image\_upload#49](https://github.com/citizenos/ep_image_upload/pull/49) for an example fix.
- The `clientReady` server-side hook is deprecated; use the new `userJoin` hook instead.
- The `init_<pluginName>` server-side hooks are now run every time Etherpad starts up, not just the first time after the named plugin is installed.
- The `userLeave` server-side hook's context properties have changed:

  - `auth`: Deprecated.
  - `author`: Deprecated; use the new `authorId` property instead.
  - `readonly`: Deprecated; use the new `readOnly` property instead.
  - `rev`: Deprecated.
- Changes to the `src/static/js/Changeset.js` library:

  - `opIterator()`: The unused start index parameter has been removed, as has the unused `lastIndex()` method on the returned object.
  - `smartOpAssembler()`: The returned object's `appendOpWithText()` method is deprecated without a replacement available to plugins (if you need one, let us know and we can make the private `opsFromText()` function public).
  - Several functions that should have never been public are no longer exported: `applyZip()`, `assert()`, `clearOp()`, `cloneOp()`, `copyOp()`, `error()`, `followAttributes()`, `opString()`, `stringOp()`, `textLinesMutator()`, `toBaseTen()`, `toSplices()`.

### Notable enhancements and fixes

- Accessibility fix for JAWS screen readers.
- Fixed "clear authorship" error (see issue [#5128](https://github.com/ether/etherpad-lite/issues/5128)).
- Etherpad now considers square brackets to be valid URL characters.
- The server no longer crashes if an exception is thrown while processing a message from a client.
- The `useMonospaceFontGlobal` setting now works (thanks [@Lastpixl](https://github.com/Lastpixl)!).
- Chat improvements:
  - The message input field is now a text area, allowing multi-line messages (use shift-enter to insert a newline).
  - Whitespace in chat messages is now preserved.
- Docker improvements:
  - New `HEALTHCHECK` instruction (thanks [@Gared](https://github.com/Gared)!).
  - New `settings.json` variables: `DB_COLLECTION`, `DB_URL`, `SOCKETIO_MAX_HTTP_BUFFER_SIZE`, `DUMP_ON_UNCLEAN_EXIT` (thanks [@JustAnotherArchivist](https://github.com/JustAnotherArchivist)!).
  - `.ep_initialized` files are no longer created.
- Worked around a [Firefox Content Security Policy bug](https://bugzilla.mozilla.org/show_bug.cgi?id=1721296) that caused CSP failures when `'self'` was in the CSP header. See issue [#4975](https://github.com/ether/etherpad-lite/issues/4975) for details.
- UeberDB upgraded from v1.4.10 to v1.4.18. For details, see the [ueberDB changelog](https://github.com/ether/ueberDB/blob/master/CHANGELOG.md). Highlights:

  - The `postgrespool` driver was renamed to `postgres`, replacing the old driver of that name. If you used the old `postgres` driver, you may see an increase in the number of database connections.
  - For `postgres`, you can now set the `dbSettings` value in `settings.json` to a connection string (e.g., `"postgres://user:password@host/dbname"`) instead of an object.
  - For `mongodb`, the `dbName` setting was renamed to `database` (but `dbName` still works for backwards compatibility) and is now optional (if unset, the database name in `url` is used).
- `/admin/settings` now honors the `--settings` command-line argument.
- Fixed "Author _X_ tried to submit changes as author _Y_" detection.
- Error message display improvements.
- Simplified pad reload after importing an `.etherpad` file.

#### For plugin authors

- `clientVars` was added to the context for the `postAceInit` client-side hook. Plugins should use this instead of the `clientVars` global variable.
- New `userJoin` server-side hook.
- The `userLeave` server-side hook has a new `socket` context property.
- The `helper.aNewPad()` function (accessible to client-side tests) now accepts hook functions to inject when opening a pad. This can be used to test any new client-side hooks your plugin provides.
- Chat improvements:
  - The `chatNewMessage` client-side hook context has new properties:

    - `message`: Provides access to the raw message object so that plugins can see the original unprocessed message text and any added metadata.
    - `rendered`: Allows plugins to completely override how the message is rendered in the UI.
  - New `chatSendMessage` client-side hook that enables plugins to process the text before sending it to the server or augment the message object with custom metadata.
  - New `chatNewMessage` server-side hook to process new chat messages before they are saved to the database and relayed to users.
- Readability improvements to browser-side error stack traces.
- Added support for socket.io message acknowledgments.

---

# mailer 4.94.2-r0-5: Merge pull request #13 from sakkiii/patch-3

> published at 2021-11-20 [source](https://github.com/devture/exim-relay/releases/tag/4.94.2-r0-5)

Security updated - Alpine Linux 3.14.3

---

# corporal 2.2.1

> published at 2021-11-20 [source](https://github.com/devture/matrix-corporal/releases/tag/2.2.1)

Release 2.2.1

---

# corporal 2.2.0

> published at 2021-11-19 [source](https://github.com/devture/matrix-corporal/releases/tag/2.2.0)

Adds support for handling `v3` Client-Server API requests, instead of rejecting them as unknown.

Synapse v1.48.0 is meant to [add support](https://github.com/matrix-org/synapse/pull/11318) for v3 APIs (as per [Matrix Spec v1.1](https://matrix.org/blog/2021/11/09/matrix-v-1-1-release)).

We patched a what-would-become a security vulnerability related to this in matrix-corporal 2.1.5. See the changelog.

The matrix-corporal 2.2.0 release continues the v3 work by actually handling v3 requests (the same way r0 requests are handled).

---

# prometheus_node_exporter 1.3.0 / 2021-10-20

> published at 2021-11-18 [source](https://github.com/prometheus/node_exporter/releases/tag/v1.3.0)

NOTE: In order to support globs in the textfile collector path, filenames exposed by

`node_textfile_mtime_seconds` now contain the full path name.

- \[CHANGE\] Add path label to rapl collector [#2146](https://github.com/prometheus/node_exporter/pull/2146)
- \[CHANGE\] Exclude filesystems under /run/credentials [#2157](https://github.com/prometheus/node_exporter/pull/2157)
- \[CHANGE\] Add TCPTimeouts to netstat default filter [#2189](https://github.com/prometheus/node_exporter/pull/2189)
- \[FEATURE\] Add lnstat collector for metrics from /proc/net/stat/ [#1771](https://github.com/prometheus/node_exporter/pull/1771)
- \[FEATURE\] Add darwin powersupply collector [#1777](https://github.com/prometheus/node_exporter/pull/1777)
- \[FEATURE\] Add support for monitoring GPUs on Linux [#1998](https://github.com/prometheus/node_exporter/pull/1998)
- \[FEATURE\] Add Darwin thermal collector [#2032](https://github.com/prometheus/node_exporter/pull/2032)
- \[FEATURE\] Add os release collector [#2094](https://github.com/prometheus/node_exporter/pull/2094)
- \[FEATURE\] Add netdev.address-info collector [#2105](https://github.com/prometheus/node_exporter/pull/2105)
- \[FEATURE\] Add clocksource metrics to time collector [#2197](https://github.com/prometheus/node_exporter/pull/2197)
- \[ENHANCEMENT\] Support glob textfile collector directories [#1985](https://github.com/prometheus/node_exporter/pull/1985)
- \[ENHANCEMENT\] ethtool: Expose node\_ethtool\_info metric [#2080](https://github.com/prometheus/node_exporter/pull/2080)
- \[ENHANCEMENT\] Use include/exclude flags for ethtool filtering [#2165](https://github.com/prometheus/node_exporter/pull/2165)
- \[ENHANCEMENT\] Add flag to disable guest CPU metrics [#2123](https://github.com/prometheus/node_exporter/pull/2123)
- \[ENHANCEMENT\] Add DMI collector [#2131](https://github.com/prometheus/node_exporter/pull/2131)
- \[ENHANCEMENT\] Add threads metrics to processes collector [#2164](https://github.com/prometheus/node_exporter/pull/2164)
- \[ENHANCMMENT\] Reduce timer GC delays in the Linux filesystem collector [#2169](https://github.com/prometheus/node_exporter/pull/2169)
- \[ENHANCMMENT\] Add TCPTimeouts to netstat default filter [#2189](https://github.com/prometheus/node_exporter/pull/2189)
- \[ENHANCMMENT\] Use SysctlTimeval for boottime collector on BSD [#2208](https://github.com/prometheus/node_exporter/pull/2208)
- \[BUGFIX\] ethtool: Sanitize metric names [#2093](https://github.com/prometheus/node_exporter/pull/2093)
- \[BUGFIX\] Fix ethtool collector for multiple interfaces [#2126](https://github.com/prometheus/node_exporter/pull/2126)
- \[BUGFIX\] Fix possible panic on macOS [#2133](https://github.com/prometheus/node_exporter/pull/2133)
- \[BUGFIX\] Collect flag\_info and bug\_info only for one core [#2156](https://github.com/prometheus/node_exporter/pull/2156)
- \[BUGFIX\] Prevent duplicate ethtool metric names [#2187](https://github.com/prometheus/node_exporter/pull/2187)

---

# ansible 5.0.0b2

> published at 2021-11-16 [source](https://pypi.org/project/ansible/5.0.0b2/)

> no description provided

---



---

# nginx-proxy release-1.20.2

> published at 2021-11-16 [source](https://github.com/nginx/nginx/releases/tag/release-1.20.2)

nginx-1.20.2-RELEASE

---

# mautrix-facebook v0.3.2

> published at 2021-11-14 [source](https://github.com/mautrix/facebook/releases/tag/v0.3.2)

- (Re-)Added support for using SQLite as the bridge database.
- Added option to not use `http_proxy` env var for downloading files from Facebook CDN.
- Changed MQTT error handling to always refresh connection instead of giving up if it errors more than once within 2 minutes.
- Fixed `login-matrix` not allowing login from other servers.
- Fixed setting portal avatars.
- Fixed error when receiving a reply to an unknown message.

---

# mautrix-telegram v0.10.2

> published at 2021-11-13 [source](https://github.com/mautrix/telegram/releases/tag/v0.10.2)

### Deprecation unwarning

While switching away from SQLAlchemy is still planned for v0.11, SQLite is no longer deprecated and will still be supported in the future using aiosqlite.

### Added

- Added extensions when bridging unnamed files from Telegram.
- Added support for custom bridge bot welcome messages (thanks to [@justinbot](https://github.com/justinbot) in [#676](https://github.com/mautrix/telegram/pull/676)).

### Improved

- Improved handling authorization errors if the bridge was logged out remotely.
- Updated room syncer to use existing power levels to find appropriate levels for admins and normal users instead of hardcoding 50 and 0.
- Updated to Telegram API layer 133 to handle 64-bit user/chat/channel IDs.
- Stopped logging message contents when message handling failed (thanks to [@justinbot](https://github.com/justinbot) in [#681](https://github.com/mautrix/telegram/pull/681)).
- Removed Element iOS compatibility hack from non-sticker files.
- Made `max_initial_member_sync` work for non-supergroups too (thanks to [@tadzik](https://github.com/tadzik) in [#680](https://github.com/mautrix/telegram/pull/680)).
- SQLite is now supported for the crypto database. Pickle is no longer supported. If you were using pickle, the bridge will create a new e2ee session and store the data in SQLite this time.

### Fixed

- Fixed generating reply fallbacks to encrypted messages.
- Fixed chat sync failing if the member list contained banned users.

---

# ansible 5.0.0b1

> published at 2021-11-09 [source](https://pypi.org/project/ansible/5.0.0b1/)

> no description provided

---



---

# ansible 5.0.0a3

> published at 2021-11-02 [source](https://pypi.org/project/ansible/5.0.0a3/)

> no description provided

---



---

# nginx-proxy release-1.21.4

> published at 2021-11-02 [source](https://github.com/nginx/nginx/releases/tag/release-1.21.4)

nginx-1.21.4-RELEASE

---

# ansible 4.8.0

> published at 2021-11-02 [source](https://pypi.org/project/ansible/4.8.0/)

> no description provided

---



---

# beeper-linkedin v0.5.1: Version 0.5.1

> published at 2021-09-28 [source](https://github.com/beeper/linkedin/releases/tag/v0.5.1)

- Add support for shared feed posts.

---

# miniflux Miniflux 2.0.33

> published at 2021-09-26 [source](https://github.com/miniflux/v2/releases/tag/2.0.33)

- Build RPM and Debian package with PIE mode enabled
- Add CSS rule to hide `<template>` tag in old browsers
- Bump `github.com/tdewolff/minify/v2 from 2.9.21 to 2.9.22`
- Bump `github.com/lib/pq from 1.10.2 to 1.10.3`
- Remove `RequestURI()` hack
- Improve `zh_CN` translation
- Add ability to change entry sort order in the UI
- Add minor improvements in integration package
- Add Telegram integration
- Add rewrite rule to remove DOM elements
- Add proxy argument to `scraper.Fetch()`
- Add mime type `application/feed+json` to discover JSON Feed v1.1
- Update scraper rule for `theregister.com`
- Add Go 1.17 to GitHub Actions
- Display option to hide feed only when category is not already hidden
- Add option to hide feeds from the global Unread list

---

# appservice-webhooks v1.0.2-01

> published at 2021-09-21 [source](https://github.com/redoonetworks/matrix-appservice-webhooks/releases/tag/v1.0.2-01)

Finallz fix requests with emoji's

---

# mailer 4.94.2-r0-4: Merge pull request #12 from sakkiii/patch-2

> published at 2021-08-30 [source](https://github.com/devture/exim-relay/releases/tag/4.94.2-r0-4)

alpine: bump 3.14.2 for openssl security fixes

---

# synapse-admin 0.8.3

> published at 2021-08-26 [source](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/0.8.3)

Increment version

Change-Id: I48fb812632dc2e498ad267477809a16fc882cf4c

---

# mailer 4.94.2-r0-3: Merge pull request #11 from sakkiii/patch-2

> published at 2021-08-23 [source](https://github.com/devture/exim-relay/releases/tag/4.94.2-r0-3)

Update alpine to 3.14.1 to fix [CVE-2021-36159](https://github.com/advisories/GHSA-cqp5-89fm-vp89 "CVE-2021-36159")

---

# mautrix-instagram v0.1.1

> published at 2021-08-20 [source](https://github.com/mautrix/instagram/releases/tag/v0.1.1)

**N.B.** Docker images have moved from `dock.mau.dev/tulir/mautrix-instagram` to `dock.mau.dev/mautrix/instagram`. New versions are only available at the new path.

- Added retrying failed syncs when refreshing Instagram connection.
- Updated displayname handling to fall back to username if user has no displayname set.
- Updated Docker image to Alpine 3.14.
- Fixed handling some Instagram message types.

---

# beeper-linkedin v0.5.0: Version 0.5.0

> published at 2021-08-19 [source](https://github.com/beeper/linkedin/releases/tag/v0.5.0)

- Upgraded mautrix to `^0.10.3`
- Upgraded asyncpg to `>=0.23.0`
- Made the ruamel.yaml requirement less strict ( `^0.17.0`)
- Fixed a few errors with bridge state sending
- Implemented support for the manhole
- Add caching for user profile to improve speed of whoami calls
- Add flags to track whether name, avatar, and topic are set on the portal
- Fixed bug with initial setting of room avatars on DMs

---

# mautrix-telegram v0.10.1

> published at 2021-08-19 [source](https://github.com/mautrix/telegram/releases/tag/v0.10.1)

**N.B.** Docker images have moved from `dock.mau.dev/tulir/mautrix-telegram` to `dock.mau.dev/mautrix/telegram`. New versions are only available at the new path.

### Added

- Warning when bridging existing room if bridge bot doesn't have redaction permissions.
- Custom flag to invite events that will be auto-accepted using double puppeting.
- Custom flags for animated stickers (same as what gifs already had).

### Improved

- Updated to Telethon 1.22.
- Updated Docker image to Alpine 3.14.

### Fixed

- Fixed Bridging Matrix location messages with additional flags in `geo_uri`.
- Editing encrypted messages will no longer add an asterisk on Telegram ( [#623](https://github.com/mautrix/telegram/issues/623)).
- Matrix typing notifications won't be echoed back for double puppeted users anymore ( [#631](https://github.com/mautrix/telegram/issues/631)).
- `AuthKeyDuplicatedError` is now handled properly instead of making the user get stuck.
- Fixed `public_portals` setting not being respected on room creation.

---

# sygnal Sygnal 0.10.1 (2021-08-16)

> published at 2021-08-16 [source](https://github.com/matrix-org/sygnal/releases/tag/v0.10.1)

This release only makes changes to the way Docker images are built and released; it is otherwise identical to 0.10.0. Administrators who do not use Docker as their installation method have no need to upgrade from 0.10.0.

## Updates to the Docker image

- Fix the docker image build from failing due to `git` not being installed. This issue was introduced in v0.10.0. ( [#246](https://github.com/matrix-org/sygnal/issues/246))
- CI now checks that the Docker image still builds after the Dockerfile is modified. ( [#248](https://github.com/matrix-org/sygnal/issues/248))
- Automatically build the Docker image for each release and push it to Docker Hub using GitHub Actions. ( [#249](https://github.com/matrix-org/sygnal/issues/249))

## Internal Changes

- Add a lint script (scripts-dev/lint.sh) for developers. ( [#243](https://github.com/matrix-org/sygnal/issues/243))
- Add more comprehensive Newsfile (changelog fragment) checks in CI. ( [#250](https://github.com/matrix-org/sygnal/issues/250))

---

# sygnal Sygnal 0.10.0 (2021-08-09)

> published at 2021-08-09 [source](https://github.com/matrix-org/sygnal/releases/tag/v0.10.0)

## Database Removal

Sygnal is now stateless, and does not rely on a database of any kind.

You may remove your existing SQLite or PostgreSQL databases once you are satisfied that this release is working as intended.

Configuration changes are not necessary, as the `database` section will be ignored if present.

- Remove legacy database to ease horizontal scaling. Contributed by H. Shay. ( [#236](https://github.com/matrix-org/sygnal/issues/236))

## Improved Documentation

- Update CONTRIBUTING.md to recommend installing libpq-dev. Contributed by Tawanda Moyo. ( [#197](https://github.com/matrix-org/sygnal/issues/197))

## Internal Changes

- Improve static type checking. Contributed by Omar Mohamed. ( [#221](https://github.com/matrix-org/sygnal/issues/221), [#223](https://github.com/matrix-org/sygnal/issues/223), [#225](https://github.com/matrix-org/sygnal/issues/225), [#227](https://github.com/matrix-org/sygnal/issues/227))
- Update towncrier CI check to run against the new default branch name. ( [#226](https://github.com/matrix-org/sygnal/issues/226))
- Update black to 21.6b0. ( [#233](https://github.com/matrix-org/sygnal/issues/233))
- Fix type hint errors from new upstream Twisted release. ( [#239](https://github.com/matrix-org/sygnal/issues/239))
- Fixup GitHub Actions pipeline to always run tests on PRs. ( [#240](https://github.com/matrix-org/sygnal/issues/240))
- Add CI testing for old dependencies. ( [#242](https://github.com/matrix-org/sygnal/issues/242))

---

# mautrix-signal v0.2.0

> published at 2021-08-07 [source](https://github.com/mautrix/signal/releases/tag/v0.2.0)

Target signald version: [v0.14.1](https://gitlab.com/signald/signald/-/releases/0.14.1)

**N.B.** Docker images have moved from `dock.mau.dev/tulir/mautrix-signal` to `dock.mau.dev/mautrix/signal`. New versions are only available at the new path.

### Added

- Relay mode (see [docs](https://docs.mau.fi/bridges/python/signal/relay-mode.html))
- Added captcha parameter to help text of register command.
- Option to delete unknown accounts from signald when starting the bridge.

### Improved

- Contact info is now synced when other devices send contact list updates.
- Contact avatars will now be used if profile avatar isn't available and contact names are allowed.
- Linking a new device or registering now uses the `overwrite` param in signald, which means it will force-login even if there is an existing signald session with the same phone number.
- Updated Docker image to Alpine 3.14.

### Fixed

- Fixed Signal delivery receipts being handled as read receipts.
- Fixed logging out causing signald to get into a bad state.
- Fixed handling conflicting puppet rows when finding UUID ( [#82](https://github.com/mautrix/signal/issues/82)).

---

# mautrix-facebook v0.3.1

> published at 2021-08-07 [source](https://github.com/mautrix/facebook/releases/tag/v0.3.1)

**N.B.** Docker images have moved from `dock.mau.dev/tulir/mautrix-facebook` to `dock.mau.dev/mautrix/facebook`. New versions are only available at the new path.

- Re-added `http_proxy` support for the Facebook connection.
- Updated Docker image to Alpine 3.14.
- Fixed messages being dropped if they came in while the portal was being created.
- Fixed bridge info causing canonical JSON errors due to the `id` field not being stringified.

---

# prometheus_node_exporter 1.2.2 / 2021-08-06

> published at 2021-08-06 [source](https://github.com/prometheus/node_exporter/releases/tag/v1.2.2)

- \[BUGFIX\] Fix processes collector long int parsing [#2112](https://github.com/prometheus/node_exporter/pull/2112)

---

# prometheus_node_exporter 1.2.1 / 2021-07-23

> published at 2021-08-05 [source](https://github.com/prometheus/node_exporter/releases/tag/v1.2.1)

- \[BUGFIX\] Fix zoneinfo parsing [prometheus/procfs#386](https://github.com/prometheus/procfs/issues/386)
- \[BUGFIX\] Fix nvme collector log noise [#2091](https://github.com/prometheus/node_exporter/pull/2091)
- \[BUGFIX\] Fix rapl collector log noise [#2092](https://github.com/prometheus/node_exporter/pull/2092)

---

# beeper-linkedin v0.4.1: Version 0.4.1

> published at 2021-08-04 [source](https://github.com/beeper/linkedin/releases/tag/v0.4.1)

- Upgraded mautrix to 0.10.1+
- Implemented new bridge state pushing
- Infra: added `latest` tag to the Docker image when running for a tag.


  Hopefully this helps speed up incremental builds in the future.

---

# registration v0.9.2.dev2

> published at 2021-08-01 [source](https://github.com/zeratax/matrix-registration/releases/tag/v0.9.2.dev2)

bump version

---

# registration v0.9.2.dev1

> published at 2021-08-01 [source](https://github.com/zeratax/matrix-registration/releases/tag/v0.9.2.dev1)

bump version

---

# beeper-linkedin v0.4.0: Version 0.4.0

> published at 2021-07-29 [source](https://github.com/beeper/linkedin/releases/tag/v0.4.0)

- Upgraded to support only Python 3.9+.
- Added Prometheus metrics support.
- Infrastructure: improved Docker container dependency management by moving more


  of the packages to use the Alpine-provided versions.
- On DM rooms, set the topic to the other users' occupation and include a link


  to to their LinkedIn profile. This option can be turned off by setting

  `bridge.set_topic_on_dms` to `false`.
- Added support for custom names on group chats and handling name change events.
- Added handling for emote formatting on plain-text messages.
- When a chat is read in Matrix, it is now marked as read in LinkedIn.
- Improved handling of promotional InMail messages.
- Bug fix: respect `bridge.initial_chat_sync` and `backfill` parameters.
- Send more bridging errors to the room.

---

# beeper-linkedin v0.3.0: Version 0.3.0

> published at 2021-07-16 [source](https://github.com/beeper/linkedin/releases/tag/v0.3.0)

- Updated `linkedin-messaging` to

  [v0.3.0](https://github.com/sumnerevans/linkedin-messaging-api/releases/tag/v0.3.0).
- Handle redactions to/from LinkedIn. ( [#18](https://github.com/beeper/linkedin/pull/18), #32, #37, #38)
- Handle real-time reactions to/from LinkedIn. ( [#19](https://github.com/beeper/linkedin/issues/19), #31, #32)
- Enabled sending app-service bot delivery receipts to the chat.
- Fixed `reaction` database table primary key to support multiple reactions per


  user, per message.

---

# prometheus_node_exporter 1.2.0 / 2021-07-15

> published at 2021-07-15 [source](https://github.com/prometheus/node_exporter/releases/tag/v1.2.0)

NOTE: Ignoring invalid network speed will be the default in 2.x

NOTE: Filesystem collector flags have been renamed. `--collector.filesystem.ignored-mount-points` is now `--collector.filesystem.mount-points-exclude` and `--collector.filesystem.ignored-fs-types` is now `--collector.filesystem.fs-types-exclude`. The old flags will be removed in 2.x.

- \[CHANGE\] Rename filesystem collector flags to match other collectors [#2012](https://github.com/prometheus/node_exporter/pull/2012)
- \[CHANGE\] Make node\_exporter print usage to STDOUT [#2039](https://github.com/prometheus/node_exporter/pull/2039)
- \[FEATURE\] Add conntrack statistics metrics [#1155](https://github.com/prometheus/node_exporter/pull/1155)
- \[FEATURE\] Add ethtool stats collector [#1832](https://github.com/prometheus/node_exporter/pull/1832)
- \[FEATURE\] Add flag to ignore network speed if it is unknown [#1989](https://github.com/prometheus/node_exporter/pull/1989)
- \[FEATURE\] Add tapestats collector for Linux [#2044](https://github.com/prometheus/node_exporter/pull/2044)
- \[FEATURE\] Add nvme collector [#2062](https://github.com/prometheus/node_exporter/pull/2062)
- \[ENHANCEMENT\] Add ErrorLog plumbing to promhttp [#1887](https://github.com/prometheus/node_exporter/pull/1887)
- \[ENHANCEMENT\] Add more Infiniband counters [#2019](https://github.com/prometheus/node_exporter/pull/2019)
- \[ENHANCEMENT\] netclass: retrieve interface names and filter before parsing [#2033](https://github.com/prometheus/node_exporter/pull/2033)
- \[ENHANCEMENT\] Add time zone offset metric [#2060](https://github.com/prometheus/node_exporter/pull/2060)
- \[BUGFIX\] Handle errors from disabled PSI subsystem [#1983](https://github.com/prometheus/node_exporter/pull/1983)
- \[BUGFIX\] Fix panic when using backwards compatible flags [#2000](https://github.com/prometheus/node_exporter/pull/2000)
- \[BUGFIX\] Fix wrong value for OpenBSD memory buffer cache [#2015](https://github.com/prometheus/node_exporter/pull/2015)
- \[BUGFIX\] Only initiate collectors once [#2048](https://github.com/prometheus/node_exporter/pull/2048)
- \[BUGFIX\] Handle small backwards jumps in CPU idle [#2067](https://github.com/prometheus/node_exporter/pull/2067)

---

# reminder-bot v0.2.1

> published at 2021-07-14 [source](https://github.com/anoadragon453/matrix-reminder-bot/releases/tag/v0.2.1)

Quick release to bump `matrix-nio` to version `>=0.18`, as otherwise that is required for the bot to work with Synapse 1.38+.

See [#86](https://github.com/anoadragon453/matrix-reminder-bot/issues/86) for details on the bug.

Otherwise, full changelog below:

- Improve the '!listreminders' output.

- Add one-letter aliases to each command.

- Add 'remind' as an alias for the 'remindme' command.

- Use a pill when reminding a user.

- Timezone errors due to daylight savings times will be corrected after a bridge restart.

- Prevent timezone-related errors when creating a reminder.

- Better parsing of reminders that have newlines.

- Add release instructions.

- Update setup.py to indicate Python 3.6+ is required.

- Bump minimum version of matrix-nio to 0.18.

- Bump the version of libolm to 3.2.4.

- Bump the version of Python in the CI to 3.9.

---

# beeper-linkedin v0.2.1: Version 0.2.1

> published at 2021-07-14 [source](https://github.com/beeper/linkedin/releases/tag/v0.2.1)

- Added `prometheus-client` as an optional dependency.
- Added a couple basic metrics to the bridge.

---

# beeper-linkedin v0.2.0: Version 0.2.0

> published at 2021-07-14 [source](https://github.com/beeper/linkedin/releases/tag/v0.2.0)

- Updated `linkedin-messaging` to

  [v0.2.1](https://github.com/sumnerevans/linkedin-messaging-api/releases/tag/v0.2.1).
- Pinned `python-olm` at 3.2.1.
- Implemented logout. (#56)
- Migrated to GitLab from GitHub. Be sure to update your remotes!
- Added automated Docker container build. See the image registry here:

  [https://gitlab.com/beeper/linkedin-matrix/container\_registry](https://gitlab.com/beeper/linkedin-matrix/container_registry).
- Changed `real_user_content_key` to `com.sumnerevans.linkedin.puppet`.
- Added provisioning API for managing the bridge over HTTP(S).
- Fixed some instances of text that was copied from other bridges to correctly


  reference LinkedIn.

---

# registration v1.0.0.dev7

> published at 2021-07-11 [source](https://github.com/zeratax/matrix-registration/releases/tag/v1.0.0.dev7)

update dependencies

---

# registration v1.0.0.dev6

> published at 2021-07-10 [source](https://github.com/zeratax/matrix-registration/releases/tag/v1.0.0.dev6)

fix workflow v13

---

# registration v1.0.0.dev5

> published at 2021-07-10 [source](https://github.com/zeratax/matrix-registration/releases/tag/v1.0.0.dev5)

update ip token association

---

# prometheus_postgres_exporter 0.10.0 / 2021-07-08

> published at 2021-07-09 [source](https://github.com/prometheus-community/postgres_exporter/releases/tag/v0.10.0)

- \[ENHANCEMENT\] Add ability to set included databases when autoDiscoverDatabases is enabled [#499](https://github.com/prometheus-community/postgres_exporter/pull/499)
- \[BUGFIX\] fix pg\_replication\_slots on postgresql versions 9.4 <> 10.0 [#537](https://github.com/prometheus-community/postgres_exporter/pull/537)

---

# synapse-admin 0.8.2: Bump version and update packages

> published at 2021-07-05 [source](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/0.8.2)

Change-Id: If148587c9e5895ab6b8a70ec34c9190f0bb8f2e0

---

# etherpad 1.8.14

> published at 2021-07-04 [source](https://github.com/ether/etherpad-lite/releases/tag/1.8.14)

# 1.8.14

### Security fixes

- Fixed a persistent XSS vulnerability in the Chat component. In case you can't update to 1.8.14 directly, we strongly recommend to cherry-pick [`a796811`](https://github.com/ether/etherpad-lite/commit/a7968115581e20ef47a533e030f59f830486bdfa). Thanks to sonarsource for the professional disclosure.

### Compatibility changes

- Node.js v12.13.0 or later is now required.
- The `favicon` setting is now interpreted as a pathname to a favicon file, not a URL. Please see the documentation comment in `settings.json.template`.
- The undocumented `faviconPad` and `faviconTimeslider` settings have been removed.
- MySQL/MariaDB now uses connection pooling, which means you will see up to 10 connections to the MySQL/MariaDB server (by default) instead of 1. This might cause Etherpad to crash with a "ER\_CON\_COUNT\_ERROR: Too many connections" error if your server is configured with a low connection limit.
- Changes to environment variable substitution in `settings.json` (see the documentation comments in `settings.json.template` for details):
- An environment variable set to the string "null" now becomes `null` instead of the string "null". Similarly, if the environment variable is unset and the default value is "null" (e.g., `"${UNSET_VAR:null}"`), the value now becomes `null` instead of the string "null". It is no longer possible to produce the string "null" via environment variable substitution.
- An environment variable set to the string "undefined" now causes the setting to be removed instead of set to the string "undefined". Similarly, if the environment variable is unset and the default value is "undefined" (e.g., `"${UNSET_VAR:undefined}"`), the setting is now removed instead of set to the string "undefined". It is no longer possible to produce the string "undefined" via environment variable substitution.
- Support for unset variables without a default value is now deprecated. Please change all instances of `"${FOO}"` in your `settings.json` to `${FOO:null}` to keep the current behavior.
- The `DB_*` variable substitutions in `settings.json.docker` that previously defaulted to `null` now default to "undefined".
- Calling `next` without argument when using `Changeset.opIterator` does always return a new Op. See [`b9753dc`](https://github.com/ether/etherpad-lite/commit/b9753dcc7156d8471a5aa5b6c9b85af47f630aa8) for details.

### Notable enhancements and fixes

- MySQL/MariaDB now uses connection pooling, which should improve stability and reduce latency.
- Bulk database writes are now retried individually on write failure.
- Minify: Avoid crash due to unhandled Promise rejection if stat fails.
- padIds are now included in /socket.io query string, e.g. `https://video.etherpad.com/socket.io/?padId=AWESOME&EIO=3&transport=websocket&t=...&sid=...`. This is useful for directing pads to separate socket.io nodes.
- <script> elements added via aceInitInnerdocbodyHead hook are now executed.

- Fix read only pad access with authentication.
- Await more db writes.
- Disabled wtfnode dump by default.
- Send `USER_NEWINFO` messages on reconnect.
- Fixed loading in a hidden iframe.
- Fixed a race condition with composition. (Thanks [@ingoncalves](https://github.com/ingoncalves) for an exceptionally detailed analysis and [@rhansen](https://github.com/rhansen) for the fix.)

---

# registration v1.0.0.dev4: add systemd loadCredentials support

> published at 2021-06-25 [source](https://github.com/zeratax/matrix-registration/releases/tag/v1.0.0.dev4)

a systemd file with `LoadCredentials="secrets:pathToFile"`

where the file contains config values like

```
registration_shared_secret=verysecuresecret

```

will securely give matrix-registration access to these values.

---

# registration v0.9.2.dev0

> published at 2021-06-16 [source](https://github.com/zeratax/matrix-registration/releases/tag/v0.9.2.dev0)

update docker workflow

---

# mailer 4.94.2-r0-2

> published at 2021-06-16 [source](https://github.com/devture/exim-relay/releases/tag/4.94.2-r0-2)

Update Alpine base (3.13 -> 3.14)

---

# cactus-comments-2 v0.10.0

> published at 2021-05-28 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.10.0)

# Cactus Comments Web Client v0.10.0

- Allow commenting with Markdown.
- Pluralize time units properly.
- Enforce maximum nesting depth of 100 when sanitizing `org.matrix.custom.html`-formatted messages.

**IPFS CID**QmaBeG7TVfNzgV1eQ9KLXJxUjEtJwFkhaR9LgjXuiZxDMM**JS URL**[https://gateway.pinata.cloud/ipfs/QmaBeG7TVfNzgV1eQ9KLXJxUjEtJwFkhaR9LgjXuiZxDMM/v0.10.0/cactus.js](https://gateway.pinata.cloud/ipfs/QmaBeG7TVfNzgV1eQ9KLXJxUjEtJwFkhaR9LgjXuiZxDMM/v0.10.0/cactus.js)**CSS URL**[https://gateway.pinata.cloud/ipfs/QmaBeG7TVfNzgV1eQ9KLXJxUjEtJwFkhaR9LgjXuiZxDMM/v0.10.0/style.css](https://gateway.pinata.cloud/ipfs/QmaBeG7TVfNzgV1eQ9KLXJxUjEtJwFkhaR9LgjXuiZxDMM/v0.10.0/style.css)

---

# synapse-admin 0.8.1

> published at 2021-05-25 [source](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/0.8.1)

No content.

---

# prometheus-blackbox-exporter 0.19.0 / 2021-05-10

> published at 2021-05-11 [source](https://github.com/prometheus/blackbox_exporter/releases/tag/v0.19.0)

In the HTTP probe, `no_follow_redirects` has been changed to `follow_redirects`.

This release accepts both, with a precedence to the `no_follow_redirects` parameter.

In the next release, `no_follow_redirects` will be removed.

- \[CHANGE\] HTTP probe: no\_follow\_redirects has been renamed to follow\_redirects.
- \[FEATURE\] Add support for decompression of HTTP responses. [#764](https://github.com/prometheus/blackbox_exporter/pull/764)
- \[FEATURE\] Enable TLS and basic authentication. [#730](https://github.com/prometheus/blackbox_exporter/pull/730)
- \[FEATURE\] HTTP probe: _experimental_ OAuth2 support. [#784](https://github.com/prometheus/blackbox_exporter/pull/784)
- \[ENHANCEMENT\] Add a health endpoint. [#752](https://github.com/prometheus/blackbox_exporter/pull/752)
- \[ENHANCEMENT\] Add a metric for unknown probes. [#716](https://github.com/prometheus/blackbox_exporter/pull/716)
- \[ENHANCEMENT\] Use preferred protocol first when resolving hostname. [#728](https://github.com/prometheus/blackbox_exporter/pull/728)
- \[ENHANCEMENT\] Validating the configuration tries to compile regexes. [#729](https://github.com/prometheus/blackbox_exporter/pull/729)
- \[BUGFIX\] HTTP probe: Fix error checking. [#723](https://github.com/prometheus/blackbox_exporter/pull/723)
- \[BUGFIX\] HTTP probe: Fix how the tls phase is calculated. [#758](https://github.com/prometheus/blackbox_exporter/pull/758)

---

# cactus-comments-2 v0.9.0

> published at 2021-05-07 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.9.0)

# Cactus Comments Web Client v0.9.0

- Comment sections can now be initialized using `data-*` attributes on the `script` tag (Thanks to [@NicolaiSoeborg](/NicolaiSoeborg "Nicolai Søborg") for [!5](/cactus-comments/cactus-client/-/merge_requests/5 "Allow specifying config using data-* attributes on the script tag")).
- Allow using strings for any config parameter, including booleans and numbers.
- Users can now set a displayname when commenting as a guest.
- Some styling improvements for text inputs.

**IPFS CID**Qmb8HeTgZs4WLT9RczFbqS7An4YhgU9ZvAoG9jQZgMQYve**JS URL**[https://gateway.pinata.cloud/ipfs/Qmb8HeTgZs4WLT9RczFbqS7An4YhgU9ZvAoG9jQZgMQYve/v0.9.0/cactus.js](https://gateway.pinata.cloud/ipfs/Qmb8HeTgZs4WLT9RczFbqS7An4YhgU9ZvAoG9jQZgMQYve/v0.9.0/cactus.js)**CSS URL**[https://gateway.pinata.cloud/ipfs/Qmb8HeTgZs4WLT9RczFbqS7An4YhgU9ZvAoG9jQZgMQYve/v0.9.0/style.css](https://gateway.pinata.cloud/ipfs/Qmb8HeTgZs4WLT9RczFbqS7An4YhgU9ZvAoG9jQZgMQYve/v0.9.0/style.css)

---

# synapse-admin AMP/2021.05: Merge tag '0.8.0' into amp.chat

> published at 2021-05-04 [source](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/AMP%2F2021.05)

Change-Id: I2e362a911083149c82a8c11b6c4594bb4c760a33

---

# synapse-admin 0.8.0

> published at 2021-05-04 [source](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/0.8.0)

Increment version

---

# mautrix-facebook v0.3.0

> published at 2021-05-02 [source](https://github.com/mautrix/facebook/releases/tag/v0.3.0)

### Removed

- Removed Alembic. Database schema upgrades are handled automatically at startup as of v0.2.0. If upgrading from an older version, upgrade to v0.2.0 first using the [upgrade instructions](https://docs.mau.fi/bridges/python/facebook/upgrading-to-v0.2.0.html).

### Added

- Support for per-room displaynames ( [#11](https://github.com/mautrix/facebook/issues/11)).
- Syncing of read receipts after backfilling.
- Option for syncing notification settings from Facebook.

### Improved

- `fbrpc:` are now handled properly instead of being posted as-is to the Matrix room.
- All HTTP requests are now retried by default if the homeserver isn't reachable.

### Fixed

- Fixed some edge cases where replies and other message references wouldn't work because the bridge hadn't received the message ID.
- Fixed bridging audio messages and other things containing 32-bit floats.
- Fixed handling multiple mentions in a Messenger message ( [#144](https://github.com/mautrix/facebook/issues/144)).
- Fixed periodic reconnect failing if user was disconnected (thanks to [@mrjohnson22](https://github.com/mrjohnson22) in [#143](https://github.com/mautrix/facebook/pull/143)).

---

# sms v0.5.7

> published at 2021-04-27 [source](https://github.com/benkuly/matrix-sms-bridge/releases/tag/v0.5.7)

compatibility fix for synapse 1.32

---

# cactus-comments-2 v0.6.0

> published at 2021-04-23 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.6.0)

# Cactus Comments Web Client v0.6.0

- Move matrix.to link into login modal.
- Removed login status string above textarea.
- Show userid in Post button.
- Add placeholder text to comment textarea.
- Textarea inherits background color, to work better with darkmode websites.
- dev.html example page now has a darkmode toggle button.
- A bunch of smaller improvements to the default CSS.

**IPFS CID**QmW5zjNcXsqcYDT5YpwmC7LUEhffofNsGjb9S9nVteeZ9c**JS URL**[https://gateway.pinata.cloud/ipfs/QmW5zjNcXsqcYDT5YpwmC7LUEhffofNsGjb9S9nVteeZ9c/v0.6.0/cactus.js](https://gateway.pinata.cloud/ipfs/QmW5zjNcXsqcYDT5YpwmC7LUEhffofNsGjb9S9nVteeZ9c/v0.6.0/cactus.js)**CSS URL**[https://gateway.pinata.cloud/ipfs/QmW5zjNcXsqcYDT5YpwmC7LUEhffofNsGjb9S9nVteeZ9c/v0.6.0/style.css](https://gateway.pinata.cloud/ipfs/QmW5zjNcXsqcYDT5YpwmC7LUEhffofNsGjb9S9nVteeZ9c/v0.6.0/style.css)

---

# cactus-comments-2 v0.8.0

> published at 2021-04-23 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.8.0)

# Cactus Comments Web Client v0.8.0

- Make comment time a semantic `time` element (Thanks to [@hectorjsmith](/hectorjsmith "Hector") for [!4](/cactus-comments/cactus-client/-/merge_requests/4 "Show comment time on hover (#10)")).
- Add hover text to comment time (Thanks to [@hectorjsmith](/hectorjsmith "Hector") for [!4](/cactus-comments/cactus-client/-/merge_requests/4 "Show comment time on hover (#10)")).
- Show "just now" instead of negative seconds if message timestamp is ahead of the client's time
- Add ability fetch new messages periodically
- New config option: `updateInterval`, which controls how often to fetch new messages.
- Change thumbnail size from 32x32 to 64x64.
- Stylesheet: allow linebreaks in comments.

**IPFS CID**QmbpLvqrwBeWh3WhrctXqFaCBApdn6qHomHrgbGUWD7b9a**JS URL**[https://gateway.pinata.cloud/ipfs/QmbpLvqrwBeWh3WhrctXqFaCBApdn6qHomHrgbGUWD7b9a/v0.8.0/cactus.js](https://gateway.pinata.cloud/ipfs/QmbpLvqrwBeWh3WhrctXqFaCBApdn6qHomHrgbGUWD7b9a/v0.8.0/cactus.js)**CSS URL**[https://gateway.pinata.cloud/ipfs/QmbpLvqrwBeWh3WhrctXqFaCBApdn6qHomHrgbGUWD7b9a/v0.8.0/style.css](https://gateway.pinata.cloud/ipfs/QmbpLvqrwBeWh3WhrctXqFaCBApdn6qHomHrgbGUWD7b9a/v0.8.0/style.css)

---

# cactus-comments-2 v0.7.0

> published at 2021-04-23 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.7.0)

# Cactus Comments Web Client v0.7.0

- New configuration option: `loginEnabled` changes the login button to be a matrix.to link, if set to false (default is true).
- New configuration option: `guestPostingEnabled` requires users to log in using their Matrix account, if set to false (default is true).
- Added HACKING.md, a guide to getting started with hacking on the client.

**IPFS CID**QmbpCDNv4FJNeUHBRm17ojC87DuU5ZaLDz3Aky5d3qYUmM**JS URL**[https://gateway.pinata.cloud/ipfs/QmbpCDNv4FJNeUHBRm17ojC87DuU5ZaLDz3Aky5d3qYUmM/v0.7.0/cactus.js](https://gateway.pinata.cloud/ipfs/QmbpCDNv4FJNeUHBRm17ojC87DuU5ZaLDz3Aky5d3qYUmM/v0.7.0/cactus.js)**CSS URL**[https://gateway.pinata.cloud/ipfs/QmbpCDNv4FJNeUHBRm17ojC87DuU5ZaLDz3Aky5d3qYUmM/v0.7.0/style.css](https://gateway.pinata.cloud/ipfs/QmbpCDNv4FJNeUHBRm17ojC87DuU5ZaLDz3Aky5d3qYUmM/v0.7.0/style.css)

---

# sygnal Sygnal 0.9.3 (2021-04-22)

> published at 2021-04-22 [source](https://github.com/matrix-org/sygnal/releases/tag/v0.9.3)

## Features

- Prevent the push key from being rejected for temporary errors and oversized payloads, add TTL logging, and support `events_only` push data flag. ( [#212](https://github.com/matrix-org/sygnal/issues/212))
- WebPush: add support for Urgency and Topic header ( [#213](https://github.com/matrix-org/sygnal/issues/213))

## Bugfixes

- Fix a long-standing bug where invalid JSON would be accepted over the HTTP interfaces. ( [#216](https://github.com/matrix-org/sygnal/issues/216))
- Limit the size of requests received from HTTP clients. ( [#220](https://github.com/matrix-org/sygnal/issues/220))

## Updates to the Docker image

- Remove manually added GeoTrust Root CA certificate from docker image as Apple is no longer using it. ( [#208](https://github.com/matrix-org/sygnal/issues/208))

## Improved Documentation

- Make `CONTIBUTING.md` more explicit about how to get tests passing. ( [#188](https://github.com/matrix-org/sygnal/issues/188))
- Update `CONTRIBUTING.md` to specify how to run code style and type checks with Tox, and add formatting to code block samples. ( [#193](https://github.com/matrix-org/sygnal/issues/193))
- Document how to work around pip installation timeout errors. Contributed by Omar Mohamed. ( [#215](https://github.com/matrix-org/sygnal/issues/215))

## Internal Changes

- Update Tox to run in the installed version of Python (instead of specifying Python 3.7) and to consider specific paths and folders while running checks, instead of the whole repository (potentially including unwanted files and folders, e.g. the virtual environment). ( [#193](https://github.com/matrix-org/sygnal/issues/193))
- Make development dependencies available as extras. Contributed by Hillery Shay. ( [#194](https://github.com/matrix-org/sygnal/issues/194))
- Update `setup.py` to specify that a minimum version of Python greater or equal to 3.7 is required. Contributed by Tawanda Moyo. ( [#207](https://github.com/matrix-org/sygnal/issues/207))
- Port CI checks to Github Actions. ( [#210](https://github.com/matrix-org/sygnal/issues/210), [#219](https://github.com/matrix-org/sygnal/issues/219))
- Upgrade development dependencies. Contributed by Omar Mohamed ( [#214](https://github.com/matrix-org/sygnal/issues/214))
- Set up `coverage.py` to run in tox environment, and add html reports ( [#217](https://github.com/matrix-org/sygnal/issues/217))

---

# ma1sd 2.5.0

> published at 2021-04-16 [source](https://github.com/ma1uta/ma1sd/releases/tag/2.5.0)

- Escape special characters in the LDAP query string.
- Support for Active Directory multidomain forest (thanks Yuri Konotopov)
- Don't require Gradle to build Docker image (thanks Matt Cengia)
- Docker build improvements (thanks Matt Cengia)
- Change column type to text for postgresql from varchar.
- Added support for unix sockets (thanks Lexxy Fox).
- Force MatrixID to be lowercase in the LDAP integration (thanks Clemens Sonnleitner)
- Encode query parameters in the validation link ( [#65](https://github.com/ma1uta/ma1sd/issues/65)).
- Mark `trusted_third_party_id_servers` synapse parameter as deprecated ( [#68](https://github.com/ma1uta/ma1sd/issues/68)).
- Set a message for error responses ( [#76](https://github.com/ma1uta/ma1sd/issues/76)).
- Add forgotten M\_TERMS\_NOT\_SIGNED error message.
- Add config option to specify period dimension of the invitation scheduler.
- Add config print full display name of the invited person (disabled by default).
- Add internal API to manually invoke invitation manager.
- Add "application/json" header to response for submitToken.
- Get rid from jcenter due to jcenter will be closed.
- Update gradle to 7.0.
- Update dependencies to newer versions.

---

# registration v0.9.1

> published at 2021-04-11 [source](https://github.com/zeratax/matrix-registration/releases/tag/v0.9.1)

# Fixes

- add element-logo.png
- updated important keys in config.py to reflect new property names

---

# registration v0.9.0

> published at 2021-04-11 [source](https://github.com/zeratax/matrix-registration/releases/tag/v0.9.0)

# Features

- now uses alembic to up- or downgrade the database scheme

**Please run the following after every update** to make sure your database scheme is uptodate

```
alembic upgrade head

```

- [#55](https://github.com/zeratax/matrix-registration/issues/55) instead of one time token you can now set arbitrary amounts of usage per token via

```
 -m, --maximum INTEGER     times token can be used

```

- [#38](https://github.com/zeratax/matrix-registration/issues/38) new option in the config file to log IPs to the database


  these are viewable by checking the status of individual token via the cli or web api

# Fixes

- [#58](https://github.com/zeratax/matrix-registration/issues/58) by renaming `shared_secret` to `registration_shared_secret` and

  `admin_secret` to `admin_api_shared_secret`

# Changes

- addresses [#16](https://github.com/zeratax/matrix-registration/issues/16) by changing the web api:

  - every endpoint is now under the `/api/` subdirectory
  - added a version endpoint `/api/version` that respons with e.g. `{"version":"0.9.0.dev0"}`
  - PUT -> PATCH for `/api/token`
- renaming the following properties
  - `valid` -\> `active`
  - `one_time` -\> `max_usage`
  - `ex_date` -\> `expiration_date`

---

# mautrix-signal v0.1.1

> published at 2021-04-07 [source](https://github.com/mautrix/signal/releases/tag/v0.1.1)

Target signald version: [v0.13.1](https://gitlab.com/signald/signald/-/tags/0.13.1)

### Added

- Support for group v2 avatars.
- Syncing of group permissions from Signal.
- Support for accepting Signal group invites.
- Support for Matrix->Signal group name and avatar changes.
- Support for real-time group info updates from Signal.
- Hidden captcha support in register command.
- Command to mark safety numbers as trusted.
- Workaround for Element iOS image rendering bug (thanks to [@celogeek](https://github.com/celogeek) in [#57](https://github.com/mautrix/signal/pull/57)).

### Improved

- Commands that take phone numbers now tolerate unnecessary characters a bit better.
- Updated to signald v1 protocol for most requests.

### Fixed

- Failure to bridge attachments if the `outgoing_attachment_dir` didn't exist.
- Errors with no messages from signald not being parsed properly.

---

# mautrix-instagram v0.1.0

> published at 2021-04-07 [source](https://github.com/mautrix/instagram/releases/tag/v0.1.0)

Bump version to 0.1.0

---

# sygnal v0.9.2

> published at 2021-03-29 [source](https://github.com/matrix-org/sygnal/releases/tag/v0.9.2)

# Sygnal v0.9.2 (2021-03-29)

## Features

- Add `allowed_endpoints` option as an understood config option for WebPush pushkins. ( [#184](https://github.com/matrix-org/sygnal/issues/184))
- Add `ttl` option as an understood config option for WebPush pushkins to make delivery more reliable ( [#185](https://github.com/matrix-org/sygnal/issues/185))

---

# sygnal v0.9.1

> published at 2021-03-23 [source](https://github.com/matrix-org/sygnal/releases/tag/v0.9.1)

# Sygnal 0.9.1 (2021-03-23)

## Features

- Add `allowed_endpoints` configuration option for limiting the endpoints that WebPush pushkins will contact. ( [#182](https://github.com/matrix-org/sygnal/issues/182))

## Bugfixes

- Fix bug where the requests from different WebPush devices could bleed into each other. ( [#180](https://github.com/matrix-org/sygnal/issues/180))
- Fix bug when using a HTTP proxy where connections would sometimes fail to establish. ( [#181](https://github.com/matrix-org/sygnal/issues/181))

---

# etherpad 1.8.13

> published at 2021-03-22 [source](https://github.com/ether/etherpad-lite/releases/tag/1.8.13)

# 1.8.13

### Notable fixes

- Fixed a bug in the safeRun.sh script ( [#4935](https://github.com/ether/etherpad-lite/pull/4935))
- Add more endpoints that do not need authentication/authorization ( [#4921](https://github.com/ether/etherpad-lite/pull/4921))
- Fixed issue with non-opening device keyboard on smartphones ( [#4929](https://github.com/ether/etherpad-lite/pull/4929))
- Add version string to iframe\_editor.css to prevent stale cache entry ( [#4964](https://github.com/ether/etherpad-lite/pull/4964))

### Notable enhancements

- Refactor pad loading (no document.write anymore) ( [#4960](https://github.com/ether/etherpad-lite/pull/4960))
- Improve import/export functionality, logging and tests ( [#4957](https://github.com/ether/etherpad-lite/pull/4957))
- Refactor CSS manager creation ( [#4963](https://github.com/ether/etherpad-lite/pull/4963))
- Better metrics
- Add test for client height ( [#4965](https://github.com/ether/etherpad-lite/pull/4965))

### Dependencies

- ueberDB2 1.3.2 -> 1.4.4
- express-rate-limit 5.2.5 -> 5.2.6
- etherpad-require-kernel 1.0.9 -> 1.0.11

---

# sygnal v0.9.0

> published at 2021-03-19 [source](https://github.com/matrix-org/sygnal/releases/tag/v0.9.0)

# Sygnal 0.9.0 (2021-03-19)

## Features

- Add experimental support for WebPush pushkins. ( [#177](https://github.com/matrix-org/sygnal/issues/177))

## Bugfixes

- Fix erroneous warning log line when setting the `max_connections` option in a GCM app config. ( [#157](https://github.com/matrix-org/sygnal/issues/157))
- Fix bug where the `sygnal_inflight_request_limit_drop` metric would not appear in prometheus until requests were actually dropped. ( [#172](https://github.com/matrix-org/sygnal/issues/172))
- Fix bug where Sygnal would not recover after losing connection to the database. ( [#179](https://github.com/matrix-org/sygnal/issues/179))

## Improved Documentation

- Add preliminary documentation ( [Troubleshooting](https://github.com/matrix-org/sygnal/tree/v0.9.0/docs/troubleshooting.md) and [Application Developers' Notes](https://github.com/matrix-org/sygnal/tree/v0.9.0/docs/applications.md)). ( [#150](https://github.com/matrix-org/sygnal/issues/150), [#154](https://github.com/matrix-org/sygnal/issues/154), [#158](https://github.com/matrix-org/sygnal/issues/158))
- Add a note to the releasing doc asking people to inform EMS and customers during the release process. ( [#155](https://github.com/matrix-org/sygnal/issues/155))

## Internal Changes

- Remove a source of noisy (but otherwise harmless) exceptions in tests. ( [#149](https://github.com/matrix-org/sygnal/issues/149))
- Add tests for HTTP Proxy support. ( [#151](https://github.com/matrix-org/sygnal/issues/151), [#152](https://github.com/matrix-org/sygnal/issues/152))
- Remove extraneous log line. ( [#174](https://github.com/matrix-org/sygnal/issues/174))
- Fix type hints due to Twisted upgrade. ( [#178](https://github.com/matrix-org/sygnal/issues/178))

---

# synapse-admin 0.7.2

> published at 2021-03-16 [source](https://github.com/Awesome-Technologies/synapse-admin/releases/tag/0.7.2)

No content.

---

# cactus-comments-2 v0.5.0

> published at 2021-03-12 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.5.0)

# Cactus Comments Web Client v0.5.0

- Support `m.image` messages
- Support `m.audio` messages
- Support `m.file` messages
- Support `m.video` messages

**IPFS CID**QmX6ZLduDt7ESNU7j9ycmopc3zgG7tgsodHYQ59fwc1Ywx**JS URL**[https://gateway.pinata.cloud/ipfs/QmX6ZLduDt7ESNU7j9ycmopc3zgG7tgsodHYQ59fwc1Ywx/v0.5.0/cactus.js](https://gateway.pinata.cloud/ipfs/QmX6ZLduDt7ESNU7j9ycmopc3zgG7tgsodHYQ59fwc1Ywx/v0.5.0/cactus.js)**CSS URL**[https://gateway.pinata.cloud/ipfs/QmX6ZLduDt7ESNU7j9ycmopc3zgG7tgsodHYQ59fwc1Ywx/v0.5.0/style.css](https://gateway.pinata.cloud/ipfs/QmX6ZLduDt7ESNU7j9ycmopc3zgG7tgsodHYQ59fwc1Ywx/v0.5.0/style.css)

---

# prometheus_node_exporter 1.1.2 / 2021-03-05

> published at 2021-03-05 [source](https://github.com/prometheus/node_exporter/releases/tag/v1.1.2)

- \[BUGFIX\] Handle errors from disabled PSI subsystem [#1983](https://github.com/prometheus/node_exporter/pull/1983)
- \[BUGFIX\] Sanitize strings from /sys/class/power\_supply [#1984](https://github.com/prometheus/node_exporter/pull/1984)
- \[BUGFIX\] Silence missing netclass errors [#1986](https://github.com/prometheus/node_exporter/pull/1986)

---

# etherpad 1.8.12

> published at 2021-03-05 [source](https://github.com/ether/etherpad-lite/releases/tag/1.8.12)

Special mention: Thanks to Sauce Labs for additional testing tunnels to help us grow! :)

### Security patches

- Fixed a regression in v1.8.11 which caused some pad names to cause Etherpad to restart.

### Notable fixes

- Fixed a bug in the `dirty` database driver that sometimes caused Node.js to crash during shutdown and lose buffered database writes.
- Fixed a regression in v1.8.8 that caused "Uncaught TypeError: Cannot read property '0' of undefined" with some plugins ( [#4885](https://github.com/ether/etherpad-lite/issues/4885))
- Less warnings in server console for supported element types on import.
- Support Azure and other network share installations by using a more truthful relative path.

### Notable enhancements

- Dependency updates
- Various Docker deployment improvements
- Various new translations
- Improvement of rendering of plugin hook list and error message handling

---

# cactus-comments-2 v0.4.2

> published at 2021-03-01 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.4.2)

# Cactus Comments Web Client v0.4.2

**IPFS CID**QmZSG1yVHpqSxBnnFLq9TKxjZF8SpVy3Y3mDV8vY7fBB3Y**JS URL**[https://gateway.pinata.cloud/ipfs/QmZSG1yVHpqSxBnnFLq9TKxjZF8SpVy3Y3mDV8vY7fBB3Y/v0.4.2/cactus.js](https://gateway.pinata.cloud/ipfs/QmZSG1yVHpqSxBnnFLq9TKxjZF8SpVy3Y3mDV8vY7fBB3Y/v0.4.2/cactus.js)**CSS URL**[https://gateway.pinata.cloud/ipfs/QmZSG1yVHpqSxBnnFLq9TKxjZF8SpVy3Y3mDV8vY7fBB3Y/v0.4.2/style.css](https://gateway.pinata.cloud/ipfs/QmZSG1yVHpqSxBnnFLq9TKxjZF8SpVy3Y3mDV8vY7fBB3Y/v0.4.2/style.css)

---

# prometheus_postgres_exporter 0.9.0 / 2021-03-01

> published at 2021-03-01 [source](https://github.com/prometheus-community/postgres_exporter/releases/tag/v0.9.0)

First release under the Prometheus Community organisation.

- \[CHANGE\] Update build to use standard Prometheus promu/Dockerfile
- \[ENHANCEMENT\] Remove duplicate column in queries.yml [#433](https://github.com/prometheus-community/postgres_exporter/pull/433)
- \[ENHANCEMENT\] Add query for 'pg\_replication\_slots' [#465](https://github.com/prometheus-community/postgres_exporter/pull/465)
- \[ENHANCEMENT\] Allow a custom prefix for metric namespace [#387](https://github.com/prometheus-community/postgres_exporter/pull/387)
- \[ENHANCEMENT\] Improve PostgreSQL replication lag detection [#395](https://github.com/prometheus-community/postgres_exporter/pull/395)
- \[ENHANCEMENT\] Support connstring syntax when discovering databases [#473](https://github.com/prometheus-community/postgres_exporter/pull/473)
- \[ENHANCEMENT\] Detect SIReadLock locks in the pg\_locks metric [#421](https://github.com/prometheus-community/postgres_exporter/pull/421)
- \[BUGFIX\] Fix pg\_database\_size\_bytes metric in queries.yaml [#357](https://github.com/prometheus-community/postgres_exporter/pull/357)
- \[BUGFIX\] Don't ignore errors in parseUserQueries [#362](https://github.com/prometheus-community/postgres_exporter/pull/362)
- \[BUGFIX\] Fix queries.yaml for AWS RDS [#370](https://github.com/prometheus-community/postgres_exporter/pull/370)
- \[BUGFIX\] Recover when connection cannot be established at startup [#415](https://github.com/prometheus-community/postgres_exporter/pull/415)
- \[BUGFIX\] Don't retry if an error occurs [#426](https://github.com/prometheus-community/postgres_exporter/pull/426)
- \[BUGFIX\] Do not panic on incorrect env [#457](https://github.com/prometheus-community/postgres_exporter/pull/457)

---

# mautrix-facebook v0.2.1

> published at 2021-02-28 [source](https://github.com/mautrix/facebook/releases/tag/v0.2.1)

- Added web-based login interface to prevent the bridge and homeserver from seeing passwords.
- Fixed error if bridge bot doesn't have permission to redact password when logging in.

---

# etherpad 1.8.11

> published at 2021-02-27 [source](https://github.com/ether/etherpad-lite/releases/tag/1.8.11)

### Notable fixes

- Fix server crash issue within PadMessageHandler due to SocketIO handling
- Fix editor issue with drop downs not being visible
- Ensure correct version is passed when loading front end resources
- Ensure underscore and jquery are available in original location for plugin comptability

### Notable enhancements

- Improved page load speeds

---

# etherpad 1.8.10

> published at 2021-02-25 [source](https://github.com/ether/etherpad-lite/releases/tag/1.8.10)

### Security Patches

- Resolve potential ReDoS vulnerability in your project - GHSL-2020-359

### Compatibility changes

- JSONP API has been removed in favor of using the mature OpenAPI implementation.
- Node 14 is now required for Docker Deployments

### Notable fixes

- Various performance and stability fixes

### Notable enhancements

- Improved line number alignment and user experience around line anchors
- Notification to admin console if a plugin is missing during user file import
- Beautiful loading and reconnecting animation
- Additional code quality improvements
- Dependency updates

---

# mautrix-facebook v0.2.0

> published at 2021-02-24 [source](https://github.com/mautrix/facebook/releases/tag/v0.2.0)

Breaking change: old cookie logins will no longer work, all users have to relogin. See upgrade instructions [on docs.mau.fi](https://docs.mau.fi/bridges/python/facebook/upgrading-to-v0.2.0.html).

---

# etherpad 1.8.9

> published at 2021-02-18 [source](https://github.com/ether/etherpad-lite/releases/tag/1.8.9)

### Notable fixes

- Fixed HTTP 400 error when importing via the UI.
- Fixed "Error: spawn npm ENOENT" crash on startup in Windows.

### Notable enhancements

- Removed some unnecessary arrow key handling logic.
- Dependency updates.

---

# cactus-comments-2 v0.4.1

> published at 2021-02-09 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.4.1)

# Cactus Comments Web Client v0.4.1

**IPFS Hash**QmWhwS27CRXumWYSwyLfBxu8iaTVoPZDm8orS2pQ6Kxr3h**JS URL**[https://gateway.pinata.cloud/ipfs/QmWhwS27CRXumWYSwyLfBxu8iaTVoPZDm8orS2pQ6Kxr3h/v0.4.1/cactus.js](https://gateway.pinata.cloud/ipfs/QmWhwS27CRXumWYSwyLfBxu8iaTVoPZDm8orS2pQ6Kxr3h/v0.4.1/cactus.js)**CSS URL**[https://gateway.pinata.cloud/ipfs/QmWhwS27CRXumWYSwyLfBxu8iaTVoPZDm8orS2pQ6Kxr3h/v0.4.1/style.css](https://gateway.pinata.cloud/ipfs/QmWhwS27CRXumWYSwyLfBxu8iaTVoPZDm8orS2pQ6Kxr3h/v0.4.1/style.css)

---

# cactus-comments-2 v0.4.0

> published at 2021-02-07 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.4.0)

# Cactus Comments Web Client v0.4.0

**IPFS Hash**QmXUnqscxDW1bMFXPxw5uypcWbXs65Fzm416zAvLtmHtia**JS URL**[https://gateway.pinata.cloud/ipfs/QmXUnqscxDW1bMFXPxw5uypcWbXs65Fzm416zAvLtmHtia/v0.4.0/cactus.js](https://gateway.pinata.cloud/ipfs/QmXUnqscxDW1bMFXPxw5uypcWbXs65Fzm416zAvLtmHtia/v0.4.0/cactus.js)**CSS URL**[https://gateway.pinata.cloud/ipfs/QmXUnqscxDW1bMFXPxw5uypcWbXs65Fzm416zAvLtmHtia/v0.4.0/style.css](https://gateway.pinata.cloud/ipfs/QmXUnqscxDW1bMFXPxw5uypcWbXs65Fzm416zAvLtmHtia/v0.4.0/style.css)

---

# mautrix-signal v0.1.0

> published at 2021-02-05 [source](https://github.com/mautrix/signal/releases/tag/v0.1.0)

Initial release

---

# cactus-comments-2 v0.3.2

> published at 2021-01-18 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.3.2)

# Cactus Comments Web Client v0.3.2

**IPFS Hash**QmcdX1AX8stm7md8xkJAbpXzaieHZx7ypfJj1GkZdeHJB3**JS URL**[https://gateway.pinata.cloud/ipfs/QmcdX1AX8stm7md8xkJAbpXzaieHZx7ypfJj1GkZdeHJB3/v0.3.2/cactus.js](https://gateway.pinata.cloud/ipfs/QmcdX1AX8stm7md8xkJAbpXzaieHZx7ypfJj1GkZdeHJB3/v0.3.2/cactus.js)**CSS URL**[https://gateway.pinata.cloud/ipfs/QmcdX1AX8stm7md8xkJAbpXzaieHZx7ypfJj1GkZdeHJB3/v0.3.2/style.css](https://gateway.pinata.cloud/ipfs/QmcdX1AX8stm7md8xkJAbpXzaieHZx7ypfJj1GkZdeHJB3/v0.3.2/style.css)

---

# cactus-comments-2 v0.3.1

> published at 2021-01-04 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.3.1)

Cactus Comments Web Client v0.3.1

---

# sms v0.5.6

> published at 2020-12-28 [source](https://github.com/benkuly/matrix-sms-bridge/releases/tag/v0.5.6)

- allows alphanumeric sender ids for receiving sms (not sending)
- fixes bug, that AndroidSmsProvider does not send answer

---

# cactus-comments-2 v0.3.0

> published at 2020-12-15 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.3.0)

Cactus Comments Web Client v0.3.0

---

# cactus-comments-2 v0.2.1

> published at 2020-12-15 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.2.1)

Cactus Comments Web Client v0.2.1

---

# mautrix-twitter v0.1.2

> published at 2020-12-11 [source](https://github.com/mautrix/twitter/releases/tag/v0.1.2)

Bump version to 0.1.2

---

# mautrix-facebook v0.1.2

> published at 2020-12-11 [source](https://github.com/mautrix/facebook/releases/tag/v0.1.2)

Bump version to 0.1.2

---

# sms v0.5.5

> published at 2020-12-09 [source](https://github.com/benkuly/matrix-sms-bridge/releases/tag/v0.5.5)

- fixed small bug which causend an database referential integrity constraint violation

---

# cactus-comments-2 v0.2.0

> published at 2020-12-08 [source](https://gitlab.com/cactus-comments/cactus-client/-/tags/v0.2.0)

Cactus Comments Web Client v0.2.0

---

# com.devture.ansible.role.traefik_certs_dumper v2.7.4

> published at 2020-12-03 [source](https://github.com/ldez/traefik-certs-dumper/releases/tag/v2.7.4)

## Changelog

---

# com.devture.ansible.role.traefik_certs_dumper v2.7.3

> published at 2020-12-03 [source](https://github.com/ldez/traefik-certs-dumper/releases/tag/v2.7.3)

chore: fix GH\_TOKEN

---

# com.devture.ansible.role.traefik_certs_dumper v2.7.2

> published at 2020-12-03 [source](https://github.com/ldez/traefik-certs-dumper/releases/tag/v2.7.2)

chore: fix seihon installation.

---

# com.devture.ansible.role.traefik_certs_dumper v2.7.1

> published at 2020-12-03 [source](https://github.com/ldez/traefik-certs-dumper/releases/tag/v2.7.1)

chore: goodbye TravisCI.

---

# appservice-discord v1.0.0

> published at 2020-12-01 [source](https://github.com/matrix-org/matrix-appservice-discord/releases/tag/v1.0.0)

\\*\\* If you are on <1.0.0 you MUST upgrade as the bridge will no longer be able to communicate with Discord on 0.x.x versions\*\*

This release updates the bridge to suppport the latest APIs in order to work with Discord. Please review your config carefully after updating as:

- The bridge now uses the latest Discord APIs (Gateway v8)
- The bridge now supports Node 12/14. Node 10 may still work but is unsupported by the team.
- The bridge does not specify a default port in the package.json anymore. You must specify this [in the config](https://github.com/Half-Shot/matrix-appservice-discord/blob/6efedfef9793697f9134b3bc0c6000e883a44b7f/config/config.sample.yaml#L11-L12), or in the command line


  if you invoke it manually.

Please take care and as always log issues and let us know if you encounter any problems.

---

# sms v0.5.4

> published at 2020-11-27 [source](https://github.com/benkuly/matrix-sms-bridge/releases/tag/v0.5.4)

features:

- Should allow to abort deferred sending (using bot) [#3](https://github.com/benkuly/matrix-sms-bridge/issues/3)

---

# sms v0.5.3

> published at 2020-11-26 [source](https://github.com/benkuly/matrix-sms-bridge/releases/tag/v0.5.3)

features:

- accepts "invalid" (not international) incoming telephone number [#2](https://github.com/benkuly/matrix-sms-bridge/issues/2)
- notifies default room when receiving messages failed [#4](https://github.com/benkuly/matrix-sms-bridge/issues/4)

bugfix:

- [#5](https://github.com/benkuly/matrix-sms-bridge/issues/5)

---

# sms v0.5.2

> published at 2020-11-23 [source](https://github.com/benkuly/matrix-sms-bridge/releases/tag/v0.5.2)

- enabled detailed logging of errors in debug mode

---

# sms v0.5.1

> published at 2020-11-23 [source](https://github.com/benkuly/matrix-sms-bridge/releases/tag/v0.5.1)

- fixed bug, where old messages were not deleted, when resend was successful

---

# sms v0.5.0

> published at 2020-11-20 [source](https://github.com/benkuly/matrix-sms-bridge/releases/tag/v0.5.0)

- added android-sms-gateway-server support
- docker containers does now have other tags, when using gammu as provider

---

# sms v0.4.4.RELEASE

> published at 2020-11-16 [source](https://github.com/benkuly/matrix-sms-bridge/releases/tag/v0.4.4.RELEASE)

- always invite users in room alias (not only when alias is created)

---

# mautrix-facebook v0.1.1

> published at 2020-11-11 [source](https://github.com/mautrix/facebook/releases/tag/v0.1.1)

Bump version to 0.1.1

---

# mautrix-twitter v0.1.1

> published at 2020-11-09 [source](https://github.com/mautrix/twitter/releases/tag/v0.1.1)

Bump version to 0.1.1

---

# mautrix-googlechat v0.1.4

> published at 2020-11-09 [source](https://github.com/mautrix/googlechat/releases/tag/v0.1.4)

Bump version to 0.1.4

---

# mautrix-hangouts v0.1.4

> published at 2020-11-09 [source](https://github.com/mautrix/googlechat/releases/tag/v0.1.4)

Bump version to 0.1.4

---

# appservice-discord v1.0.0-rc3

> published at 2020-11-06 [source](https://github.com/matrix-org/matrix-appservice-discord/releases/tag/v1.0.0-rc3)

1.0.0-rc3

---

# appservice-discord v1.0.0-rc2

> published at 2020-11-05 [source](https://github.com/matrix-org/matrix-appservice-discord/releases/tag/v1.0.0-rc2)

This release:

- Includes [#609](https://github.com/matrix-org/matrix-appservice-discord/pull/609) which reapplies config overrides from the environment
- Ensures that the bridge is using Discord Intents, which is required for all bridges after the 7th of November.

---

# appservice-discord 1.0.0-rc1

> published at 2020-11-03 [source](https://github.com/matrix-org/matrix-appservice-discord/releases/tag/1.0.0-rc1)

This release upgrades our supported version of the Discord API from v11 to v12.

---

# appservice-discord v1.0.0-experimental

> published at 2020-11-03 [source](https://github.com/matrix-org/matrix-appservice-discord/releases/tag/v1.0.0-experimental)

Tweak CI

---

# prometheus-blackbox-exporter 0.18.0 / 2020-10-12

> published at 2020-10-12 [source](https://github.com/prometheus/blackbox_exporter/releases/tag/v0.18.0)

\[ENHANCEMENT\] Expose probe\_dns\_duration\_seconds metric ( [#662](https://github.com/prometheus/blackbox_exporter/pull/662))

\[ENHANCEMENT\] Add probe\_icmp\_reply\_hop\_limit ( [#694](https://github.com/prometheus/blackbox_exporter/pull/694))

\[ENHANCEMENT\] prober/tls: adding metric to expose certificate fingerprint info ( [#678](https://github.com/prometheus/blackbox_exporter/pull/678))

\[BUGFIX\] prober/tls: fix probe\_ssl\_last\_chain\_expiry\_timestamp\_seconds ( [#681](https://github.com/prometheus/blackbox_exporter/pull/681))

\[BUGFIX\] Fix incorrect content length reporting when using regexes ( [#674](https://github.com/prometheus/blackbox_exporter/pull/674))

\[BUGFIX\] Fix panic when running ICMPv4 probe with DontFragment ( [#686](https://github.com/prometheus/blackbox_exporter/pull/686))

\[BUGFIX\] Deal with URLs with literal IPv6 addresses ( [#645](https://github.com/prometheus/blackbox_exporter/pull/645))

\[BUGFIX\] Change DoT default port to 853 ( [#655](https://github.com/prometheus/blackbox_exporter/pull/655))

---

# mautrix-googlechat v0.1.3

> published at 2020-09-27 [source](https://github.com/mautrix/googlechat/releases/tag/v0.1.3)

Bump version to 0.1.3

---

# mautrix-hangouts v0.1.3

> published at 2020-09-27 [source](https://github.com/mautrix/googlechat/releases/tag/v0.1.3)

Bump version to 0.1.3

---

# rageshake v1.2

> published at 2020-09-16 [source](https://github.com/matrix-org/rageshake/releases/tag/v1.2)

Add email support.

---

# reminder-bot v0.2.0

> published at 2020-09-13 [source](https://github.com/anoadragon453/matrix-reminder-bot/releases/tag/v0.2.0)

Lots of changes, updates and polishing! Find the list below:

### Features

- Better support for command prefixes other than the default `!`.
- Just writing `!silence` now silences the currently active alarm.
- The bot will now print the correct syntax of a command if the user fails to follow it.
- The bot will reply to events if it cannot decrypt them, as well as offer helpful tips that both the user and bot operator can try to fix things.

### Bugfixes

- Timezones. They should finally work correctly! ...no 100% guarantees though.
- Alarms were a bit broken. They're fixed now.
- Fix commands with formatting and newlines not being picked up by the bot.
- Fix non-latin characters preventing reminders from being deleted.

### Internal changes

- Add a dev-optimised Dockerfile for quicker iteration during development.
- Better wording revolving alarms. They're just reminders that alarm repeatedly when they go off.
- Log why the bot is unable to start.
- Don't print "Unknown help topic" in case the user is trying to ask another bot for help.
- The config dict is now a singleton.
- Type hints everywhere!

The minimum Python version is now 3.6.

---

# mautrix-googlechat v0.1.2

> published at 2020-09-11 [source](https://github.com/mautrix/googlechat/releases/tag/v0.1.2)

Bump version to 0.1.2

---

# mautrix-hangouts v0.1.2

> published at 2020-09-11 [source](https://github.com/mautrix/googlechat/releases/tag/v0.1.2)

Bump version to 0.1.2

---

# mautrix-twitter v0.1.0

> published at 2020-09-04 [source](https://github.com/mautrix/twitter/releases/tag/v0.1.0)

Update deps and bump version to 0.1.0

---

# mautrix-googlechat v0.1.1

> published at 2020-09-04 [source](https://github.com/mautrix/googlechat/releases/tag/v0.1.1)

Update deps and bump version to 0.1.1

---

# mautrix-hangouts v0.1.1

> published at 2020-09-04 [source](https://github.com/mautrix/googlechat/releases/tag/v0.1.1)

Update deps and bump version to 0.1.1

---

# mautrix-googlechat v0.1.0

> published at 2020-09-04 [source](https://github.com/mautrix/googlechat/releases/tag/v0.1.0)

Bump version to 0.1.0

---

# mautrix-hangouts v0.1.0

> published at 2020-09-04 [source](https://github.com/mautrix/googlechat/releases/tag/v0.1.0)

Bump version to 0.1.0

---

# sygnal Sygnal v0.8.2

> published at 2020-08-06 [source](https://github.com/matrix-org/sygnal/releases/tag/v0.8.2)

## Features

- Add the ability to configure custom FCM options, which is necessary for using iOS with Firebase. ( [#145](https://github.com/matrix-org/sygnal/issues/145))
- Add a Prometheus metric ( `sygnal_inflight_request_limit_drop`) that shows the number of notifications dropped due to exceeding the in-flight concurrent request limit. ( [#146](https://github.com/matrix-org/sygnal/issues/146))

---

# sygnal Sygnal v0.8.1

> published at 2020-07-28 [source](https://github.com/matrix-org/sygnal/releases/tag/v0.8.1)

## Updates to the Docker image

- Include GeoTrust Global CA's certificate in the Docker image as it is needed for APNs (and was removed by Debian). ( [#141](https://github.com/matrix-org/sygnal/issues/141))

---

# reminder-bot v0.1.0

> published at 2020-07-03 [source](https://github.com/anoadragon453/matrix-reminder-bot/releases/tag/v0.1.0)

Initial release! If the issue tracker is to be believed, there are no more show-stopping bugs for the moment, only new feature requests.

So... seems like a good time to do a release!

---

# ma1sd 2.4.0

> published at 2020-06-28 [source](https://github.com/ma1uta/ma1sd/releases/tag/2.4.0)

Changes:

- Enabled v2 API by default.
- Added experimental support of the database connection pooling for postgresql
- Added option to bind ma1sd to specified address.
- Added error logging for LDAP authorization.
- Added full request and response logs for debug.
- Avoid including bridged user in directory lookups ( [#45](https://github.com/ma1uta/ma1sd/pull/45))
- Add experimental multi-platform buillds for amd64 and arm64 platforms.
- remove warning about matrix-synapse-ldap3 ( [#50](https://github.com/ma1uta/ma1sd/pull/50))


  Bugfixes:
- [#26](https://github.com/ma1uta/ma1sd/issues/26)
- [#29](https://github.com/ma1uta/ma1sd/issues/29) (partial)
- [#22](https://github.com/ma1uta/ma1sd/issues/22)
- [#27](https://github.com/ma1uta/ma1sd/issues/27)

---

# prometheus-blackbox-exporter 0.17.0 / 2020-06-19

> published at 2020-06-19 [source](https://github.com/prometheus/blackbox_exporter/releases/tag/v0.17.0)

\[FEATURE\] Add none/all matches to DNS RRset validation ( [#552](https://github.com/prometheus/blackbox_exporter/pull/552))

\[FEATURE\] Allow specifying DNS query Class ( [#635](https://github.com/prometheus/blackbox_exporter/pull/635))

\[FEATURE\] Add support for DoT to DNS probes ( [#643](https://github.com/prometheus/blackbox_exporter/pull/643)) ( [#644](https://github.com/prometheus/blackbox_exporter/pull/644))

\[ENHANCEMENT\] Add new probe\_ssl\_last\_chain\_expiry\_timestamp\_seconds metric ( [#636](https://github.com/prometheus/blackbox_exporter/pull/636))

\[ENHANCEMENT\] Add probe\_ip\_addr\_hash to detect if the IP changes ( [#584](https://github.com/prometheus/blackbox_exporter/pull/584))

\[ENHANCEMENT\] Add support for "rootless" ping ( [#642](https://github.com/prometheus/blackbox_exporter/pull/642))

\[BUGFIX\] Seed RNG to ensure different ICMP ids. ( [#638](https://github.com/prometheus/blackbox_exporter/pull/638))

---

# rageshake 1.1 (2020-06-04)

> published at 2020-06-04 [source](https://github.com/matrix-org/rageshake/releases/tag/v1.1)

## Features

- Add support for Slack notifications. Contributed by [@awesome-manuel](https://github.com/awesome-manuel). ( [#28](https://github.com/matrix-org/rageshake/issues/28))

## Internal Changes

- Update minimum go version to 1.11. ( [#29](https://github.com/matrix-org/rageshake/issues/29), [#30](https://github.com/matrix-org/rageshake/issues/30))
- Replace vendored libraries with `go mod`. ( [#31](https://github.com/matrix-org/rageshake/issues/31))
- Add Dockerfile. Contributed by [@awesome-manuel](https://github.com/awesome-manuel). ( [#32](https://github.com/matrix-org/rageshake/issues/32))

---

# ma1sd 2.4.0-rc2

> published at 2020-05-31 [source](https://github.com/ma1uta/ma1sd/releases/tag/2.4.0-rc2)

- Avoid including bridged user in directory lookups ( [#45](https://github.com/ma1uta/ma1sd/pull/45))
- Add experimental multi-platform buillds for amd64 and arm64 platforms.

---

# ma1sd 2.4.0-rc1

> published at 2020-05-18 [source](https://github.com/ma1uta/ma1sd/releases/tag/2.4.0-rc1)

- Enabled v2 API by default.
- Added experimental support of the database connection pooling for postgresql
- Added option to bind ma1sd to specified address.
- Added error logging for LDAP authorization.
- Added full request and response logs for debug.


  Bugfixes:
- [#26](https://github.com/ma1uta/ma1sd/issues/26)
- [#29](https://github.com/ma1uta/ma1sd/issues/29) (partial)
- [#22](https://github.com/ma1uta/ma1sd/issues/22)
- [#27](https://github.com/ma1uta/ma1sd/issues/27)

---

# ma1sd 2.3.0

> published at 2020-01-30 [source](https://github.com/ma1uta/ma1sd/releases/tag/2.3.0)

Changes:

- Load DNS overwrite config on startup (from [https://github.com/NullIsNot0](https://github.com/NullIsNot0)) [`ce938bb`](https://github.com/ma1uta/ma1sd/commit/ce938bb4a5b21f4759e9a3a08416ff53b09a0315)
- Remove duplicates from identity store before email notifications (from [https://github.com/NullIsNot0](https://github.com/NullIsNot0)) [`be915ae`](https://github.com/ma1uta/ma1sd/commit/be915aed947a6d38e5f26f0a7834d12e3a0799f2)
- Fix room name retrieval after Synapse dropped table room\_names (from [https://github.com/NullIsNot0](https://github.com/NullIsNot0)) [`6b7a4c8`](https://github.com/ma1uta/ma1sd/commit/6b7a4c8a2356339f59e81f60bda72ff89f7e3f38)
- Add logging configuration. Add `--dump` and `--dump-and-exit` options to just print the full configuration. [`9219bd4`](https://github.com/ma1uta/ma1sd/commit/9219bd47232c81186c931439958e07d98d2dba9a) [`aed12e5`](https://github.com/ma1uta/ma1sd/commit/aed12e553642e2cc32b25a9dea155ca7e8e3a161)
- Add the postgresql backend for internal storage. [`7555fff`](https://github.com/ma1uta/ma1sd/commit/7555fff1a50a99fab3c6bfcae7845ccb8398b30d) [`72977d6`](https://github.com/ma1uta/ma1sd/commit/72977d65aeb2148c535cc66804f1be9843ff8d77)
- Improve logging configuration. Introduce the root and the app log levels. [`75efd99`](https://github.com/ma1uta/ma1sd/commit/75efd9921d6179c1c9a20524902ec715ead1d2ca)

---

# ma1sd 2.2.2

> published at 2019-12-26 [source](https://github.com/ma1uta/ma1sd/releases/tag/2.2.2)

Changes:

- added hash lookup for LDAP provider.
- fixed [#9](https://github.com/ma1uta/ma1sd/issues/9)

---

# com.devture.ansible.role.traefik_certs_dumper v2.7.0

> published at 2019-12-26 [source](https://github.com/ldez/traefik-certs-dumper/releases/tag/v2.7.0)

## Changelog

[`7c381b5`](https://github.com/ldez/traefik-certs-dumper/commit/7c381b582e55f86ab438920221ba2d6bfc3fca2b) feat: add more unit tests.

[`1ef3d37`](https://github.com/ldez/traefik-certs-dumper/commit/1ef3d37498843e9b0bb34168e65bb360a9933280) feat: improve error handling.

---

# ma1sd 2.2.1

> published at 2019-12-10 [source](https://github.com/ma1uta/ma1sd/releases/tag/2.2.1)

Changes:

- Fix homeserver verification with wildcards certificates.
- Disable v2 by default.
- Add migration to fix the accepted table (due to sqlite unable to change constraint, drop table and create again).
- Fix displaying the expiration period of the new token.
- Remove duplicated code.
- Use v1 single lookup when receive the request with `none` algorithm and the only one argument.
- Hide v2 endpoint if v2 API disabled.
- Add unique id for the accepted table.
- Add a little more logs.

---

# ma1sd 2.2.0

> published at 2019-12-06 [source](https://github.com/ma1uta/ma1sd/releases/tag/2.2.0)

- MSC2140 (API v2)
- MSC2134 (Hash lookup)

---

# ma1sd 2.2

> published at 2019-12-06 [source](https://github.com/ma1uta/ma1sd/releases/tag/2.2)

Fix hash generation.

---

# ma1sd 2.2.0-rc3

> published at 2019-11-29 [source](https://github.com/ma1uta/ma1sd/releases/tag/2.2.0-rc3)

Changelog:

- Fixed the matrix homeserver verification.
- Fixed MSC2140 and MSC2134.

---

# prometheus_postgres_exporter v0.8.0

> published at 2019-11-25 [source](https://github.com/prometheus-community/postgres_exporter/releases/tag/v0.8.0)

v0.8.0

\\* Add a build info metric ( [#323](https://github.com/prometheus-community/postgres_exporter/pull/323))

\\* Re-add pg\_stat\_bgwriter metrics which were accidentally removed in the previous version. (resolves [#336](https://github.com/prometheus-community/postgres_exporter/issues/336))

\\* Export pg\_stat\_archiver metrics ( [#324](https://github.com/prometheus-community/postgres_exporter/pull/324))

\\* Add support for 'DATA\_SOURCE\_URI\_FILE' envvar.

\\* Resolve [#329](https://github.com/prometheus-community/postgres_exporter/issues/329)

\\* Added new field "master" to queries.yaml. (credit to [@sfalkon](https://github.com/sfalkon))

\- If "master" is true, query will be call only on once database in instance

\\* Change queries.yaml for work with autoDiscoveryDatabases options (credit to [@sfalkon](https://github.com/sfalkon))

\- added current database name to metrics because any database in cluster maybe have the same table names

\- added "master" field for query instance metrics.

---

# prometheus_postgres_exporter v0.7.0

> published at 2019-10-31 [source](https://github.com/prometheus-community/postgres_exporter/releases/tag/v0.7.0)

v0.7.0

Introduces some more significant changes, hence the minor version bump in

such a short time frame.

\\* Rename pg\_database\_size to pg\_database\_size\_bytes in queries.yml.

\\* Add pg\_stat\_statements to sample queries.yml file.

\\* Add support for optional namespace caching. ( [#319](https://github.com/prometheus-community/postgres_exporter/pull/319))

\\* Fix some autodiscovery problems ( [#314](https://github.com/prometheus-community/postgres_exporter/pull/314)) (resolves [#308](https://github.com/prometheus-community/postgres_exporter/issues/308))

\\* Yaml parsing refactor ( [#299](https://github.com/prometheus-community/postgres_exporter/pull/299))

\\* Don't stop generating fingerprint while encountering value with "=" sign ( [#318](https://github.com/prometheus-community/postgres_exporter/pull/318))

(may resolve problems with passwords and special characters).

---

# prometheus_postgres_exporter v0.6.0

> published at 2019-10-30 [source](https://github.com/prometheus-community/postgres_exporter/releases/tag/v0.6.0)

v0.6.0

\\* Add SQL for grant connect ( [#303](https://github.com/prometheus-community/postgres_exporter/pull/303))

\\* Expose pg\_current\_wal\_lsn\_bytes ( [#307](https://github.com/prometheus-community/postgres_exporter/pull/307))

\\* \[minor\] fix landing page content-type ( [#305](https://github.com/prometheus-community/postgres_exporter/pull/305))

\\* Updated lib/pg driver to 1.2.0 in order to support stronger SCRAM-SHA-256 authentication. This drops support for Go < 1.11 and PostgreSQL < 9.4. ( [#304](https://github.com/prometheus-community/postgres_exporter/pull/304))

\\* Provide more helpful default values for tables that have never been vacuumed ( [#310](https://github.com/prometheus-community/postgres_exporter/pull/310))

\\* Add retries to getServer() ( [#316](https://github.com/prometheus-community/postgres_exporter/pull/316))

\\* Fix pg\_up metric returns last calculated value without explicit resetting ( [#291](https://github.com/prometheus-community/postgres_exporter/pull/291))

\\* Discover only databases that are not templates and allow connections ( [#297](https://github.com/prometheus-community/postgres_exporter/pull/297))

\\* Add --exclude-databases option ( [#298](https://github.com/prometheus-community/postgres_exporter/pull/298))

---

# appservice-discord v0.5.2

> published at 2019-10-22 [source](https://github.com/matrix-org/matrix-appservice-discord/releases/tag/v0.5.2)

# Changes for 0.5.2

This is a minor release to update some dependencies for Node 12 support.

## Misc

- Fix [#563](https://github.com/matrix-org/matrix-appservice-discord/issues/563) by updating version of `better-sqlite` to `5.4.3`
- Run `npm audit fix` to fix some outstanding vulns.

---

# com.devture.ansible.role.traefik_certs_dumper v2.6.0

> published at 2019-10-09 [source](https://github.com/ldez/traefik-certs-dumper/releases/tag/v2.6.0)

## Changelog

[`072d407`](https://github.com/ldez/traefik-certs-dumper/commit/072d407807649070603b2f91ac78b32f7a708ebe) feat(kv): support suffix/storage

[`e309f6b`](https://github.com/ldez/traefik-certs-dumper/commit/e309f6b7a8cbb3f0112af2610169c1c3f94bfb66) feat: support etcd v3.

[`e07b613`](https://github.com/ldez/traefik-certs-dumper/commit/e07b613ac49798455178c401588f3ccbb938bdfd) fix(kv): etcd v3 support.

[`70543d3`](https://github.com/ldez/traefik-certs-dumper/commit/70543d34490f5a79b0eecd987a2efe18a1f6ff4b) fix: lint.

---

# com.devture.ansible.role.traefik_certs_dumper v2.5.7

> published at 2019-09-30 [source](https://github.com/ldez/traefik-certs-dumper/releases/tag/v2.5.7)

## Changelog

[`5384d88`](https://github.com/ldez/traefik-certs-dumper/commit/5384d88e8c9d509619012d263e4ecc2dc54d38a1) fix: version flag.

---

# com.devture.ansible.role.traefik_certs_dumper v2.5.6

> published at 2019-09-30 [source](https://github.com/ldez/traefik-certs-dumper/releases/tag/v2.5.6)

## Changelog

[`574db1a`](https://github.com/ldez/traefik-certs-dumper/commit/574db1a1e0987341a77bdbadf8154a395f57bbb2) fix: multiple resolvers.

---

# email2matrix 1.0.1

> published at 2019-08-09 [source](https://github.com/devture/email2matrix/releases/tag/1.0.1)

Bump version to 1.0.1

---

# email2matrix 1.0

> published at 2019-08-05 [source](https://github.com/devture/email2matrix/releases/tag/1.0)

Fix some typos

---

# prometheus_postgres_exporter v0.5.1

> published at 2019-07-12 [source](https://github.com/prometheus-community/postgres_exporter/releases/tag/v0.5.1)

v0.5.1

\\* Add application\_name as a label for pg\_stat\_replication metrics ( [#285](https://github.com/prometheus-community/postgres_exporter/pull/285)).

---

# rageshake v1.0

> published at 2018-12-04 [source](https://github.com/matrix-org/rageshake/releases/tag/v1.0)

Initial tag of rageshake.

No reason other than for consistent build & deployment

---

# go-neb 0.1.2

> published at 2016-09-21 [source](https://github.com/matrix-org/go-neb/releases/tag/0.1.2)

0.1.2

---

# go-neb 0.1.1

> published at 2016-09-09 [source](https://github.com/matrix-org/go-neb/releases/tag/0.1.1)

0.1.1

---

# go-neb 0.1.0

> published at 2016-09-08 [source](https://github.com/matrix-org/go-neb/releases/tag/0.1.0)

0.1.0

---

# mx-puppet-slack v0.1.0

> published at 0001-01-01 [source](https://gitlab.com/mx-puppet/slack/mx-puppet-slack/-/tags/v0.1.0)

> no description provided

---



---

# cactus-comments 0.9.0

> published at 0001-01-01 [source](https://gitlab.com/cactus-comments/cactus-appservice/-/tags/0.9.0)

> no description provided

---



---

# cactus-comments 0.8.0

> published at 0001-01-01 [source](https://gitlab.com/cactus-comments/cactus-appservice/-/tags/0.8.0)

> no description provided

---



---

# cactus-comments 0.7.1

> published at 0001-01-01 [source](https://gitlab.com/cactus-comments/cactus-appservice/-/tags/0.7.1)

> no description provided

---



---

# cactus-comments 0.7.0

> published at 0001-01-01 [source](https://gitlab.com/cactus-comments/cactus-appservice/-/tags/0.7.0)

> no description provided

---



---

# cactus-comments 0.6.0

> published at 0001-01-01 [source](https://gitlab.com/cactus-comments/cactus-appservice/-/tags/0.6.0)

> no description provided

---



---

# cactus-comments 0.5.0

> published at 0001-01-01 [source](https://gitlab.com/cactus-comments/cactus-appservice/-/tags/0.5.0)

> no description provided

---



---

# cactus-comments 0.4.0

> published at 0001-01-01 [source](https://gitlab.com/cactus-comments/cactus-appservice/-/tags/0.4.0)

> no description provided

---



---

# cactus-comments 0.3.2

> published at 0001-01-01 [source](https://gitlab.com/cactus-comments/cactus-appservice/-/tags/0.3.2)

> no description provided

---



---

# cactus-comments 0.3.1

> published at 0001-01-01 [source](https://gitlab.com/cactus-comments/cactus-appservice/-/tags/0.3.1)

> no description provided

---



---

# cactus-comments 0.3.0

> published at 0001-01-01 [source](https://gitlab.com/cactus-comments/cactus-appservice/-/tags/0.3.0)

> no description provided

---



---

# cactus-comments 0.2.0

> published at 0001-01-01 [source](https://gitlab.com/cactus-comments/cactus-appservice/-/tags/0.2.0)

> no description provided

---



---

# cactus-comments 0.1.0

> published at 0001-01-01 [source](https://gitlab.com/cactus-comments/cactus-appservice/-/tags/0.1.0)

> no description provided

---



---

# postmoogle v0.9.14

> published at 0001-01-01 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.14)

> no description provided

---



---

# postmoogle v0.9.13

> published at 0001-01-01 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.13)

> no description provided

---



---

# postmoogle v0.9.10

> published at 0001-01-01 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.10)

> no description provided

---



---

# postmoogle v0.9.9

> published at 0001-01-01 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.9)

> no description provided

---



---

# postmoogle v0.9.8

> published at 0001-01-01 [source](https://gitlab.com/etke.cc/postmoogle/-/tags/v0.9.8)

> no description provided

---



---

# honoroit v0.9.16

> published at 0001-01-01 [source](https://gitlab.com/etke.cc/honoroit/-/tags/v0.9.16)

> no description provided

---



---

# buscarron v1.3.0

> published at 0001-01-01 [source](https://gitlab.com/etke.cc/buscarron/-/tags/v1.3.0)

> no description provided

---



---

# maubot v0.4.0

> published at 0001-01-01 [source](https://mau.dev/maubot/maubot/-/tags/v0.4.0)

> no description provided

---



---

# maubot v0.3.1

> published at 0001-01-01 [source](https://mau.dev/maubot/maubot/-/tags/v0.3.1)

> no description provided

---



---

# maubot v0.3.0

> published at 0001-01-01 [source](https://mau.dev/maubot/maubot/-/tags/v0.3.0)

> no description provided

---



---

# maubot v0.2.1

> published at 0001-01-01 [source](https://mau.dev/maubot/maubot/-/tags/v0.2.1)

> no description provided

---



---

# maubot v0.2.0

> published at 0001-01-01 [source](https://mau.dev/maubot/maubot/-/tags/v0.2.0)

> no description provided

---



---

# maubot v0.1.2

> published at 0001-01-01 [source](https://mau.dev/maubot/maubot/-/tags/v0.1.2)

> no description provided

---



---

# maubot v0.1.1

> published at 0001-01-01 [source](https://mau.dev/maubot/maubot/-/tags/v0.1.1)

> no description provided

---



---

# maubot v0.1.0

> published at 0001-01-01 [source](https://mau.dev/maubot/maubot/-/tags/v0.1.0)

> no description provided

---



---

# mx-puppet-discord v0.1.0

> published at 0001-01-01 [source](https://gitlab.com/mx-puppet/discord/mx-puppet-discord/-/tags/v0.1.0)

> no description provided

---



---

