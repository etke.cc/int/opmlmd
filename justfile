# show help by default
default:
    @just --list --justfile {{ justfile() }}

# update go deps
update:
    go get .
    go mod tidy
    go mod vendor

# run linter
lint:
    golangci-lint run ./...

# automatically fix liter issues
lintfix:
    golangci-lint run --fix ./...

# run unit tests
test:
    @go test -coverprofile=cover.out ./...
    @go tool cover -func=cover.out
    -@rm -f cover.out

# run app
run +args:
    @go run . {{ args }}

# build app
build:
    go build .
