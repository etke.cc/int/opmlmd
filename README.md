# opmlmd

Gets list of feeds from OPML xml, parses the feeds and stores all items in the markdown file

Example output is [opml.md](./opml.md)
